import { Wallet } from "src/app/pages/entities/wallet";
import { WalletProfilSummary } from './wallet-profil-summary-model';
import { Spent } from 'src/app/pages/entities/spent';
import { SpentSharing } from 'src/app/pages/entities/spent-sharing';

export class WalletSummary {
    wallet: Wallet;
    walletProfilSummaries: WalletProfilSummary[]
    constructor(wallet: Wallet) {
        this.wallet = wallet;
        this.walletProfilSummaries = [];
    }

    public addSpent(spent: Spent, spentSharings:SpentSharing[]) {
        const spenderWalletProfilSummary = this.getWalletProfilSummary(spent.spenderId, spent.spenderDisplayName);
        spenderWalletProfilSummary.addSpent(spent.amount);
        for (let spentSharing of spentSharings) {
            const spentSharingWalletProfilSummary = this.getWalletProfilSummary(spentSharing.sharingProfilId, spentSharing.sharingProfilDisplayName);
            spentSharingWalletProfilSummary.addAmount(spentSharing.amountShare);
        }
    }

    getWalletProfilSummary(spenderId: number, spenderDisplayName?: string):WalletProfilSummary {
        let wps = this.walletProfilSummaries.find(wps => wps.profilId == spenderId);
        if (!wps) {
            wps = new WalletProfilSummary(spenderId, spenderDisplayName);
            this.walletProfilSummaries.push(wps);
        }
        return wps;
    }
}