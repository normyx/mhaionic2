import { TodoListTemplate } from 'src/app/pages/entities/todo-list-template';
import { TodoTemplate } from 'src/app/pages/entities/todo-template';

export class TodoListTemplateSummary {
    todoListTemplate: TodoListTemplate;

    openedTodos: number = 0;
    constructor(todoListTemplate: TodoListTemplate) {
        this.todoListTemplate = todoListTemplate;
    }

    public addTodo(todoTemplate: TodoTemplate) {

        this.openedTodos++;


    }

    public addTodos(todoTemplates: TodoTemplate[]) {
        for (let todoTemplate of todoTemplates) {
            this.addTodo(todoTemplate);
        }

    }

    /* getTaskProjectProfilSummary(profil: Profil): TaskProjectProfilSummary {
        let tpps = this.taskProjectProfilSummaries.find(tpps => tpps.profil.id == profil.id);
        if (!tpps) {
            tpps = new TaskProjectProfilSummary(profil);
            this.taskProjectProfilSummaries.push(tpps);
        }
        return tpps;
    } */
}