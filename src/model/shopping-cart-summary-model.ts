import { ShoppingCart } from 'src/app/pages/entities/shopping-cart';
import { ShoppingItem } from 'src/app/pages/entities/shopping-item';

export class ShoppingCartSummary {
    shoppingCart: ShoppingCart;

    openedShoppingItems: number = 0;
    constructor(shoppingCart: ShoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public addShoppingItem(shoppingItem: ShoppingItem) {
        if (!shoppingItem.checked) {
            this.openedShoppingItems++;
        }

    }

    public addShoppingItems(shoppingItems: ShoppingItem[]) {
        for (let shoppingItem of shoppingItems) {
            this.addShoppingItem(shoppingItem);
        }

    }

    /* getTaskProjectProfilSummary(profil: Profil): TaskProjectProfilSummary {
        let tpps = this.taskProjectProfilSummaries.find(tpps => tpps.profil.id == profil.id);
        if (!tpps) {
            tpps = new TaskProjectProfilSummary(profil);
            this.taskProjectProfilSummaries.push(tpps);
        }
        return tpps;
    } */
}