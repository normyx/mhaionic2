import { Profil } from 'src/app/pages/entities/profil';
import { TodoList } from 'src/app/pages/entities/todo-list';
import { Todo } from 'src/app/pages/entities/todo';

export class TodoListSummary {
    todoList: TodoList;

    openedTodos: number = 0;
    constructor(todoList: TodoList) {
        this.todoList = todoList;
    }

    public addTodo(todo: Todo) {
        if (!todo.done) {
            this.openedTodos++;
        }

    }

    public addTodos(todos: Todo[]) {
        for (let todo of todos) {
            this.addTodo(todo);
        }

    }

    /* getTaskProjectProfilSummary(profil: Profil): TaskProjectProfilSummary {
        let tpps = this.taskProjectProfilSummaries.find(tpps => tpps.profil.id == profil.id);
        if (!tpps) {
            tpps = new TaskProjectProfilSummary(profil);
            this.taskProjectProfilSummaries.push(tpps);
        }
        return tpps;
    } */
}