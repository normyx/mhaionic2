export interface BaseEntity {
  // using type any to avoid methods complaining of invalid type
  id?: any;
  syncStatus?: SyncStatus;
  lastUpdate?: any;
}

export enum SyncStatus {
  NONE,
  UPDATE,
  CREATE,
  DELETE
}

