import { Profil } from 'src/app/pages/entities/profil';

export class TaskProjectProfilSummary {
    profil: Profil;
    openedTasks: number = 0;

    constructor(profil: Profil) {
        this.profil = profil;
    }

    addTask() {
        this.openedTasks += 1;
    }

    
}