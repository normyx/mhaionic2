import { Profil } from 'src/app/pages/entities/profil';
import { Task } from 'src/app/pages/entities/task';
import { TaskProject } from 'src/app/pages/entities/task-project';
import { TaskProjectProfilSummary } from './task-project-profil-summary-model';

export class TaskProjectSummary {
    taskProject: TaskProject;
    taskProjectProfilSummaries: TaskProjectProfilSummary[];
    openedTasks: number = 0;
    constructor(taskProject: TaskProject) {
        this.taskProject = taskProject;
        this.taskProjectProfilSummaries = [];
    }

    public addTask(task: Task) {
        this.openedTasks += 1;
        if (task.owners) {
            for (let owner of task.owners) {
                const taskProjectProfilSummary = this.getTaskProjectProfilSummary(owner);
                taskProjectProfilSummary.addTask();
            }
        }

    }

    public addTasks(tasks: Task[]) {
        for (let task of tasks) {
            this.addTask(task);
        }

    }

    getTaskProjectProfilSummary(profil: Profil): TaskProjectProfilSummary {
        let tpps = this.taskProjectProfilSummaries.find(tpps => tpps.profil.id == profil.id);
        if (!tpps) {
            tpps = new TaskProjectProfilSummary(profil);
            this.taskProjectProfilSummaries.push(tpps);
        }
        return tpps;
    }
}