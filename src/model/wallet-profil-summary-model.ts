import { Profil } from 'src/app/pages/entities/profil';

export class WalletProfilSummary {
    profilId: number;
    profilDisplayName: String
    spent: number = 0;
    amount: number = 0;

    constructor(profilId: number, profilDisplayName: string) {
        this.profilId = profilId;
        this.profilDisplayName = profilDisplayName;
    }

    addSpent(spent: number) {
        this.spent += spent;
    }

    addAmount(amount: number) {
        this.amount += amount;
    }

    getBalance() {
        return this.spent - this.amount;
    }

}