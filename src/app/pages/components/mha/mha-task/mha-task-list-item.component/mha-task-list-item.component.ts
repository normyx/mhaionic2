import { Component, OnInit, Input } from '@angular/core';
import { Task } from 'src/app/pages/entities/task';
import { NavController } from '@ionic/angular';
import { MhaTaskService } from 'src/app/services/mha/mha-task.service/mha-task.service';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { TaskProject } from 'src/app/pages/entities/task-project';
import { ProfilData } from 'src/app/pages/entities/profil-data';
import { MhaProfilDataService } from 'src/app/services/mha/mha-profil-data-service/mha-profil-data-service';
import { MhaProfilService } from 'src/app/services/mha/mha-profil.service/mha-profil.service';
import { filter, map } from 'rxjs/operators';
import { Profil } from 'src/app/pages/entities/profil';
import { MhaActiveProfilService } from 'src/app/services/mha/mha-active-profil.service/mha-active-profil.service';

@Component({
  selector: 'app-mha-task-list-item',
  templateUrl: './mha-task-list-item.component.html',
  styleUrls: ['mha-task-list-item.component.scss'],
})
export class MhaTaskListItemComponent implements OnInit {

  @Input()
  task: Task;
  @Input() taskProject: TaskProject;
  ownersProfilData: ProfilData[] = [];

  constructor(
    private navController: NavController,
    private taskService: MhaTaskService,
    private profilDataService: MhaProfilDataService,
    private profilService: MhaProfilService,
    private activeProfilService: MhaActiveProfilService,) {
      

  }

  async ngOnInit() {
    if (this.task && this.task.owners) {
      for (let owner of this.task.owners) {
        this.profilService.find(owner.id).pipe(
          filter((res: HttpResponse<Profil>) => res.ok),
          map((res: HttpResponse<Profil>) => res.body)
        )
          .subscribe(
            (profil: Profil) => {
              if (profil.profilDataId) {
                this.profilDataService.find(profil.profilDataId)
                  .pipe(
                    filter((res: HttpResponse<ProfilData>) => res.ok),
                    map((res: HttpResponse<ProfilData>) => res.body)
                  )
                  .subscribe(
                    (profilData: ProfilData) => {
                      this.ownersProfilData.push(profilData);
                    },
                    (res: HttpErrorResponse) => console.error(res.message)
                  );
              }
            },
            (res: HttpErrorResponse) => console.error(res.message)
          );

      }

    }
  }

  canMarkDone() : boolean {
    return this.taskService.canMarkDone(this.task, this.taskProject, this.activeProfilService.getStoredProfil());
  }

  openTask() {
    this.navController.navigateForward('/menu/task/task/' + this.task.id + '/edit');
  }

  updateTask($event) {
    this.taskService.update(this.task).subscribe((res: HttpResponse<Task>) => { }, (res: HttpErrorResponse) => { });
  }

}
