
import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MhaShoppingItemComponent } from './mha-shopping-item.component/mha-shopping-item.component';

@NgModule({
  imports: [CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule],
  declarations: [MhaShoppingItemComponent],
  entryComponents: [MhaShoppingItemComponent],
  exports: [MhaShoppingItemComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MhaShoppingComponentModule {
  constructor() {
  }
}
