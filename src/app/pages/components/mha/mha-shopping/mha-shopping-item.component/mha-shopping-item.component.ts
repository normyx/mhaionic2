import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Events } from 'src/app/shared/util/Events';
import { ShoppingItem } from 'src/app/pages/entities/shopping-item';
import { ShoppingCart } from 'src/app/pages/entities/shopping-cart';
import { MhaShoppingItemService } from 'src/app/services/mha/mha-shopping-item.service/mha-shopping-item.service';

@Component({
  selector: 'app-mha-shopping-item',
  templateUrl: './mha-shopping-item.component.html',
  styleUrls: ['mha-shopping-item.component.scss'],
})
export class MhaShoppingItemComponent implements OnInit {

  @Input()
  shoppingItem: ShoppingItem;
  @Input() shoppingCart: ShoppingCart;

  isUpdating: boolean;

  constructor(
    private shoppingItemService: MhaShoppingItemService,
    public events: Events) {
    this.isUpdating = false;

  }

  ngOnInit() {
  }

  toUpdate() {
    //if (!this.todo.done) this.isUpdating = true;
  }

  update($event) {
    /*this.isUpdating = false;
    this.todoService.update(this.todo).subscribe((res: HttpResponse<Todo>) => { }, (res: HttpErrorResponse) => { });
    this.events.publish('todolist:reorder', null);*/
  }

  async delete(slidingItem) {
    /*slidingItem.close();
    this.todoService.delete(this.todo.id).subscribe(() => {
      this.events.publish('todolist:reorder', null);

    });*/
  }




}
