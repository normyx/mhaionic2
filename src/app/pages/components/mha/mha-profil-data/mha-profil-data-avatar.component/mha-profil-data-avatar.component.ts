import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { filter, map } from 'rxjs/operators';
import { ProfilData } from 'src/app/pages/entities/profil-data';
import { MhaProfilDataService } from 'src/app/services/mha/mha-profil-data-service/mha-profil-data-service';
import { MhaProfilService } from 'src/app/services/mha/mha-profil.service/mha-profil.service';


@Component({
  selector: 'mha-profil-data-avatar',
  styleUrls: ['mha-profil-data-avatar.component.scss'],
  templateUrl: './mha-profil-data-avatar.component.html'
})
export class MhaCompProfilDataAvatarComponent implements OnInit, OnChanges {

  profilData: ProfilData;
  @Input() profilDataId: number;

  @Input() profilId: number;

  @Input() size: string

  constructor(protected profilDataService: MhaProfilDataService, protected profilService: MhaProfilService) {
    this.size = this.size? this.size:"small";
  }

  ngOnInit() {
  }

  async loadAll() {
    if (this.profilId) {
      let profilResp = await this.profilService.find(this.profilId).toPromise();
      this.profilDataId = profilResp.body.profilDataId;
    }
    if (this.profilDataId) {
      this.profilDataService.find(this.profilDataId)
        .pipe(
          filter((res: HttpResponse<ProfilData>) => res.ok),
          map((res: HttpResponse<ProfilData>) => res.body)
        )
        .subscribe(
          (res: ProfilData) => {
            this.profilData = res;
          },
          (res: HttpErrorResponse) => console.error(res.message)
        );
    }
  }

  ngOnChanges(): void {
    this.loadAll();
  }

}
