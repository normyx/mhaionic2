import { Component, OnInit, Input } from '@angular/core';
import { NavParams, PopoverController, ModalController, AlertController } from '@ionic/angular';
import { MhaActiveProfilService } from 'src/app/services/mha/mha-active-profil.service/mha-active-profil.service';
import { Profil } from 'src/app/pages/entities/profil';

@Component({
  selector: 'app-mha-profile-chooser-modal',
  templateUrl: './mha-profile-chooser-modal.component.html',
  styleUrls: ['./mha-profile-chooser-modal.component.scss'],
})
export class MhaProfileChooserModalComponent implements OnInit {

  @Input() profils: Profil[];
  @Input() availableProfils: Profil[];
  @Input() warningIfConnectedUserRemoved: boolean;
  @Input() canBeWithNoActor: boolean;
  @Input() singleSelectMode: boolean;
  @Input() color: string;

  checkboxProfils: CheckboxProfil[];


  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private alertController: AlertController,
    private activeProfilService: MhaActiveProfilService) { }

  ngOnInit() {
    if (!this.canBeWithNoActor && this.canBeWithNoActor != false) {
      this.canBeWithNoActor = true;
    }
    if (!this.singleSelectMode && this.singleSelectMode != false) {
      this.singleSelectMode = true;
    }
    if (!this.availableProfils) {
      this.availableProfils = new Array<Profil>();
    }
    if (!this.warningIfConnectedUserRemoved) {
      this.warningIfConnectedUserRemoved = false;
    }
    if (!this.profils) {
      this.profils = new Array<Profil>();
    }
    this.checkboxProfils = [];
    for (let profil of this.profils) {
      this.checkboxProfils.push(new CheckboxProfil(true, profil));
    }
    for (let profil of this.availableProfils) {
      if (this.checkboxProfils.find(cbp => cbp.profil.id === profil.id) == undefined) {
        this.checkboxProfils.push(new CheckboxProfil(false, profil));
      }
    }
  }

  getSelectedProfils(): Profil[] {
    let profils: Profil[] = [];
    for (let checkboxProfil of this.checkboxProfils) {
      if (checkboxProfil.isChecked) {
        profils.push(checkboxProfil.profil);
      }
    }
    return profils;
  }

  async dismiss() {
    if (this.warningIfConnectedUserRemoved) {
      let activeProfilInOwner = false;
      for (let checkboxProfil of this.checkboxProfils) {
        if (checkboxProfil.profil.id == this.activeProfilService.getStoredProfil().id && checkboxProfil.isChecked) {
          activeProfilInOwner = true;
        }
      }
      if (!activeProfilInOwner) {
        const alert = await this.alertController.create({
          header: 'Attention, vous ne ferez plus parti des patricipants de cette liste. Elle ne sera plus visible. Etes vous certain de confirmer ? ',
          buttons: [
            {
              text: 'Annuler',
              role: 'cancel',
              cssClass: 'secondary'
            }, {
              text: 'Confirmer',
              handler: () => {
                this.modalController.dismiss(this.getSelectedProfils());

              }
            }
          ]
        });
        await alert.present();
      } else {
        this.modalController.dismiss(this.getSelectedProfils());
      }
    } else {
      this.modalController.dismiss(this.getSelectedProfils());
    }

  }

  isDisabled(checkboxProfil: CheckboxProfil): boolean {
    if (this.canBeWithNoActor || !checkboxProfil.isChecked) return false;
    let selected = 0;
    for (let checkboxProfil of this.checkboxProfils) {
      if (checkboxProfil.isChecked) selected++;
    }
    return selected == 1;
  }

  checked(selectedCheckboxProfil: CheckboxProfil) {
    if (this.singleSelectMode) {
      for (let checkboxProfil of this.checkboxProfils) {
        if (selectedCheckboxProfil.profil.id != checkboxProfil.profil.id) {
          checkboxProfil.isChecked = false;
        } else {
          checkboxProfil.isChecked = true;
        }
      }
      this.dismiss();
    }
  }
}

class CheckboxProfil {
  isChecked: boolean;
  profil: Profil;
  constructor(isChecked: boolean, profil: Profil) {
    this.isChecked = isChecked;
    this.profil = profil;
  }
}
