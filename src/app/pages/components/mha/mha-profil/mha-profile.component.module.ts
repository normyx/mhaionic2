import { CommonModule } from '@angular/common';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { MhaProfilDataComponentModule } from '../mha-profil-data/mha-profil-data.component.module';
import { MhaProfileChooserPopoverComponent } from './mha-profile-chooser-popover.component/mha-profile-chooser-popover.component';
import { MhaProfileChooserModalComponent } from './mha-profile-chooser-modal.component/mha-profile-chooser-modal.component';

@NgModule({
  imports: [
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    TranslateModule,
    MhaProfilDataComponentModule
  ],
  declarations: [MhaProfileChooserPopoverComponent, MhaProfileChooserModalComponent],
  entryComponents: [MhaProfileChooserPopoverComponent, MhaProfileChooserModalComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MhaProfileComponentModule { }
