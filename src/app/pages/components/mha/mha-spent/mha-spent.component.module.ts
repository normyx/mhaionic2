
import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MhaSpentListItemComponent } from './mha-spent-list-item.component/mha-spent-list-item.component';
import { MhaProfilDataComponentModule } from '../mha-profil-data/mha-profil-data.component.module';

@NgModule({
  imports: [CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MhaProfilDataComponentModule,
    IonicModule],
  declarations: [MhaSpentListItemComponent],
  entryComponents: [MhaSpentListItemComponent],
  exports: [MhaSpentListItemComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MhaSpentComponentModule {
  constructor() {
  }
}
