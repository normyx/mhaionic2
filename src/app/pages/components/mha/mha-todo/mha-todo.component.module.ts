
import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { MhaTodoItemComponent } from './mha-todo-item.component/mha-todo-item.component';
import { MhaTodoTemplateItemComponent } from './mha-todo-template-item.component/mha-todo-template-item.component';

@NgModule({
  imports: [CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule],
  declarations: [MhaTodoItemComponent, MhaTodoTemplateItemComponent],
  entryComponents: [MhaTodoItemComponent, MhaTodoTemplateItemComponent],
  exports: [MhaTodoItemComponent, MhaTodoTemplateItemComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MhaTodoComponentModule {
  constructor() {
  }
}
