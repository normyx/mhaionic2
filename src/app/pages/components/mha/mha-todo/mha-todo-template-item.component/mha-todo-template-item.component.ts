import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Events } from 'src/app/shared/util/Events';
import { TodoTemplate } from 'src/app/pages/entities/todo-template';
import { TodoListTemplate } from 'src/app/pages/entities/todo-list-template';
import { MhaTodoTemplateService } from 'src/app/services/mha/mha-todo-template.service/mha-todo-template.service';

@Component({
  selector: 'app-mha-todo-template-item',
  templateUrl: './mha-todo-template-item.component.html',
  styleUrls: ['mha-todo-template-item.component.scss'],
})
export class MhaTodoTemplateItemComponent implements OnInit {

  @Input()
  todoTemplate: TodoTemplate;
  @Input() todoListTemplate: TodoListTemplate;

  isUpdating: boolean;

  constructor(
    private todoTemplateService: MhaTodoTemplateService,
    public events: Events) {
    this.isUpdating = false;

  }

  ngOnInit() {
  }

  toUpdate() {
    this.isUpdating = true;
  }

  update($event) {
    this.isUpdating = false;
    this.todoTemplateService.update(this.todoTemplate).subscribe((res: HttpResponse<TodoTemplate>) => { }, (res: HttpErrorResponse) => { });
    this.events.publish('todolistTemplate:reorder', null);
  }

  async delete(slidingItem) {
    slidingItem.close();
    this.todoTemplateService.delete(this.todoTemplate.id).subscribe(() => {
      this.events.publish('todolistTemplate:reorder', null);

    });
  }




}
