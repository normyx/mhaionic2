import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Todo } from 'src/app/pages/entities/todo';
import { TodoList } from 'src/app/pages/entities/todo-list';
import { MhaTodoService } from 'src/app/services/mha/mha-todo.service/mha-todo.service';
import { Events } from 'src/app/shared/util/Events';

@Component({
  selector: 'app-mha-todo-item',
  templateUrl: './mha-todo-item.component.html',
  styleUrls: ['mha-todo-item.component.scss'],
})
export class MhaTodoItemComponent implements OnInit {

  @Input()
  todo: Todo;
  @Input() todoList: TodoList;

  isUpdating: boolean;

  constructor(
    private todoService: MhaTodoService,
    public events: Events) {
    this.isUpdating = false;

  }

  ngOnInit() {
  }

  toUpdate() {
    if (!this.todo.done) this.isUpdating = true;
  }

  update($event) {
    this.isUpdating = false;
    this.todoService.update(this.todo).subscribe((res: HttpResponse<Todo>) => { }, (res: HttpErrorResponse) => { });
    this.events.publish('todolist:reorder', null);
  }

  async delete(slidingItem) {
    slidingItem.close();
    this.todoService.delete(this.todo.id).subscribe(() => {
      this.events.publish('todolist:reorder', null);

    });
  }




}
