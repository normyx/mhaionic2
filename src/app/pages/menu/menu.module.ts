import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { MenuPage } from './menu.page';



const routes: Routes = [
  {
    path: '',
    redirectTo: '/menu/home',
    pathMatch: 'full'
  },
  {
    path: '',
    component: MenuPage,
    children: [
      {
        path: 'home',
        loadChildren: '../mha/mha-home/mha-home.module#MhaHomePageModule'
      },
      {
        path: 'info',
        loadChildren: '../mha/mha-info/mha-info.module#MhaInfoPageModule'
      },
      {
        path: 'workspace',
        loadChildren: '../mha/mha-workspace/mha-workspace.module#MhaWorkspacePageModule'
      },
      {
        path: 'wallet',
        loadChildren: '../mha/mha-wallet/mha-wallet.module#MhaWalletPageModule'
      },
      {
        path: 'task',
        loadChildren: '../mha/mha-task/mha-task.module#MhaTaskPageModule'
      },
      {
        path: 'todo',
        loadChildren: '../mha/mha-todo/mha-todo.module#MhaTodoPageModule'
      },
      {
        path: 'shopping',
        loadChildren: '../mha/mha-shopping/mha-shopping.module#MhaShoppingPageModule'
      },

    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MenuPage]
})
export class MenuPageModule {}
