import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { LoginService } from 'src/app/services/login/login.service';
import { MhaActiveProfilService } from 'src/app/services/mha/mha-active-profil.service/mha-active-profil.service';
import { Workspace } from '../entities/workspace';
import { Profil } from '../entities/profil';
import { MhaWorkspaceService } from 'src/app/services/mha/mha-workspace.service/mha-workspace.service';
import { MhaAsyncService } from 'src/app/services/mha/mha-async.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  workspace: Workspace;
  profil: Profil;

  

  constructor(
    public navController: NavController,
    public alertController: AlertController,
    private workspaceService: MhaWorkspaceService,
    private profilService: MhaActiveProfilService,
    private asyncService: MhaAsyncService,
    private loginService: LoginService
  ) { }

  ngOnInit() {this.loadAll(); }

  ionViewWillEnter() { /* this.loadAll(); */ }
  ionWillOpen() { /* this.loadAll(); */ }
  async loadAll() {
    this.workspace = this.workspaceService.getStoredActiveWorkspace();
    //await this.asyncService.refresh();
    this.profil = this.profilService.getStoredProfil();
    if (!this.workspace) {
      this.navController.navigateRoot('/menu/workspace');
    }
  }

  async logout() {
    const alert = await this.alertController.create({
      header: 'Confirmer vous votre déconnexion ? Toutes les données non synchronisées seront perdues si vous n\'êtes pas connecté.',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary'
        }, {
          text: 'Déconnexion',
          cssClass: 'danger',
          handler: () => {
            this.loginService.logout();
            this.profilService.clear();
            this.navController.navigateRoot('');
          }
        }]
    });
    await alert.present();
  }




}
