import { Component, OnInit } from '@angular/core';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { LoginService } from 'src/app/services/login/login.service';
import { MhaAsyncService } from 'src/app/services/mha/mha-async.service';
import { setStatusBarBackgroundColor } from 'src/app/shared/util/helpers';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  // The account fields for the login form.
  account: { username: string; password: string; rememberMe: boolean } = {
    username: '',
    password: '',
    rememberMe: false,
  };

  // Our translated text strings
  private loginErrorString: string;

  constructor(
    public translateService: TranslateService,
    public loginService: LoginService,
    public toastController: ToastController,
    public navController: NavController,
    protected asyncService: MhaAsyncService,
    public platform: Platform,
    
  ) {}

  ngOnInit() {
    setStatusBarBackgroundColor("info", this.platform);
    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    });
  }

  async doLogin() {

    this.loginService.login(this.account).then(
      async () => {
        await this.asyncService.refresh(true);
        this.navController.navigateRoot('/menu');
      },
      async (err) => {
        // Unable to log in
        this.account.password = '';
        const toast = await this.toastController.create({
          message: this.loginErrorString,
          duration: 3000,
          position: 'top',
        });
        toast.present();
      }
    );
  }

  async doClear() {
    await this.asyncService.clear();
  }
}
