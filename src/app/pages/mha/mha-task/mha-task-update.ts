import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AlertController, ModalController, NavController, Platform, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { MhaActiveProfilService } from 'src/app/services/mha/mha-active-profil.service/mha-active-profil.service';
import { MhaTaskProjectService } from '../../../services/mha/mha-task-project.service/mha-task-project.service';
import { MhaTaskService } from '../../../services/mha/mha-task.service/mha-task.service';
import { setStatusBarBackgroundColor } from '../../../shared/util/helpers';
import { Profil } from '../../entities/profil';
import { Task } from '../../entities/task';
import { TaskProject } from '../../entities/task-project';
import { MhaProfileChooserModalComponent } from '../../components/mha/mha-profil/mha-profile-chooser-modal.component/mha-profile-chooser-modal.component';

@Component({
    selector: 'mha-page-task-update',
    templateUrl: 'mha-task-update.html',
    styleUrls: ['./mha-task.scss']
})
export class MhaTaskUpdatePage implements OnInit {

    task: Task;
    taskProject: TaskProject;
    dueDateDp: any;
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;



    form = this.formBuilder.group({
        id: [],
        label: [null, [Validators.required]],
        description: [null, []],
        done: ['false', [Validators.required]],
        dueDate: [null, []],
        taskProjectId: [null, []],
        owners: [null, []],
    });

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        protected formBuilder: FormBuilder,
        public platform: Platform,
        protected toastCtrl: ToastController,
        private alertController: AlertController,
        private mhaTaskService: MhaTaskService,
        private taskProjectService: MhaTaskProjectService,
        private activeProfilService: MhaActiveProfilService,
        public modalController: ModalController,
        private statusBar: StatusBar
    ) {

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ngOnInit() {
        setStatusBarBackgroundColor('task', this.platform);
        this.activatedRoute.data.subscribe((response) => {

            this.task = response.data;
            this.isNew = this.task.id === null || this.task.id === undefined;
            this.updateForm(response.data);
        });
    }

    updateForm(task: Task) {
        this.form.patchValue({
            id: task.id,
            label: task.label,
            description: task.description,
            done: task.done,
            dueDate: task.dueDate,
            taskProjectId: task.taskProjectId,
            owners: task.owners,
        });

        this.taskProjectService.find(task.taskProjectId).subscribe(data => {
            this.taskProject = data.body;
        }, (error) => this.onError(error));


    }

    getAvailableProfils(): Profil[] {
        let profils = new Array<Profil>();
        if (!this.task.owners) {
            this.task.owners = new Array<Profil>();
        }
        for (let taskProjectProfil of this.taskProject.owners) {
            if (!this.task.owners.some(e => e.id === taskProjectProfil.id)) {
                profils.push(taskProjectProfil);
            }
        }
        return profils;

    }



    save() {
        this.isSaving = true;
        const task = this.createFromForm();
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.mhaTaskService.update(task));
        } else {
            this.subscribeToSaveResponse(this.mhaTaskService.create(task));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<Task>>) {
        result.subscribe((res: HttpResponse<Task>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        /* let action = 'updated';
        if (response.status === 201) {
            action = 'created';
        } */
        this.isSaving = false;
        //const toast = await this.toastCtrl.create({ message: `Task ${action} successfully.`, duration: 2000, position: 'middle' });
        //toast.present();

        this.navController.navigateBack('/menu/task/task-project/' + this.task.taskProjectId + '/view');
    }

    previousState() {
        window.history.back();
    }

    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({ message: 'Echec de l\'opération', duration: 2000, position: 'middle' });
        toast.present();
    }

    private createFromForm(): Task {
        return {
            ...new Task(),
            id: this.form.get(['id']).value,
            label: this.form.get(['label']).value,
            description: this.form.get(['description']).value,
            done: this.form.get(['done']).value,
            dueDate: this.form.get(['dueDate']).value,
            taskProjectId: this.form.get(['taskProjectId']).value,
            owners: this.form.get(['owners']).value,
        };
    }

    trackProfilById(index: number, item: Profil) {
        return item.id;
    }

    canMarkDone(): boolean {
        return this.mhaTaskService.canMarkDone(this.task, this.taskProject, this.activeProfilService.getStoredProfil());
    }

    async chooseProfile() {
        const modal = await this.modalController.create({
            component: MhaProfileChooserModalComponent,
            animated: true,
            showBackdrop: true,
            keyboardClose: true,
            componentProps: { "profils": this.task.owners, "availableProfils": this.getAvailableProfils(), "color": "task", "singleSelectMode":false }
        });
        modal.onDidDismiss().then((dataReturned) => {
            if (dataReturned !== null && dataReturned.data && dataReturned.data != null) {
                this.task.owners = dataReturned.data;
                this.form.patchValue({ 'owners': this.task.owners });
            }
        });
        return await modal.present();
    }


    async deleteModal() {
        const alert = await this.alertController.create({
            header: 'Confirmer vous la suppression de ' + this.task.label + '?',
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Supprimer',
                    handler: () => {
                        this.mhaTaskService.delete(this.task.id).subscribe(() => {
                            this.navController.navigateRoot('/menu/task/task-project/' + this.task.taskProjectId + '/view');

                        });

                    }
                }
            ]
        });
        await alert.present();
    }
}
