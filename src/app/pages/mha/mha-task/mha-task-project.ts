import { HttpResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { MhaTaskService } from 'src/app/services/mha/mha-task.service/mha-task.service';
import { MhaWorkspaceService } from 'src/app/services/mha/mha-workspace.service/mha-workspace.service';
import { setStatusBarBackgroundColor } from 'src/app/shared/util/helpers';
import { SyncStatus } from 'src/model/base-entity';
import { MhaAsyncService } from '../../../services/mha/mha-async.service';
import { MhaTaskProjectService } from '../../../services/mha/mha-task-project.service/mha-task-project.service';
import { TaskProject } from '../../entities/task-project';
import { TaskProjectSummary } from 'src/model/task-project-summary-model';
import { Profil } from '../../entities/profil';
import { MhaActiveProfilService } from 'src/app/services/mha/mha-active-profil.service/mha-active-profil.service';


@Component({
  selector: 'app-mha-task-project',
  templateUrl: './mha-task-project.html',
  styleUrls: ['./mha-task.scss'],
})
export class MhaTaskProjectPage {

  taskProjects: TaskProject[];
  taskProjectSummaries: TaskProjectSummary[];
  hasModification: boolean;
  activeProfil: Profil;

  constructor(
    private taskProjectService: MhaTaskProjectService,
    private taskService: MhaTaskService,
    private workspaceService: MhaWorkspaceService,
    private asyncService: MhaAsyncService,
    private profilService: MhaActiveProfilService,
    private toastCtrl: ToastController,
    private navController: NavController,
    public plt: Platform,
    private statusBar: StatusBar) { }

  ionViewWillEnter() {
    setStatusBarBackgroundColor('task', this.plt);
    this.loadAll();
  }

  async loadAll(refresher?) {
    if (typeof (refresher) !== 'undefined') {
      await this.asyncService.refresh(false);
      setTimeout(() => {
        refresher.target.complete();
      }, 750);
    }
    this.activeProfil = this.profilService.getStoredProfil();
    this.hasModification = this.taskProjectService.hasModification() || this.taskService.hasModification();
    this.taskProjectService.findWhereParent([this.workspaceService.getStoredActiveWorkspace()]).pipe(
      filter((res: HttpResponse<TaskProject[]>) => res.ok),
      map((res: HttpResponse<TaskProject[]>) => res.body)
    )
      .subscribe(
        async (response: TaskProject[]) => {
          this.taskProjects = response.filter(tp => tp.syncStatus != SyncStatus.DELETE && tp.owners.some(o => o.id === this.activeProfil.id));
          this.taskProjectSummaries = [];
          for (let taskProject of this.taskProjects) {
            await this.taskProjectService.getTaskProjectSummary(taskProject).then(tps => { this.taskProjectSummaries.push(tps); });
          }
        },
        async (error) => {
          console.error(error);
          const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
          toast.present();
        });
  }

  trackId(index: number, item: TaskProject) {
    return item.id;
  }
  view(taskProject: TaskProject) {
    this.navController.navigateForward('/menu/task/task-project/' + taskProject.id + '/view');
  }

  newTaskProject() {
    this.navController.navigateForward('/menu/task/task-project/' + this.workspaceService.getStoredActiveWorkspace().id + '/new');
  }

  


}
