import { CommonModule } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, Injectable, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRouteSnapshot, Resolve, RouterModule, RouterStateSnapshot, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { UserRouteAccessService } from 'src/app/services/auth/user-route-access.service';
import { Task } from '../../entities/task';
import { MhaTaskService } from '../../../services/mha/mha-task.service/mha-task.service';
import { MhaTaskUpdatePage } from './mha-task-update';
import { MhaTaskProjectService } from 'src/app/services/mha/mha-task-project.service/mha-task-project.service';
import { TaskProject } from '../../entities/task-project';
import { MhaActiveProfilService } from 'src/app/services/mha/mha-active-profil.service/mha-active-profil.service';
import { MhaTaskProjectPage } from './mha-task-project';
import { MhaTaskProjectDetailPage } from './mha-task-project-detail';
import { MhaTaskProjectUpdatePage } from './mha-task-project-update';
import { MhaProfileComponentModule } from '../../components/mha/mha-profil/mha-profile.component.module';
import { MhaProfilDataComponentModule } from '../../components/mha/mha-profil-data/mha-profil-data.component.module';
import { MhaSyncComponentModule } from '../../components/mha/mha-sync/mha-sync.component.module';
import { MhaTaskComponentModule } from '../../components/mha/mha-task/mha-task.component.module';

@Injectable({ providedIn: 'root' })
export class MhaTaskProjectResolve implements Resolve<TaskProject> {
  constructor(private service: MhaTaskProjectService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TaskProject> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<TaskProject>) => response.ok),
        map((taskProject: HttpResponse<TaskProject>) => taskProject.body)
      );
    }
    return of(new TaskProject());
  }
}

@Injectable({ providedIn: 'root' })
export class MhaTaskProjectFromWorkspaceResolve implements Resolve<TaskProject> {
  constructor(private profilService: MhaActiveProfilService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TaskProject> {
    const workspaceId = route.params.workspaceId ? route.params.workspaceId : null;
    if (workspaceId) {
      let taskProject = new TaskProject();
      taskProject.workspaceId = workspaceId;
      taskProject.owners = [this.profilService.getStoredProfil()];
      return of(taskProject);

    }
    return of(new TaskProject());
  }
}

@Injectable({ providedIn: 'root' })
export class MhaTaskResolve implements Resolve<Task> {
  constructor(private service:MhaTaskService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Task> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Task>) => response.ok),
        map((task: HttpResponse<Task>) => task.body)
      );
    }
    return of(new Task());
  }
}

@Injectable({ providedIn: 'root' })
export class MhaTaskFromTaskProjectResolve implements Resolve<Task> {
  constructor() { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Task> {
    const taskProjectId = route.params.taskProjectId ? route.params.taskProjectId : null;
    if (taskProjectId) {
      let task = new Task();
      task.taskProjectId = taskProjectId;
      return of(task);

    }
    return of(new Task());
  }
}

const routes: Routes = [
  {
    path: 'task-project',
    component: MhaTaskProjectPage
  },
  {
    path: 'task-project/:id/view',
    component: MhaTaskProjectDetailPage,
    resolve: {
      data: MhaTaskProjectResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'task-project/:id/edit',
    component: MhaTaskProjectUpdatePage,
    resolve: {
      data: MhaTaskProjectResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'task-project/:workspaceId/new',
    component: MhaTaskProjectUpdatePage,
    resolve: {
      data: MhaTaskProjectFromWorkspaceResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'task/:id/edit',
    component: MhaTaskUpdatePage,
    resolve: {
      data: MhaTaskResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'task/:taskProjectId/new',
    component: MhaTaskUpdatePage,
    resolve: {
      data: MhaTaskFromTaskProjectResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
];

@NgModule({
  imports: [
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    TranslateModule,
    MhaProfileComponentModule,
    MhaProfilDataComponentModule,
    MhaTaskComponentModule,
    MhaSyncComponentModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MhaTaskProjectPage, MhaTaskProjectDetailPage, MhaTaskProjectUpdatePage, MhaTaskUpdatePage],
  entryComponents: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MhaTaskPageModule { }
