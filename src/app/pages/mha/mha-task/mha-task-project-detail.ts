import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { NavController, Platform, PopoverController, ToastController, AlertController } from '@ionic/angular';
import * as moment from 'moment';
import { filter, map } from 'rxjs/operators';
import { MhaTaskProjectService } from 'src/app/services/mha/mha-task-project.service/mha-task-project.service';
import { setStatusBarBackgroundColor } from 'src/app/shared/util/helpers';
import { SyncStatus } from 'src/model/base-entity';
import { MhaAsyncService } from '../../../services/mha/mha-async.service';
import { MhaTaskService } from '../../../services/mha/mha-task.service/mha-task.service';
import { Task } from '../../entities/task';
import { TaskProject } from '../../entities/task-project';

@Component({
    selector: 'mha-page-task-project-detail',
    templateUrl: 'mha-task-project-detail.html',
    styleUrls: ['./mha-task.scss'],
})
export class MhaTaskProjectDetailPage implements OnInit {

    taskProject: TaskProject;
    tasks: Task[];
    hasModification: boolean;

    constructor(
        private navController: NavController,
        private activatedRoute: ActivatedRoute,
        private taskService: MhaTaskService,
        private taskProjectService: MhaTaskProjectService,
        private asyncService: MhaAsyncService,
        private toastCtrl: ToastController,
        public plt: Platform,
        public popoverController: PopoverController,
        private statusBar: StatusBar,
        private alertController: AlertController,
    ) { }

    ngOnInit() {
    }

    ionViewWillEnter() {
        setStatusBarBackgroundColor('task', this.plt);
        this.loadAll();
    }

    async loadAll(refresher?) {
        if (typeof (refresher) !== 'undefined') {
            await this.asyncService.refresh(false);
            setTimeout(() => {
                refresher.target.complete();
            }, 750);
        }
        this.hasModification = this.taskProjectService.hasModification() || this.taskService.hasModification();
        this.activatedRoute.data.subscribe((response) => {
            this.taskProject = response.data;
            this.taskService.findWhereParent([this.taskProject]).pipe(
                filter((res: HttpResponse<Task[]>) => res.ok),
                map((res: HttpResponse<Task[]>) => res.body))
                .subscribe(
                    (response: Task[]) => {
                        this.tasks = response.filter(t => t.syncStatus != SyncStatus.DELETE).sort((a: Task, b: Task) => {
                            if (a.done == b.done) {
                                return moment(b.dueDate).diff(moment(a.dueDate));
                            } else {
                                return a.done ? 1 : -1;
                            }
                        });
                    },
                    async (error) => {
                        console.error(error);
                        const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
                        toast.present();
                    });
        });
    }

    newTask() {
        this.navController.navigateForward('/menu/task/task/' + this.taskProject.id + '/new');
    }

    goBack() {
        this.navController.navigateRoot('/menu/task/task-project');
    }


    trackId(index: number, item: Task) {
        return item.id;
    }

    goUpdatePage(event) {
        this.navController.navigateForward('/menu/task/task-project/' + this.taskProject.id + '/edit');
        //await this.popController.dismiss(event.detail.value);
    }

    async cleanModal() {
        const alert = await this.alertController.create({
            header: 'Confirmer vous la suppression des tâches terminées ? ',
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Nettoyer',
                    handler: () => {
                        for (let task of this.tasks) {
                            if (task.done) this.taskService.delete(task.id);
                        }
                        
                        this.loadAll();

                    }
                }
            ]
        });
        await alert.present();
    }







}
