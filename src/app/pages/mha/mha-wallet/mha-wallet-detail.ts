import { HttpResponse } from '@angular/common/http';
import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonInfiniteScroll, NavController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { MhaSpentService } from 'src/app/services/mha/mha-spent.service/mha-spent.service';
import { MhaWalletService } from 'src/app/services/mha/mha-wallet.service/mha-wallet.service';
import { setStatusBarBackgroundColor } from 'src/app/shared/util/helpers';
import { SyncStatus } from 'src/model/base-entity';
import { MhaAsyncService } from '../../../services/mha/mha-async.service';
import { Spent } from '../../entities/spent';
import { Wallet } from '../../entities/wallet';
import { Events } from 'src/app/shared/util/Events';
import { MhaSpentSharingService } from 'src/app/services/mha/mha-spent-sharing.service/mha-spent-sharing.service';

class WalletProfilSummary {
    profilId: number;
    profilName: string;
    spent: number = 0;
    amount: number = 0;

    getBalance() {
        return this.spent - this.amount;
    }
}
@Component({
    selector: 'mha-page-wallet-detail',
    templateUrl: 'mha-wallet-detail.html',
    styleUrls: ['./mha-wallet.scss'],
})
export class MhaWalletDetailPage implements OnInit {

    private static SCROLL_PAGE_LENGTH = 25;


    @ViewChild(IonInfiniteScroll, { static: false }) infiniteScroll: IonInfiniteScroll;

    wallet: Wallet;
    spents: Spent[];
    scrolledSpents: Spent[] = [];
    hasModification: boolean;
    profilSummaries: WalletProfilSummary[] = [];
    pageToDisplay: string;

    constructor(
        private navController: NavController,
        private activatedRoute: ActivatedRoute,
        private spentService: MhaSpentService,
        private walletService: MhaWalletService,
        private spentSharingService: MhaSpentSharingService,
        private asyncService: MhaAsyncService,
        private toastCtrl: ToastController,
        public plt: Platform,
        public popoverController: PopoverController,
        public events: Events,
        private statusBar: StatusBar,
        private zone: NgZone
    ) {
        this.events.subscribe('wallet:update', () => {
            console.warn("wallet:update");
            this.zone.run(() => {
                this.loadAll();
            });
        });
        this.pageToDisplay = "list";
    }

    ngOnInit() {
    }


    ionViewWillEnter() {
        setStatusBarBackgroundColor('wallet', this.plt);
        this.loadAll();
    }

    loadScrolledData(event) {
        setTimeout(() => {
            this.fillScrolledSpents();
            event.target.complete();
        }, 500);
    }

    fillScrolledSpents(loading?: boolean) {
        if (loading) {
            this.scrolledSpents = [];
        }
        if (this.spents && (loading || !this.infiniteScroll.disabled)) {
            const scrolledLength = this.scrolledSpents.length;
            for (let i = 0; i < MhaWalletDetailPage.SCROLL_PAGE_LENGTH && i < this.spents.length - scrolledLength; i++) {
                this.scrolledSpents.push(this.spents[this.scrolledSpents.length]);
            }
        }
        if (this.spents.length == this.scrolledSpents.length) {
            this.infiniteScroll.disabled = true;
        }
    }


    async loadAll(refresher?) {
        if (typeof (refresher) !== 'undefined') {
            await this.asyncService.refresh(false);
            setTimeout(() => {
                refresher.target.complete();
            }, 750);
        }
        this.hasModification = this.walletService.hasModification() || this.spentService.hasModification();
        this.activatedRoute.data.subscribe((response) => {
            this.wallet = response.data;
            this.spentService.findWhereParent([this.wallet]).pipe(
                filter((res: HttpResponse<Spent[]>) => res.ok),
                map((res: HttpResponse<Spent[]>) => res.body))
                .subscribe(
                    async (response: Spent[]) => {

                        this.spents = await response.filter(s => { return s.syncStatus != SyncStatus.DELETE });
                        await this.fillScrolledSpents(true);
                        this.profilSummaries = [];
                        // Build Profil Summary objects
                        for (let spent of this.spents) {
                            let profilSummary = this.profilSummaries.find(ps => ps.profilId === spent.spenderId);
                            if (!profilSummary) {
                                profilSummary = new WalletProfilSummary();
                                profilSummary.profilId = spent.spenderId;
                                profilSummary.profilName = spent.spenderDisplayName;
                                this.profilSummaries.push(profilSummary);
                            }
                        }
                        // Fill Profil Summary objects
                        for (let spent of this.spents) {
                            let profilSummary = this.profilSummaries.find(ps => ps.profilId === spent.spenderId);
                            profilSummary.spent += spent.amount;
                            let spentSharings = await this.spentSharingService.findWhereParent([spent]).toPromise();
                            if (spentSharings.body) {
                                for (let spentSharing of spentSharings.body) {
                                    let profilSummary2 = this.profilSummaries.find(ps => ps.profilId === spentSharing.sharingProfilId);
                                    profilSummary2.amount += spentSharing.amountShare;
                                }
                            }
                        }
                    },
                    async (error) => {
                        console.error(error);
                        const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
                        toast.present();
                    });
        });
    }

    newSpent() {
        this.navController.navigateForward('/menu/wallet/spent/' + this.wallet.id + '/new');
    }

    getTotalSpent() {
        let total = 0;
        this.profilSummaries.forEach(ps => total += ps.spent);
        return total;
    }

    getTotalAmount() {
        let total = 0;
        this.profilSummaries.forEach(ps => total += ps.amount);
        return total;
    }
    getTotalBalance() {
        let total = 0;
        this.profilSummaries.forEach(ps => total += ps.getBalance());
        return total;
    }

    goBack() {
        this.navController.navigateRoot('/menu/wallet/wallet');
    }

    trackId(index: number, item: Spent) {
        return item.id;
    }

    goUpdatePage(event) {
        this.navController.navigateForward('/menu/wallet/wallet/' + this.wallet.id + '/edit');
        //await this.popController.dismiss(event.detail.value);
    }

    segmentChanged($event) {
        //this.loadAll();
        this.pageToDisplay = $event.detail.value;
    }

}
