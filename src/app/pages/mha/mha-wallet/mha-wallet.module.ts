import { CommonModule } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, Injectable, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRouteSnapshot, Resolve, RouterModule, RouterStateSnapshot, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { UserRouteAccessService } from 'src/app/services/auth/user-route-access.service';
import { MhaActiveProfilService } from 'src/app/services/mha/mha-active-profil.service/mha-active-profil.service';
import { MhaSpentConfigService } from 'src/app/services/mha/mha-spent.-config.service/mha-spent-config.service';
import { MhaSpentService } from 'src/app/services/mha/mha-spent.service/mha-spent.service';
import { MhaWalletService } from 'src/app/services/mha/mha-wallet.service/mha-wallet.service';
import { MhaProfilDataComponentModule } from '../../components/mha/mha-profil-data/mha-profil-data.component.module';
import { MhaProfileComponentModule } from '../../components/mha/mha-profil/mha-profile.component.module';
import { MhaSpentComponentModule } from '../../components/mha/mha-spent/mha-spent.component.module';
import { MhaSyncComponentModule } from '../../components/mha/mha-sync/mha-sync.component.module';
import { Spent } from '../../entities/spent';
import { SpentConfig } from '../../entities/spent-config';
import { Wallet } from '../../entities/wallet';
import { MhaSpentConfigPage } from './mha-spent-config';
import { MhaSpentConfigUpdatePage } from './mha-spent-config-update';
import { MhaSpentUpdatePage } from './mha-spent-update';
import { MhaWalletPage } from './mha-wallet';
import { MhaWalletDetailPage } from './mha-wallet-detail';
import { MhaWalletUpdatePage } from './mha-wallet-update';



@Injectable({ providedIn: 'root' })
export class MhaWalletResolve implements Resolve<Wallet> {
  constructor(private service: MhaWalletService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Wallet> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Wallet>) => response.ok),
        map((wallet: HttpResponse<Wallet>) => wallet.body)
      );
    }
    return of(new Wallet());
  }
}

@Injectable({ providedIn: 'root' })
export class MhaWalletFromWorkspaceResolve implements Resolve<Wallet> {
  constructor() { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Wallet> {
    const workspaceId = route.params.workspaceId ? route.params.workspaceId : null;
    if (workspaceId) {
      let wallet = new Wallet();
      wallet.workspaceId = workspaceId;
      return of(wallet);

    }
    return of(new Wallet());
  }
}

@Injectable({ providedIn: 'root' })
export class MhaSpentResolve implements Resolve<Spent> {
  constructor(private service:MhaSpentService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Spent> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Spent>) => response.ok),
        map((task: HttpResponse<Spent>) => task.body)
      );
    }
    return of(new Spent());
  }
}

@Injectable({ providedIn: 'root' })
export class MhaSpentFromWalletResolve implements Resolve<Spent> {
  constructor(protected profilService: MhaActiveProfilService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Spent> {
    const walletId = route.params.walletId ? route.params.walletId : null;
    const profilId = this.profilService.getStoredProfil().id;
    if (walletId) {
      let spent = new Spent();
      spent.walletId = walletId;
      spent.spenderId = profilId;
      return of(spent);

    }
    return of(new Spent());
  }
}

@Injectable({ providedIn: 'root' })
export class MhaSpentConfigResolve implements Resolve<SpentConfig> {
  constructor(private service: MhaSpentConfigService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<SpentConfig> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<SpentConfig>) => response.ok),
        map((spentConfig: HttpResponse<SpentConfig>) => spentConfig.body)
      );
    }
    return of(new SpentConfig());
  }
}

@Injectable({ providedIn: 'root' })
export class MhaSpentConfigListFromWalletResolve implements Resolve<Wallet> {
  constructor(private spentConfigService: MhaSpentConfigService, private walletService: MhaWalletService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Wallet> {
    const walletId = route.params.walletId ? route.params.walletId : null;
    if (walletId) {
      return this.walletService.find(walletId).pipe(
        filter((response: HttpResponse<Wallet>) => response.ok),
        map((wallet: HttpResponse<Wallet>) => wallet.body)
      );
    }
    return of(new Wallet());
  }
}
@Injectable({ providedIn: 'root' })
export class MhaSpentConfigFromWalletResolve implements Resolve<SpentConfig> {
  constructor() { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Wallet> {
    const walletId = route.params.walletId ? route.params.walletId : null;
    if (walletId) {
      let spentConfig = new SpentConfig();
      spentConfig.walletId = walletId;
      return of(spentConfig);

    }
    return of(new SpentConfig());
  }
}


const routes: Routes = [
  {
    path: 'wallet',
    component: MhaWalletPage
  },
  
  {
    path: 'wallet/:id/view',
    component: MhaWalletDetailPage,
    resolve: {
      data: MhaWalletResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService],
    
  },
  {
    path: 'wallet/:id/edit',
    component: MhaWalletUpdatePage,
    resolve: {
      data: MhaWalletResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'wallet/:workspaceId/new',
    component: MhaWalletUpdatePage,
    resolve: {
      data: MhaWalletFromWorkspaceResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'spent/:id/edit',
    component: MhaSpentUpdatePage,
    resolve: {
      data: MhaSpentResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'spent/:walletId/new',
    component: MhaSpentUpdatePage,
    resolve: {
      data: MhaSpentFromWalletResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'spent-config/:walletId',
    component: MhaSpentConfigPage,
    resolve: {
      data: MhaSpentConfigListFromWalletResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService],
  },

  
  {
    path: 'spent-config/:id/edit',
    component: MhaSpentConfigUpdatePage,
    resolve: {
      data: MhaSpentConfigResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'spent-config/:walletId/new',
    component: MhaSpentConfigUpdatePage,
    resolve: {
      data: MhaSpentConfigFromWalletResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    TranslateModule,
    MhaProfileComponentModule,
    MhaProfilDataComponentModule,
    MhaSpentComponentModule,
    MhaSyncComponentModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MhaWalletPage, MhaWalletDetailPage,MhaWalletUpdatePage, MhaSpentUpdatePage, MhaSpentConfigPage, MhaSpentConfigUpdatePage],
  //entryComponents: [],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class MhaWalletPageModule {}
