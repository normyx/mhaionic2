import { HttpResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { MhaSpentService } from 'src/app/services/mha/mha-spent.service/mha-spent.service';
import { MhaWalletService } from 'src/app/services/mha/mha-wallet.service/mha-wallet.service';
import { MhaWorkspaceService } from 'src/app/services/mha/mha-workspace.service/mha-workspace.service';
import { setStatusBarBackgroundColor } from 'src/app/shared/util/helpers';
import { SyncStatus } from 'src/model/base-entity';
import { MhaAsyncService } from '../../../services/mha/mha-async.service';
import { Wallet } from '../../entities/wallet';
import { WalletSummary } from 'src/model/wallet-summary-model';
import { Profil } from '../../entities/profil';
import { MhaActiveProfilService } from 'src/app/services/mha/mha-active-profil.service/mha-active-profil.service';



@Component({
  selector: 'app-mha-wallet',
  templateUrl: './mha-wallet.html',
  styleUrls: ['./mha-wallet.scss'],
})
export class MhaWalletPage {

  wallets: Wallet[];
  hasModification: boolean;
  walletSummaries: WalletSummary[];
  activeProfil: Profil;


  constructor(
    private walletService: MhaWalletService,
    private spentService: MhaSpentService,
    private workspaceService: MhaWorkspaceService,
    private asyncService: MhaAsyncService,
    private profilService: MhaActiveProfilService,
    private toastCtrl: ToastController,
    private navController: NavController,
    private statusBar: StatusBar,
    public plt: Platform) { }

  ionViewWillEnter() {
    this.loadAll();
    setStatusBarBackgroundColor('wallet', this.plt);
  }

  async loadAll(refresher?) {
    if (typeof (refresher) !== 'undefined') {
      await this.asyncService.refresh(false);
      setTimeout(() => {
        refresher.target.complete();
      }, 750);
    }
    this.activeProfil = this.profilService.getStoredProfil();
    this.hasModification = this.walletService.hasModification() || this.spentService.hasModification();
    this.walletService.findWhereParent([this.workspaceService.getStoredActiveWorkspace()]).pipe(
      filter((res: HttpResponse<Wallet[]>) => res.ok),
      map((res: HttpResponse<Wallet[]>) => res.body)
    )
      .subscribe(
        async   (response: Wallet[]) => {
          this.wallets = response.filter(w => w.syncStatus != SyncStatus.DELETE && w.owners.some(o => o.id === this.activeProfil.id));
          this.walletSummaries = [];
          for (let wallet of this.wallets) {
            await this.walletService.getWalletSummary(wallet).then(ws => {this.walletSummaries.push(ws);})
          }
        },
        async (error) => {
          console.error(error);
          const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
          toast.present();
        });
  }

  trackId(index: number, item: Wallet) {
    return item.id;
  }
  view(wallet: Wallet) {
    this.navController.navigateForward('/menu/wallet/wallet/' + wallet.id + '/view');
  }

  newWallet() {
    this.navController.navigateForward('/menu/wallet/wallet/' + this.workspaceService.getStoredActiveWorkspace().id + '/new');
  }

  


}
