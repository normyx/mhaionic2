import { HttpResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { setStatusBarBackgroundColor } from 'src/app/shared/util/helpers';
import { MhaAsyncService } from '../../../services/mha/mha-async.service';
import { MhaWorkspaceService } from '../../../services/mha/mha-workspace.service/mha-workspace.service';
import { Workspace } from '../../entities/workspace';


@Component({
    selector: 'mha-page-workspace',
    templateUrl: 'mha-workspace.html'
})
export class MhaWorkspacePage {
    workspaces: Workspace[];
    selectedWorkspace: Workspace;
    // todo: add pagination

    constructor(
        private navController: NavController,
        private workspaceService: MhaWorkspaceService,
        private asyncService: MhaAsyncService,
        private toastCtrl: ToastController,
        public plt: Platform,
        private statusBar: StatusBar
    ) {
        this.workspaces = [];
        this.selectedWorkspace = this.workspaceService.getStoredActiveWorkspace();
    }

    ionViewWillEnter() {
        setStatusBarBackgroundColor('workspace', this.plt);
        this.loadAll();
    }

    async loadAll(refresher?) {
        if (typeof(refresher) !== 'undefined') {
            await this.asyncService.refresh(false);
            setTimeout(() => {
                refresher.target.complete();
            }, 2000);
        }
        this.workspaceService.findWhereParent(null).pipe(
            filter((res: HttpResponse<Workspace[]>) => res.ok),
            map((res: HttpResponse<Workspace[]>) => res.body)
        )
        .subscribe(
            (response: Workspace[]) => {
                this.workspaces = response;
            },
            async (error) => {
                const toast = await this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
            
    }

    trackId(index: number, item: Workspace) {
        return item.id;
    }


    selectWorkspace(workspace: Workspace) {
        this.workspaceService.setStoredActiveWorkspace(workspace);
        this.navController.navigateForward('/menu/home');
    }
}
