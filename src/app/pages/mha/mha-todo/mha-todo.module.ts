import { CommonModule } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, Injectable, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRouteSnapshot, Resolve, RouterModule, RouterStateSnapshot, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { UserRouteAccessService } from 'src/app/services/auth/user-route-access.service';
import { MhaActiveProfilService } from 'src/app/services/mha/mha-active-profil.service/mha-active-profil.service';
import { MhaTodoListTemplateService } from 'src/app/services/mha/mha-todo-list-template.service/mha-todo-list-template.service';
import { MhaTodoListService } from 'src/app/services/mha/mha-todo-list.service/mha-todo-list.service';
import { MhaProfilDataComponentModule } from '../../components/mha/mha-profil-data/mha-profil-data.component.module';
import { MhaProfileComponentModule } from '../../components/mha/mha-profil/mha-profile.component.module';
import { MhaSyncComponentModule } from '../../components/mha/mha-sync/mha-sync.component.module';
import { TodoList } from '../../entities/todo-list';
import { TodoListTemplate } from '../../entities/todo-list-template';
import { MhaTodoListPage } from './mha-todo-list';
import { MhaTodoListDetailPage } from './mha-todo-list-detail';
import { MhaTodoListTemplatePage } from './mha-todo-list-template';
import { MhaTodoListTemplateDetailPage } from './mha-todo-list-template-detail';
import { MhaTodoListTemplateUpdatePage } from './mha-todo-list-template-update';
import { MhaTodoListUpdatePage } from './mha-todo-list-update';
import { MhaTodoComponentModule } from '../../components/mha/mha-todo/mha-todo.component.module';


@Injectable({ providedIn: 'root' })
export class MhaTodoListResolve implements Resolve<TodoList> {
  constructor(private service: MhaTodoListService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TodoList> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<TodoList>) => response.ok),
        map((todoList: HttpResponse<TodoList>) => todoList.body)
      );
    }
    return of(new TodoList());
  }
}

@Injectable({ providedIn: 'root' })
export class MhaTodoListFromWorkspaceResolve implements Resolve<TodoList> {
  constructor(private profilService: MhaActiveProfilService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TodoList> {
    const workspaceId = route.params.workspaceId ? route.params.workspaceId : null;
    if (workspaceId) {
      let todoList = new TodoList();
      todoList.workspaceId = workspaceId;
      todoList.owners = [this.profilService.getStoredProfil()];
      return of(todoList);

    }
    return of(new TodoList());
  }
}

@Injectable({ providedIn: 'root' })
export class MhaTodoListTemplateResolve implements Resolve<TodoListTemplate> {
  constructor(private service: MhaTodoListTemplateService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TodoListTemplate> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<TodoListTemplate>) => response.ok),
        map((todoListTemplate: HttpResponse<TodoListTemplate>) => todoListTemplate.body)
      );
    }
    return of(new TodoListTemplate());
  }
}

@Injectable({ providedIn: 'root' })
export class MhaTodoListTemplateFromWorkspaceResolve implements Resolve<TodoListTemplate> {
  constructor(private profilService: MhaActiveProfilService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TodoListTemplate> {
    const workspaceId = route.params.workspaceId ? route.params.workspaceId : null;
    if (workspaceId) {
      let todoListTemplate = new TodoListTemplate();
      todoListTemplate.workspaceId = workspaceId;
      todoListTemplate.owners = [this.profilService.getStoredProfil()];
      return of(todoListTemplate);

    }
    return of(new TodoListTemplate());
  }
}

const routes: Routes = [
  {
    path: 'todo-list',
    component: MhaTodoListPage
  },
  {
    path: 'todo-list/:id/view',
    component: MhaTodoListDetailPage,
    resolve: {
      data: MhaTodoListResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'todo-list/:id/edit',
    component: MhaTodoListUpdatePage,
    resolve: {
      data: MhaTodoListResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'todo-list/:workspaceId/new',
    component: MhaTodoListUpdatePage,
    resolve: {
      data: MhaTodoListFromWorkspaceResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'todo-list-template',
    component: MhaTodoListTemplatePage
  },
  {
    path: 'todo-list-template/:workspaceId/new',
    component: MhaTodoListTemplateUpdatePage,
    resolve: {
      data: MhaTodoListTemplateFromWorkspaceResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'todo-list-template/:id/view',
    component: MhaTodoListTemplateDetailPage,
    resolve: {
      data: MhaTodoListTemplateResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'todo-list-template/:id/edit',
    component: MhaTodoListTemplateUpdatePage,
    resolve: {
      data: MhaTodoListTemplateResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },

];

@NgModule({
  imports: [
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    TranslateModule,
    MhaProfileComponentModule,
    MhaProfilDataComponentModule,
    MhaSyncComponentModule,
    MhaTodoComponentModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MhaTodoListPage, MhaTodoListDetailPage, MhaTodoListUpdatePage, MhaTodoListTemplatePage, MhaTodoListTemplateUpdatePage, MhaTodoListTemplateDetailPage],
  entryComponents: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MhaTodoPageModule { }
