import { HttpResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { MhaActiveProfilService } from 'src/app/services/mha/mha-active-profil.service/mha-active-profil.service';
import { MhaWorkspaceService } from 'src/app/services/mha/mha-workspace.service/mha-workspace.service';
import { setStatusBarBackgroundColor } from 'src/app/shared/util/helpers';
import { SyncStatus } from 'src/model/base-entity';
import { MhaAsyncService } from '../../../services/mha/mha-async.service';
import { Profil } from '../../entities/profil';
import { TodoListTemplate } from '../../entities/todo-list-template';
import { MhaTodoListTemplateService } from 'src/app/services/mha/mha-todo-list-template.service/mha-todo-list-template.service';


@Component({
  selector: 'app-mha-todo-list-template',
  templateUrl: './mha-todo-list-template.html',
  styleUrls: ['./mha-todo.scss'],
})
export class MhaTodoListTemplatePage {

  todoListTemplates: TodoListTemplate[];
  hasModification: boolean;
  activeProfil: Profil;

  constructor(
    private todoListTemplateService: MhaTodoListTemplateService,
    private workspaceService: MhaWorkspaceService,
    private asyncService: MhaAsyncService,
    private profilService: MhaActiveProfilService,
    private toastCtrl: ToastController,
    private navController: NavController,
    public plt: Platform,
    private statusBar: StatusBar) { }

  ionViewWillEnter() {
    setStatusBarBackgroundColor('todo', this.plt);
    this.loadAll();
  }

  async loadAll(refresher?) {
    if (typeof (refresher) !== 'undefined') {
      await this.asyncService.refresh(false);
      setTimeout(() => {
        refresher.target.complete();
      }, 750);
    }
    this.activeProfil = this.profilService.getStoredProfil();
    this.hasModification = this.todoListTemplateService.hasModification();
    this.todoListTemplateService.findWhereParent([this.workspaceService.getStoredActiveWorkspace()]).pipe(
      filter((res: HttpResponse<TodoListTemplate[]>) => res.ok),
      map((res: HttpResponse<TodoListTemplate[]>) => res.body)
    )
      .subscribe(
        async (response: TodoListTemplate[]) => {
          this.todoListTemplates = response.filter(tl => tl.syncStatus != SyncStatus.DELETE && tl.owners.some(o => o.id === this.activeProfil.id));
          
        },
        async (error) => {
          console.error(error);
          const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
          toast.present();
        });
  }

  trackId(index: number, item: TodoListTemplate) {
    return item.id;
  }
  view(todoList: TodoListTemplate) {
    this.navController.navigateForward('/menu/todo/todo-list-template/' + todoList.id + '/view');
  }

 
  new() {
    this.navController.navigateForward('/menu/todo/todo-list-template/' + this.workspaceService.getStoredActiveWorkspace().id + '/new');
  }

  


}
