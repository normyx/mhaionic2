import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, NgZone, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AlertController, NavController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { MhaTodoTemplateService } from 'src/app/services/mha/mha-todo-template.service/mha-todo-template.service';
import { Events } from 'src/app/shared/util/Events';
import { setStatusBarBackgroundColor } from 'src/app/shared/util/helpers';
import { SyncStatus } from 'src/model/base-entity';
import { MhaAsyncService } from '../../../services/mha/mha-async.service';
import { TodoListTemplate } from '../../entities/todo-list-template';
import { TodoTemplate } from '../../entities/todo-template';

@Component({
    selector: 'mha-page-todo-list-template-detail',
    templateUrl: 'mha-todo-list-template-detail.html',
    styleUrls: ['./mha-todo.scss'],
})
export class MhaTodoListTemplateDetailPage implements OnInit {

    todoListTemplate: TodoListTemplate;
    todoTemplates: TodoTemplate[];
    hasModification: boolean;

    constructor(
        private navController: NavController,
        private activatedRoute: ActivatedRoute,
        private todoTemplateService: MhaTodoTemplateService,
        private todoListTemplateService: MhaTodoTemplateService,
        private asyncService: MhaAsyncService,
        private toastCtrl: ToastController,
        public plt: Platform,
        public popoverController: PopoverController,
        private statusBar: StatusBar,
        private alertController: AlertController,
        public events: Events,
        private zone: NgZone
    ) {
        this.events.subscribe('todolistTemplate:reorder', () => {
            this.zone.run(() => {
                this.reorderTodoTemplates();
            });
        });
    }

    ngOnInit() {
    }

    ionViewWillEnter() {
        setStatusBarBackgroundColor('todo', this.plt);
        this.loadAll();
    }

    async loadAll(refresher?) {
        if (typeof (refresher) !== 'undefined') {
            await this.asyncService.refresh(false);
            setTimeout(() => {
                refresher.target.complete();
            }, 750);
        }
        this.hasModification = this.todoListTemplateService.hasModification() || this.todoTemplateService.hasModification();
        this.activatedRoute.data.subscribe((response) => {
            this.todoListTemplate = response.data;
            this.todoTemplateService.findWhereParent([this.todoListTemplate]).pipe(
                filter((res: HttpResponse<TodoTemplate[]>) => res.ok),
                map((res: HttpResponse<TodoTemplate[]>) => res.body))
                .subscribe(
                    (response: TodoTemplate[]) => {
                        this.todoTemplates = response;
                        this.reorderTodoTemplates();
                    },
                    async (error) => {
                        console.error(error);
                        const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
                        toast.present();
                    });
        });
    }

    reorderTodoTemplates() {
        this.todoTemplates = this.todoTemplates.filter(t => t.syncStatus != SyncStatus.DELETE);
    }

    newTodoTemplate() {
        //this.navController.navigateForward('/menu/todo/todo/' + this.todoList.id + '/new');
        let todo: TodoTemplate = new TodoTemplate();
        todo.label = "Nouveau";
        todo.todoListTemplateId = this.todoListTemplate.id;
        this.todoTemplateService.create(todo).subscribe((res: HttpResponse<TodoTemplate>) => { }, (res: HttpErrorResponse) => { });
        this.todoTemplates.push(todo);
        this.reorderTodoTemplates();
    }

    goBack() {
        this.navController.navigateRoot('/menu/todo/todo-list-template');
    }


    trackId(index: number, item: TodoTemplate) {
        return item.id;
    }

    goUpdatePage(event) {
        this.navController.navigateForward('/menu/todo/todo-list-template/' + this.todoListTemplate.id + '/edit');
        //await this.popController.dismiss(event.detail.value);
    }

    







}
