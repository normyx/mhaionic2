import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AlertController, ModalController, NavController, Platform, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { setStatusBarBackgroundColor } from 'src/app/shared/util/helpers';
import { Profil } from '../../entities/profil';
import { TodoListTemplate } from '../../entities/todo-list-template';
import { Workspace, WorkspaceService } from '../../entities/workspace';
import { MhaTodoListTemplateService } from 'src/app/services/mha/mha-todo-list-template.service/mha-todo-list-template.service';
import { MhaProfileChooserModalComponent } from '../../components/mha/mha-profil/mha-profile-chooser-modal.component/mha-profile-chooser-modal.component';


@Component({
    selector: 'mha-page-todo-list-template-update',
    templateUrl: 'mha-todo-list-template-update.html',
    styleUrls: ['./mha-todo.scss']
})
export class MhaTodoListTemplateUpdatePage implements OnInit {

    todoListTemplate: TodoListTemplate;
    workspace: Workspace;
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;

    form = this.formBuilder.group({
        id: [],
        label: [null, [Validators.required]],
        workspaceId: [null, []],
        owners: [null, []],
    });

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        private alertController: AlertController,
        protected formBuilder: FormBuilder,
        public platform: Platform,
        protected toastCtrl: ToastController,
        private workspaceService: WorkspaceService,
        private todoListTemplateService: MhaTodoListTemplateService,
        public modalController: ModalController,
        private statusBar: StatusBar
    ) {

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ngOnInit() {
        setStatusBarBackgroundColor('todo', this.platform);
        this.activatedRoute.data.subscribe((response) => {
            this.updateForm(response.data);
            this.todoListTemplate = response.data;
            this.isNew = this.todoListTemplate.id === null || this.todoListTemplate.id === undefined;
        });
    }

    updateForm(todoListTemplate: TodoListTemplate) {
        this.form.patchValue({
            id: todoListTemplate.id,
            label: todoListTemplate.label,
            workspaceId: todoListTemplate.workspaceId,
            owners: todoListTemplate.owners,
        });
        this.workspaceService.find(todoListTemplate.workspaceId).subscribe(data => {
            this.workspace = data.body;
        }, (error) => this.onError(error));
    }

    save() {
        this.isSaving = true;
        const taskProject = this.createFromForm();
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.todoListTemplateService.update(taskProject));
        } else {
            this.subscribeToSaveResponse(this.todoListTemplateService.create(taskProject));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<TodoListTemplate>>) {
        result.subscribe((res: HttpResponse<TodoListTemplate>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        let action = 'updated';
        if (response.status === 201) {
            action = 'created';
            this.todoListTemplate = response.body;
        }
        this.isSaving = false;
        const toast = await this.toastCtrl.create({ message: `TodoListTemplate ${action} successfully.`, duration: 2000, position: 'middle' });
        toast.present();
        this.navController.navigateForward('/menu/todo/todo-list-template/' + this.todoListTemplate.id + '/view');
    }


    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
        toast.present();
    }

    private createFromForm(): TodoListTemplate {
        return {
            ...new TodoListTemplate(),
            id: this.form.get(['id']).value,
            label: this.form.get(['label']).value,
            workspaceId: this.form.get(['workspaceId']).value,
            owners: this.form.get(['owners']).value,
            syncStatus: this.todoListTemplate.syncStatus
        };
    }

    compareWorkspace(first: Workspace, second: Workspace): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackWorkspaceById(index: number, item: Workspace) {
        return item.id;
    }
    compareProfil(first: Profil, second: Profil): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackProfilById(index: number, item: Profil) {
        return item.id;
    }

    getAvailableProfils(): Profil[] {
        let profils = new Array<Profil>();
        if (!this.todoListTemplate.owners) {
            this.todoListTemplate.owners = new Array<Profil>();
        }
        for (let workspaceProfil of this.workspace.owners) {
            if (!this.todoListTemplate.owners.some(e => e.id === workspaceProfil.id)) {
                profils.push(workspaceProfil);
            }
        }
        return profils;

    }
    async chooseProfile() {
        const modal = await this.modalController.create({
            component: MhaProfileChooserModalComponent,
            animated: true,
            showBackdrop: true,
            keyboardClose: true,
            componentProps: { "profils": this.todoListTemplate.owners, "availableProfils": this.getAvailableProfils(), "canBeWithNoActor": false, "warningIfConnectedUserRemoved": true, "color": "task", "singleSelectMode":false }
        });
        modal.onDidDismiss().then((dataReturned) => {
            if (dataReturned !== null && dataReturned.data && dataReturned.data != null) {
                this.todoListTemplate.owners = dataReturned.data;
                this.form.patchValue({ 'owners': this.todoListTemplate.owners });
            }
        });
        return await modal.present();
    }
    async deleteModal() {
        const alert = await this.alertController.create({
            header: 'Confirmer vous la suppression de ' + this.todoListTemplate.label + ' ? Toutes les todos correspondantes seront également supprimées.',
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Supprimer',
                    handler: () => {
                        this.todoListTemplateService.delete(this.todoListTemplate.id).subscribe(() => {
                            this.navController.navigateRoot('/menu/todo/todo-list-template');

                        });

                    }
                }
            ]
        });
        await alert.present();
    }
}
