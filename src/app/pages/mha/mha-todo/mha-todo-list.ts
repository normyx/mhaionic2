import { HttpResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { MhaActiveProfilService } from 'src/app/services/mha/mha-active-profil.service/mha-active-profil.service';
import { MhaTodoListService } from 'src/app/services/mha/mha-todo-list.service/mha-todo-list.service';
import { MhaWorkspaceService } from 'src/app/services/mha/mha-workspace.service/mha-workspace.service';
import { setStatusBarBackgroundColor } from 'src/app/shared/util/helpers';
import { SyncStatus } from 'src/model/base-entity';
import { MhaAsyncService } from '../../../services/mha/mha-async.service';
import { Profil } from '../../entities/profil';
import { TodoList } from '../../entities/todo-list';
import { TodoListSummary } from 'src/model/todo-list-summary-model';


@Component({
  selector: 'app-mha-todo-list',
  templateUrl: './mha-todo-list.html',
  styleUrls: ['./mha-todo.scss'],
})
export class MhaTodoListPage {

  todoListSummaries: TodoListSummary[];
  hasModification: boolean;
  activeProfil: Profil;

  constructor(
    private todoListService: MhaTodoListService,
    private workspaceService: MhaWorkspaceService,
    private asyncService: MhaAsyncService,
    private profilService: MhaActiveProfilService,
    private toastCtrl: ToastController,
    private navController: NavController,
    public plt: Platform,
    private statusBar: StatusBar) {
    this.todoListSummaries = [];
  }

  ionViewWillEnter() {
    setStatusBarBackgroundColor('todo', this.plt);
    this.loadAll();
  }

  async loadAll(refresher?) {
    if (typeof (refresher) !== 'undefined') {
      await this.asyncService.refresh(false);
      setTimeout(() => {
        refresher.target.complete();
      }, 750);
    }
    this.activeProfil = this.profilService.getStoredProfil();
    this.hasModification = this.todoListService.hasModification();
    this.todoListService.findWhereParent([this.workspaceService.getStoredActiveWorkspace()]).pipe(
      filter((res: HttpResponse<TodoList[]>) => res.ok),
      map((res: HttpResponse<TodoList[]>) => res.body)
    )
      .subscribe(
        async (response: TodoList[]) => {
          this.todoListSummaries = [];
          let todoLists = response.filter(tl => tl.syncStatus != SyncStatus.DELETE && tl.owners.some(o => o.id === this.activeProfil.id));
          for (let todoList of todoLists) {
            await this.todoListService.getTodoListSummary(todoList).then(tls => { this.todoListSummaries.push(tls); });
          }

        },
        async (error) => {
          console.error(error);
          const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
          toast.present();
        });
  }

  trackId(index: number, item: TodoList) {
    return item.id;
  }
  view(todoList: TodoList) {
    this.navController.navigateForward('/menu/todo/todo-list/' + todoList.id + '/view');
  }

  goTodoListTemplatePage() {
    this.navController.navigateForward('/menu/todo/todo-list-template');
  }
  new() {
    this.navController.navigateForward('/menu/todo/todo-list/' + this.workspaceService.getStoredActiveWorkspace().id + '/new');
  }




}
