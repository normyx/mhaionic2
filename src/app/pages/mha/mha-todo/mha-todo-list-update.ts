import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AlertController, ModalController, NavController, Platform, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { setStatusBarBackgroundColor } from 'src/app/shared/util/helpers';
import { Profil } from '../../entities/profil';
import { Workspace, WorkspaceService } from '../../entities/workspace';
import { TodoList } from '../../entities/todo-list';
import { MhaTodoListService } from 'src/app/services/mha/mha-todo-list.service/mha-todo-list.service';
import { MhaProfileChooserModalComponent } from '../../components/mha/mha-profil/mha-profile-chooser-modal.component/mha-profile-chooser-modal.component';


@Component({
    selector: 'mha-page-todo-list-update',
    templateUrl: 'mha-todo-list-update.html',
    styleUrls: ['./mha-todo.scss']
})
export class MhaTodoListUpdatePage implements OnInit {

    todoList: TodoList;
    workspace: Workspace;
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;

    form = this.formBuilder.group({
        id: [],
        label: [null, [Validators.required]],
        workspaceId: [null, []],
        owners: [null, []],
    });

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        private alertController: AlertController,
        protected formBuilder: FormBuilder,
        public platform: Platform,
        protected toastCtrl: ToastController,
        private workspaceService: WorkspaceService,
        private todoListService: MhaTodoListService,
        public modalController: ModalController,
        private statusBar: StatusBar
    ) {

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ngOnInit() {
        setStatusBarBackgroundColor('todo', this.platform);
        this.activatedRoute.data.subscribe((response) => {
            this.updateForm(response.data);
            this.todoList = response.data;
            this.isNew = this.todoList.id === null || this.todoList.id === undefined;
        });
    }

    updateForm(todoList: TodoList) {
        this.form.patchValue({
            id: todoList.id,
            label: todoList.label,
            workspaceId: todoList.workspaceId,
            owners: todoList.owners,
        });
        this.workspaceService.find(todoList.workspaceId).subscribe(data => {
            this.workspace = data.body;
        }, (error) => this.onError(error));
    }

    save() {
        this.isSaving = true;
        const taskProject = this.createFromForm();
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.todoListService.update(taskProject));
        } else {
            this.subscribeToSaveResponse(this.todoListService.create(taskProject));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<TodoList>>) {
        result.subscribe((res: HttpResponse<TodoList>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        let action = 'updated';
        if (response.status === 201) {
            action = 'created';
            this.todoList = response.body;
        }
        this.isSaving = false;
        const toast = await this.toastCtrl.create({ message: `TodoList ${action} successfully.`, duration: 2000, position: 'middle' });
        toast.present();
        this.navController.navigateForward('/menu/todo/todo-list/' + this.todoList.id + '/view');
    }


    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
        toast.present();
    }

    private createFromForm(): TodoList {
        return {
            ...new TodoList(),
            id: this.form.get(['id']).value,
            label: this.form.get(['label']).value,
            workspaceId: this.form.get(['workspaceId']).value,
            owners: this.form.get(['owners']).value,
            syncStatus: this.todoList.syncStatus
        };
    }

    compareWorkspace(first: Workspace, second: Workspace): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackWorkspaceById(index: number, item: Workspace) {
        return item.id;
    }
    compareProfil(first: Profil, second: Profil): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackProfilById(index: number, item: Profil) {
        return item.id;
    }

    getAvailableProfils(): Profil[] {
        let profils = new Array<Profil>();
        if (!this.todoList.owners) {
            this.todoList.owners = new Array<Profil>();
        }
        for (let workspaceProfil of this.workspace.owners) {
            if (!this.todoList.owners.some(e => e.id === workspaceProfil.id)) {
                profils.push(workspaceProfil);
            }
        }
        return profils;

    }
    async chooseProfile() {
        const modal = await this.modalController.create({
            component: MhaProfileChooserModalComponent,
            animated: true,
            showBackdrop: true,
            keyboardClose: true,
            componentProps: { "profils": this.todoList.owners, "availableProfils": this.getAvailableProfils(), "canBeWithNoActor": false, "warningIfConnectedUserRemoved": true, "color": "task", "singleSelectMode":false }
        });
        modal.onDidDismiss().then((dataReturned) => {
            if (dataReturned !== null && dataReturned.data && dataReturned.data != null) {
                this.todoList.owners = dataReturned.data;
                this.form.patchValue({ 'owners': this.todoList.owners });
            }
        });
        return await modal.present();
    }
    async deleteModal() {
        const alert = await this.alertController.create({
            header: 'Confirmer vous la suppression de ' + this.todoList.label + ' ? Toutes les todos correspondantes seront également supprimées.',
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Supprimer',
                    handler: () => {
                        this.todoListService.delete(this.todoList.id).subscribe(() => {
                            this.navController.navigateRoot('/menu/todo/todo-list');

                        });

                    }
                }
            ]
        });
        await alert.present();
    }
}
