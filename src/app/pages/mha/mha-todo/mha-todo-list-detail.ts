import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AlertController, NavController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { MhaTodoService } from 'src/app/services/mha/mha-todo.service/mha-todo.service';
import { setStatusBarBackgroundColor } from 'src/app/shared/util/helpers';
import { SyncStatus } from 'src/model/base-entity';
import { MhaAsyncService } from '../../../services/mha/mha-async.service';
import { Todo } from '../../entities/todo';
import { TodoList } from '../../entities/todo-list';
import { Events } from 'src/app/shared/util/Events';

@Component({
    selector: 'mha-page-todo-list-detail',
    templateUrl: 'mha-todo-list-detail.html',
    styleUrls: ['./mha-todo.scss'],
})
export class MhaTodoListDetailPage implements OnInit {

    todoList: TodoList;
    todos: Todo[];
    hasModification: boolean;

    constructor(
        private navController: NavController,
        private activatedRoute: ActivatedRoute,
        private todoService: MhaTodoService,
        private todoListService: MhaTodoService,
        private asyncService: MhaAsyncService,
        private toastCtrl: ToastController,
        public plt: Platform,
        public popoverController: PopoverController,
        private statusBar: StatusBar,
        private alertController: AlertController,
        public events: Events,
        private zone: NgZone
    ) {
        this.events.subscribe('todolist:reorder', () => {
            this.zone.run(() => {
                this.reorderTodos();
            });
        });
    }

    ngOnInit() {
    }

    ionViewWillEnter() {
        setStatusBarBackgroundColor('todo', this.plt);
        this.loadAll();
    }

    async loadAll(refresher?) {
        if (typeof (refresher) !== 'undefined') {
            await this.asyncService.refresh(false);
            setTimeout(() => {
                refresher.target.complete();
            }, 750);
        }
        this.hasModification = this.todoListService.hasModification() || this.todoService.hasModification();
        this.activatedRoute.data.subscribe((response) => {
            this.todoList = response.data;
            this.todoService.findWhereParent([this.todoList]).pipe(
                filter((res: HttpResponse<Todo[]>) => res.ok),
                map((res: HttpResponse<Todo[]>) => res.body))
                .subscribe(
                    (response: Todo[]) => {
                        this.todos = response;
                        this.reorderTodos();
                    },
                    async (error) => {
                        console.error(error);
                        const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
                        toast.present();
                    });
        });
    }

    reorderTodos() {
        this.todos = this.todos.filter(t => t.syncStatus != SyncStatus.DELETE).sort((a: Todo, b: Todo) => {
            if (a.done == b.done) {
                return a.id - b.id;
            } else {
                return a.done ? 1 : -1;
            }
        });
    }

    newTask() {
        //this.navController.navigateForward('/menu/todo/todo/' + this.todoList.id + '/new');
        let todo: Todo = new Todo();
        todo.done = false;
        todo.label = "Nouveau";
        todo.todoListId = this.todoList.id;
        this.todoService.create(todo).subscribe((res: HttpResponse<Todo>) => { }, (res: HttpErrorResponse) => { });
        this.todos.push(todo);
        this.reorderTodos();
    }

    goBack() {
        this.navController.navigateRoot('/menu/todo/todo-list');
    }


    trackId(index: number, item: Todo) {
        return item.id;
    }

    goUpdatePage(event) {
        this.navController.navigateForward('/menu/todo/todo-list/' + this.todoList.id + '/edit');
        //await this.popController.dismiss(event.detail.value);
    }

    async cleanModal() {
        const alert = await this.alertController.create({
            header: 'Confirmer vous la suppression des todos terminées ? ',
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Nettoyer',
                    handler: () => {
                        for (let todo of this.todos) {
                            if (todo.done) this.todoService.delete(todo.id);
                        }

                        this.loadAll();

                    }
                }
            ]
        });
        await alert.present();
    }







}
