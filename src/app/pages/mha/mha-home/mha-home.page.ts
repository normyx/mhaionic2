import { HttpResponse } from '@angular/common/http';
import { Component, Injectable, OnInit } from '@angular/core';
import { NetworkStatus, Plugins } from '@capacitor/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { MhaActiveProfilService } from 'src/app/services/mha/mha-active-profil.service/mha-active-profil.service';
import { MhaShoppingCartService } from 'src/app/services/mha/mha-shopping-cart.service/mha-shopping-cart.service';
import { MhaTaskProjectService } from 'src/app/services/mha/mha-task-project.service/mha-task-project.service';
import { MhaTodoListService } from 'src/app/services/mha/mha-todo-list.service/mha-todo-list.service';
import { MhaWalletService } from 'src/app/services/mha/mha-wallet.service/mha-wallet.service';
import { setStatusBarBackgroundColor } from 'src/app/shared/util/helpers';
import { SyncStatus } from 'src/model/base-entity';
import { ShoppingCartSummary } from 'src/model/shopping-cart-summary-model';
import { TaskProjectSummary } from 'src/model/task-project-summary-model';
import { TodoListSummary } from 'src/model/todo-list-summary-model';
import { WalletSummary } from 'src/model/wallet-summary-model';
import { MhaAsyncService } from '../../../services/mha/mha-async.service';
//import { NetworkService } from '../../../services/mha/mha-network.service';
import { MhaWorkspaceService } from '../../../services/mha/mha-workspace.service/mha-workspace.service';
import { Profil } from '../../entities/profil';
import { ShoppingCart } from '../../entities/shopping-cart';
import { Task } from '../../entities/task';
import { TaskProject } from '../../entities/task-project';
import { TodoList } from '../../entities/todo-list';
import { Wallet } from '../../entities/wallet';
import { Workspace } from '../../entities/workspace';
const { Network } = Plugins;

@Injectable()
@Component({
  selector: 'app-mha-home',
  templateUrl: './mha-home.page.html',
  styleUrls: ['./mha-home.page.scss'],
})
export class MhaHomePage implements OnInit {

  workspace: Workspace;
  walletSummaries: WalletSummary[];
  taskProjectSummaries: TaskProjectSummary[];
  todoListSummaries: TodoListSummary[];
  shoppingCartSummaries: ShoppingCartSummary[];
  tasks: Task[];
  activeProfil: Profil;
  networkListener: any;
  networkStatus: NetworkStatus;

  constructor(
    protected asyncService: MhaAsyncService,
    protected workspaceService: MhaWorkspaceService,
    private walletService: MhaWalletService,
    private taskProjectService: MhaTaskProjectService,
    private todoListService: MhaTodoListService,
    private profilService: MhaActiveProfilService,
    private shoppingCartService: MhaShoppingCartService,
    public plt: Platform,
    private toastCtrl: ToastController,
    private navController: NavController,
    public platform: Platform,
    /*public networkService: NetworkService,*/
    private statusBar: StatusBar

  ) {
    this.walletSummaries = [];
    this.taskProjectSummaries = [];
    this.todoListSummaries = [];
    this.shoppingCartSummaries = [];
  }

  
  ngOnInit() {
    setStatusBarBackgroundColor('home', this.platform);
    this.getStatus(); this.startListenNetwork();
    this.activeProfil = this.profilService.getStoredProfil();
    this.workspace = this.workspaceService.getStoredActiveWorkspace();

    this.walletService.findWhereParent([this.workspace]).pipe(
      filter((res: HttpResponse<Wallet[]>) => res.ok),
      map((res: HttpResponse<Wallet[]>) => res.body)
    ).subscribe(
      async (response: Wallet[]) => {
        let wallets = response.filter(w => w.syncStatus != SyncStatus.DELETE && w.owners.some(o => o.id === this.activeProfil.id));
        for (let wallet of wallets) {
          await this.walletService.getWalletSummary(wallet).then(ws => { this.walletSummaries.push(ws) });
        }
      },
      async (error) => {
        console.error(error);
        const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
        toast.present();
      });
    this.taskProjectService.findWhereParent([this.workspace]).pipe(
      filter((res: HttpResponse<TaskProject[]>) => res.ok),
      map((res: HttpResponse<TaskProject[]>) => res.body)
    ).subscribe(
      async (response: TaskProject[]) => {
        let taskProjects = response.filter(tp => tp.syncStatus != SyncStatus.DELETE && tp.owners.some(o => o.id === this.activeProfil.id));
        for (let taskProject of taskProjects) {
          await this.taskProjectService.getTaskProjectSummary(taskProject).then(tps => { this.taskProjectSummaries.push(tps); });
        }
      },
      async (error) => {
        console.error(error);
        const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
        toast.present();
      });

    this.todoListService.findWhereParent([this.workspace]).pipe(
      filter((res: HttpResponse<TodoList[]>) => res.ok),
      map((res: HttpResponse<TodoList[]>) => res.body)
    ).subscribe(
      async (response: TodoList[]) => {
        let todoLists = response.filter(tl => tl.syncStatus != SyncStatus.DELETE && tl.owners.some(o => o.id === this.activeProfil.id));
        for (let todoList of todoLists) {
          await this.todoListService.getTodoListSummary(todoList).then(tls => { this.todoListSummaries.push(tls); });
        }
      },
      async (error) => {
        console.error(error);
        const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
        toast.present();
      });

    this.shoppingCartService.findWhereParent([this.workspace]).pipe(
      filter((res: HttpResponse<ShoppingCart[]>) => res.ok),
      map((res: HttpResponse<ShoppingCart[]>) => res.body)
    ).subscribe(
      async (response: ShoppingCart[]) => {
        let shoppingCarts = response.filter(sc => sc.syncStatus != SyncStatus.DELETE && sc.owners.some(o => o.id === this.activeProfil.id));
        for (let shoppingCart of shoppingCarts) {
          await this.shoppingCartService.getShoppingCartSummary(shoppingCart).then(tls => { this.shoppingCartSummaries.push(tls); });
        }
      },
      async (error) => {
        console.error(error);
        const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
        toast.present();
      });

  }


  async getStatus() {
    try {
      this.networkStatus = await Network.getStatus();
    } catch (e) { console.log("Error", e) }
  }
  startListenNetwork() {
    this.networkListener = Network.addListener('networkStatusChange', (status) => {
      if (!status.connected) {
        this.presentToast("Your internet connection appears to be offline. Data integrity is not guaranteed.");
      } else {
        this.presentToast("Your internet connection appears to be online.");
      }
    });
  }

  async presentToast(msg) {
    let toast = await this.toastCtrl.create({ message: msg, duration: 3000, position: 'bottom' });

    toast.present();
  }

  async loadAll(refresher?) {
    if (typeof (refresher) !== 'undefined') {
      await this.asyncService.refresh(false);
      setTimeout(() => {
        refresher.target.complete();
      }, 750);
    }
  }



  viewWallet(wallet: Wallet) {
    this.navController.navigateForward('/menu/wallet/wallet/' + wallet.id + '/view');
  }
  viewWallets() {
    this.navController.navigateForward('/menu/wallet/wallet');
  }
  viewTaskProject(taskProject: TaskProject) {
    this.navController.navigateForward('/menu/task/task-project/' + taskProject.id + '/view');
  }
  viewTaskProjects() {
    this.navController.navigateForward('/menu/task/task-project');
  }

  viewTodoList(todoList: TodoList) {
    this.navController.navigateForward('/menu/todo/todo-list/' + todoList.id + '/view');
  }
  viewTodoLists() {
    this.navController.navigateForward('/menu/todo/todo-list');
  }
  viewShoppingCart(shoppingCart: ShoppingCart) {
    this.navController.navigateForward('/menu/shopping/shopping-cart/' + shoppingCart.id + '/view');
  }
  viewShoppingCarts() {
    this.navController.navigateForward('/menu/shopping/shopping-cart');
  }
}
