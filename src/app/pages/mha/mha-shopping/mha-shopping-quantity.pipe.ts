import { Pipe, PipeTransform } from '@angular/core';
import { ShoppingItem, Unit } from '../../entities/shopping-item';

@Pipe({ name: 'shoppingQuantity' })
export class ShoppingItemQuantityPipe implements PipeTransform {
    transform(item: ShoppingItem): String {
        let returnStr: string;
        if (item) {
            if (item.quantity) {
                returnStr = ""+item.quantity;
                if (item.unit) {
                    let unitStr;
                    switch (item.unit.toString()) {
                        case 'G' : {
                            unitStr = " g";
                            break;
                        }
                        case 'KG' :{
                            unitStr = " kg";
                            break;
                        }
                        case 'L' :{
                            unitStr = " l";
                            break;
                        }
                        case 'ML' :{
                            unitStr = " ml";
                            break;
                        }
                        case 'QUANTITY' :{
                            unitStr = "x";
                            break;
                        }
                        
                        default: {
                            unitStr = "";
                        }
                    }
                    returnStr += unitStr;
                }
            }
        }
        return returnStr;
    }
}