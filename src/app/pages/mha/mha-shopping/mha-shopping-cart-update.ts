import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AlertController, ModalController, NavController, Platform, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { setStatusBarBackgroundColor } from 'src/app/shared/util/helpers';
import { Profil } from '../../entities/profil';
import { Workspace, WorkspaceService } from '../../entities/workspace';
import { MhaShoppingCartService } from 'src/app/services/mha/mha-shopping-cart.service/mha-shopping-cart.service';
import { ShoppingCart } from '../../entities/shopping-cart';
import { ShoppingCatalogCart } from '../../entities/shopping-catalog-cart';
import { MhaShoppingCatalogCartService } from 'src/app/services/mha/mha-shopping-catalog-cart.service/mha-shopping-catalog-cart.service';
import { MhaProfileChooserModalComponent } from '../../components/mha/mha-profil/mha-profile-chooser-modal.component/mha-profile-chooser-modal.component';


@Component({
    selector: 'mha-page-shopping-cart-update',
    templateUrl: 'mha-shopping-cart-update.html',
    styleUrls: ['./mha-shopping.scss']
})
export class MhaShoppingCartUpdatePage implements OnInit {

    shoppingCart: ShoppingCart;
    shoppingCatalogCarts: ShoppingCatalogCart[];
    workspace: Workspace;
    isSaving = false;
    isNew = true;
    isReadyToSave: boolean;

    form = this.formBuilder.group({
        id: [],
        label: [null, [Validators.required]],
        workspaceId: [null, []],
        owners: [null, []],
        shoppingCatalogCartId: [null, []],
    });

    constructor(
        protected activatedRoute: ActivatedRoute,
        protected navController: NavController,
        private alertController: AlertController,
        protected formBuilder: FormBuilder,
        public platform: Platform,
        protected toastCtrl: ToastController,
        private workspaceService: WorkspaceService,
        private shoppingCartService: MhaShoppingCartService,
        private shoppingCatalogCartService: MhaShoppingCatalogCartService,
        public modalController: ModalController,
        private statusBar: StatusBar
    ) {

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ngOnInit() {
        setStatusBarBackgroundColor('shopping', this.platform);
        this.activatedRoute.data.subscribe((response) => {
            this.updateForm(response.data);
            this.shoppingCart = response.data;
            this.isNew = this.shoppingCart.id === null || this.shoppingCart.id === undefined;
        });
        this.shoppingCatalogCartService.findWhereParent([])
            .subscribe(data => { this.shoppingCatalogCarts = data.body; }, (error) => this.onError(error));
    }

    updateForm(shoppingCart: ShoppingCart) {
        this.form.patchValue({
            id: shoppingCart.id,
            label: shoppingCart.label,
            workspaceId: shoppingCart.workspaceId,
            owners: shoppingCart.owners,
            shoppingCatalogCartId: shoppingCart.shoppingCatalogCartId,
        });
        this.workspaceService.find(shoppingCart.workspaceId).subscribe(data => {
            this.workspace = data.body;
        }, (error) => this.onError(error));
    }

    save() {
        this.isSaving = true;
        const shoppingCart = this.createFromForm();
        // update the shoppingCatalogCartLabel
        shoppingCart.shoppingCatalogCartLabel = this.shoppingCatalogCarts.find(scc => scc.id == shoppingCart.shoppingCatalogCartId).label;
        if (!this.isNew) {
            this.subscribeToSaveResponse(this.shoppingCartService.update(shoppingCart));
        } else {
            this.subscribeToSaveResponse(this.shoppingCartService.create(shoppingCart));
        }
    }

    protected subscribeToSaveResponse(result: Observable<HttpResponse<ShoppingCart>>) {
        result.subscribe((res: HttpResponse<ShoppingCart>) => this.onSaveSuccess(res), (res: HttpErrorResponse) => this.onError(res.error));
    }

    async onSaveSuccess(response) {
        let action = 'updated';
        if (response.status === 201) {
            action = 'created';
            this.shoppingCart = response.body;
        }
        this.isSaving = false;
        const toast = await this.toastCtrl.create({ message: `ShoppingCart ${action} successfully.`, duration: 2000, position: 'middle' });
        toast.present();
        this.navController.navigateForward('/menu/shopping/shopping-cart/' + this.shoppingCart.id + '/view');
    }


    async onError(error) {
        this.isSaving = false;
        console.error(error);
        const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
        toast.present();
    }

    private createFromForm(): ShoppingCart {
        return {
            ...new ShoppingCart(),
            id: this.form.get(['id']).value,
            label: this.form.get(['label']).value,
            workspaceId: this.form.get(['workspaceId']).value,
            owners: this.form.get(['owners']).value,
            shoppingCatalogCartId: this.form.get(['shoppingCatalogCartId']).value,
            syncStatus: this.shoppingCart.syncStatus
        };
    }

    compareWorkspace(first: Workspace, second: Workspace): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackWorkspaceById(index: number, item: Workspace) {
        return item.id;
    }
    compareProfil(first: Profil, second: Profil): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackProfilById(index: number, item: Profil) {
        return item.id;
    }

    compareShoppingCatalogCart(first: ShoppingCatalogCart, second: ShoppingCatalogCart): boolean {
        return first && second ? first.id === second.id : first === second;
    }

    trackShoppingCatalogCartById(index: number, item: ShoppingCatalogCart) {
        return item.id;
    }

    getAvailableProfils(): Profil[] {
        let profils = new Array<Profil>();
        if (!this.shoppingCart.owners) {
            this.shoppingCart.owners = new Array<Profil>();
        }
        for (let workspaceProfil of this.workspace.owners) {
            if (!this.shoppingCart.owners.some(e => e.id === workspaceProfil.id)) {
                profils.push(workspaceProfil);
            }
        }
        return profils;

    }
    async chooseProfile() {
        const modal = await this.modalController.create({
            component: MhaProfileChooserModalComponent,
            animated: true,
            showBackdrop: true,
            keyboardClose: true,
            componentProps: { "profils": this.shoppingCart.owners, "availableProfils": this.getAvailableProfils(), "canBeWithNoActor": false, "warningIfConnectedUserRemoved": true, "color": "shopping", "singleSelectMode": false }
        });
        modal.onDidDismiss().then((dataReturned) => {
            if (dataReturned !== null && dataReturned.data && dataReturned.data != null) {
                this.shoppingCart.owners = dataReturned.data;
                this.form.patchValue({ 'owners': this.shoppingCart.owners });
            }
        });
        return await modal.present();
    }
    async deleteModal() {
        const alert = await this.alertController.create({
            header: 'Confirmer vous la suppression de ' + this.shoppingCart.label + ' ? Toutes les todos correspondantes seront également supprimées.',
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Supprimer',
                    handler: () => {
                        this.shoppingCartService.delete(this.shoppingCart.id).subscribe(() => {
                            this.navController.navigateRoot('/menu/shopping/shopping-cart');

                        });

                    }
                }
            ]
        });
        await alert.present();
    }
}
