import { Injectable } from '@angular/core';
import { AutoCompleteService } from 'ionic4-auto-complete';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { MhaShoppingCatalogItemService } from 'src/app/services/mha/mha-shopping-catalog-item.service/mha-shopping-catalog-item.service';
import { ShoppingCatalogItem } from '../../entities/shopping-catalog-item';
import { ShoppingCatalogSheld } from '../../entities/shopping-catalog-sheld';
import { ShoppingItem } from '../../entities/shopping-item';

@Injectable()
export class AutocompleteShoppingCatalogItemService implements AutoCompleteService {
    labelAttribute = 'label';

    public shoppingItems: ShoppingCatalogItem[] = [];
    private defaultShoppingItem: ShoppingCatalogItem;

    constructor(private shoppingCatalogItemService: MhaShoppingCatalogItemService) {

    }

    setDefaultShoppingCatalagSheld(scs: ShoppingCatalogSheld) {
        this.defaultShoppingItem = new ShoppingItem();
        this.defaultShoppingItem.shoppingCatalogSheldId = scs.id;
        this.defaultShoppingItem.shoppingCatalogSheldLabel = scs.label;
    }

    getResults(keyword: string): Observable<any[]> {
        let observable: Observable<ShoppingCatalogItem[]>;
        observable = of(this.shoppingItems);
        return observable.pipe(
            map(
                (result) => {
                    let filteredShoppingItem = result.filter(
                        (item) => {
                            return item.label.toLowerCase().indexOf(
                                keyword.toLowerCase()
                            ) >= 0;
                        }
                    );
                    if (keyword && keyword.length > 0) {
                        const fakeItem = Object.assign({}, this.defaultShoppingItem);
                        fakeItem.label = keyword;
                        filteredShoppingItem.unshift(fakeItem);
                        console.warn(fakeItem);
                    }
                    return filteredShoppingItem;
                }
            )
        );
    }
}