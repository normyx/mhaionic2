import { CommonModule } from '@angular/common';
import { HttpResponse } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, Injectable, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRouteSnapshot, Resolve, RouterModule, RouterStateSnapshot, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { AutoCompleteModule } from 'ionic4-auto-complete';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { UserRouteAccessService } from 'src/app/services/auth/user-route-access.service';
import { MhaActiveProfilService } from 'src/app/services/mha/mha-active-profil.service/mha-active-profil.service';
import { MhaShoppingCartService } from 'src/app/services/mha/mha-shopping-cart.service/mha-shopping-cart.service';
import { ShoppingCart } from '../../entities/shopping-cart';
import { MhaShoppingCartDetailPage } from './mha-shopping-cart-detail';
import { MhaShoppingCartPage } from './mha-shopping-cart-list';
import { AutocompleteShoppingCatalogItemService } from './mha-shopping-catalog-item-autocompletion.service';
import { ShoppingItemQuantityPipe } from './mha-shopping-quantity.pipe';
import { MhaShoppingCartUpdatePage } from './mha-shopping-cart-update';
import { MhaProfileComponentModule } from '../../components/mha/mha-profil/mha-profile.component.module';
import { MhaProfilDataComponentModule } from '../../components/mha/mha-profil-data/mha-profil-data.component.module';
import { MhaSyncComponentModule } from '../../components/mha/mha-sync/mha-sync.component.module';
import { MhaShoppingComponentModule } from '../../components/mha/mha-shopping/mha-shopping.component.module';


@Injectable({ providedIn: 'root' })
export class MhaShoppingCartResolve implements Resolve<ShoppingCart> {
  constructor(private service: MhaShoppingCartService) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ShoppingCart> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ShoppingCart>) => response.ok),
        map((shoppingCart: HttpResponse<ShoppingCart>) => shoppingCart.body)
      );
    }
    return of(new ShoppingCart());
  }
}

@Injectable({ providedIn: 'root' })
export class MhaShoppingCartFromWorkspaceResolve implements Resolve<ShoppingCart> {
  constructor(private profilService: MhaActiveProfilService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ShoppingCart> {
    const workspaceId = route.params.workspaceId ? route.params.workspaceId : null;
    if (workspaceId) {
      let shoppingCart = new ShoppingCart();
      shoppingCart.workspaceId = workspaceId;
      shoppingCart.owners = [this.profilService.getStoredProfil()];
      return of(shoppingCart);

    }
    return of(new ShoppingCart());
  }
}



const routes: Routes = [
  {
    path: 'shopping-cart',
    component: MhaShoppingCartPage
  },
  {
    path: 'shopping-cart/:id/view',
    component: MhaShoppingCartDetailPage,
    resolve: {
      data: MhaShoppingCartResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'shopping-cart/:id/edit',
    component: MhaShoppingCartUpdatePage,
    resolve: {
      data: MhaShoppingCartResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'shopping-cart/:workspaceId/new',
    component: MhaShoppingCartUpdatePage,
    resolve: {
      data: MhaShoppingCartFromWorkspaceResolve
    },
    data: {
      authorities: ['ROLE_USER']
    },
    canActivate: [UserRouteAccessService]
  },

];

@NgModule({
  imports: [
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    TranslateModule,
    MhaProfileComponentModule,
    MhaProfilDataComponentModule,
    MhaSyncComponentModule,
    MhaShoppingComponentModule,
    AutoCompleteModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MhaShoppingCartPage, MhaShoppingCartDetailPage, ShoppingItemQuantityPipe, MhaShoppingCartUpdatePage],
  entryComponents: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    AutocompleteShoppingCatalogItemService
  ]
})
export class MhaShoppingPageModule { }
