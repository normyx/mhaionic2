import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AlertController, NavController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { AutoCompleteComponent, AutoCompleteOptions } from 'ionic4-auto-complete';
import { filter, map } from 'rxjs/operators';
import { MhaShoppingCatalogCartService } from 'src/app/services/mha/mha-shopping-catalog-cart.service/mha-shopping-catalog-cart.service';
import { MhaShoppingCatalogItemService } from 'src/app/services/mha/mha-shopping-catalog-item.service/mha-shopping-catalog-item.service';
import { MhaShoppingCatalogSheldService } from 'src/app/services/mha/mha-shopping-catalog-sheld.service/mha-shopping-catalog-sheld.service';
import { Events } from 'src/app/shared/util/Events';
import { setStatusBarBackgroundColor } from 'src/app/shared/util/helpers';
import { SyncStatus } from 'src/model/base-entity';
import { MhaAsyncService } from '../../../services/mha/mha-async.service';
import { MhaShoppingCartService } from '../../../services/mha/mha-shopping-cart.service/mha-shopping-cart.service';
import { MhaShoppingItemService } from '../../../services/mha/mha-shopping-item.service/mha-shopping-item.service';
import { ShoppingCart } from '../../entities/shopping-cart/shopping-cart.model';
import { ShoppingCatalogItem } from '../../entities/shopping-catalog-item';
import { ShoppingCatalogSheld } from '../../entities/shopping-catalog-sheld';
import { ShoppingItem } from '../../entities/shopping-item/shopping-item.model';
import { AutocompleteShoppingCatalogItemService } from './mha-shopping-catalog-item-autocompletion.service';



@Component({
    selector: 'mha-page-shopping-cart-detail',
    templateUrl: 'mha-shopping-cart-detail.html',
    styleUrls: ['./mha-shopping.scss'],
})
export class MhaShoppingCartDetailPage implements OnInit {

    shoppingCart: ShoppingCart;
    shoppingItems: ShoppingItem[];
    hasModification: boolean;
    newShoppingItemLabel: string;
    shoppingCartSummary: ShoppingCartSummary;
    shoppingCatalogShelds: ShoppingCatalogSheld[];
    itemListwithChecks: boolean;

    public autoCompleteOptions: AutoCompleteOptions;

    @ViewChild('autocompleteShoppingCatalogItem', { static: false })
    autoCompleteComponent: AutoCompleteComponent;
    //@ViewChild(IonReorderGroup, { static: false }) reorderGroup: IonReorderGroup;

    constructor(
        private navController: NavController,
        private activatedRoute: ActivatedRoute,
        private shoppingItemService: MhaShoppingItemService,
        private shopphingCartService: MhaShoppingCartService,
        private shoppingCatalagSheldService: MhaShoppingCatalogSheldService,
        private shoppingCatalagItemService: MhaShoppingCatalogItemService,
        private shoppingCatalagCartService: MhaShoppingCatalogCartService,
        private asyncService: MhaAsyncService,
        private toastCtrl: ToastController,
        public autocompleteShoppingCatalogItemService: AutocompleteShoppingCatalogItemService,
        public plt: Platform,
        public popoverController: PopoverController,
        private statusBar: StatusBar,
        private alertController: AlertController,
        public events: Events,
        private zone: NgZone,
        public platform: Platform
    ) {
        this.itemListwithChecks = false;
        this.events.subscribe('shoppingCart:reorder', () => {
            this.zone.run(() => {
                this.reorderShoppingItems();
            });
        });

        this.autoCompleteOptions = new AutoCompleteOptions();

        this.autoCompleteOptions.autocomplete = 'on';
        this.autoCompleteOptions.cancelButtonIcon = 'assets/icons/clear.svg';
        this.autoCompleteOptions.clearIcon = 'assets/icons/clear.svg';
        this.autoCompleteOptions.debounce = 750;
        this.autoCompleteOptions.placeholder = 'Entrer le nom d\'un article...';
        this.autoCompleteOptions.searchIcon = 'assets/icons/cart.svg';
        this.autoCompleteOptions.type = 'search';
        //this.autoCompleteOptions. = "text";
        this.autoCompleteOptions.animated = true;
        this.autoCompleteOptions.clearInput = true;
        this.autoCompleteOptions.color = 'shopping';

    }

    ngOnInit() {
    }

    ionViewWillEnter() {
        setStatusBarBackgroundColor('shopping', this.platform);
        this.loadAll();
    }

    async loadAll(refresher?) {
        if (typeof (refresher) !== 'undefined') {
            await this.asyncService.refresh(false);
            setTimeout(() => {
                refresher.target.complete();
            }, 750);
        }
        this.hasModification = this.shopphingCartService.hasModification() || this.shoppingItemService.hasModification();

        this.activatedRoute.data.subscribe((response) => {
            this.shoppingCart = response.data;

            this.shoppingCatalagCartService.find(this.shoppingCart.shoppingCatalogCartId).subscribe(sccResponse => {
                this.shoppingCatalagSheldService.findWhereParent([sccResponse.body]).subscribe(scssResponse => {
                    this.shoppingCatalogShelds = scssResponse.body;
                    this.shoppingCatalagItemService.findWhereParent(this.shoppingCatalogShelds).subscribe(sciResponse => {
                        this.autocompleteShoppingCatalogItemService.setDefaultShoppingCatalagSheld(this.shoppingCatalagSheldService.getDefaultCatalogSheld(this.shoppingCart.shoppingCatalogCartId));
                        this.autocompleteShoppingCatalogItemService.shoppingItems = sciResponse.body;
                    });
                });
            });

            this.shoppingItemService.findWhereParent([this.shoppingCart]).pipe(
                filter((res: HttpResponse<ShoppingItem[]>) => res.ok),
                map((res: HttpResponse<ShoppingItem[]>) => res.body))
                .subscribe(
                    (response: ShoppingItem[]) => {
                        this.shoppingItems = response.filter(si => si.syncStatus != SyncStatus.DELETE);

                        this.reorderShoppingItems();
                    },
                    async (error) => {
                        console.error(error);
                        const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
                        toast.present();
                    });
        });
    }

    reorderShoppingItems() {
        this.shoppingCartSummary = new ShoppingCartSummary();
        this.shoppingCartSummary.addSheldsAndItems(this.shoppingCatalogShelds, this.shoppingItems);
        this.shoppingCartSummary.reorder(this.itemListwithChecks);
    }

    goBack() {
        this.navController.navigateRoot('/menu/shopping/shopping-cart');
    }


    trackId(index: number, item: ShoppingItem) {
        return item.id;
    }

    goUpdatePage(event) {
        this.navController.navigateForward('/menu/shopping/shopping-cart/' + this.shoppingCart.id + '/edit');
    }

    async cleanModal() {
        const alert = await this.alertController.create({
            header: 'Confirmer vous la suppression des articles validés ? ',
            buttons: [
                {
                    text: 'Annuler',
                    role: 'cancel',
                    cssClass: 'secondary'
                }, {
                    text: 'Nettoyer',
                    handler: () => {
                        for (let shoppingItem of this.shoppingItems) {
                            if (shoppingItem.checked) {
                                this.shoppingItemService.delete(shoppingItem.id);
                            }
                        }

                        this.loadAll();

                    }
                }
            ]
        });
        await alert.present();
    }

    addShoppingItem(label: string, shoppingCatalogSheldId: number, shoppingCatalogSheldLabel: string) {
        let si = new ShoppingItem();
        si.checked = false;
        si.label = label;
        si.shoppingCartId = this.shoppingCart.id;
        si.shoppingCatalogSheldId = shoppingCatalogSheldId;
        si.shoppingCatalogSheldLabel = shoppingCatalogSheldLabel;
        this.shoppingItemService.create(si).subscribe((res: HttpResponse<ShoppingItem>) => {
            si = res.body;
            this.shoppingItems.push(si);
            this.reorderShoppingItems();
        }, (res: HttpErrorResponse) => { });
        this.autoCompleteComponent.keyword = "";
    }

    addSelectedShoppingItem(event: ShoppingCatalogItem) {
        let sci = event;
        this.addShoppingItem(sci.label, sci.shoppingCatalogSheldId, sci.shoppingCatalogSheldLabel)
    }

    setShoppingItemLabel(label: string) {
        this.newShoppingItemLabel = label;
    }
    doReorder(ev: any) {
        this.shoppingItems = ev.detail.complete(this.shoppingItems);
        this.shoppingItems = this.shoppingItems.filter(item => item !== null && item !== undefined);
        let shoppingItem = this.shoppingCartSummary.getItemFromReorder(ev.detail.from, this.itemListwithChecks);
        let shoppingSheld = this.shoppingCartSummary.getSheldToReorder(ev.detail.from, ev.detail.to, this.itemListwithChecks);
        if (shoppingItem.shoppingCatalogSheldId != shoppingSheld.id) {
            shoppingItem.shoppingCatalogSheldId = shoppingSheld.id;
            shoppingItem.shoppingCatalogSheldLabel = shoppingSheld.label;
            this.shoppingItemService.update(shoppingItem).subscribe((res: HttpResponse<ShoppingItem>) => { }, (res: HttpErrorResponse) => { });
        }
        ev.detail.complete();
        this.reorderShoppingItems();
    }

    toUpdateItem(shoppingItemSummary: ShoppingItemSummary) {
        for (let sheld of this.shoppingCartSummary.sheldSummaries) {
            for (let item of sheld.shoppingItemSummaries) {
                if (item.shoppingItem.id != shoppingItemSummary.shoppingItem.id) {
                    item.onUpdate = false;
                }
            }
        }
        if (!shoppingItemSummary.shoppingItem.checked) {
            shoppingItemSummary.onUpdate = true;
        }
    }
    itemsUpdated() {
        for (let sheld of this.shoppingCartSummary.sheldSummaries) {
            for (let item of sheld.shoppingItemSummaries) {
                item.onUpdate = false;
            }
        }
    }



    updateItem($event, shoppingSheldSummary: ShoppingSheldSummary, shoppingItemSummary: ShoppingItemSummary) {
        shoppingSheldSummary.reorder();
        this.shoppingItemService.update(shoppingItemSummary.shoppingItem).subscribe((res: HttpResponse<ShoppingItem>) => { }, (res: HttpErrorResponse) => { });
    }
}

class ShoppingCartSummary {
    public sheldSummaries: ShoppingSheldSummary[] = [];

    addSheldsAndItems(shelds: ShoppingCatalogSheld[], items: ShoppingItem[]) {
        for (let sheld of shelds) {
            this.addSheld(sheld);
        }
        for (let item of items) {
            this.addItem(item);
        }
    }

    addSheld(sheld: ShoppingCatalogSheld) {
        let sheldSummary = this.sheldSummaries.find(s => s.sheldId == sheld.id);
        if (!sheldSummary) {
            sheldSummary = new ShoppingSheldSummary(sheld.id, sheld.label);
            this.sheldSummaries.push(sheldSummary);
        }
    }


    addItem(item: ShoppingItem) {
        let sheldSummary = this.sheldSummaries.find(s => s.sheldId == item.shoppingCatalogSheldId);
        if (!sheldSummary) {
            console.error("Sheld " + item.shoppingCatalogSheldLabel + " not found");
            //sheldSummary = new ShoppingSheldSummary(item.shoppingCatalogSheldId, item.shoppingCatalogSheldLabel);
            //this.sheldSummaries.push(sheldSummary);
        }
        sheldSummary.addItem(item);
    }

    // This add the Sheld count to get the count
    getItemFromReorder(from: number, itemListwithChecks: boolean): ShoppingItem {
        let count = 0;
        for (let sheldSummary of this.sheldSummaries) {
            count++;
            if (sheldSummary.numberOfItems(itemListwithChecks) >= from - count) {
                return sheldSummary.shoppingItemSummaries[from - count].shoppingItem;
            } else {
                count += sheldSummary.numberOfItems(itemListwithChecks);
            }
        }
        return null;
    }
    getSheldToReorder(from: number, to: number, itemListwithChecks: boolean): ShoppingCatalogSheld {
        let count = from < to ? 0 : 1;
        for (let sheldSummary of this.sheldSummaries) {
            //count++;
            if (sheldSummary.numberOfItems(itemListwithChecks) >= to - count) {
                let sheld = new ShoppingCatalogSheld();
                sheld.id = sheldSummary.sheldId;
                sheld.label = sheldSummary.sheldLabel;
                return sheld;
            } else {
                count += sheldSummary.numberOfItems(itemListwithChecks) + 1;
            }
        }
        return null;
    }

    reorder(itemListwithChecks: boolean) {
        this.sheldSummaries = this.sheldSummaries.sort((a: ShoppingSheldSummary, b: ShoppingSheldSummary) => {
            const numA = a.numberOfItems(itemListwithChecks);
            const numB = b.numberOfItems(itemListwithChecks);
            if (numA == 0) {
                return numB == 0 ? a.sheldId - b.sheldId : 1;
            } else {
                return numB == 0 ? -1 : a.sheldId - b.sheldId;
            }
        });
        for (let sheldSummary of this.sheldSummaries) {
            sheldSummary.reorder();
        }
    }

}



class ShoppingSheldSummary {
    public sheldId: number;
    public sheldLabel: string;
    public shoppingItemSummaries: ShoppingItemSummary[] = [];

    constructor(sheldId: number, sheldLabel: string) {
        this.sheldId = sheldId;
        this.sheldLabel = sheldLabel;
    }

    addItem(item: ShoppingItem) {
        this.shoppingItemSummaries.push(new ShoppingItemSummary(item));
    }

    numberOfItemsIncludedChecked(): number {
        return this.shoppingItemSummaries.length;
    }

    numberOfItemsExcludedChecked(): number {
        return this.shoppingItemSummaries.filter(sis => !sis.shoppingItem.checked).length;
    }


    numberOfItems(itemListwithChecks: boolean): number {
        return itemListwithChecks ? this.numberOfItemsIncludedChecked() : this.numberOfItemsExcludedChecked();
    }

    reorder() {
        this.shoppingItemSummaries = this.shoppingItemSummaries.sort((a: ShoppingItemSummary, b: ShoppingItemSummary) => {
            return a.shoppingItem.checked == b.shoppingItem.checked ? a.shoppingItem.label.localeCompare(b.shoppingItem.label) : a.shoppingItem.checked ? 1 : -1;
        });
    }
}

class ShoppingItemSummary {
    public shoppingItem: ShoppingItem;
    public onUpdate: boolean = false;

    constructor(shoppingItem: ShoppingItem) {
        this.shoppingItem = shoppingItem;
    }


}





