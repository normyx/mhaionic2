import { HttpResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { MhaActiveProfilService } from 'src/app/services/mha/mha-active-profil.service/mha-active-profil.service';
import { MhaShoppingCartService } from 'src/app/services/mha/mha-shopping-cart.service/mha-shopping-cart.service';
import { MhaWorkspaceService } from 'src/app/services/mha/mha-workspace.service/mha-workspace.service';
import { setStatusBarBackgroundColor } from 'src/app/shared/util/helpers';
import { SyncStatus } from 'src/model/base-entity';
import { ShoppingCartSummary } from 'src/model/shopping-cart-summary-model';
import { MhaAsyncService } from '../../../services/mha/mha-async.service';
import { Profil } from '../../entities/profil';
import { ShoppingCart } from '../../entities/shopping-cart';


@Component({
  selector: 'app-mha-shopping-cart-list',
  templateUrl: './mha-shopping-cart-list.html',
  styleUrls: ['./mha-shopping.scss'],
})
export class MhaShoppingCartPage {

  shoppingCartSummaries: ShoppingCartSummary[];
  hasModification: boolean;
  activeProfil: Profil;

  constructor(
    private shoppingCartService: MhaShoppingCartService,
    private workspaceService: MhaWorkspaceService,
    private asyncService: MhaAsyncService,
    private profilService: MhaActiveProfilService,
    private toastCtrl: ToastController,
    private navController: NavController,
    public plt: Platform,
    private statusBar: StatusBar) {
    this.shoppingCartSummaries = []
  }

  ionViewWillEnter() {
    setStatusBarBackgroundColor('shopping', this.plt);
    this.loadAll();
  }

  async loadAll(refresher?) {
    if (typeof (refresher) !== 'undefined') {
      await this.asyncService.refresh(false);
      setTimeout(() => {
        refresher.target.complete();
      }, 750);
    }
    this.activeProfil = this.profilService.getStoredProfil();
    this.hasModification = this.shoppingCartService.hasModification();
    this.shoppingCartService.findWhereParent([this.workspaceService.getStoredActiveWorkspace()]).pipe(
      filter((res: HttpResponse<ShoppingCart[]>) => res.ok),
      map((res: HttpResponse<ShoppingCart[]>) => res.body)
    )
      .subscribe(
        async (response: ShoppingCart[]) => {
          this.shoppingCartSummaries = [];
          let shoppingCarts = response.filter(tl => tl.syncStatus != SyncStatus.DELETE && tl.owners.some(o => o.id === this.activeProfil.id));
          for (let shoppingCart of shoppingCarts) {
            await this.shoppingCartService.getShoppingCartSummary(shoppingCart).then(scs => this.shoppingCartSummaries.push(scs));
          }

        },
        async (error) => {
          console.error(error);
          const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
          toast.present();
        });
  }

  trackId(index: number, item: ShoppingCart) {
    return item.id;
  }
  view(shoppingCart: ShoppingCart) {
    this.navController.navigateForward('/menu/shopping/shopping-cart/' + shoppingCart.id + '/view');
  }

  numberOfActiveShoppingItem(shoppingCart: ShoppingCart): number {
    return shoppingCart && shoppingCart.shoppingItems ? shoppingCart.shoppingItems.filter(si => si.syncStatus != SyncStatus.DELETE && !si.checked).length : 0;
  }

  new() {
    this.navController.navigateForward('/menu/shopping/shopping-cart/' + this.workspaceService.getStoredActiveWorkspace().id + '/new');
  }




}
