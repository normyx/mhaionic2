import { Component, Injectable, OnInit } from '@angular/core';
import { Plugins } from '@capacitor/core';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Platform, ToastController, NavController } from '@ionic/angular';
import { MhaWorkspaceService } from 'src/app/services/mha/mha-workspace.service/mha-workspace.service';
import { setStatusBarBackgroundColor } from 'src/app/shared/util/helpers';
import { MhaAsyncService } from '../../../services/mha/mha-async.service';
import { Workspace } from '../../entities/workspace';


@Injectable()
@Component({
  selector: 'app-mha-info',
  templateUrl: './mha-info.page.html',
  styleUrls: ['./mha-info.page.scss'],
})
export class MhaInfoPage implements OnInit {

  lastUpdate: any;
  workspace: Workspace;

  constructor(
    protected asyncService: MhaAsyncService,
    protected workspaceService: MhaWorkspaceService,
    protected toastCtrl: ToastController,
    public plt: Platform,
    private navController: NavController,
    /*public networkService: NetworkService,*/
    private statusBar: StatusBar

  ) {

  }


  ngOnInit() {
    setStatusBarBackgroundColor('home', this.plt);
    this.loadAll();

  }

  async loadAll() {
    this.lastUpdate = this.asyncService.lastUpdate;
    this.workspace = this.workspaceService.getStoredActiveWorkspace();


  }

  async refresh() {
    this.asyncService.refresh(false);
    this.lastUpdate = this.asyncService.lastUpdate;
    setTimeout(async () => {
      const toast = await this.toastCtrl.create({ message: 'Synchronisation réalisée', duration: 2000, position: 'middle' });
      toast.present();

    }, 750);

  }




}
