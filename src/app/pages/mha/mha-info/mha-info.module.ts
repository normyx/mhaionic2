import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
//import { NetworkService } from 'src/app/services/mha/mha-network.service';
import { MhaInfoPage } from './mha-info.page';



const routes: Routes = [
  {
    path: '',
    component: MhaInfoPage
  }
];

@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [MhaInfoPage],
  providers: [/*NetworkService*/]
})
export class MhaInfoPageModule { }
