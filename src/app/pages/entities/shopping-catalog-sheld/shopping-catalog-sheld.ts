import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { ShoppingCatalogSheld } from './shopping-catalog-sheld.model';
import { ShoppingCatalogSheldService } from './shopping-catalog-sheld.service';

@Component({
  selector: 'page-shopping-catalog-sheld',
  templateUrl: 'shopping-catalog-sheld.html',
})
export class ShoppingCatalogSheldPage {
  shoppingCatalogShelds: ShoppingCatalogSheld[];

  // todo: add pagination

  constructor(
    private navController: NavController,
    private shoppingCatalogSheldService: ShoppingCatalogSheldService,
    private toastCtrl: ToastController,
    public plt: Platform
  ) {
    this.shoppingCatalogShelds = [];
  }

  ionViewWillEnter() {
    this.loadAll();
  }

  async loadAll(refresher?) {
    this.shoppingCatalogSheldService
      .query()
      .pipe(
        filter((res: HttpResponse<ShoppingCatalogSheld[]>) => res.ok),
        map((res: HttpResponse<ShoppingCatalogSheld[]>) => res.body)
      )
      .subscribe(
        (response: ShoppingCatalogSheld[]) => {
          this.shoppingCatalogShelds = response;
          if (typeof refresher !== 'undefined') {
            setTimeout(() => {
              refresher.target.complete();
            }, 750);
          }
        },
        async (error) => {
          console.error(error);
          const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
          toast.present();
        }
      );
  }

  trackId(index: number, item: ShoppingCatalogSheld) {
    return item.id;
  }

  new() {
    this.navController.navigateForward('/tabs/entities/shopping-catalog-sheld/new');
  }

  edit(item: IonItemSliding, shoppingCatalogSheld: ShoppingCatalogSheld) {
    this.navController.navigateForward('/tabs/entities/shopping-catalog-sheld/' + shoppingCatalogSheld.id + '/edit');
    item.close();
  }

  async delete(shoppingCatalogSheld) {
    this.shoppingCatalogSheldService.delete(shoppingCatalogSheld.id).subscribe(
      async () => {
        const toast = await this.toastCtrl.create({
          message: 'ShoppingCatalogSheld deleted successfully.',
          duration: 3000,
          position: 'middle',
        });
        toast.present();
        this.loadAll();
      },
      (error) => console.error(error)
    );
  }

  view(shoppingCatalogSheld: ShoppingCatalogSheld) {
    this.navController.navigateForward('/tabs/entities/shopping-catalog-sheld/' + shoppingCatalogSheld.id + '/view');
  }
}
