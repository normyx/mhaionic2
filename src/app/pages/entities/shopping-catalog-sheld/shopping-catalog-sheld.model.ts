import { BaseEntity, SyncStatus } from 'src/model/base-entity';
import { ShoppingCatalogItem } from '../shopping-catalog-item/shopping-catalog-item.model';

export class ShoppingCatalogSheld implements BaseEntity {
  constructor(
    public id?: number,
    public label?: string,
    public lastUpdate?: any,
    public isDefault?: boolean,
    public isDeleted?: boolean,
    public shoppingCatalogItems?: ShoppingCatalogItem[],
    public shoppingCatalogCartLabel?: string,
    public shoppingCatalogCartId?: number, public syncStatus?: SyncStatus
  ) {
    this.isDefault = false;
    this.isDeleted = false;
  }
}
