import { Component, OnInit } from '@angular/core';
import { ShoppingCatalogSheld } from './shopping-catalog-sheld.model';
import { ShoppingCatalogSheldService } from './shopping-catalog-sheld.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'page-shopping-catalog-sheld-detail',
  templateUrl: 'shopping-catalog-sheld-detail.html',
})
export class ShoppingCatalogSheldDetailPage implements OnInit {
  shoppingCatalogSheld: ShoppingCatalogSheld = {};

  constructor(
    private navController: NavController,
    private shoppingCatalogSheldService: ShoppingCatalogSheldService,
    private activatedRoute: ActivatedRoute,
    private alertController: AlertController
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((response) => {
      this.shoppingCatalogSheld = response.data;
    });
  }

  open(item: ShoppingCatalogSheld) {
    this.navController.navigateForward('/tabs/entities/shopping-catalog-sheld/' + item.id + '/edit');
  }

  async deleteModal(item: ShoppingCatalogSheld) {
    const alert = await this.alertController.create({
      header: 'Confirm the deletion?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Delete',
          handler: () => {
            this.shoppingCatalogSheldService.delete(item.id).subscribe(() => {
              this.navController.navigateForward('/tabs/entities/shopping-catalog-sheld');
            });
          },
        },
      ],
    });
    await alert.present();
  }
}
