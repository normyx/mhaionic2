import { NgModule, Injectable } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserRouteAccessService } from '../../../services/auth/user-route-access.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

import { ShoppingCatalogSheldPage } from './shopping-catalog-sheld';
import { ShoppingCatalogSheldUpdatePage } from './shopping-catalog-sheld-update';
import { ShoppingCatalogSheld, ShoppingCatalogSheldService, ShoppingCatalogSheldDetailPage } from '.';

@Injectable({ providedIn: 'root' })
export class ShoppingCatalogSheldResolve implements Resolve<ShoppingCatalogSheld> {
  constructor(private service: ShoppingCatalogSheldService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ShoppingCatalogSheld> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ShoppingCatalogSheld>) => response.ok),
        map((shoppingCatalogSheld: HttpResponse<ShoppingCatalogSheld>) => shoppingCatalogSheld.body)
      );
    }
    return of(new ShoppingCatalogSheld());
  }
}

const routes: Routes = [
  {
    path: '',
    component: ShoppingCatalogSheldPage,
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ShoppingCatalogSheldUpdatePage,
    resolve: {
      data: ShoppingCatalogSheldResolve,
    },
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ShoppingCatalogSheldDetailPage,
    resolve: {
      data: ShoppingCatalogSheldResolve,
    },
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ShoppingCatalogSheldUpdatePage,
    resolve: {
      data: ShoppingCatalogSheldResolve,
    },
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  declarations: [ShoppingCatalogSheldPage, ShoppingCatalogSheldUpdatePage, ShoppingCatalogSheldDetailPage],
  imports: [IonicModule, FormsModule, ReactiveFormsModule, CommonModule, TranslateModule, RouterModule.forChild(routes)],
})
export class ShoppingCatalogSheldPageModule {}
