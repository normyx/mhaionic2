import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { ShoppingCatalogSheld } from './shopping-catalog-sheld.model';

@Injectable({ providedIn: 'root' })
export class ShoppingCatalogSheldService {
  private resourceUrl = ApiService.API_URL + '/shopping-catalog-shelds';

  constructor(protected http: HttpClient) {}

  create(shoppingCatalogSheld: ShoppingCatalogSheld): Observable<HttpResponse<ShoppingCatalogSheld>> {
    return this.http.post<ShoppingCatalogSheld>(this.resourceUrl, shoppingCatalogSheld, { observe: 'response' });
  }

  update(shoppingCatalogSheld: ShoppingCatalogSheld): Observable<HttpResponse<ShoppingCatalogSheld>> {
    return this.http.put(this.resourceUrl, shoppingCatalogSheld, { observe: 'response' });
  }

  find(id: number): Observable<HttpResponse<ShoppingCatalogSheld>> {
    return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<HttpResponse<ShoppingCatalogSheld[]>> {
    const options = createRequestOption(req);
    return this.http.get<ShoppingCatalogSheld[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
