import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ShoppingCatalogSheld } from './shopping-catalog-sheld.model';
import { ShoppingCatalogSheldService } from './shopping-catalog-sheld.service';
import { ShoppingCatalogCart, ShoppingCatalogCartService } from '../shopping-catalog-cart';

@Component({
  selector: 'page-shopping-catalog-sheld-update',
  templateUrl: 'shopping-catalog-sheld-update.html',
})
export class ShoppingCatalogSheldUpdatePage implements OnInit {
  shoppingCatalogSheld: ShoppingCatalogSheld;
  shoppingCatalogCarts: ShoppingCatalogCart[];
  lastUpdateDp: any;
  isSaving = false;
  isNew = true;
  isReadyToSave: boolean;

  form = this.formBuilder.group({
    id: [],
    label: [null, [Validators.required]],
    lastUpdate: [null, [Validators.required]],
    isDefault: ['false', [Validators.required]],
    isDeleted: ['false', [Validators.required]],
    shoppingCatalogCartId: [null, [Validators.required]],
  });

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected navController: NavController,
    protected formBuilder: FormBuilder,
    public platform: Platform,
    protected toastCtrl: ToastController,
    private shoppingCatalogCartService: ShoppingCatalogCartService,
    private shoppingCatalogSheldService: ShoppingCatalogSheldService
  ) {
    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  ngOnInit() {
    this.shoppingCatalogCartService.query().subscribe(
      (data) => {
        this.shoppingCatalogCarts = data.body;
      },
      (error) => this.onError(error)
    );
    this.activatedRoute.data.subscribe((response) => {
      this.shoppingCatalogSheld = response.data;
      this.isNew = this.shoppingCatalogSheld.id === null || this.shoppingCatalogSheld.id === undefined;
      this.updateForm(this.shoppingCatalogSheld);
    });
  }

  updateForm(shoppingCatalogSheld: ShoppingCatalogSheld) {
    this.form.patchValue({
      id: shoppingCatalogSheld.id,
      label: shoppingCatalogSheld.label,
      lastUpdate: this.isNew ? new Date().toISOString().split('T')[0] : shoppingCatalogSheld.lastUpdate,
      isDefault: shoppingCatalogSheld.isDefault,
      isDeleted: shoppingCatalogSheld.isDeleted,
      shoppingCatalogCartId: shoppingCatalogSheld.shoppingCatalogCartId,
    });
  }

  save() {
    this.isSaving = true;
    const shoppingCatalogSheld = this.createFromForm();
    shoppingCatalogSheld.lastUpdate = new Date(shoppingCatalogSheld.lastUpdate).toISOString().split('T')[0];
    if (!this.isNew) {
      this.subscribeToSaveResponse(this.shoppingCatalogSheldService.update(shoppingCatalogSheld));
    } else {
      this.subscribeToSaveResponse(this.shoppingCatalogSheldService.create(shoppingCatalogSheld));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ShoppingCatalogSheld>>) {
    result.subscribe(
      (res: HttpResponse<ShoppingCatalogSheld>) => this.onSaveSuccess(res),
      (res: HttpErrorResponse) => this.onError(res.error)
    );
  }

  async onSaveSuccess(response) {
    let action = 'updated';
    if (response.status === 201) {
      action = 'created';
    }
    this.isSaving = false;
    const toast = await this.toastCtrl.create({
      message: `ShoppingCatalogSheld ${action} successfully.`,
      duration: 2000,
      position: 'middle',
    });
    toast.present();
    this.navController.navigateBack('/tabs/entities/shopping-catalog-sheld');
  }

  previousState() {
    window.history.back();
  }

  async onError(error) {
    this.isSaving = false;
    console.error(error);
    const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
    toast.present();
  }

  private createFromForm(): ShoppingCatalogSheld {
    return {
      ...new ShoppingCatalogSheld(),
      id: this.form.get(['id']).value,
      label: this.form.get(['label']).value,
      lastUpdate: this.form.get(['lastUpdate']).value,
      isDefault: this.form.get(['isDefault']).value,
      isDeleted: this.form.get(['isDeleted']).value,
      shoppingCatalogCartId: this.form.get(['shoppingCatalogCartId']).value,
    };
  }

  compareShoppingCatalogCart(first: ShoppingCatalogCart, second: ShoppingCatalogCart): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackShoppingCatalogCartById(index: number, item: ShoppingCatalogCart) {
    return item.id;
  }
}
