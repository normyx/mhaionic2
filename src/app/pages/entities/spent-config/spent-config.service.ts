import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { SpentConfig } from './spent-config.model';

@Injectable({ providedIn: 'root' })
export class SpentConfigService {
  private resourceUrl = ApiService.API_URL + '/spent-configs';

  constructor(protected http: HttpClient) {}

  create(spentConfig: SpentConfig): Observable<HttpResponse<SpentConfig>> {
    return this.http.post<SpentConfig>(this.resourceUrl, spentConfig, { observe: 'response' });
  }

  update(spentConfig: SpentConfig): Observable<HttpResponse<SpentConfig>> {
    return this.http.put(this.resourceUrl, spentConfig, { observe: 'response' });
  }

  find(id: number): Observable<HttpResponse<SpentConfig>> {
    return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<HttpResponse<SpentConfig[]>> {
    const options = createRequestOption(req);
    return this.http.get<SpentConfig[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
