import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { SpentConfig } from './spent-config.model';
import { SpentConfigService } from './spent-config.service';

@Component({
  selector: 'page-spent-config',
  templateUrl: 'spent-config.html',
})
export class SpentConfigPage {
  spentConfigs: SpentConfig[];

  // todo: add pagination

  constructor(
    private navController: NavController,
    private spentConfigService: SpentConfigService,
    private toastCtrl: ToastController,
    public plt: Platform
  ) {
    this.spentConfigs = [];
  }

  ionViewWillEnter() {
    this.loadAll();
  }

  async loadAll(refresher?) {
    this.spentConfigService
      .query()
      .pipe(
        filter((res: HttpResponse<SpentConfig[]>) => res.ok),
        map((res: HttpResponse<SpentConfig[]>) => res.body)
      )
      .subscribe(
        (response: SpentConfig[]) => {
          this.spentConfigs = response;
          if (typeof refresher !== 'undefined') {
            setTimeout(() => {
              refresher.target.complete();
            }, 750);
          }
        },
        async (error) => {
          console.error(error);
          const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
          toast.present();
        }
      );
  }

  trackId(index: number, item: SpentConfig) {
    return item.id;
  }

  new() {
    this.navController.navigateForward('/tabs/entities/spent-config/new');
  }

  edit(item: IonItemSliding, spentConfig: SpentConfig) {
    this.navController.navigateForward('/tabs/entities/spent-config/' + spentConfig.id + '/edit');
    item.close();
  }

  async delete(spentConfig) {
    this.spentConfigService.delete(spentConfig.id).subscribe(
      async () => {
        const toast = await this.toastCtrl.create({ message: 'SpentConfig deleted successfully.', duration: 3000, position: 'middle' });
        toast.present();
        this.loadAll();
      },
      (error) => console.error(error)
    );
  }

  view(spentConfig: SpentConfig) {
    this.navController.navigateForward('/tabs/entities/spent-config/' + spentConfig.id + '/view');
  }
}
