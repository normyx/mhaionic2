import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { SpentConfig } from './spent-config.model';
import { SpentConfigService } from './spent-config.service';
import { Wallet, WalletService } from '../wallet';

@Component({
  selector: 'page-spent-config-update',
  templateUrl: 'spent-config-update.html',
})
export class SpentConfigUpdatePage implements OnInit {
  spentConfig: SpentConfig;
  wallets: Wallet[];
  lastUpdateDp: any;
  isSaving = false;
  isNew = true;
  isReadyToSave: boolean;

  form = this.formBuilder.group({
    id: [],
    label: [null, [Validators.required]],
    amount: [null, []],
    spentLabel: [null, []],
    lastUpdate: [null, [Validators.required]],
    walletId: [null, [Validators.required]],
  });

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected navController: NavController,
    protected formBuilder: FormBuilder,
    public platform: Platform,
    protected toastCtrl: ToastController,
    private walletService: WalletService,
    private spentConfigService: SpentConfigService
  ) {
    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  ngOnInit() {
    this.walletService.query().subscribe(
      (data) => {
        this.wallets = data.body;
      },
      (error) => this.onError(error)
    );
    this.activatedRoute.data.subscribe((response) => {
      this.spentConfig = response.data;
      this.isNew = this.spentConfig.id === null || this.spentConfig.id === undefined;
      this.updateForm(this.spentConfig);
    });
  }

  updateForm(spentConfig: SpentConfig) {
    this.form.patchValue({
      id: spentConfig.id,
      label: spentConfig.label,
      amount: spentConfig.amount,
      spentLabel: spentConfig.spentLabel,
      lastUpdate: this.isNew ? new Date().toISOString().split('T')[0] : spentConfig.lastUpdate,
      walletId: spentConfig.walletId,
    });
  }

  save() {
    this.isSaving = true;
    const spentConfig = this.createFromForm();
    spentConfig.lastUpdate = new Date(spentConfig.lastUpdate).toISOString().split('T')[0];
    if (!this.isNew) {
      this.subscribeToSaveResponse(this.spentConfigService.update(spentConfig));
    } else {
      this.subscribeToSaveResponse(this.spentConfigService.create(spentConfig));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<SpentConfig>>) {
    result.subscribe(
      (res: HttpResponse<SpentConfig>) => this.onSaveSuccess(res),
      (res: HttpErrorResponse) => this.onError(res.error)
    );
  }

  async onSaveSuccess(response) {
    let action = 'updated';
    if (response.status === 201) {
      action = 'created';
    }
    this.isSaving = false;
    const toast = await this.toastCtrl.create({ message: `SpentConfig ${action} successfully.`, duration: 2000, position: 'middle' });
    toast.present();
    this.navController.navigateBack('/tabs/entities/spent-config');
  }

  previousState() {
    window.history.back();
  }

  async onError(error) {
    this.isSaving = false;
    console.error(error);
    const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
    toast.present();
  }

  private createFromForm(): SpentConfig {
    return {
      ...new SpentConfig(),
      id: this.form.get(['id']).value,
      label: this.form.get(['label']).value,
      amount: this.form.get(['amount']).value,
      spentLabel: this.form.get(['spentLabel']).value,
      lastUpdate: this.form.get(['lastUpdate']).value,
      walletId: this.form.get(['walletId']).value,
    };
  }

  compareWallet(first: Wallet, second: Wallet): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackWalletById(index: number, item: Wallet) {
    return item.id;
  }
}
