import { BaseEntity, SyncStatus } from 'src/model/base-entity';
import { ShoppingItem } from '../shopping-item/shopping-item.model';
import { Profil } from '../profil/profil.model';

export class ShoppingCart implements BaseEntity {
  constructor(
    public id?: number,
    public label?: string,
    public lastUpdate?: any,
    public shoppingItems?: ShoppingItem[],
    public shoppingCatalogCartLabel?: string,
    public shoppingCatalogCartId?: number,
    public owners?: Profil[],
    public workspaceLabel?: string,
    public workspaceId?: number, public syncStatus?: SyncStatus
  ) {}
}
