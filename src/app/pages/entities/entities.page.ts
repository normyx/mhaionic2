import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-entities',
  templateUrl: 'entities.page.html',
  styleUrls: ['entities.page.scss'],
})
export class EntitiesPage {
  entities: Array<any> = [
    { name: 'Profil', component: 'ProfilPage', route: 'profil' },
    { name: 'ProfilData', component: 'ProfilDataPage', route: 'profil-data' },
    { name: 'ShoppingCart', component: 'ShoppingCartPage', route: 'shopping-cart' },
    { name: 'ShoppingCatalogCart', component: 'ShoppingCatalogCartPage', route: 'shopping-catalog-cart' },
    { name: 'ShoppingCatalogItem', component: 'ShoppingCatalogItemPage', route: 'shopping-catalog-item' },
    { name: 'ShoppingCatalogSheld', component: 'ShoppingCatalogSheldPage', route: 'shopping-catalog-sheld' },
    { name: 'ShoppingItem', component: 'ShoppingItemPage', route: 'shopping-item' },
    { name: 'Spent', component: 'SpentPage', route: 'spent' },
    { name: 'SpentConfig', component: 'SpentConfigPage', route: 'spent-config' },
    { name: 'SpentSharing', component: 'SpentSharingPage', route: 'spent-sharing' },
    { name: 'SpentSharingConfig', component: 'SpentSharingConfigPage', route: 'spent-sharing-config' },
    { name: 'Task', component: 'TaskPage', route: 'task' },
    { name: 'TaskProject', component: 'TaskProjectPage', route: 'task-project' },
    { name: 'Todo', component: 'TodoPage', route: 'todo' },
    { name: 'TodoList', component: 'TodoListPage', route: 'todo-list' },
    { name: 'TodoListTemplate', component: 'TodoListTemplatePage', route: 'todo-list-template' },
    { name: 'TodoTemplate', component: 'TodoTemplatePage', route: 'todo-template' },
    { name: 'Wallet', component: 'WalletPage', route: 'wallet' },
    { name: 'Workspace', component: 'WorkspacePage', route: 'workspace' },
    /* jhipster-needle-add-entity-page - JHipster will add entity pages here */
  ];

  constructor(public navController: NavController) {}

  openPage(page) {
    this.navController.navigateForward('/tabs/entities/' + page.route);
  }
}
