import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { ProfilData } from './profil-data.model';

@Injectable({ providedIn: 'root' })
export class ProfilDataService {
  private resourceUrl = ApiService.API_URL + '/profil-data';

  constructor(protected http: HttpClient) {}

  create(profilData: ProfilData): Observable<HttpResponse<ProfilData>> {
    return this.http.post<ProfilData>(this.resourceUrl, profilData, { observe: 'response' });
  }

  update(profilData: ProfilData): Observable<HttpResponse<ProfilData>> {
    return this.http.put(this.resourceUrl, profilData, { observe: 'response' });
  }

  find(id: number): Observable<HttpResponse<ProfilData>> {
    return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<HttpResponse<ProfilData[]>> {
    const options = createRequestOption(req);
    return this.http.get<ProfilData[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
