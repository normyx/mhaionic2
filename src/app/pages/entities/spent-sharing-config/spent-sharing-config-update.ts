import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { SpentSharingConfig } from './spent-sharing-config.model';
import { SpentSharingConfigService } from './spent-sharing-config.service';
import { SpentConfig, SpentConfigService } from '../spent-config';
import { Profil, ProfilService } from '../profil';

@Component({
  selector: 'page-spent-sharing-config-update',
  templateUrl: 'spent-sharing-config-update.html',
})
export class SpentSharingConfigUpdatePage implements OnInit {
  spentSharingConfig: SpentSharingConfig;
  spentConfigs: SpentConfig[];
  profils: Profil[];
  lastUpdateDp: any;
  isSaving = false;
  isNew = true;
  isReadyToSave: boolean;

  form = this.formBuilder.group({
    id: [],
    share: [null, [Validators.required]],
    lastUpdate: [null, [Validators.required]],
    spentConfigId: [null, [Validators.required]],
    profilId: [null, [Validators.required]],
  });

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected navController: NavController,
    protected formBuilder: FormBuilder,
    public platform: Platform,
    protected toastCtrl: ToastController,
    private spentConfigService: SpentConfigService,
    private profilService: ProfilService,
    private spentSharingConfigService: SpentSharingConfigService
  ) {
    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  ngOnInit() {
    this.spentConfigService.query().subscribe(
      (data) => {
        this.spentConfigs = data.body;
      },
      (error) => this.onError(error)
    );
    this.profilService.query().subscribe(
      (data) => {
        this.profils = data.body;
      },
      (error) => this.onError(error)
    );
    this.activatedRoute.data.subscribe((response) => {
      this.spentSharingConfig = response.data;
      this.isNew = this.spentSharingConfig.id === null || this.spentSharingConfig.id === undefined;
      this.updateForm(this.spentSharingConfig);
    });
  }

  updateForm(spentSharingConfig: SpentSharingConfig) {
    this.form.patchValue({
      id: spentSharingConfig.id,
      share: spentSharingConfig.share,
      lastUpdate: this.isNew ? new Date().toISOString().split('T')[0] : spentSharingConfig.lastUpdate,
      spentConfigId: spentSharingConfig.spentConfigId,
      profilId: spentSharingConfig.profilId,
    });
  }

  save() {
    this.isSaving = true;
    const spentSharingConfig = this.createFromForm();
    spentSharingConfig.lastUpdate = new Date(spentSharingConfig.lastUpdate).toISOString().split('T')[0];
    if (!this.isNew) {
      this.subscribeToSaveResponse(this.spentSharingConfigService.update(spentSharingConfig));
    } else {
      this.subscribeToSaveResponse(this.spentSharingConfigService.create(spentSharingConfig));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<SpentSharingConfig>>) {
    result.subscribe(
      (res: HttpResponse<SpentSharingConfig>) => this.onSaveSuccess(res),
      (res: HttpErrorResponse) => this.onError(res.error)
    );
  }

  async onSaveSuccess(response) {
    let action = 'updated';
    if (response.status === 201) {
      action = 'created';
    }
    this.isSaving = false;
    const toast = await this.toastCtrl.create({
      message: `SpentSharingConfig ${action} successfully.`,
      duration: 2000,
      position: 'middle',
    });
    toast.present();
    this.navController.navigateBack('/tabs/entities/spent-sharing-config');
  }

  previousState() {
    window.history.back();
  }

  async onError(error) {
    this.isSaving = false;
    console.error(error);
    const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
    toast.present();
  }

  private createFromForm(): SpentSharingConfig {
    return {
      ...new SpentSharingConfig(),
      id: this.form.get(['id']).value,
      share: this.form.get(['share']).value,
      lastUpdate: this.form.get(['lastUpdate']).value,
      spentConfigId: this.form.get(['spentConfigId']).value,
      profilId: this.form.get(['profilId']).value,
    };
  }

  compareSpentConfig(first: SpentConfig, second: SpentConfig): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackSpentConfigById(index: number, item: SpentConfig) {
    return item.id;
  }
  compareProfil(first: Profil, second: Profil): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackProfilById(index: number, item: Profil) {
    return item.id;
  }
}
