import { BaseEntity, SyncStatus } from 'src/model/base-entity';

export class SpentSharingConfig implements BaseEntity {
  constructor(
    public id?: number,
    public share?: number,
    public lastUpdate?: any,
    public spentConfigLabel?: string,
    public spentConfigId?: number,
    public profilDisplayName?: string,
    public profilId?: number, public syncStatus?: SyncStatus
  ) {}
}
