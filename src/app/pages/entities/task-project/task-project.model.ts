import { BaseEntity, SyncStatus } from 'src/model/base-entity';
import { Task } from '../task/task.model';
import { Profil } from '../profil/profil.model';

export class TaskProject implements BaseEntity {
  constructor(
    public id?: number,
    public label?: string,
    public lastUpdate?: any,
    public tasks?: Task[],
    public owners?: Profil[],
    public workspaceLabel?: string,
    public workspaceId?: number, public syncStatus?: SyncStatus
  ) {}
}
