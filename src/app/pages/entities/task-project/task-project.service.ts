import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { TaskProject } from './task-project.model';

@Injectable({ providedIn: 'root' })
export class TaskProjectService {
  private resourceUrl = ApiService.API_URL + '/task-projects';

  constructor(protected http: HttpClient) {}

  create(taskProject: TaskProject): Observable<HttpResponse<TaskProject>> {
    return this.http.post<TaskProject>(this.resourceUrl, taskProject, { observe: 'response' });
  }

  update(taskProject: TaskProject): Observable<HttpResponse<TaskProject>> {
    return this.http.put(this.resourceUrl, taskProject, { observe: 'response' });
  }

  find(id: number): Observable<HttpResponse<TaskProject>> {
    return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<HttpResponse<TaskProject[]>> {
    const options = createRequestOption(req);
    return this.http.get<TaskProject[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
