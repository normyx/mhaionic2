import { NgModule, Injectable } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserRouteAccessService } from '../../../services/auth/user-route-access.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

import { TaskProjectPage } from './task-project';
import { TaskProjectUpdatePage } from './task-project-update';
import { TaskProject, TaskProjectService, TaskProjectDetailPage } from '.';

@Injectable({ providedIn: 'root' })
export class TaskProjectResolve implements Resolve<TaskProject> {
  constructor(private service: TaskProjectService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TaskProject> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<TaskProject>) => response.ok),
        map((taskProject: HttpResponse<TaskProject>) => taskProject.body)
      );
    }
    return of(new TaskProject());
  }
}

const routes: Routes = [
  {
    path: '',
    component: TaskProjectPage,
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: TaskProjectUpdatePage,
    resolve: {
      data: TaskProjectResolve,
    },
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TaskProjectDetailPage,
    resolve: {
      data: TaskProjectResolve,
    },
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: TaskProjectUpdatePage,
    resolve: {
      data: TaskProjectResolve,
    },
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  declarations: [TaskProjectPage, TaskProjectUpdatePage, TaskProjectDetailPage],
  imports: [IonicModule, FormsModule, ReactiveFormsModule, CommonModule, TranslateModule, RouterModule.forChild(routes)],
})
export class TaskProjectPageModule {}
