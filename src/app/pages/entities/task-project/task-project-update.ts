import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { TaskProject } from './task-project.model';
import { TaskProjectService } from './task-project.service';
import { Profil, ProfilService } from '../profil';
import { Workspace, WorkspaceService } from '../workspace';

@Component({
  selector: 'page-task-project-update',
  templateUrl: 'task-project-update.html',
})
export class TaskProjectUpdatePage implements OnInit {
  taskProject: TaskProject;
  profils: Profil[];
  workspaces: Workspace[];
  lastUpdateDp: any;
  isSaving = false;
  isNew = true;
  isReadyToSave: boolean;

  form = this.formBuilder.group({
    id: [],
    label: [null, [Validators.required]],
    lastUpdate: [null, [Validators.required]],
    owners: [null, []],
    workspaceId: [null, []],
  });

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected navController: NavController,
    protected formBuilder: FormBuilder,
    public platform: Platform,
    protected toastCtrl: ToastController,
    private profilService: ProfilService,
    private workspaceService: WorkspaceService,
    private taskProjectService: TaskProjectService
  ) {
    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  ngOnInit() {
    this.profilService.query().subscribe(
      (data) => {
        this.profils = data.body;
      },
      (error) => this.onError(error)
    );
    this.workspaceService.query().subscribe(
      (data) => {
        this.workspaces = data.body;
      },
      (error) => this.onError(error)
    );
    this.activatedRoute.data.subscribe((response) => {
      this.taskProject = response.data;
      this.isNew = this.taskProject.id === null || this.taskProject.id === undefined;
      this.updateForm(this.taskProject);
    });
  }

  updateForm(taskProject: TaskProject) {
    this.form.patchValue({
      id: taskProject.id,
      label: taskProject.label,
      lastUpdate: this.isNew ? new Date().toISOString().split('T')[0] : taskProject.lastUpdate,
      owners: taskProject.owners,
      workspaceId: taskProject.workspaceId,
    });
  }

  save() {
    this.isSaving = true;
    const taskProject = this.createFromForm();
    taskProject.lastUpdate = new Date(taskProject.lastUpdate).toISOString().split('T')[0];
    if (!this.isNew) {
      this.subscribeToSaveResponse(this.taskProjectService.update(taskProject));
    } else {
      this.subscribeToSaveResponse(this.taskProjectService.create(taskProject));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<TaskProject>>) {
    result.subscribe(
      (res: HttpResponse<TaskProject>) => this.onSaveSuccess(res),
      (res: HttpErrorResponse) => this.onError(res.error)
    );
  }

  async onSaveSuccess(response) {
    let action = 'updated';
    if (response.status === 201) {
      action = 'created';
    }
    this.isSaving = false;
    const toast = await this.toastCtrl.create({ message: `TaskProject ${action} successfully.`, duration: 2000, position: 'middle' });
    toast.present();
    this.navController.navigateBack('/tabs/entities/task-project');
  }

  previousState() {
    window.history.back();
  }

  async onError(error) {
    this.isSaving = false;
    console.error(error);
    const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
    toast.present();
  }

  private createFromForm(): TaskProject {
    return {
      ...new TaskProject(),
      id: this.form.get(['id']).value,
      label: this.form.get(['label']).value,
      lastUpdate: this.form.get(['lastUpdate']).value,
      owners: this.form.get(['owners']).value,
      workspaceId: this.form.get(['workspaceId']).value,
    };
  }

  compareProfil(first: Profil, second: Profil): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackProfilById(index: number, item: Profil) {
    return item.id;
  }
  compareWorkspace(first: Workspace, second: Workspace): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackWorkspaceById(index: number, item: Workspace) {
    return item.id;
  }
}
