import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Profil } from './profil.model';
import { ProfilService } from './profil.service';
import { User } from '../../../services/user/user.model';
import { UserService } from '../../../services/user/user.service';
import { ProfilData, ProfilDataService } from '../profil-data';
import { ShoppingCart, ShoppingCartService } from '../shopping-cart';
import { Task, TaskService } from '../task';
import { TaskProject, TaskProjectService } from '../task-project';
import { TodoList, TodoListService } from '../todo-list';
import { TodoListTemplate, TodoListTemplateService } from '../todo-list-template';
import { Wallet, WalletService } from '../wallet';
import { Workspace, WorkspaceService } from '../workspace';

@Component({
  selector: 'page-profil-update',
  templateUrl: 'profil-update.html',
})
export class ProfilUpdatePage implements OnInit {
  profil: Profil;
  users: User[];
  profilData: ProfilData[];
  shoppingCarts: ShoppingCart[];
  tasks: Task[];
  taskProjects: TaskProject[];
  todoLists: TodoList[];
  todoListTemplates: TodoListTemplate[];
  wallets: Wallet[];
  workspaces: Workspace[];
  lastUpdateDp: any;
  isSaving = false;
  isNew = true;
  isReadyToSave: boolean;

  form = this.formBuilder.group({
    id: [],
    displayName: [null, [Validators.required]],
    lastUpdate: [null, [Validators.required]],
    userId: [null, []],
    profilDataId: [null, []],
  });

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected navController: NavController,
    protected formBuilder: FormBuilder,
    public platform: Platform,
    protected toastCtrl: ToastController,
    private userService: UserService,
    private profilDataService: ProfilDataService,
    private shoppingCartService: ShoppingCartService,
    private taskService: TaskService,
    private taskProjectService: TaskProjectService,
    private todoListService: TodoListService,
    private todoListTemplateService: TodoListTemplateService,
    private walletService: WalletService,
    private workspaceService: WorkspaceService,
    private profilService: ProfilService
  ) {
    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  ngOnInit() {
    this.userService.findAll().subscribe(
      (data) => (this.users = data),
      (error) => this.onError(error)
    );
    this.profilDataService.query({ filter: 'profil-is-null' }).subscribe(
      (data) => {
        if (!this.profil.profilDataId) {
          this.profilData = data.body;
        } else {
          this.profilDataService.find(this.profil.profilDataId).subscribe(
            (subData: HttpResponse<ProfilData>) => {
              this.profilData = [subData.body].concat(subData.body);
            },
            (error) => this.onError(error)
          );
        }
      },
      (error) => this.onError(error)
    );
    this.shoppingCartService.query().subscribe(
      (data) => {
        this.shoppingCarts = data.body;
      },
      (error) => this.onError(error)
    );
    this.taskService.query().subscribe(
      (data) => {
        this.tasks = data.body;
      },
      (error) => this.onError(error)
    );
    this.taskProjectService.query().subscribe(
      (data) => {
        this.taskProjects = data.body;
      },
      (error) => this.onError(error)
    );
    this.todoListService.query().subscribe(
      (data) => {
        this.todoLists = data.body;
      },
      (error) => this.onError(error)
    );
    this.todoListTemplateService.query().subscribe(
      (data) => {
        this.todoListTemplates = data.body;
      },
      (error) => this.onError(error)
    );
    this.walletService.query().subscribe(
      (data) => {
        this.wallets = data.body;
      },
      (error) => this.onError(error)
    );
    this.workspaceService.query().subscribe(
      (data) => {
        this.workspaces = data.body;
      },
      (error) => this.onError(error)
    );
    this.activatedRoute.data.subscribe((response) => {
      this.profil = response.data;
      this.isNew = this.profil.id === null || this.profil.id === undefined;
      this.updateForm(this.profil);
    });
  }

  updateForm(profil: Profil) {
    this.form.patchValue({
      id: profil.id,
      displayName: profil.displayName,
      lastUpdate: this.isNew ? new Date().toISOString().split('T')[0] : profil.lastUpdate,
      userId: profil.userId,
      profilDataId: profil.profilDataId,
    });
  }

  save() {
    this.isSaving = true;
    const profil = this.createFromForm();
    profil.lastUpdate = new Date(profil.lastUpdate).toISOString().split('T')[0];
    if (!this.isNew) {
      this.subscribeToSaveResponse(this.profilService.update(profil));
    } else {
      this.subscribeToSaveResponse(this.profilService.create(profil));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<Profil>>) {
    result.subscribe(
      (res: HttpResponse<Profil>) => this.onSaveSuccess(res),
      (res: HttpErrorResponse) => this.onError(res.error)
    );
  }

  async onSaveSuccess(response) {
    let action = 'updated';
    if (response.status === 201) {
      action = 'created';
    }
    this.isSaving = false;
    const toast = await this.toastCtrl.create({ message: `Profil ${action} successfully.`, duration: 2000, position: 'middle' });
    toast.present();
    this.navController.navigateBack('/tabs/entities/profil');
  }

  previousState() {
    window.history.back();
  }

  async onError(error) {
    this.isSaving = false;
    console.error(error);
    const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
    toast.present();
  }

  private createFromForm(): Profil {
    return {
      ...new Profil(),
      id: this.form.get(['id']).value,
      displayName: this.form.get(['displayName']).value,
      lastUpdate: this.form.get(['lastUpdate']).value,
      userId: this.form.get(['userId']).value,
      profilDataId: this.form.get(['profilDataId']).value,
    };
  }

  compareUser(first: User, second: User): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackUserById(index: number, item: User) {
    return item.id;
  }
  compareProfilData(first: ProfilData, second: ProfilData): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackProfilDataById(index: number, item: ProfilData) {
    return item.id;
  }
  compareShoppingCart(first: ShoppingCart, second: ShoppingCart): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackShoppingCartById(index: number, item: ShoppingCart) {
    return item.id;
  }
  compareTask(first: Task, second: Task): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackTaskById(index: number, item: Task) {
    return item.id;
  }
  compareTaskProject(first: TaskProject, second: TaskProject): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackTaskProjectById(index: number, item: TaskProject) {
    return item.id;
  }
  compareTodoList(first: TodoList, second: TodoList): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackTodoListById(index: number, item: TodoList) {
    return item.id;
  }
  compareTodoListTemplate(first: TodoListTemplate, second: TodoListTemplate): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackTodoListTemplateById(index: number, item: TodoListTemplate) {
    return item.id;
  }
  compareWallet(first: Wallet, second: Wallet): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackWalletById(index: number, item: Wallet) {
    return item.id;
  }
  compareWorkspace(first: Workspace, second: Workspace): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackWorkspaceById(index: number, item: Workspace) {
    return item.id;
  }
}
