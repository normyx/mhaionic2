import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { Profil } from './profil.model';
import { ProfilService } from './profil.service';

@Component({
  selector: 'page-profil',
  templateUrl: 'profil.html',
})
export class ProfilPage {
  profils: Profil[];

  // todo: add pagination

  constructor(
    private navController: NavController,
    private profilService: ProfilService,
    private toastCtrl: ToastController,
    public plt: Platform
  ) {
    this.profils = [];
  }

  ionViewWillEnter() {
    this.loadAll();
  }

  async loadAll(refresher?) {
    this.profilService
      .query()
      .pipe(
        filter((res: HttpResponse<Profil[]>) => res.ok),
        map((res: HttpResponse<Profil[]>) => res.body)
      )
      .subscribe(
        (response: Profil[]) => {
          this.profils = response;
          if (typeof refresher !== 'undefined') {
            setTimeout(() => {
              refresher.target.complete();
            }, 750);
          }
        },
        async (error) => {
          console.error(error);
          const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
          toast.present();
        }
      );
  }

  trackId(index: number, item: Profil) {
    return item.id;
  }

  new() {
    this.navController.navigateForward('/tabs/entities/profil/new');
  }

  edit(item: IonItemSliding, profil: Profil) {
    this.navController.navigateForward('/tabs/entities/profil/' + profil.id + '/edit');
    item.close();
  }

  async delete(profil) {
    this.profilService.delete(profil.id).subscribe(
      async () => {
        const toast = await this.toastCtrl.create({ message: 'Profil deleted successfully.', duration: 3000, position: 'middle' });
        toast.present();
        this.loadAll();
      },
      (error) => console.error(error)
    );
  }

  view(profil: Profil) {
    this.navController.navigateForward('/tabs/entities/profil/' + profil.id + '/view');
  }
}
