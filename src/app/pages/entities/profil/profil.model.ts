import { BaseEntity, SyncStatus } from 'src/model/base-entity';
import { Spent } from '../spent/spent.model';
import { ShoppingCart } from '../shopping-cart/shopping-cart.model';
import { Task } from '../task/task.model';
import { TaskProject } from '../task-project/task-project.model';
import { TodoList } from '../todo-list/todo-list.model';
import { TodoListTemplate } from '../todo-list-template/todo-list-template.model';
import { Wallet } from '../wallet/wallet.model';
import { Workspace } from '../workspace/workspace.model';

export class Profil implements BaseEntity {
  constructor(
    public id?: number,
    public displayName?: string,
    public lastUpdate?: any,
    public userLogin?: string,
    public userId?: number,
    public profilDataId?: number,
    public spents?: Spent[],
    public shoppingCarts?: ShoppingCart[],
    public tasks?: Task[],
    public taskProjects?: TaskProject[],
    public todoLists?: TodoList[],
    public todoListTemplates?: TodoListTemplate[],
    public wallets?: Wallet[],
    public workspaces?: Workspace[],
    public syncStatus?: SyncStatus
  ) {}
}
