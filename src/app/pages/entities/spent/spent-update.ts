import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Spent } from './spent.model';
import { SpentService } from './spent.service';
import { Wallet, WalletService } from '../wallet';
import { Profil, ProfilService } from '../profil';

@Component({
  selector: 'page-spent-update',
  templateUrl: 'spent-update.html',
})
export class SpentUpdatePage implements OnInit {
  spent: Spent;
  wallets: Wallet[];
  profils: Profil[];
  spentDateDp: any;
  lastUpdateDp: any;
  isSaving = false;
  isNew = true;
  isReadyToSave: boolean;

  form = this.formBuilder.group({
    id: [],
    label: [null, [Validators.required]],
    description: [null, []],
    amount: [null, [Validators.required]],
    spentDate: [null, [Validators.required]],
    confirmed: ['false', [Validators.required]],
    lastUpdate: [null, [Validators.required]],
    walletId: [null, []],
    spenderId: [null, []],
  });

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected navController: NavController,
    protected formBuilder: FormBuilder,
    public platform: Platform,
    protected toastCtrl: ToastController,
    private walletService: WalletService,
    private profilService: ProfilService,
    private spentService: SpentService
  ) {
    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  ngOnInit() {
    this.walletService.query().subscribe(
      (data) => {
        this.wallets = data.body;
      },
      (error) => this.onError(error)
    );
    this.profilService.query().subscribe(
      (data) => {
        this.profils = data.body;
      },
      (error) => this.onError(error)
    );
    this.activatedRoute.data.subscribe((response) => {
      this.spent = response.data;
      this.isNew = this.spent.id === null || this.spent.id === undefined;
      this.updateForm(this.spent);
    });
  }

  updateForm(spent: Spent) {
    this.form.patchValue({
      id: spent.id,
      label: spent.label,
      description: spent.description,
      amount: spent.amount,
      spentDate: this.isNew ? new Date().toISOString().split('T')[0] : spent.spentDate,
      confirmed: spent.confirmed,
      lastUpdate: this.isNew ? new Date().toISOString().split('T')[0] : spent.lastUpdate,
      walletId: spent.walletId,
      spenderId: spent.spenderId,
    });
  }

  save() {
    this.isSaving = true;
    const spent = this.createFromForm();
    spent.spentDate = new Date(spent.spentDate).toISOString().split('T')[0];
    spent.lastUpdate = new Date(spent.lastUpdate).toISOString().split('T')[0];
    if (!this.isNew) {
      this.subscribeToSaveResponse(this.spentService.update(spent));
    } else {
      this.subscribeToSaveResponse(this.spentService.create(spent));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<Spent>>) {
    result.subscribe(
      (res: HttpResponse<Spent>) => this.onSaveSuccess(res),
      (res: HttpErrorResponse) => this.onError(res.error)
    );
  }

  async onSaveSuccess(response) {
    let action = 'updated';
    if (response.status === 201) {
      action = 'created';
    }
    this.isSaving = false;
    const toast = await this.toastCtrl.create({ message: `Spent ${action} successfully.`, duration: 2000, position: 'middle' });
    toast.present();
    this.navController.navigateBack('/tabs/entities/spent');
  }

  previousState() {
    window.history.back();
  }

  async onError(error) {
    this.isSaving = false;
    console.error(error);
    const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
    toast.present();
  }

  private createFromForm(): Spent {
    return {
      ...new Spent(),
      id: this.form.get(['id']).value,
      label: this.form.get(['label']).value,
      description: this.form.get(['description']).value,
      amount: this.form.get(['amount']).value,
      spentDate: this.form.get(['spentDate']).value,
      confirmed: this.form.get(['confirmed']).value,
      lastUpdate: this.form.get(['lastUpdate']).value,
      walletId: this.form.get(['walletId']).value,
      spenderId: this.form.get(['spenderId']).value,
    };
  }

  compareWallet(first: Wallet, second: Wallet): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackWalletById(index: number, item: Wallet) {
    return item.id;
  }
  compareProfil(first: Profil, second: Profil): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackProfilById(index: number, item: Profil) {
    return item.id;
  }
}
