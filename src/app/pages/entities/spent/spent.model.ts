import { BaseEntity, SyncStatus } from 'src/model/base-entity';
import { SpentSharing } from '../spent-sharing/spent-sharing.model';

export class Spent implements BaseEntity {
  constructor(
    public id?: number,
    public label?: string,
    public description?: string,
    public amount?: number,
    public spentDate?: any,
    public confirmed?: boolean,
    public lastUpdate?: any,
    public spentSharings?: SpentSharing[],
    public walletLabel?: string,
    public walletId?: number,
    public spenderDisplayName?: string,
    public spenderId?: number, public syncStatus?: SyncStatus
  ) {
    this.confirmed = false;
  }
}
