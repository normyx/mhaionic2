import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { Spent } from './spent.model';
import { SpentService } from './spent.service';

@Component({
  selector: 'page-spent',
  templateUrl: 'spent.html',
})
export class SpentPage {
  spents: Spent[];

  // todo: add pagination

  constructor(
    private navController: NavController,
    private spentService: SpentService,
    private toastCtrl: ToastController,
    public plt: Platform
  ) {
    this.spents = [];
  }

  ionViewWillEnter() {
    this.loadAll();
  }

  async loadAll(refresher?) {
    this.spentService
      .query()
      .pipe(
        filter((res: HttpResponse<Spent[]>) => res.ok),
        map((res: HttpResponse<Spent[]>) => res.body)
      )
      .subscribe(
        (response: Spent[]) => {
          this.spents = response;
          if (typeof refresher !== 'undefined') {
            setTimeout(() => {
              refresher.target.complete();
            }, 750);
          }
        },
        async (error) => {
          console.error(error);
          const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
          toast.present();
        }
      );
  }

  trackId(index: number, item: Spent) {
    return item.id;
  }

  new() {
    this.navController.navigateForward('/tabs/entities/spent/new');
  }

  edit(item: IonItemSliding, spent: Spent) {
    this.navController.navigateForward('/tabs/entities/spent/' + spent.id + '/edit');
    item.close();
  }

  async delete(spent) {
    this.spentService.delete(spent.id).subscribe(
      async () => {
        const toast = await this.toastCtrl.create({ message: 'Spent deleted successfully.', duration: 3000, position: 'middle' });
        toast.present();
        this.loadAll();
      },
      (error) => console.error(error)
    );
  }

  view(spent: Spent) {
    this.navController.navigateForward('/tabs/entities/spent/' + spent.id + '/view');
  }
}
