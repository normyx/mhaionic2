import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { ShoppingCatalogItem } from './shopping-catalog-item.model';

@Injectable({ providedIn: 'root' })
export class ShoppingCatalogItemService {
  private resourceUrl = ApiService.API_URL + '/shopping-catalog-items';

  constructor(protected http: HttpClient) {}

  create(shoppingCatalogItem: ShoppingCatalogItem): Observable<HttpResponse<ShoppingCatalogItem>> {
    return this.http.post<ShoppingCatalogItem>(this.resourceUrl, shoppingCatalogItem, { observe: 'response' });
  }

  update(shoppingCatalogItem: ShoppingCatalogItem): Observable<HttpResponse<ShoppingCatalogItem>> {
    return this.http.put(this.resourceUrl, shoppingCatalogItem, { observe: 'response' });
  }

  find(id: number): Observable<HttpResponse<ShoppingCatalogItem>> {
    return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<HttpResponse<ShoppingCatalogItem[]>> {
    const options = createRequestOption(req);
    return this.http.get<ShoppingCatalogItem[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
