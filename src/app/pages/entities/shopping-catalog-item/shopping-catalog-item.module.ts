import { NgModule, Injectable } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserRouteAccessService } from '../../../services/auth/user-route-access.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

import { ShoppingCatalogItemPage } from './shopping-catalog-item';
import { ShoppingCatalogItemUpdatePage } from './shopping-catalog-item-update';
import { ShoppingCatalogItem, ShoppingCatalogItemService, ShoppingCatalogItemDetailPage } from '.';

@Injectable({ providedIn: 'root' })
export class ShoppingCatalogItemResolve implements Resolve<ShoppingCatalogItem> {
  constructor(private service: ShoppingCatalogItemService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ShoppingCatalogItem> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ShoppingCatalogItem>) => response.ok),
        map((shoppingCatalogItem: HttpResponse<ShoppingCatalogItem>) => shoppingCatalogItem.body)
      );
    }
    return of(new ShoppingCatalogItem());
  }
}

const routes: Routes = [
  {
    path: '',
    component: ShoppingCatalogItemPage,
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ShoppingCatalogItemUpdatePage,
    resolve: {
      data: ShoppingCatalogItemResolve,
    },
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ShoppingCatalogItemDetailPage,
    resolve: {
      data: ShoppingCatalogItemResolve,
    },
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ShoppingCatalogItemUpdatePage,
    resolve: {
      data: ShoppingCatalogItemResolve,
    },
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  declarations: [ShoppingCatalogItemPage, ShoppingCatalogItemUpdatePage, ShoppingCatalogItemDetailPage],
  imports: [IonicModule, FormsModule, ReactiveFormsModule, CommonModule, TranslateModule, RouterModule.forChild(routes)],
})
export class ShoppingCatalogItemPageModule {}
