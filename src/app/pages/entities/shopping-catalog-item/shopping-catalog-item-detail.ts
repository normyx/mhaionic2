import { Component, OnInit } from '@angular/core';
import { ShoppingCatalogItem } from './shopping-catalog-item.model';
import { ShoppingCatalogItemService } from './shopping-catalog-item.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'page-shopping-catalog-item-detail',
  templateUrl: 'shopping-catalog-item-detail.html',
})
export class ShoppingCatalogItemDetailPage implements OnInit {
  shoppingCatalogItem: ShoppingCatalogItem = {};

  constructor(
    private navController: NavController,
    private shoppingCatalogItemService: ShoppingCatalogItemService,
    private activatedRoute: ActivatedRoute,
    private alertController: AlertController
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((response) => {
      this.shoppingCatalogItem = response.data;
    });
  }

  open(item: ShoppingCatalogItem) {
    this.navController.navigateForward('/tabs/entities/shopping-catalog-item/' + item.id + '/edit');
  }

  async deleteModal(item: ShoppingCatalogItem) {
    const alert = await this.alertController.create({
      header: 'Confirm the deletion?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Delete',
          handler: () => {
            this.shoppingCatalogItemService.delete(item.id).subscribe(() => {
              this.navController.navigateForward('/tabs/entities/shopping-catalog-item');
            });
          },
        },
      ],
    });
    await alert.present();
  }
}
