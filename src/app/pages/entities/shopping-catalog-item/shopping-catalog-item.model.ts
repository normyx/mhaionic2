import { BaseEntity, SyncStatus } from 'src/model/base-entity';

export class ShoppingCatalogItem implements BaseEntity {
  constructor(
    public id?: number,
    public label?: string,
    public lastUpdate?: any,
    public shoppingCatalogSheldLabel?: string,
    public shoppingCatalogSheldId?: number, public syncStatus?: SyncStatus
  ) {}
}
