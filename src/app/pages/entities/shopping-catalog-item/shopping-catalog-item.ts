import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { ShoppingCatalogItem } from './shopping-catalog-item.model';
import { ShoppingCatalogItemService } from './shopping-catalog-item.service';

@Component({
  selector: 'page-shopping-catalog-item',
  templateUrl: 'shopping-catalog-item.html',
})
export class ShoppingCatalogItemPage {
  shoppingCatalogItems: ShoppingCatalogItem[];

  // todo: add pagination

  constructor(
    private navController: NavController,
    private shoppingCatalogItemService: ShoppingCatalogItemService,
    private toastCtrl: ToastController,
    public plt: Platform
  ) {
    this.shoppingCatalogItems = [];
  }

  ionViewWillEnter() {
    this.loadAll();
  }

  async loadAll(refresher?) {
    this.shoppingCatalogItemService
      .query()
      .pipe(
        filter((res: HttpResponse<ShoppingCatalogItem[]>) => res.ok),
        map((res: HttpResponse<ShoppingCatalogItem[]>) => res.body)
      )
      .subscribe(
        (response: ShoppingCatalogItem[]) => {
          this.shoppingCatalogItems = response;
          if (typeof refresher !== 'undefined') {
            setTimeout(() => {
              refresher.target.complete();
            }, 750);
          }
        },
        async (error) => {
          console.error(error);
          const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
          toast.present();
        }
      );
  }

  trackId(index: number, item: ShoppingCatalogItem) {
    return item.id;
  }

  new() {
    this.navController.navigateForward('/tabs/entities/shopping-catalog-item/new');
  }

  edit(item: IonItemSliding, shoppingCatalogItem: ShoppingCatalogItem) {
    this.navController.navigateForward('/tabs/entities/shopping-catalog-item/' + shoppingCatalogItem.id + '/edit');
    item.close();
  }

  async delete(shoppingCatalogItem) {
    this.shoppingCatalogItemService.delete(shoppingCatalogItem.id).subscribe(
      async () => {
        const toast = await this.toastCtrl.create({
          message: 'ShoppingCatalogItem deleted successfully.',
          duration: 3000,
          position: 'middle',
        });
        toast.present();
        this.loadAll();
      },
      (error) => console.error(error)
    );
  }

  view(shoppingCatalogItem: ShoppingCatalogItem) {
    this.navController.navigateForward('/tabs/entities/shopping-catalog-item/' + shoppingCatalogItem.id + '/view');
  }
}
