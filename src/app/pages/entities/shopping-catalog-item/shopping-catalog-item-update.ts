import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ShoppingCatalogItem } from './shopping-catalog-item.model';
import { ShoppingCatalogItemService } from './shopping-catalog-item.service';
import { ShoppingCatalogSheld, ShoppingCatalogSheldService } from '../shopping-catalog-sheld';

@Component({
  selector: 'page-shopping-catalog-item-update',
  templateUrl: 'shopping-catalog-item-update.html',
})
export class ShoppingCatalogItemUpdatePage implements OnInit {
  shoppingCatalogItem: ShoppingCatalogItem;
  shoppingCatalogShelds: ShoppingCatalogSheld[];
  lastUpdateDp: any;
  isSaving = false;
  isNew = true;
  isReadyToSave: boolean;

  form = this.formBuilder.group({
    id: [],
    label: [null, [Validators.required]],
    lastUpdate: [null, [Validators.required]],
    shoppingCatalogSheldId: [null, [Validators.required]],
  });

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected navController: NavController,
    protected formBuilder: FormBuilder,
    public platform: Platform,
    protected toastCtrl: ToastController,
    private shoppingCatalogSheldService: ShoppingCatalogSheldService,
    private shoppingCatalogItemService: ShoppingCatalogItemService
  ) {
    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  ngOnInit() {
    this.shoppingCatalogSheldService.query().subscribe(
      (data) => {
        this.shoppingCatalogShelds = data.body;
      },
      (error) => this.onError(error)
    );
    this.activatedRoute.data.subscribe((response) => {
      this.shoppingCatalogItem = response.data;
      this.isNew = this.shoppingCatalogItem.id === null || this.shoppingCatalogItem.id === undefined;
      this.updateForm(this.shoppingCatalogItem);
    });
  }

  updateForm(shoppingCatalogItem: ShoppingCatalogItem) {
    this.form.patchValue({
      id: shoppingCatalogItem.id,
      label: shoppingCatalogItem.label,
      lastUpdate: this.isNew ? new Date().toISOString().split('T')[0] : shoppingCatalogItem.lastUpdate,
      shoppingCatalogSheldId: shoppingCatalogItem.shoppingCatalogSheldId,
    });
  }

  save() {
    this.isSaving = true;
    const shoppingCatalogItem = this.createFromForm();
    shoppingCatalogItem.lastUpdate = new Date(shoppingCatalogItem.lastUpdate).toISOString().split('T')[0];
    if (!this.isNew) {
      this.subscribeToSaveResponse(this.shoppingCatalogItemService.update(shoppingCatalogItem));
    } else {
      this.subscribeToSaveResponse(this.shoppingCatalogItemService.create(shoppingCatalogItem));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ShoppingCatalogItem>>) {
    result.subscribe(
      (res: HttpResponse<ShoppingCatalogItem>) => this.onSaveSuccess(res),
      (res: HttpErrorResponse) => this.onError(res.error)
    );
  }

  async onSaveSuccess(response) {
    let action = 'updated';
    if (response.status === 201) {
      action = 'created';
    }
    this.isSaving = false;
    const toast = await this.toastCtrl.create({
      message: `ShoppingCatalogItem ${action} successfully.`,
      duration: 2000,
      position: 'middle',
    });
    toast.present();
    this.navController.navigateBack('/tabs/entities/shopping-catalog-item');
  }

  previousState() {
    window.history.back();
  }

  async onError(error) {
    this.isSaving = false;
    console.error(error);
    const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
    toast.present();
  }

  private createFromForm(): ShoppingCatalogItem {
    return {
      ...new ShoppingCatalogItem(),
      id: this.form.get(['id']).value,
      label: this.form.get(['label']).value,
      lastUpdate: this.form.get(['lastUpdate']).value,
      shoppingCatalogSheldId: this.form.get(['shoppingCatalogSheldId']).value,
    };
  }

  compareShoppingCatalogSheld(first: ShoppingCatalogSheld, second: ShoppingCatalogSheld): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackShoppingCatalogSheldById(index: number, item: ShoppingCatalogSheld) {
    return item.id;
  }
}
