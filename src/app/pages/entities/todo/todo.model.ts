import { BaseEntity, SyncStatus } from 'src/model/base-entity';

export class Todo implements BaseEntity {
  constructor(
    public id?: number,
    public label?: string,
    public done?: boolean,
    public lastUpdate?: any,
    public todoListLabel?: string,
    public todoListId?: number, public syncStatus?: SyncStatus
  ) {
    this.done = false;
  }
}
