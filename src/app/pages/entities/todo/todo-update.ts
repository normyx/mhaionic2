import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Todo } from './todo.model';
import { TodoService } from './todo.service';
import { TodoList, TodoListService } from '../todo-list';

@Component({
  selector: 'page-todo-update',
  templateUrl: 'todo-update.html',
})
export class TodoUpdatePage implements OnInit {
  todo: Todo;
  todoLists: TodoList[];
  lastUpdateDp: any;
  isSaving = false;
  isNew = true;
  isReadyToSave: boolean;

  form = this.formBuilder.group({
    id: [],
    label: [null, [Validators.required]],
    done: ['false', [Validators.required]],
    lastUpdate: [null, [Validators.required]],
    todoListId: [null, [Validators.required]],
  });

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected navController: NavController,
    protected formBuilder: FormBuilder,
    public platform: Platform,
    protected toastCtrl: ToastController,
    private todoListService: TodoListService,
    private todoService: TodoService
  ) {
    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  ngOnInit() {
    this.todoListService.query().subscribe(
      (data) => {
        this.todoLists = data.body;
      },
      (error) => this.onError(error)
    );
    this.activatedRoute.data.subscribe((response) => {
      this.todo = response.data;
      this.isNew = this.todo.id === null || this.todo.id === undefined;
      this.updateForm(this.todo);
    });
  }

  updateForm(todo: Todo) {
    this.form.patchValue({
      id: todo.id,
      label: todo.label,
      done: todo.done,
      lastUpdate: this.isNew ? new Date().toISOString().split('T')[0] : todo.lastUpdate,
      todoListId: todo.todoListId,
    });
  }

  save() {
    this.isSaving = true;
    const todo = this.createFromForm();
    todo.lastUpdate = new Date(todo.lastUpdate).toISOString().split('T')[0];
    if (!this.isNew) {
      this.subscribeToSaveResponse(this.todoService.update(todo));
    } else {
      this.subscribeToSaveResponse(this.todoService.create(todo));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<Todo>>) {
    result.subscribe(
      (res: HttpResponse<Todo>) => this.onSaveSuccess(res),
      (res: HttpErrorResponse) => this.onError(res.error)
    );
  }

  async onSaveSuccess(response) {
    let action = 'updated';
    if (response.status === 201) {
      action = 'created';
    }
    this.isSaving = false;
    const toast = await this.toastCtrl.create({ message: `Todo ${action} successfully.`, duration: 2000, position: 'middle' });
    toast.present();
    this.navController.navigateBack('/tabs/entities/todo');
  }

  previousState() {
    window.history.back();
  }

  async onError(error) {
    this.isSaving = false;
    console.error(error);
    const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
    toast.present();
  }

  private createFromForm(): Todo {
    return {
      ...new Todo(),
      id: this.form.get(['id']).value,
      label: this.form.get(['label']).value,
      done: this.form.get(['done']).value,
      lastUpdate: this.form.get(['lastUpdate']).value,
      todoListId: this.form.get(['todoListId']).value,
    };
  }

  compareTodoList(first: TodoList, second: TodoList): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackTodoListById(index: number, item: TodoList) {
    return item.id;
  }
}
