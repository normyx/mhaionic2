import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { Todo } from './todo.model';
import { TodoService } from './todo.service';

@Component({
  selector: 'page-todo',
  templateUrl: 'todo.html',
})
export class TodoPage {
  todos: Todo[];

  // todo: add pagination

  constructor(
    private navController: NavController,
    private todoService: TodoService,
    private toastCtrl: ToastController,
    public plt: Platform
  ) {
    this.todos = [];
  }

  ionViewWillEnter() {
    this.loadAll();
  }

  async loadAll(refresher?) {
    this.todoService
      .query()
      .pipe(
        filter((res: HttpResponse<Todo[]>) => res.ok),
        map((res: HttpResponse<Todo[]>) => res.body)
      )
      .subscribe(
        (response: Todo[]) => {
          this.todos = response;
          if (typeof refresher !== 'undefined') {
            setTimeout(() => {
              refresher.target.complete();
            }, 750);
          }
        },
        async (error) => {
          console.error(error);
          const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
          toast.present();
        }
      );
  }

  trackId(index: number, item: Todo) {
    return item.id;
  }

  new() {
    this.navController.navigateForward('/tabs/entities/todo/new');
  }

  edit(item: IonItemSliding, todo: Todo) {
    this.navController.navigateForward('/tabs/entities/todo/' + todo.id + '/edit');
    item.close();
  }

  async delete(todo) {
    this.todoService.delete(todo.id).subscribe(
      async () => {
        const toast = await this.toastCtrl.create({ message: 'Todo deleted successfully.', duration: 3000, position: 'middle' });
        toast.present();
        this.loadAll();
      },
      (error) => console.error(error)
    );
  }

  view(todo: Todo) {
    this.navController.navigateForward('/tabs/entities/todo/' + todo.id + '/view');
  }
}
