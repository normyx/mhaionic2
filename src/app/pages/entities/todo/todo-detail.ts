import { Component, OnInit } from '@angular/core';
import { Todo } from './todo.model';
import { TodoService } from './todo.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'page-todo-detail',
  templateUrl: 'todo-detail.html',
})
export class TodoDetailPage implements OnInit {
  todo: Todo = {};

  constructor(
    private navController: NavController,
    private todoService: TodoService,
    private activatedRoute: ActivatedRoute,
    private alertController: AlertController
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((response) => {
      this.todo = response.data;
    });
  }

  open(item: Todo) {
    this.navController.navigateForward('/tabs/entities/todo/' + item.id + '/edit');
  }

  async deleteModal(item: Todo) {
    const alert = await this.alertController.create({
      header: 'Confirm the deletion?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Delete',
          handler: () => {
            this.todoService.delete(item.id).subscribe(() => {
              this.navController.navigateForward('/tabs/entities/todo');
            });
          },
        },
      ],
    });
    await alert.present();
  }
}
