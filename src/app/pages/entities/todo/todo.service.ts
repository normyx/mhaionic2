import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { Todo } from './todo.model';

@Injectable({ providedIn: 'root' })
export class TodoService {
  private resourceUrl = ApiService.API_URL + '/todos';

  constructor(protected http: HttpClient) {}

  create(todo: Todo): Observable<HttpResponse<Todo>> {
    return this.http.post<Todo>(this.resourceUrl, todo, { observe: 'response' });
  }

  update(todo: Todo): Observable<HttpResponse<Todo>> {
    return this.http.put(this.resourceUrl, todo, { observe: 'response' });
  }

  find(id: number): Observable<HttpResponse<Todo>> {
    return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<HttpResponse<Todo[]>> {
    const options = createRequestOption(req);
    return this.http.get<Todo[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
