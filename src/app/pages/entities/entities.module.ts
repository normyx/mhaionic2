import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { UserRouteAccessService } from 'src/app/services/auth/user-route-access.service';
import { EntitiesPage } from './entities.page';

const routes: Routes = [
  {
    path: '',
    component: EntitiesPage,
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'profil',
    loadChildren: './profil/profil.module#ProfilPageModule',
  },
  {
    path: 'profil-data',
    loadChildren: './profil-data/profil-data.module#ProfilDataPageModule',
  },
  {
    path: 'shopping-cart',
    loadChildren: './shopping-cart/shopping-cart.module#ShoppingCartPageModule',
  },
  {
    path: 'shopping-catalog-cart',
    loadChildren: './shopping-catalog-cart/shopping-catalog-cart.module#ShoppingCatalogCartPageModule',
  },
  {
    path: 'shopping-catalog-item',
    loadChildren: './shopping-catalog-item/shopping-catalog-item.module#ShoppingCatalogItemPageModule',
  },
  {
    path: 'shopping-catalog-sheld',
    loadChildren: './shopping-catalog-sheld/shopping-catalog-sheld.module#ShoppingCatalogSheldPageModule',
  },
  {
    path: 'shopping-item',
    loadChildren: './shopping-item/shopping-item.module#ShoppingItemPageModule',
  },
  {
    path: 'spent',
    loadChildren: './spent/spent.module#SpentPageModule',
  },
  {
    path: 'spent-config',
    loadChildren: './spent-config/spent-config.module#SpentConfigPageModule',
  },
  {
    path: 'spent-sharing',
    loadChildren: './spent-sharing/spent-sharing.module#SpentSharingPageModule',
  },
  {
    path: 'spent-sharing-config',
    loadChildren: './spent-sharing-config/spent-sharing-config.module#SpentSharingConfigPageModule',
  },
  {
    path: 'task',
    loadChildren: './task/task.module#TaskPageModule',
  },
  {
    path: 'task-project',
    loadChildren: './task-project/task-project.module#TaskProjectPageModule',
  },
  {
    path: 'todo',
    loadChildren: './todo/todo.module#TodoPageModule',
  },
  {
    path: 'todo-list',
    loadChildren: './todo-list/todo-list.module#TodoListPageModule',
  },
  {
    path: 'todo-list-template',
    loadChildren: './todo-list-template/todo-list-template.module#TodoListTemplatePageModule',
  },
  {
    path: 'todo-template',
    loadChildren: './todo-template/todo-template.module#TodoTemplatePageModule',
  },
  {
    path: 'wallet',
    loadChildren: './wallet/wallet.module#WalletPageModule',
  },
  {
    path: 'workspace',
    loadChildren: './workspace/workspace.module#WorkspacePageModule',
  },
  /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
];

@NgModule({
  imports: [IonicModule, CommonModule, FormsModule, RouterModule.forChild(routes), TranslateModule],
  declarations: [EntitiesPage],
})
export class EntitiesPageModule {}
