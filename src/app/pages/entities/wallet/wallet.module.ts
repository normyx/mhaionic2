import { NgModule, Injectable } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserRouteAccessService } from '../../../services/auth/user-route-access.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

import { WalletPage } from './wallet';
import { WalletUpdatePage } from './wallet-update';
import { Wallet, WalletService, WalletDetailPage } from '.';

@Injectable({ providedIn: 'root' })
export class WalletResolve implements Resolve<Wallet> {
  constructor(private service: WalletService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Wallet> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Wallet>) => response.ok),
        map((wallet: HttpResponse<Wallet>) => wallet.body)
      );
    }
    return of(new Wallet());
  }
}

const routes: Routes = [
  {
    path: '',
    component: WalletPage,
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: WalletUpdatePage,
    resolve: {
      data: WalletResolve,
    },
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: WalletDetailPage,
    resolve: {
      data: WalletResolve,
    },
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: WalletUpdatePage,
    resolve: {
      data: WalletResolve,
    },
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  declarations: [WalletPage, WalletUpdatePage, WalletDetailPage],
  imports: [IonicModule, FormsModule, ReactiveFormsModule, CommonModule, TranslateModule, RouterModule.forChild(routes)],
})
export class WalletPageModule {}
