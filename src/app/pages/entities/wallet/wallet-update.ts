import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Wallet } from './wallet.model';
import { WalletService } from './wallet.service';
import { Profil, ProfilService } from '../profil';
import { Workspace, WorkspaceService } from '../workspace';

@Component({
  selector: 'page-wallet-update',
  templateUrl: 'wallet-update.html',
})
export class WalletUpdatePage implements OnInit {
  wallet: Wallet;
  profils: Profil[];
  workspaces: Workspace[];
  lastUpdateDp: any;
  isSaving = false;
  isNew = true;
  isReadyToSave: boolean;

  form = this.formBuilder.group({
    id: [],
    label: [null, [Validators.required]],
    lastUpdate: [null, [Validators.required]],
    owners: [null, []],
    workspaceId: [null, [Validators.required]],
  });

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected navController: NavController,
    protected formBuilder: FormBuilder,
    public platform: Platform,
    protected toastCtrl: ToastController,
    private profilService: ProfilService,
    private workspaceService: WorkspaceService,
    private walletService: WalletService
  ) {
    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  ngOnInit() {
    this.profilService.query().subscribe(
      (data) => {
        this.profils = data.body;
      },
      (error) => this.onError(error)
    );
    this.workspaceService.query().subscribe(
      (data) => {
        this.workspaces = data.body;
      },
      (error) => this.onError(error)
    );
    this.activatedRoute.data.subscribe((response) => {
      this.wallet = response.data;
      this.isNew = this.wallet.id === null || this.wallet.id === undefined;
      this.updateForm(this.wallet);
    });
  }

  updateForm(wallet: Wallet) {
    this.form.patchValue({
      id: wallet.id,
      label: wallet.label,
      lastUpdate: this.isNew ? new Date().toISOString().split('T')[0] : wallet.lastUpdate,
      owners: wallet.owners,
      workspaceId: wallet.workspaceId,
    });
  }

  save() {
    this.isSaving = true;
    const wallet = this.createFromForm();
    wallet.lastUpdate = new Date(wallet.lastUpdate).toISOString().split('T')[0];
    if (!this.isNew) {
      this.subscribeToSaveResponse(this.walletService.update(wallet));
    } else {
      this.subscribeToSaveResponse(this.walletService.create(wallet));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<Wallet>>) {
    result.subscribe(
      (res: HttpResponse<Wallet>) => this.onSaveSuccess(res),
      (res: HttpErrorResponse) => this.onError(res.error)
    );
  }

  async onSaveSuccess(response) {
    let action = 'updated';
    if (response.status === 201) {
      action = 'created';
    }
    this.isSaving = false;
    const toast = await this.toastCtrl.create({ message: `Wallet ${action} successfully.`, duration: 2000, position: 'middle' });
    toast.present();
    this.navController.navigateBack('/tabs/entities/wallet');
  }

  previousState() {
    window.history.back();
  }

  async onError(error) {
    this.isSaving = false;
    console.error(error);
    const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
    toast.present();
  }

  private createFromForm(): Wallet {
    return {
      ...new Wallet(),
      id: this.form.get(['id']).value,
      label: this.form.get(['label']).value,
      lastUpdate: this.form.get(['lastUpdate']).value,
      owners: this.form.get(['owners']).value,
      workspaceId: this.form.get(['workspaceId']).value,
    };
  }

  compareProfil(first: Profil, second: Profil): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackProfilById(index: number, item: Profil) {
    return item.id;
  }
  compareWorkspace(first: Workspace, second: Workspace): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackWorkspaceById(index: number, item: Workspace) {
    return item.id;
  }
}
