import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { Wallet } from './wallet.model';

@Injectable({ providedIn: 'root' })
export class WalletService {
  private resourceUrl = ApiService.API_URL + '/wallets';

  constructor(protected http: HttpClient) {}

  create(wallet: Wallet): Observable<HttpResponse<Wallet>> {
    return this.http.post<Wallet>(this.resourceUrl, wallet, { observe: 'response' });
  }

  update(wallet: Wallet): Observable<HttpResponse<Wallet>> {
    return this.http.put(this.resourceUrl, wallet, { observe: 'response' });
  }

  find(id: number): Observable<HttpResponse<Wallet>> {
    return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<HttpResponse<Wallet[]>> {
    const options = createRequestOption(req);
    return this.http.get<Wallet[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
