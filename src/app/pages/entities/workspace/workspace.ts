import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { Workspace } from './workspace.model';
import { WorkspaceService } from './workspace.service';

@Component({
  selector: 'page-workspace',
  templateUrl: 'workspace.html',
})
export class WorkspacePage {
  workspaces: Workspace[];

  // todo: add pagination

  constructor(
    private navController: NavController,
    private workspaceService: WorkspaceService,
    private toastCtrl: ToastController,
    public plt: Platform
  ) {
    this.workspaces = [];
  }

  ionViewWillEnter() {
    this.loadAll();
  }

  async loadAll(refresher?) {
    this.workspaceService
      .query()
      .pipe(
        filter((res: HttpResponse<Workspace[]>) => res.ok),
        map((res: HttpResponse<Workspace[]>) => res.body)
      )
      .subscribe(
        (response: Workspace[]) => {
          this.workspaces = response;
          if (typeof refresher !== 'undefined') {
            setTimeout(() => {
              refresher.target.complete();
            }, 750);
          }
        },
        async (error) => {
          console.error(error);
          const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
          toast.present();
        }
      );
  }

  trackId(index: number, item: Workspace) {
    return item.id;
  }

  new() {
    this.navController.navigateForward('/tabs/entities/workspace/new');
  }

  edit(item: IonItemSliding, workspace: Workspace) {
    this.navController.navigateForward('/tabs/entities/workspace/' + workspace.id + '/edit');
    item.close();
  }

  async delete(workspace) {
    this.workspaceService.delete(workspace.id).subscribe(
      async () => {
        const toast = await this.toastCtrl.create({ message: 'Workspace deleted successfully.', duration: 3000, position: 'middle' });
        toast.present();
        this.loadAll();
      },
      (error) => console.error(error)
    );
  }

  view(workspace: Workspace) {
    this.navController.navigateForward('/tabs/entities/workspace/' + workspace.id + '/view');
  }
}
