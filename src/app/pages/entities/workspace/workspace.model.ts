import { BaseEntity, SyncStatus } from 'src/model/base-entity';
import { ShoppingCart } from '../shopping-cart/shopping-cart.model';
import { TaskProject } from '../task-project/task-project.model';
import { TodoList } from '../todo-list/todo-list.model';
import { TodoListTemplate } from '../todo-list-template/todo-list-template.model';
import { Wallet } from '../wallet/wallet.model';
import { Profil } from '../profil/profil.model';

export class Workspace implements BaseEntity {
  constructor(
    public id?: number,
    public label?: string,
    public lastUpdate?: any,
    public shoppingCarts?: ShoppingCart[],
    public taskProjects?: TaskProject[],
    public todoLists?: TodoList[],
    public todoListTemplates?: TodoListTemplate[],
    public wallets?: Wallet[],
    public owners?: Profil[], public syncStatus?: SyncStatus
  ) {}
}
