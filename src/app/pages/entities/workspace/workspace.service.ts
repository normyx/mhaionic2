import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { Workspace } from './workspace.model';

@Injectable({ providedIn: 'root' })
export class WorkspaceService {
  private resourceUrl = ApiService.API_URL + '/workspaces';

  constructor(protected http: HttpClient) {}

  create(workspace: Workspace): Observable<HttpResponse<Workspace>> {
    return this.http.post<Workspace>(this.resourceUrl, workspace, { observe: 'response' });
  }

  update(workspace: Workspace): Observable<HttpResponse<Workspace>> {
    return this.http.put(this.resourceUrl, workspace, { observe: 'response' });
  }

  find(id: number): Observable<HttpResponse<Workspace>> {
    return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<HttpResponse<Workspace[]>> {
    const options = createRequestOption(req);
    return this.http.get<Workspace[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
