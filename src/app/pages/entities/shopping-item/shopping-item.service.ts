import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { ShoppingItem } from './shopping-item.model';

@Injectable({ providedIn: 'root' })
export class ShoppingItemService {
  private resourceUrl = ApiService.API_URL + '/shopping-items';

  constructor(protected http: HttpClient) {}

  create(shoppingItem: ShoppingItem): Observable<HttpResponse<ShoppingItem>> {
    return this.http.post<ShoppingItem>(this.resourceUrl, shoppingItem, { observe: 'response' });
  }

  update(shoppingItem: ShoppingItem): Observable<HttpResponse<ShoppingItem>> {
    return this.http.put(this.resourceUrl, shoppingItem, { observe: 'response' });
  }

  find(id: number): Observable<HttpResponse<ShoppingItem>> {
    return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<HttpResponse<ShoppingItem[]>> {
    const options = createRequestOption(req);
    return this.http.get<ShoppingItem[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
