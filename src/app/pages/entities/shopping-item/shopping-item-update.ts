import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ShoppingItem } from './shopping-item.model';
import { ShoppingItemService } from './shopping-item.service';
import { ShoppingCatalogSheld, ShoppingCatalogSheldService } from '../shopping-catalog-sheld';
import { ShoppingCart, ShoppingCartService } from '../shopping-cart';

@Component({
  selector: 'page-shopping-item-update',
  templateUrl: 'shopping-item-update.html',
})
export class ShoppingItemUpdatePage implements OnInit {
  shoppingItem: ShoppingItem;
  shoppingCatalogShelds: ShoppingCatalogSheld[];
  shoppingCarts: ShoppingCart[];
  lastUpdateDp: any;
  isSaving = false;
  isNew = true;
  isReadyToSave: boolean;

  form = this.formBuilder.group({
    id: [],
    label: [null, [Validators.required]],
    lastUpdate: [null, [Validators.required]],
    checked: ['false', [Validators.required]],
    quantity: [null, []],
    unit: [null, []],
    shoppingCatalogSheldId: [null, [Validators.required]],
    shoppingCartId: [null, [Validators.required]],
  });

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected navController: NavController,
    protected formBuilder: FormBuilder,
    public platform: Platform,
    protected toastCtrl: ToastController,
    private shoppingCatalogSheldService: ShoppingCatalogSheldService,
    private shoppingCartService: ShoppingCartService,
    private shoppingItemService: ShoppingItemService
  ) {
    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  ngOnInit() {
    this.shoppingCatalogSheldService.query().subscribe(
      (data) => {
        this.shoppingCatalogShelds = data.body;
      },
      (error) => this.onError(error)
    );
    this.shoppingCartService.query().subscribe(
      (data) => {
        this.shoppingCarts = data.body;
      },
      (error) => this.onError(error)
    );
    this.activatedRoute.data.subscribe((response) => {
      this.shoppingItem = response.data;
      this.isNew = this.shoppingItem.id === null || this.shoppingItem.id === undefined;
      this.updateForm(this.shoppingItem);
    });
  }

  updateForm(shoppingItem: ShoppingItem) {
    this.form.patchValue({
      id: shoppingItem.id,
      label: shoppingItem.label,
      lastUpdate: this.isNew ? new Date().toISOString().split('T')[0] : shoppingItem.lastUpdate,
      checked: shoppingItem.checked,
      quantity: shoppingItem.quantity,
      unit: shoppingItem.unit,
      shoppingCatalogSheldId: shoppingItem.shoppingCatalogSheldId,
      shoppingCartId: shoppingItem.shoppingCartId,
    });
  }

  save() {
    this.isSaving = true;
    const shoppingItem = this.createFromForm();
    shoppingItem.lastUpdate = new Date(shoppingItem.lastUpdate).toISOString().split('T')[0];
    if (!this.isNew) {
      this.subscribeToSaveResponse(this.shoppingItemService.update(shoppingItem));
    } else {
      this.subscribeToSaveResponse(this.shoppingItemService.create(shoppingItem));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ShoppingItem>>) {
    result.subscribe(
      (res: HttpResponse<ShoppingItem>) => this.onSaveSuccess(res),
      (res: HttpErrorResponse) => this.onError(res.error)
    );
  }

  async onSaveSuccess(response) {
    let action = 'updated';
    if (response.status === 201) {
      action = 'created';
    }
    this.isSaving = false;
    const toast = await this.toastCtrl.create({ message: `ShoppingItem ${action} successfully.`, duration: 2000, position: 'middle' });
    toast.present();
    this.navController.navigateBack('/tabs/entities/shopping-item');
  }

  previousState() {
    window.history.back();
  }

  async onError(error) {
    this.isSaving = false;
    console.error(error);
    const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
    toast.present();
  }

  private createFromForm(): ShoppingItem {
    return {
      ...new ShoppingItem(),
      id: this.form.get(['id']).value,
      label: this.form.get(['label']).value,
      lastUpdate: this.form.get(['lastUpdate']).value,
      checked: this.form.get(['checked']).value,
      quantity: this.form.get(['quantity']).value,
      unit: this.form.get(['unit']).value,
      shoppingCatalogSheldId: this.form.get(['shoppingCatalogSheldId']).value,
      shoppingCartId: this.form.get(['shoppingCartId']).value,
    };
  }

  compareShoppingCatalogSheld(first: ShoppingCatalogSheld, second: ShoppingCatalogSheld): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackShoppingCatalogSheldById(index: number, item: ShoppingCatalogSheld) {
    return item.id;
  }
  compareShoppingCart(first: ShoppingCart, second: ShoppingCart): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackShoppingCartById(index: number, item: ShoppingCart) {
    return item.id;
  }
}
