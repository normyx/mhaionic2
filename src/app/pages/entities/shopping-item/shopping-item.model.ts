import { BaseEntity, SyncStatus } from 'src/model/base-entity';

export const enum Unit {
  'QUANTITY',
  'G',
  'KG',
  'L',
  'ML',
}

export class ShoppingItem implements BaseEntity {
  constructor(
    public id?: number,
    public label?: string,
    public lastUpdate?: any,
    public checked?: boolean,
    public quantity?: number,
    public unit?: Unit,
    public shoppingCatalogSheldLabel?: string,
    public shoppingCatalogSheldId?: number,
    public shoppingCartLabel?: string,
    public shoppingCartId?: number, public syncStatus?: SyncStatus
  ) {
    this.checked = false;
  }
}
