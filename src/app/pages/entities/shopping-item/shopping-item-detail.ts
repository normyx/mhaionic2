import { Component, OnInit } from '@angular/core';
import { ShoppingItem } from './shopping-item.model';
import { ShoppingItemService } from './shopping-item.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'page-shopping-item-detail',
  templateUrl: 'shopping-item-detail.html',
})
export class ShoppingItemDetailPage implements OnInit {
  shoppingItem: ShoppingItem = {};

  constructor(
    private navController: NavController,
    private shoppingItemService: ShoppingItemService,
    private activatedRoute: ActivatedRoute,
    private alertController: AlertController
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((response) => {
      this.shoppingItem = response.data;
    });
  }

  open(item: ShoppingItem) {
    this.navController.navigateForward('/tabs/entities/shopping-item/' + item.id + '/edit');
  }

  async deleteModal(item: ShoppingItem) {
    const alert = await this.alertController.create({
      header: 'Confirm the deletion?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Delete',
          handler: () => {
            this.shoppingItemService.delete(item.id).subscribe(() => {
              this.navController.navigateForward('/tabs/entities/shopping-item');
            });
          },
        },
      ],
    });
    await alert.present();
  }
}
