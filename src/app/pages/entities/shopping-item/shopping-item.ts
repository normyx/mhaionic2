import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { ShoppingItem } from './shopping-item.model';
import { ShoppingItemService } from './shopping-item.service';

@Component({
  selector: 'page-shopping-item',
  templateUrl: 'shopping-item.html',
})
export class ShoppingItemPage {
  shoppingItems: ShoppingItem[];

  // todo: add pagination

  constructor(
    private navController: NavController,
    private shoppingItemService: ShoppingItemService,
    private toastCtrl: ToastController,
    public plt: Platform
  ) {
    this.shoppingItems = [];
  }

  ionViewWillEnter() {
    this.loadAll();
  }

  async loadAll(refresher?) {
    this.shoppingItemService
      .query()
      .pipe(
        filter((res: HttpResponse<ShoppingItem[]>) => res.ok),
        map((res: HttpResponse<ShoppingItem[]>) => res.body)
      )
      .subscribe(
        (response: ShoppingItem[]) => {
          this.shoppingItems = response;
          if (typeof refresher !== 'undefined') {
            setTimeout(() => {
              refresher.target.complete();
            }, 750);
          }
        },
        async (error) => {
          console.error(error);
          const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
          toast.present();
        }
      );
  }

  trackId(index: number, item: ShoppingItem) {
    return item.id;
  }

  new() {
    this.navController.navigateForward('/tabs/entities/shopping-item/new');
  }

  edit(item: IonItemSliding, shoppingItem: ShoppingItem) {
    this.navController.navigateForward('/tabs/entities/shopping-item/' + shoppingItem.id + '/edit');
    item.close();
  }

  async delete(shoppingItem) {
    this.shoppingItemService.delete(shoppingItem.id).subscribe(
      async () => {
        const toast = await this.toastCtrl.create({ message: 'ShoppingItem deleted successfully.', duration: 3000, position: 'middle' });
        toast.present();
        this.loadAll();
      },
      (error) => console.error(error)
    );
  }

  view(shoppingItem: ShoppingItem) {
    this.navController.navigateForward('/tabs/entities/shopping-item/' + shoppingItem.id + '/view');
  }
}
