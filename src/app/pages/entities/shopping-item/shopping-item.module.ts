import { NgModule, Injectable } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserRouteAccessService } from '../../../services/auth/user-route-access.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

import { ShoppingItemPage } from './shopping-item';
import { ShoppingItemUpdatePage } from './shopping-item-update';
import { ShoppingItem, ShoppingItemService, ShoppingItemDetailPage } from '.';

@Injectable({ providedIn: 'root' })
export class ShoppingItemResolve implements Resolve<ShoppingItem> {
  constructor(private service: ShoppingItemService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ShoppingItem> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ShoppingItem>) => response.ok),
        map((shoppingItem: HttpResponse<ShoppingItem>) => shoppingItem.body)
      );
    }
    return of(new ShoppingItem());
  }
}

const routes: Routes = [
  {
    path: '',
    component: ShoppingItemPage,
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ShoppingItemUpdatePage,
    resolve: {
      data: ShoppingItemResolve,
    },
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ShoppingItemDetailPage,
    resolve: {
      data: ShoppingItemResolve,
    },
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ShoppingItemUpdatePage,
    resolve: {
      data: ShoppingItemResolve,
    },
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  declarations: [ShoppingItemPage, ShoppingItemUpdatePage, ShoppingItemDetailPage],
  imports: [IonicModule, FormsModule, ReactiveFormsModule, CommonModule, TranslateModule, RouterModule.forChild(routes)],
})
export class ShoppingItemPageModule {}
