import { BaseEntity, SyncStatus } from 'src/model/base-entity';

export class TodoTemplate implements BaseEntity {
  constructor(
    public id?: number,
    public label?: string,
    public lastUpdate?: any,
    public todoListTemplateLabel?: string,
    public todoListTemplateId?: number, public syncStatus?: SyncStatus
  ) {}
}
