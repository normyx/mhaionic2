import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { TodoTemplate } from './todo-template.model';
import { TodoTemplateService } from './todo-template.service';
import { TodoListTemplate, TodoListTemplateService } from '../todo-list-template';

@Component({
  selector: 'page-todo-template-update',
  templateUrl: 'todo-template-update.html',
})
export class TodoTemplateUpdatePage implements OnInit {
  todoTemplate: TodoTemplate;
  todoListTemplates: TodoListTemplate[];
  lastUpdateDp: any;
  isSaving = false;
  isNew = true;
  isReadyToSave: boolean;

  form = this.formBuilder.group({
    id: [],
    label: [null, [Validators.required]],
    lastUpdate: [null, [Validators.required]],
    todoListTemplateId: [null, [Validators.required]],
  });

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected navController: NavController,
    protected formBuilder: FormBuilder,
    public platform: Platform,
    protected toastCtrl: ToastController,
    private todoListTemplateService: TodoListTemplateService,
    private todoTemplateService: TodoTemplateService
  ) {
    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  ngOnInit() {
    this.todoListTemplateService.query().subscribe(
      (data) => {
        this.todoListTemplates = data.body;
      },
      (error) => this.onError(error)
    );
    this.activatedRoute.data.subscribe((response) => {
      this.todoTemplate = response.data;
      this.isNew = this.todoTemplate.id === null || this.todoTemplate.id === undefined;
      this.updateForm(this.todoTemplate);
    });
  }

  updateForm(todoTemplate: TodoTemplate) {
    this.form.patchValue({
      id: todoTemplate.id,
      label: todoTemplate.label,
      lastUpdate: this.isNew ? new Date().toISOString().split('T')[0] : todoTemplate.lastUpdate,
      todoListTemplateId: todoTemplate.todoListTemplateId,
    });
  }

  save() {
    this.isSaving = true;
    const todoTemplate = this.createFromForm();
    todoTemplate.lastUpdate = new Date(todoTemplate.lastUpdate).toISOString().split('T')[0];
    if (!this.isNew) {
      this.subscribeToSaveResponse(this.todoTemplateService.update(todoTemplate));
    } else {
      this.subscribeToSaveResponse(this.todoTemplateService.create(todoTemplate));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<TodoTemplate>>) {
    result.subscribe(
      (res: HttpResponse<TodoTemplate>) => this.onSaveSuccess(res),
      (res: HttpErrorResponse) => this.onError(res.error)
    );
  }

  async onSaveSuccess(response) {
    let action = 'updated';
    if (response.status === 201) {
      action = 'created';
    }
    this.isSaving = false;
    const toast = await this.toastCtrl.create({ message: `TodoTemplate ${action} successfully.`, duration: 2000, position: 'middle' });
    toast.present();
    this.navController.navigateBack('/tabs/entities/todo-template');
  }

  previousState() {
    window.history.back();
  }

  async onError(error) {
    this.isSaving = false;
    console.error(error);
    const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
    toast.present();
  }

  private createFromForm(): TodoTemplate {
    return {
      ...new TodoTemplate(),
      id: this.form.get(['id']).value,
      label: this.form.get(['label']).value,
      lastUpdate: this.form.get(['lastUpdate']).value,
      todoListTemplateId: this.form.get(['todoListTemplateId']).value,
    };
  }

  compareTodoListTemplate(first: TodoListTemplate, second: TodoListTemplate): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackTodoListTemplateById(index: number, item: TodoListTemplate) {
    return item.id;
  }
}
