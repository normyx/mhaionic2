import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { TodoTemplate } from './todo-template.model';
import { TodoTemplateService } from './todo-template.service';

@Component({
  selector: 'page-todo-template',
  templateUrl: 'todo-template.html',
})
export class TodoTemplatePage {
  todoTemplates: TodoTemplate[];

  // todo: add pagination

  constructor(
    private navController: NavController,
    private todoTemplateService: TodoTemplateService,
    private toastCtrl: ToastController,
    public plt: Platform
  ) {
    this.todoTemplates = [];
  }

  ionViewWillEnter() {
    this.loadAll();
  }

  async loadAll(refresher?) {
    this.todoTemplateService
      .query()
      .pipe(
        filter((res: HttpResponse<TodoTemplate[]>) => res.ok),
        map((res: HttpResponse<TodoTemplate[]>) => res.body)
      )
      .subscribe(
        (response: TodoTemplate[]) => {
          this.todoTemplates = response;
          if (typeof refresher !== 'undefined') {
            setTimeout(() => {
              refresher.target.complete();
            }, 750);
          }
        },
        async (error) => {
          console.error(error);
          const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
          toast.present();
        }
      );
  }

  trackId(index: number, item: TodoTemplate) {
    return item.id;
  }

  new() {
    this.navController.navigateForward('/tabs/entities/todo-template/new');
  }

  edit(item: IonItemSliding, todoTemplate: TodoTemplate) {
    this.navController.navigateForward('/tabs/entities/todo-template/' + todoTemplate.id + '/edit');
    item.close();
  }

  async delete(todoTemplate) {
    this.todoTemplateService.delete(todoTemplate.id).subscribe(
      async () => {
        const toast = await this.toastCtrl.create({ message: 'TodoTemplate deleted successfully.', duration: 3000, position: 'middle' });
        toast.present();
        this.loadAll();
      },
      (error) => console.error(error)
    );
  }

  view(todoTemplate: TodoTemplate) {
    this.navController.navigateForward('/tabs/entities/todo-template/' + todoTemplate.id + '/view');
  }
}
