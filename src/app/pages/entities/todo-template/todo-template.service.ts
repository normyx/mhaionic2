import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { TodoTemplate } from './todo-template.model';

@Injectable({ providedIn: 'root' })
export class TodoTemplateService {
  private resourceUrl = ApiService.API_URL + '/todo-templates';

  constructor(protected http: HttpClient) {}

  create(todoTemplate: TodoTemplate): Observable<HttpResponse<TodoTemplate>> {
    return this.http.post<TodoTemplate>(this.resourceUrl, todoTemplate, { observe: 'response' });
  }

  update(todoTemplate: TodoTemplate): Observable<HttpResponse<TodoTemplate>> {
    return this.http.put(this.resourceUrl, todoTemplate, { observe: 'response' });
  }

  find(id: number): Observable<HttpResponse<TodoTemplate>> {
    return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<HttpResponse<TodoTemplate[]>> {
    const options = createRequestOption(req);
    return this.http.get<TodoTemplate[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
