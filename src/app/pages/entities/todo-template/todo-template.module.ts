import { NgModule, Injectable } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserRouteAccessService } from '../../../services/auth/user-route-access.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

import { TodoTemplatePage } from './todo-template';
import { TodoTemplateUpdatePage } from './todo-template-update';
import { TodoTemplate, TodoTemplateService, TodoTemplateDetailPage } from '.';

@Injectable({ providedIn: 'root' })
export class TodoTemplateResolve implements Resolve<TodoTemplate> {
  constructor(private service: TodoTemplateService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TodoTemplate> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<TodoTemplate>) => response.ok),
        map((todoTemplate: HttpResponse<TodoTemplate>) => todoTemplate.body)
      );
    }
    return of(new TodoTemplate());
  }
}

const routes: Routes = [
  {
    path: '',
    component: TodoTemplatePage,
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: TodoTemplateUpdatePage,
    resolve: {
      data: TodoTemplateResolve,
    },
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TodoTemplateDetailPage,
    resolve: {
      data: TodoTemplateResolve,
    },
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: TodoTemplateUpdatePage,
    resolve: {
      data: TodoTemplateResolve,
    },
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  declarations: [TodoTemplatePage, TodoTemplateUpdatePage, TodoTemplateDetailPage],
  imports: [IonicModule, FormsModule, ReactiveFormsModule, CommonModule, TranslateModule, RouterModule.forChild(routes)],
})
export class TodoTemplatePageModule {}
