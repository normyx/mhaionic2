import { Component, OnInit } from '@angular/core';
import { TodoTemplate } from './todo-template.model';
import { TodoTemplateService } from './todo-template.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'page-todo-template-detail',
  templateUrl: 'todo-template-detail.html',
})
export class TodoTemplateDetailPage implements OnInit {
  todoTemplate: TodoTemplate = {};

  constructor(
    private navController: NavController,
    private todoTemplateService: TodoTemplateService,
    private activatedRoute: ActivatedRoute,
    private alertController: AlertController
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((response) => {
      this.todoTemplate = response.data;
    });
  }

  open(item: TodoTemplate) {
    this.navController.navigateForward('/tabs/entities/todo-template/' + item.id + '/edit');
  }

  async deleteModal(item: TodoTemplate) {
    const alert = await this.alertController.create({
      header: 'Confirm the deletion?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Delete',
          handler: () => {
            this.todoTemplateService.delete(item.id).subscribe(() => {
              this.navController.navigateForward('/tabs/entities/todo-template');
            });
          },
        },
      ],
    });
    await alert.present();
  }
}
