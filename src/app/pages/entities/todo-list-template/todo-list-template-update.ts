import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { TodoListTemplate } from './todo-list-template.model';
import { TodoListTemplateService } from './todo-list-template.service';
import { Profil, ProfilService } from '../profil';
import { Workspace, WorkspaceService } from '../workspace';

@Component({
  selector: 'page-todo-list-template-update',
  templateUrl: 'todo-list-template-update.html',
})
export class TodoListTemplateUpdatePage implements OnInit {
  todoListTemplate: TodoListTemplate;
  profils: Profil[];
  workspaces: Workspace[];
  lastUpdateDp: any;
  isSaving = false;
  isNew = true;
  isReadyToSave: boolean;

  form = this.formBuilder.group({
    id: [],
    label: [null, [Validators.required]],
    lastUpdate: [null, [Validators.required]],
    owners: [null, []],
    workspaceId: [null, [Validators.required]],
  });

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected navController: NavController,
    protected formBuilder: FormBuilder,
    public platform: Platform,
    protected toastCtrl: ToastController,
    private profilService: ProfilService,
    private workspaceService: WorkspaceService,
    private todoListTemplateService: TodoListTemplateService
  ) {
    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  ngOnInit() {
    this.profilService.query().subscribe(
      (data) => {
        this.profils = data.body;
      },
      (error) => this.onError(error)
    );
    this.workspaceService.query().subscribe(
      (data) => {
        this.workspaces = data.body;
      },
      (error) => this.onError(error)
    );
    this.activatedRoute.data.subscribe((response) => {
      this.todoListTemplate = response.data;
      this.isNew = this.todoListTemplate.id === null || this.todoListTemplate.id === undefined;
      this.updateForm(this.todoListTemplate);
    });
  }

  updateForm(todoListTemplate: TodoListTemplate) {
    this.form.patchValue({
      id: todoListTemplate.id,
      label: todoListTemplate.label,
      lastUpdate: this.isNew ? new Date().toISOString().split('T')[0] : todoListTemplate.lastUpdate,
      owners: todoListTemplate.owners,
      workspaceId: todoListTemplate.workspaceId,
    });
  }

  save() {
    this.isSaving = true;
    const todoListTemplate = this.createFromForm();
    todoListTemplate.lastUpdate = new Date(todoListTemplate.lastUpdate).toISOString().split('T')[0];
    if (!this.isNew) {
      this.subscribeToSaveResponse(this.todoListTemplateService.update(todoListTemplate));
    } else {
      this.subscribeToSaveResponse(this.todoListTemplateService.create(todoListTemplate));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<TodoListTemplate>>) {
    result.subscribe(
      (res: HttpResponse<TodoListTemplate>) => this.onSaveSuccess(res),
      (res: HttpErrorResponse) => this.onError(res.error)
    );
  }

  async onSaveSuccess(response) {
    let action = 'updated';
    if (response.status === 201) {
      action = 'created';
    }
    this.isSaving = false;
    const toast = await this.toastCtrl.create({ message: `TodoListTemplate ${action} successfully.`, duration: 2000, position: 'middle' });
    toast.present();
    this.navController.navigateBack('/tabs/entities/todo-list-template');
  }

  previousState() {
    window.history.back();
  }

  async onError(error) {
    this.isSaving = false;
    console.error(error);
    const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
    toast.present();
  }

  private createFromForm(): TodoListTemplate {
    return {
      ...new TodoListTemplate(),
      id: this.form.get(['id']).value,
      label: this.form.get(['label']).value,
      lastUpdate: this.form.get(['lastUpdate']).value,
      owners: this.form.get(['owners']).value,
      workspaceId: this.form.get(['workspaceId']).value,
    };
  }

  compareProfil(first: Profil, second: Profil): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackProfilById(index: number, item: Profil) {
    return item.id;
  }
  compareWorkspace(first: Workspace, second: Workspace): boolean {
    return first && first.id && second && second.id ? first.id === second.id : first === second;
  }

  trackWorkspaceById(index: number, item: Workspace) {
    return item.id;
  }
}
