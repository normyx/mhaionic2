import { NgModule, Injectable } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserRouteAccessService } from '../../../services/auth/user-route-access.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

import { TodoListTemplatePage } from './todo-list-template';
import { TodoListTemplateUpdatePage } from './todo-list-template-update';
import { TodoListTemplate, TodoListTemplateService, TodoListTemplateDetailPage } from '.';

@Injectable({ providedIn: 'root' })
export class TodoListTemplateResolve implements Resolve<TodoListTemplate> {
  constructor(private service: TodoListTemplateService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<TodoListTemplate> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<TodoListTemplate>) => response.ok),
        map((todoListTemplate: HttpResponse<TodoListTemplate>) => todoListTemplate.body)
      );
    }
    return of(new TodoListTemplate());
  }
}

const routes: Routes = [
  {
    path: '',
    component: TodoListTemplatePage,
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: TodoListTemplateUpdatePage,
    resolve: {
      data: TodoListTemplateResolve,
    },
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TodoListTemplateDetailPage,
    resolve: {
      data: TodoListTemplateResolve,
    },
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: TodoListTemplateUpdatePage,
    resolve: {
      data: TodoListTemplateResolve,
    },
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  declarations: [TodoListTemplatePage, TodoListTemplateUpdatePage, TodoListTemplateDetailPage],
  imports: [IonicModule, FormsModule, ReactiveFormsModule, CommonModule, TranslateModule, RouterModule.forChild(routes)],
})
export class TodoListTemplatePageModule {}
