import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { TodoListTemplate } from './todo-list-template.model';
import { TodoListTemplateService } from './todo-list-template.service';

@Component({
  selector: 'page-todo-list-template',
  templateUrl: 'todo-list-template.html',
})
export class TodoListTemplatePage {
  todoListTemplates: TodoListTemplate[];

  // todo: add pagination

  constructor(
    private navController: NavController,
    private todoListTemplateService: TodoListTemplateService,
    private toastCtrl: ToastController,
    public plt: Platform
  ) {
    this.todoListTemplates = [];
  }

  ionViewWillEnter() {
    this.loadAll();
  }

  async loadAll(refresher?) {
    this.todoListTemplateService
      .query()
      .pipe(
        filter((res: HttpResponse<TodoListTemplate[]>) => res.ok),
        map((res: HttpResponse<TodoListTemplate[]>) => res.body)
      )
      .subscribe(
        (response: TodoListTemplate[]) => {
          this.todoListTemplates = response;
          if (typeof refresher !== 'undefined') {
            setTimeout(() => {
              refresher.target.complete();
            }, 750);
          }
        },
        async (error) => {
          console.error(error);
          const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
          toast.present();
        }
      );
  }

  trackId(index: number, item: TodoListTemplate) {
    return item.id;
  }

  new() {
    this.navController.navigateForward('/tabs/entities/todo-list-template/new');
  }

  edit(item: IonItemSliding, todoListTemplate: TodoListTemplate) {
    this.navController.navigateForward('/tabs/entities/todo-list-template/' + todoListTemplate.id + '/edit');
    item.close();
  }

  async delete(todoListTemplate) {
    this.todoListTemplateService.delete(todoListTemplate.id).subscribe(
      async () => {
        const toast = await this.toastCtrl.create({
          message: 'TodoListTemplate deleted successfully.',
          duration: 3000,
          position: 'middle',
        });
        toast.present();
        this.loadAll();
      },
      (error) => console.error(error)
    );
  }

  view(todoListTemplate: TodoListTemplate) {
    this.navController.navigateForward('/tabs/entities/todo-list-template/' + todoListTemplate.id + '/view');
  }
}
