import { BaseEntity, SyncStatus } from 'src/model/base-entity';
import { TodoTemplate } from '../todo-template/todo-template.model';
import { Profil } from '../profil/profil.model';

export class TodoListTemplate implements BaseEntity {
  constructor(
    public id?: number,
    public label?: string,
    public lastUpdate?: any,
    public todoTemplates?: TodoTemplate[],
    public owners?: Profil[],
    public workspaceLabel?: string,
    public workspaceId?: number, public syncStatus?: SyncStatus
  ) {}
}
