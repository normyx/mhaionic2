import { Component, OnInit } from '@angular/core';
import { TodoListTemplate } from './todo-list-template.model';
import { TodoListTemplateService } from './todo-list-template.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'page-todo-list-template-detail',
  templateUrl: 'todo-list-template-detail.html',
})
export class TodoListTemplateDetailPage implements OnInit {
  todoListTemplate: TodoListTemplate = {};

  constructor(
    private navController: NavController,
    private todoListTemplateService: TodoListTemplateService,
    private activatedRoute: ActivatedRoute,
    private alertController: AlertController
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((response) => {
      this.todoListTemplate = response.data;
    });
  }

  open(item: TodoListTemplate) {
    this.navController.navigateForward('/tabs/entities/todo-list-template/' + item.id + '/edit');
  }

  async deleteModal(item: TodoListTemplate) {
    const alert = await this.alertController.create({
      header: 'Confirm the deletion?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Delete',
          handler: () => {
            this.todoListTemplateService.delete(item.id).subscribe(() => {
              this.navController.navigateForward('/tabs/entities/todo-list-template');
            });
          },
        },
      ],
    });
    await alert.present();
  }
}
