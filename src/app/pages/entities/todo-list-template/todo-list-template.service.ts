import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { TodoListTemplate } from './todo-list-template.model';

@Injectable({ providedIn: 'root' })
export class TodoListTemplateService {
  private resourceUrl = ApiService.API_URL + '/todo-list-templates';

  constructor(protected http: HttpClient) {}

  create(todoListTemplate: TodoListTemplate): Observable<HttpResponse<TodoListTemplate>> {
    return this.http.post<TodoListTemplate>(this.resourceUrl, todoListTemplate, { observe: 'response' });
  }

  update(todoListTemplate: TodoListTemplate): Observable<HttpResponse<TodoListTemplate>> {
    return this.http.put(this.resourceUrl, todoListTemplate, { observe: 'response' });
  }

  find(id: number): Observable<HttpResponse<TodoListTemplate>> {
    return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<HttpResponse<TodoListTemplate[]>> {
    const options = createRequestOption(req);
    return this.http.get<TodoListTemplate[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
