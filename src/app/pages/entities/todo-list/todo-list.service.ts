import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { TodoList } from './todo-list.model';

@Injectable({ providedIn: 'root' })
export class TodoListService {
  private resourceUrl = ApiService.API_URL + '/todo-lists';

  constructor(protected http: HttpClient) {}

  create(todoList: TodoList): Observable<HttpResponse<TodoList>> {
    return this.http.post<TodoList>(this.resourceUrl, todoList, { observe: 'response' });
  }

  update(todoList: TodoList): Observable<HttpResponse<TodoList>> {
    return this.http.put(this.resourceUrl, todoList, { observe: 'response' });
  }

  find(id: number): Observable<HttpResponse<TodoList>> {
    return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<HttpResponse<TodoList[]>> {
    const options = createRequestOption(req);
    return this.http.get<TodoList[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
