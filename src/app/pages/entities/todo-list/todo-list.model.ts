import { BaseEntity, SyncStatus } from 'src/model/base-entity';
import { Todo } from '../todo/todo.model';
import { Profil } from '../profil/profil.model';

export class TodoList implements BaseEntity {
  constructor(
    public id?: number,
    public label?: string,
    public lastUpdate?: any,
    public todos?: Todo[],
    public owners?: Profil[],
    public workspaceLabel?: string,
    public workspaceId?: number, public syncStatus?: SyncStatus
  ) {}
}
