import { Component, OnInit } from '@angular/core';
import { TodoList } from './todo-list.model';
import { TodoListService } from './todo-list.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'page-todo-list-detail',
  templateUrl: 'todo-list-detail.html',
})
export class TodoListDetailPage implements OnInit {
  todoList: TodoList = {};

  constructor(
    private navController: NavController,
    private todoListService: TodoListService,
    private activatedRoute: ActivatedRoute,
    private alertController: AlertController
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((response) => {
      this.todoList = response.data;
    });
  }

  open(item: TodoList) {
    this.navController.navigateForward('/tabs/entities/todo-list/' + item.id + '/edit');
  }

  async deleteModal(item: TodoList) {
    const alert = await this.alertController.create({
      header: 'Confirm the deletion?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Delete',
          handler: () => {
            this.todoListService.delete(item.id).subscribe(() => {
              this.navController.navigateForward('/tabs/entities/todo-list');
            });
          },
        },
      ],
    });
    await alert.present();
  }
}
