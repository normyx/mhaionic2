import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { TodoList } from './todo-list.model';
import { TodoListService } from './todo-list.service';

@Component({
  selector: 'page-todo-list',
  templateUrl: 'todo-list.html',
})
export class TodoListPage {
  todoLists: TodoList[];

  // todo: add pagination

  constructor(
    private navController: NavController,
    private todoListService: TodoListService,
    private toastCtrl: ToastController,
    public plt: Platform
  ) {
    this.todoLists = [];
  }

  ionViewWillEnter() {
    this.loadAll();
  }

  async loadAll(refresher?) {
    this.todoListService
      .query()
      .pipe(
        filter((res: HttpResponse<TodoList[]>) => res.ok),
        map((res: HttpResponse<TodoList[]>) => res.body)
      )
      .subscribe(
        (response: TodoList[]) => {
          this.todoLists = response;
          if (typeof refresher !== 'undefined') {
            setTimeout(() => {
              refresher.target.complete();
            }, 750);
          }
        },
        async (error) => {
          console.error(error);
          const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
          toast.present();
        }
      );
  }

  trackId(index: number, item: TodoList) {
    return item.id;
  }

  new() {
    this.navController.navigateForward('/tabs/entities/todo-list/new');
  }

  edit(item: IonItemSliding, todoList: TodoList) {
    this.navController.navigateForward('/tabs/entities/todo-list/' + todoList.id + '/edit');
    item.close();
  }

  async delete(todoList) {
    this.todoListService.delete(todoList.id).subscribe(
      async () => {
        const toast = await this.toastCtrl.create({ message: 'TodoList deleted successfully.', duration: 3000, position: 'middle' });
        toast.present();
        this.loadAll();
      },
      (error) => console.error(error)
    );
  }

  view(todoList: TodoList) {
    this.navController.navigateForward('/tabs/entities/todo-list/' + todoList.id + '/view');
  }
}
