import { NgModule, Injectable } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserRouteAccessService } from '../../../services/auth/user-route-access.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { filter, map } from 'rxjs/operators';

import { ShoppingCatalogCartPage } from './shopping-catalog-cart';
import { ShoppingCatalogCartUpdatePage } from './shopping-catalog-cart-update';
import { ShoppingCatalogCart, ShoppingCatalogCartService, ShoppingCatalogCartDetailPage } from '.';

@Injectable({ providedIn: 'root' })
export class ShoppingCatalogCartResolve implements Resolve<ShoppingCatalogCart> {
  constructor(private service: ShoppingCatalogCartService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ShoppingCatalogCart> {
    const id = route.params.id ? route.params.id : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<ShoppingCatalogCart>) => response.ok),
        map((shoppingCatalogCart: HttpResponse<ShoppingCatalogCart>) => shoppingCatalogCart.body)
      );
    }
    return of(new ShoppingCatalogCart());
  }
}

const routes: Routes = [
  {
    path: '',
    component: ShoppingCatalogCartPage,
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ShoppingCatalogCartUpdatePage,
    resolve: {
      data: ShoppingCatalogCartResolve,
    },
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ShoppingCatalogCartDetailPage,
    resolve: {
      data: ShoppingCatalogCartResolve,
    },
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ShoppingCatalogCartUpdatePage,
    resolve: {
      data: ShoppingCatalogCartResolve,
    },
    data: {
      authorities: ['ROLE_USER'],
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  declarations: [ShoppingCatalogCartPage, ShoppingCatalogCartUpdatePage, ShoppingCatalogCartDetailPage],
  imports: [IonicModule, FormsModule, ReactiveFormsModule, CommonModule, TranslateModule, RouterModule.forChild(routes)],
})
export class ShoppingCatalogCartPageModule {}
