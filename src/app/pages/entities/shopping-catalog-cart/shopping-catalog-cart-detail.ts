import { Component, OnInit } from '@angular/core';
import { ShoppingCatalogCart } from './shopping-catalog-cart.model';
import { ShoppingCatalogCartService } from './shopping-catalog-cart.service';
import { NavController, AlertController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'page-shopping-catalog-cart-detail',
  templateUrl: 'shopping-catalog-cart-detail.html',
})
export class ShoppingCatalogCartDetailPage implements OnInit {
  shoppingCatalogCart: ShoppingCatalogCart = {};

  constructor(
    private navController: NavController,
    private shoppingCatalogCartService: ShoppingCatalogCartService,
    private activatedRoute: ActivatedRoute,
    private alertController: AlertController
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe((response) => {
      this.shoppingCatalogCart = response.data;
    });
  }

  open(item: ShoppingCatalogCart) {
    this.navController.navigateForward('/tabs/entities/shopping-catalog-cart/' + item.id + '/edit');
  }

  async deleteModal(item: ShoppingCatalogCart) {
    const alert = await this.alertController.create({
      header: 'Confirm the deletion?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: 'Delete',
          handler: () => {
            this.shoppingCatalogCartService.delete(item.id).subscribe(() => {
              this.navController.navigateForward('/tabs/entities/shopping-catalog-cart');
            });
          },
        },
      ],
    });
    await alert.present();
  }
}
