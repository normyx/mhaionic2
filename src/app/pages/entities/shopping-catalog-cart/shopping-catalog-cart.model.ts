import { BaseEntity, SyncStatus } from 'src/model/base-entity';
import { ShoppingCatalogSheld } from '../shopping-catalog-sheld/shopping-catalog-sheld.model';

export class ShoppingCatalogCart implements BaseEntity {
  constructor(public id?: number, public label?: string, public lastUpdate?: any, public shoppingCatalogShelds?: ShoppingCatalogSheld[], public syncStatus?: SyncStatus) {}
}
