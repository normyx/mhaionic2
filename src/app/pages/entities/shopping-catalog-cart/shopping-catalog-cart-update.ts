import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ShoppingCatalogCart } from './shopping-catalog-cart.model';
import { ShoppingCatalogCartService } from './shopping-catalog-cart.service';

@Component({
  selector: 'page-shopping-catalog-cart-update',
  templateUrl: 'shopping-catalog-cart-update.html',
})
export class ShoppingCatalogCartUpdatePage implements OnInit {
  shoppingCatalogCart: ShoppingCatalogCart;
  lastUpdateDp: any;
  isSaving = false;
  isNew = true;
  isReadyToSave: boolean;

  form = this.formBuilder.group({
    id: [],
    label: [null, [Validators.required]],
    lastUpdate: [null, [Validators.required]],
  });

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected navController: NavController,
    protected formBuilder: FormBuilder,
    public platform: Platform,
    protected toastCtrl: ToastController,
    private shoppingCatalogCartService: ShoppingCatalogCartService
  ) {
    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  ngOnInit() {
    this.activatedRoute.data.subscribe((response) => {
      this.shoppingCatalogCart = response.data;
      this.isNew = this.shoppingCatalogCart.id === null || this.shoppingCatalogCart.id === undefined;
      this.updateForm(this.shoppingCatalogCart);
    });
  }

  updateForm(shoppingCatalogCart: ShoppingCatalogCart) {
    this.form.patchValue({
      id: shoppingCatalogCart.id,
      label: shoppingCatalogCart.label,
      lastUpdate: this.isNew ? new Date().toISOString().split('T')[0] : shoppingCatalogCart.lastUpdate,
    });
  }

  save() {
    this.isSaving = true;
    const shoppingCatalogCart = this.createFromForm();
    shoppingCatalogCart.lastUpdate = new Date(shoppingCatalogCart.lastUpdate).toISOString().split('T')[0];
    if (!this.isNew) {
      this.subscribeToSaveResponse(this.shoppingCatalogCartService.update(shoppingCatalogCart));
    } else {
      this.subscribeToSaveResponse(this.shoppingCatalogCartService.create(shoppingCatalogCart));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ShoppingCatalogCart>>) {
    result.subscribe(
      (res: HttpResponse<ShoppingCatalogCart>) => this.onSaveSuccess(res),
      (res: HttpErrorResponse) => this.onError(res.error)
    );
  }

  async onSaveSuccess(response) {
    let action = 'updated';
    if (response.status === 201) {
      action = 'created';
    }
    this.isSaving = false;
    const toast = await this.toastCtrl.create({
      message: `ShoppingCatalogCart ${action} successfully.`,
      duration: 2000,
      position: 'middle',
    });
    toast.present();
    this.navController.navigateBack('/tabs/entities/shopping-catalog-cart');
  }

  previousState() {
    window.history.back();
  }

  async onError(error) {
    this.isSaving = false;
    console.error(error);
    const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
    toast.present();
  }

  private createFromForm(): ShoppingCatalogCart {
    return {
      ...new ShoppingCatalogCart(),
      id: this.form.get(['id']).value,
      label: this.form.get(['label']).value,
      lastUpdate: this.form.get(['lastUpdate']).value,
    };
  }
}
