import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';
import { createRequestOption } from 'src/app/shared';
import { ShoppingCatalogCart } from './shopping-catalog-cart.model';

@Injectable({ providedIn: 'root' })
export class ShoppingCatalogCartService {
  private resourceUrl = ApiService.API_URL + '/shopping-catalog-carts';

  constructor(protected http: HttpClient) {}

  create(shoppingCatalogCart: ShoppingCatalogCart): Observable<HttpResponse<ShoppingCatalogCart>> {
    return this.http.post<ShoppingCatalogCart>(this.resourceUrl, shoppingCatalogCart, { observe: 'response' });
  }

  update(shoppingCatalogCart: ShoppingCatalogCart): Observable<HttpResponse<ShoppingCatalogCart>> {
    return this.http.put(this.resourceUrl, shoppingCatalogCart, { observe: 'response' });
  }

  find(id: number): Observable<HttpResponse<ShoppingCatalogCart>> {
    return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<HttpResponse<ShoppingCatalogCart[]>> {
    const options = createRequestOption(req);
    return this.http.get<ShoppingCatalogCart[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
