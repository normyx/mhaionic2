import { Component } from '@angular/core';
import { NavController, ToastController, Platform, IonItemSliding } from '@ionic/angular';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { ShoppingCatalogCart } from './shopping-catalog-cart.model';
import { ShoppingCatalogCartService } from './shopping-catalog-cart.service';

@Component({
  selector: 'page-shopping-catalog-cart',
  templateUrl: 'shopping-catalog-cart.html',
})
export class ShoppingCatalogCartPage {
  shoppingCatalogCarts: ShoppingCatalogCart[];

  // todo: add pagination

  constructor(
    private navController: NavController,
    private shoppingCatalogCartService: ShoppingCatalogCartService,
    private toastCtrl: ToastController,
    public plt: Platform
  ) {
    this.shoppingCatalogCarts = [];
  }

  ionViewWillEnter() {
    this.loadAll();
  }

  async loadAll(refresher?) {
    this.shoppingCatalogCartService
      .query()
      .pipe(
        filter((res: HttpResponse<ShoppingCatalogCart[]>) => res.ok),
        map((res: HttpResponse<ShoppingCatalogCart[]>) => res.body)
      )
      .subscribe(
        (response: ShoppingCatalogCart[]) => {
          this.shoppingCatalogCarts = response;
          if (typeof refresher !== 'undefined') {
            setTimeout(() => {
              refresher.target.complete();
            }, 750);
          }
        },
        async (error) => {
          console.error(error);
          const toast = await this.toastCtrl.create({ message: 'Failed to load data', duration: 2000, position: 'middle' });
          toast.present();
        }
      );
  }

  trackId(index: number, item: ShoppingCatalogCart) {
    return item.id;
  }

  new() {
    this.navController.navigateForward('/tabs/entities/shopping-catalog-cart/new');
  }

  edit(item: IonItemSliding, shoppingCatalogCart: ShoppingCatalogCart) {
    this.navController.navigateForward('/tabs/entities/shopping-catalog-cart/' + shoppingCatalogCart.id + '/edit');
    item.close();
  }

  async delete(shoppingCatalogCart) {
    this.shoppingCatalogCartService.delete(shoppingCatalogCart.id).subscribe(
      async () => {
        const toast = await this.toastCtrl.create({
          message: 'ShoppingCatalogCart deleted successfully.',
          duration: 3000,
          position: 'middle',
        });
        toast.present();
        this.loadAll();
      },
      (error) => console.error(error)
    );
  }

  view(shoppingCatalogCart: ShoppingCatalogCart) {
    this.navController.navigateForward('/tabs/entities/shopping-catalog-cart/' + shoppingCatalogCart.id + '/view');
  }
}
