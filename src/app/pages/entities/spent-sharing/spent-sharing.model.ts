import { BaseEntity, SyncStatus } from 'src/model/base-entity';

export class SpentSharing implements BaseEntity {
  constructor(
    public id?: number,
    public amountShare?: number,
    public share?: number,
    public lastUpdate?: any,
    public sharingProfilDisplayName?: string,
    public sharingProfilId?: number,
    public spentLabel?: string,
    public spentId?: number, public syncStatus?: SyncStatus
  ) {}
}
