//import { StatusBar } from '@ionic-native/status-bar/ngx';
import {
    Plugins,
    StatusBarStyle,
} from '@capacitor/core';
import { Platform } from '@ionic/angular';

const { StatusBar } = Plugins;

export function setStatusBarBackgroundColor(part: string, platform: Platform): void {
    console.info(platform.is("desktop"));
    console.info(platform.platforms());
    if (!platform.is('mobileweb')) {
        switch (part) {
            case 'task': {
                StatusBar.setBackgroundColor({ color: '#d41243' });
                break;
            }
            case 'workspace': {
                StatusBar.setBackgroundColor({ color: '#3880ff' });
                break;
            }
            case 'wallet': {
                StatusBar.setBackgroundColor({ color: '#f47835' });
                break;
            }
            case 'home': {
                StatusBar.setBackgroundColor({ color: '#00aedb' });
                break;
            }
            case 'todo': {
                StatusBar.setBackgroundColor({ color: '#a200ff' });
                break;
            }
            case 'shopping': {
                StatusBar.setBackgroundColor({ color: '#8ec127' });
                break;
            }
            case 'info': {
                StatusBar.setBackgroundColor({ color: '#000080' });
                break;
            }
            default: {
                StatusBar.setBackgroundColor({ color: "white" });
            }
        }
    }
}