import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { Observable, of } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { ShoppingCatalogItem } from 'src/app/pages/entities/shopping-catalog-item';
import { ShoppingCatalogSheld } from 'src/app/pages/entities/shopping-catalog-sheld';
import { MhaEntityLocalImplService } from 'src/app/services/mha/mha-entity.service/mha-entity-local-impl.service';
import { MhaShoppingCatalogItemInterfaceService } from './mha-shopping-catalog-item-interface.service';


@Injectable({ providedIn: 'root' })
export class MhaShoppingCatalogItemLocalService extends MhaEntityLocalImplService<ShoppingCatalogItem, ShoppingCatalogSheld, Profil> implements MhaShoppingCatalogItemInterfaceService  {
    
    

    @LocalStorage('dbShoppingCatalogItem')
    entities: ShoppingCatalogItem[];

    @LocalStorage('dbShoppingCatalogItemId')
    newId: number;

    constructor() {
        super();
    }

    findWhereParent(parents: ShoppingCatalogSheld[]): Observable<HttpResponse<ShoppingCatalogItem[]>> {
        return of(new HttpResponse(
            { body: this.entities.filter(t => parents.some(scs => {return scs.id == t.shoppingCatalogSheldId;})) }
        ));
    }

    changeParentId(oldParentId: number, newParentId: number) {
        this.entities.forEach(t => {
            if (t.shoppingCatalogSheldId == oldParentId) {
                t.shoppingCatalogSheldId = newParentId;
            }
        });
        this.entities = this.entities;
    }

    sort() {
        // does nothing
    }

    
}