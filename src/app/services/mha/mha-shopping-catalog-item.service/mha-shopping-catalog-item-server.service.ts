import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { ShoppingCatalogItem, ShoppingCatalogItemService } from 'src/app/pages/entities/shopping-catalog-item';
import { ShoppingCatalogSheld } from 'src/app/pages/entities/shopping-catalog-sheld';
import { MhaEntityServerInterfaceService } from 'src/app/services/mha/mha-entity.service/mha-entity-server-interface.service';
import { MhaEntityServerRecursiveCallService } from '../mha-entity.service/mha-entity-server-recursive.service';
import { beforeCreateOrUpdate } from '../mha-entity.service/mha-entity.function';
@Injectable({ providedIn: 'root' })
export class MhaShoppingCatalogItemServerService extends ShoppingCatalogItemService implements MhaEntityServerInterfaceService<ShoppingCatalogItem, ShoppingCatalogSheld, Profil> {


    constructor(protected http: HttpClient,
        protected recursiveRetriever: MhaEntityServerRecursiveCallService<ShoppingCatalogItem, ShoppingCatalogSheld, Profil>) {
        super(http);
    }

    findWhereParent(parents: ShoppingCatalogSheld[], refreshDate: moment.Moment): Observable<HttpResponse<ShoppingCatalogItem[]>> {
        return this.recursiveRetriever.findWhereParent(this, parents,'shoppingCatalogSheldId.in', refreshDate);
    }

    create(entity: ShoppingCatalogItem): Observable<HttpResponse<ShoppingCatalogItem>> {
        return super.create(beforeCreateOrUpdate(entity));
    }

    update(entity: ShoppingCatalogItem): Observable<HttpResponse<ShoppingCatalogItem>> {
        return super.update(beforeCreateOrUpdate(entity));
    }

}
