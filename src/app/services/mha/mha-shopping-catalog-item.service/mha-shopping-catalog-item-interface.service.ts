import { Profil } from 'src/app/pages/entities/profil';
import { ShoppingCatalogItem } from 'src/app/pages/entities/shopping-catalog-item';
import { ShoppingCatalogSheld } from 'src/app/pages/entities/shopping-catalog-sheld';
import { MhaEntityInterfaceService } from '../mha-entity.service/mha-entity-interface.service';

export interface MhaShoppingCatalogItemInterfaceService extends MhaEntityInterfaceService<ShoppingCatalogItem, ShoppingCatalogSheld, Profil> {
    
}
