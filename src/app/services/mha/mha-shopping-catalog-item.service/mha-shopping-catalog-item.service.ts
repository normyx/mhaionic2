import { Injectable } from '@angular/core';
import { Profil } from 'src/app/pages/entities/profil';
import { ShoppingCatalogItem } from 'src/app/pages/entities/shopping-catalog-item';
import { ShoppingCatalogSheld } from 'src/app/pages/entities/shopping-catalog-sheld';
import { MhaProxyClassService } from '../mha-entity.service/mha-entity-proxy.service';
import { MhaShoppingCatalogItemLocalService } from './mha-shopping-catalog-item-local.service';
import { MhaShoppingCatalogItemServerService } from './mha-shopping-catalog-item-server.service';

@Injectable({ providedIn: 'root' })
export class MhaShoppingCatalogItemService extends MhaProxyClassService<ShoppingCatalogItem, ShoppingCatalogSheld, Profil> {

    constructor(protected shoppingCatalogItemLocalService: MhaShoppingCatalogItemLocalService,
        protected shoppingCatalogItemServerService: MhaShoppingCatalogItemServerService) {
        super(shoppingCatalogItemLocalService, shoppingCatalogItemServerService, [])
    }

    getAll(): ShoppingCatalogItem[] {
        return this.shoppingCatalogItemLocalService.entities;
    }

 

}