import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { ProfilData, ProfilDataService } from 'src/app/pages/entities/profil-data';
import { Workspace } from 'src/app/pages/entities/workspace';
import { ApiService } from '../../api/api.service';
import { MhaEntityServerInterfaceService } from '../mha-entity.service/mha-entity-server-interface.service';
import { beforeCreateOrUpdate } from '../mha-entity.service/mha-entity.function';
import { MhaProfilDataInterfaceService } from './mha-profil-data-interface-service';
import * as moment from 'moment';
@Injectable({ providedIn: 'root' })
export class MhaProfilDataServerService extends ProfilDataService implements MhaEntityServerInterfaceService<ProfilData, Workspace, Profil>, MhaProfilDataInterfaceService {
    constructor(protected http: HttpClient) {
        super(http);
    }
    findWhereParent(parents: Workspace[], refreshDate?:  moment.Moment): Observable<HttpResponse<ProfilData[]>> {
        if (!parents || parents.length == 0) return of(new HttpResponse({body:[]}));
        let parentIds : number[] = [];
        for (let workspace of parents) {
            parentIds.push(workspace.id);
        }
        const path = refreshDate ? `/mysha-profil-data-where-workspace-and-last-update/${parentIds}/${moment(moment(refreshDate).subtract(1,"d")).format("YYYY-MM-DD")}`:`/mysha-profil-data-where-workspace/${parentIds}`;
        return this.http.get<ProfilData[]>(ApiService.API_URL + path, { observe: 'response' });
    }

    create(entity: ProfilData): Observable<HttpResponse<ProfilData>> {
        return super.create(beforeCreateOrUpdate(entity));
    }

    update(entity: ProfilData): Observable<HttpResponse<ProfilData>> {
        return super.update(beforeCreateOrUpdate(entity));
    }

    
}

