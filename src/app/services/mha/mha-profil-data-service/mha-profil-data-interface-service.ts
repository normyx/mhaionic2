import { Profil } from 'src/app/pages/entities/profil';
import { ProfilData } from 'src/app/pages/entities/profil-data';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaEntityInterfaceService } from 'src/app/services/mha/mha-entity.service/mha-entity-interface.service';

export interface MhaProfilDataInterfaceService extends MhaEntityInterfaceService<ProfilData, Workspace, Profil> {
}
