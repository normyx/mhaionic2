import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { Todo, TodoService } from 'src/app/pages/entities/todo';
import { TodoList } from 'src/app/pages/entities/todo-list';
import { MhaEntityServerInterfaceService } from 'src/app/services/mha/mha-entity.service/mha-entity-server-interface.service';
import { MhaEntityServerRecursiveCallService } from '../mha-entity.service/mha-entity-server-recursive.service';
import { beforeCreateOrUpdate } from '../mha-entity.service/mha-entity.function';
import * as moment from 'moment';
@Injectable({ providedIn: 'root' })
export class MhaTodoServerService extends TodoService implements MhaEntityServerInterfaceService<Todo, TodoList, Profil> {


    constructor(protected http: HttpClient,
        protected recursiveRetriever: MhaEntityServerRecursiveCallService<Todo, TodoList, Profil>) {
        super(http);
    }

    findWhereParent(parents: TodoList[], refreshDate: moment.Moment): Observable<HttpResponse<TodoList[]>> {
        return this.recursiveRetriever.findWhereParent(this, parents,'todoListId.in', refreshDate);
    }

    create(entity: Todo): Observable<HttpResponse<Todo>> {
        return super.create(beforeCreateOrUpdate(entity));
    }

    update(entity: Todo): Observable<HttpResponse<Todo>> {
        return super.update(beforeCreateOrUpdate(entity));
    }

}
