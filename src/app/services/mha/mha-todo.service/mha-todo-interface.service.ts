import { Profil } from 'src/app/pages/entities/profil';
import { Todo } from 'src/app/pages/entities/todo';
import { TodoList } from 'src/app/pages/entities/todo-list';
import { MhaEntityInterfaceService } from '../mha-entity.service/mha-entity-interface.service';

export interface MhaTodoInterfaceService extends MhaEntityInterfaceService<Todo, TodoList, Profil> {
    
}
