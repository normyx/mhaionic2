import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { Observable, of } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { Todo } from 'src/app/pages/entities/todo';
import { MhaEntityLocalImplService } from 'src/app/services/mha/mha-entity.service/mha-entity-local-impl.service';
import { MhaTodoInterfaceService } from './mha-todo-interface.service';
import { TodoList } from 'src/app/pages/entities/todo-list';


@Injectable({ providedIn: 'root' })
export class MhaTodoLocalService extends MhaEntityLocalImplService<Todo, TodoList, Profil> implements MhaTodoInterfaceService  {
    
    

    @LocalStorage('dbTodo')
    entities: Todo[];

    @LocalStorage('dbTodoId')
    newId: number;

    constructor() {
        super();
    }

    findWhereParent(parents: TodoList[]): Observable<HttpResponse<Todo[]>> {
        return of(new HttpResponse(
            { body: this.entities.filter(t => parents.some(tl => {return tl.id == t.todoListId;})) }
        ));
    }

    changeParentId(oldParentId: number, newParentId: number) {
        this.entities.forEach(t => {
            if (t.todoListId == oldParentId) {
                t.todoListId = newParentId;
            }
        });
        this.entities = this.entities;
    }

    sort() {
        // does nothing
    }

    
}