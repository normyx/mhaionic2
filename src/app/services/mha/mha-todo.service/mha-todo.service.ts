import { Injectable } from '@angular/core';
import { Profil } from 'src/app/pages/entities/profil';
import { Todo } from 'src/app/pages/entities/todo';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaProxyClassService } from '../mha-entity.service/mha-entity-proxy.service';
import { MhaWorkspaceLocalService } from '../mha-workspace.service/mha-workspace-local.service';
import { MhaTodoLocalService } from './mha-todo-local.service';
import { MhaTodoServerService } from './mha-todo-server.service';

@Injectable({ providedIn: 'root' })
export class MhaTodoService extends MhaProxyClassService<Todo, Workspace, Profil> {

    constructor(protected taskProjectLocalService: MhaTodoLocalService,
        protected taskProjectServerService: MhaTodoServerService,
        protected workspaceService: MhaWorkspaceLocalService) {
        super(taskProjectLocalService, taskProjectServerService, [])
    }

 

}