import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { ShoppingCatalogCart, ShoppingCatalogCartService } from 'src/app/pages/entities/shopping-catalog-cart';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaEntityServerInterfaceService } from 'src/app/services/mha/mha-entity.service/mha-entity-server-interface.service';
import { MhaEntityServerRecursiveCallService } from '../mha-entity.service/mha-entity-server-recursive.service';
import { beforeCreateOrUpdate } from '../mha-entity.service/mha-entity.function';

@Injectable({ providedIn: 'root' })
export class MhaShoppingCatalogCartServerService extends ShoppingCatalogCartService implements MhaEntityServerInterfaceService<ShoppingCatalogCart, Workspace, Profil> {


    constructor(protected http: HttpClient,
        protected recursiveRetriever: MhaEntityServerRecursiveCallService<ShoppingCatalogCart, Workspace, Profil>) {
        super(http);
    }

    findWhereParent(parents: Workspace[], refreshDate: moment.Moment, profil: Profil): Observable<HttpResponse<ShoppingCatalogCart[]>> {
        return this.recursiveRetriever.findWhereParent(this, null,null, refreshDate, null, null);
    }

    create(entity: ShoppingCatalogCart): Observable<HttpResponse<ShoppingCatalogCart>> {
        return super.create(beforeCreateOrUpdate(entity));
    }

    update(entity: ShoppingCatalogCart): Observable<HttpResponse<ShoppingCatalogCart>> {
        return super.update(beforeCreateOrUpdate(entity));
    }


}
