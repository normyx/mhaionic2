import { Profil } from 'src/app/pages/entities/profil';
import { ShoppingCatalogCart } from 'src/app/pages/entities/shopping-catalog-cart';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaEntityInterfaceService } from '../mha-entity.service/mha-entity-interface.service';

export interface MhaShoppingCatalogCartInterfaceService extends MhaEntityInterfaceService<ShoppingCatalogCart, Workspace, Profil> {
    
}
