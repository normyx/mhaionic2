import { Injectable } from '@angular/core';
import { Profil } from 'src/app/pages/entities/profil';
import { ShoppingCatalogCart } from 'src/app/pages/entities/shopping-catalog-cart';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaProxyClassService } from '../mha-entity.service/mha-entity-proxy.service';
import { MhaShoppingCatalogCartLocalService } from './mha-shopping-catalog-cart-local.service';
import { MhaShoppingCatalogCartServerService } from './mha-shopping-catalog-cart-server.service';
import { MhaShoppingCatalogSheldService } from '../mha-shopping-catalog-sheld.service/mha-shopping-catalog-sheld.service';


@Injectable({ providedIn: 'root' })
export class MhaShoppingCatalogCartService extends MhaProxyClassService<ShoppingCatalogCart, Workspace, Profil> {

    constructor(protected shoppingCatalogCartLocalService: MhaShoppingCatalogCartLocalService,
        protected shoppingCatalogCartServerService: MhaShoppingCatalogCartServerService,
        protected shoppingCatalogSheldService: MhaShoppingCatalogSheldService) {
        super(shoppingCatalogCartLocalService, shoppingCatalogCartServerService, [shoppingCatalogSheldService])
    }


}