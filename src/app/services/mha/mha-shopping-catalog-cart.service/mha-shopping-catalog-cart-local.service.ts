import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { Observable, of } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { ShoppingCatalogCart } from 'src/app/pages/entities/shopping-catalog-cart';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaEntityLocalImplService } from 'src/app/services/mha/mha-entity.service/mha-entity-local-impl.service';
import { MhaShoppingCatalogCartInterfaceService } from './mha-shopping-catalog-cart-interface.service';


@Injectable({ providedIn: 'root' })
export class MhaShoppingCatalogCartLocalService extends MhaEntityLocalImplService<ShoppingCatalogCart, Workspace, Profil> implements MhaShoppingCatalogCartInterfaceService  {
    
    

    @LocalStorage('dbShoppingCatalogCart')
    entities: ShoppingCatalogCart[];

    @LocalStorage('dbShoppingCatalogCartId')
    newId: number;

    constructor() {
        super();
    }

    findWhereParent(parents: Workspace[]): Observable<HttpResponse<ShoppingCatalogCart[]>> {
        return of(new HttpResponse(
            { body: this.entities }
        ));
    }

    changeParentId(oldParentId: number, newParentId: number) {
        
    }

    sort() {
        // does nothing
    }

    
}