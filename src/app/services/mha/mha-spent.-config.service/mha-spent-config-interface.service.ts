import { Profil } from 'src/app/pages/entities/profil';
import { SpentConfig } from 'src/app/pages/entities/spent-config';
import { Wallet } from 'src/app/pages/entities/wallet';
import { MhaEntityInterfaceService } from '../mha-entity.service/mha-entity-interface.service';

export interface MhaSpentConfigInterfaceService extends MhaEntityInterfaceService<SpentConfig, Wallet, Profil> {
    
}
