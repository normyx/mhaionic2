import { Injectable } from '@angular/core';
import { Profil } from 'src/app/pages/entities/profil';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaProxyClassService } from '../mha-entity.service/mha-entity-proxy.service';
import { MhaProfilDataService } from '../mha-profil-data-service/mha-profil-data-service';
import { MhaShoppingCartService } from '../mha-shopping-cart.service/mha-shopping-cart.service';
import { MhaShoppingCatalogCartService } from '../mha-shopping-catalog-cart.service/mha-shopping-catalog-cart.service';
import { MhaTaskProjectService } from '../mha-task-project.service/mha-task-project.service';
import { MhaTodoListTemplateService } from '../mha-todo-list-template.service/mha-todo-list-template.service';
import { MhaTodoListService } from '../mha-todo-list.service/mha-todo-list.service';
import { MhaWalletService } from '../mha-wallet.service/mha-wallet.service';
import { MhaWorkspaceLocalService } from './mha-workspace-local.service';
import { MhaWorkspaceServerService } from './mha-workspace-server.service';

@Injectable({ providedIn: 'root' })
export class MhaWorkspaceService extends MhaProxyClassService<Workspace, Profil, Profil> {

    constructor(protected workspaceLocalService: MhaWorkspaceLocalService,
        protected workspaceServerService: MhaWorkspaceServerService,
        protected taskProjectService: MhaTaskProjectService,
        protected walletService: MhaWalletService,
        protected profilDataService: MhaProfilDataService,
        protected todoListService: MhaTodoListService,
        protected todoListTemplateService: MhaTodoListTemplateService,
        protected shoppingCartService: MhaShoppingCartService,
        protected shoppingCatalogCartService: MhaShoppingCatalogCartService) {
        super(workspaceLocalService, workspaceServerService, [taskProjectService, walletService, profilDataService, todoListService, todoListTemplateService, shoppingCartService, shoppingCatalogCartService])
    }


    public setStoredActiveWorkspace(workspace: Workspace) {
        this.workspaceLocalService.setStoredActiveWorkspace(workspace);
    }

    public getStoredActiveWorkspace(): Workspace {
        return this.workspaceLocalService.getStoredActiveWorkspace();
    }
}