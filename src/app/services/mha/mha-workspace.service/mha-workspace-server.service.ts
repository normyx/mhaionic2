import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { Workspace, WorkspaceService } from 'src/app/pages/entities/workspace';
import { MhaEntityServerInterfaceService } from '../mha-entity.service/mha-entity-server-interface.service';
import { MhaEntityServerRecursiveCallService } from '../mha-entity.service/mha-entity-server-recursive.service';
import { beforeCreateOrUpdate } from '../mha-entity.service/mha-entity.function';
import * as moment from 'moment';
@Injectable({ providedIn: 'root' })
export class MhaWorkspaceServerService extends WorkspaceService implements MhaEntityServerInterfaceService<Workspace, Profil, Profil> {
    

    constructor(protected http: HttpClient,
        protected recursiveRetriever: MhaEntityServerRecursiveCallService<Workspace, Profil, Profil>) {
        super(http);
    }
    
    findWhereParent(parents: Profil[], refreshDate: moment.Moment): Observable<HttpResponse<Workspace[]>> {
        return this.recursiveRetriever.findWhereParent(this, parents,'ownerId.in', refreshDate);
    }

    create(entity: Workspace): Observable<HttpResponse<Workspace>> {
        return super.create(beforeCreateOrUpdate(entity));
    }

    update(entity: Workspace): Observable<HttpResponse<Workspace>> {
        return super.update(beforeCreateOrUpdate(entity));
    }


}
