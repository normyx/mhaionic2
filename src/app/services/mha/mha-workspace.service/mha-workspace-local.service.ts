import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { Observable, of } from 'rxjs';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaEntityLocalImplService } from 'src/app/services/mha/mha-entity.service/mha-entity-local-impl.service';
import { MhaWorkspaceInterfaceService } from './mha-workspace-interface.service';
import { Profil } from 'src/app/pages/entities/profil';


@Injectable({ providedIn: 'root' })
export class MhaWorkspaceLocalService extends MhaEntityLocalImplService<Workspace, Profil, Profil> implements MhaWorkspaceInterfaceService  {
     

    @LocalStorage('dbWorkspace')
    entities: Workspace[];

    @LocalStorage('activeWorkspace')
    activeWorkspace: Workspace;

    @LocalStorage('dbWorkspaceId')
    newId: number;

    constructor() {
        super();
    }

    
    changeParentId(oldParentId: number, newParentId: number) {
        throw new Error("Method not implemented.");
    }
    findWhereParent(parents: Profil[]): Observable<HttpResponse<Workspace[]>> {
        // TODO : filter with parent 
        return of(new HttpResponse({body:this.entities}));
    }
    public setStoredActiveWorkspace(workspace: Workspace) {
        this.activeWorkspace = workspace;
    }

    public getStoredActiveWorkspace() {
        return this.activeWorkspace;
    }

    sort() {
        // does nothing
    }
        
}