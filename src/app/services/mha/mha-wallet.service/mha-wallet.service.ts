import { Injectable } from '@angular/core';
import { Profil } from 'src/app/pages/entities/profil';
import { Wallet } from 'src/app/pages/entities/wallet';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaProxyClassService } from '../mha-entity.service/mha-entity-proxy.service';
import { MhaSpentConfigService } from '../mha-spent.-config.service/mha-spent-config.service';
import { MhaSpentService } from '../mha-spent.service/mha-spent.service';
import { MhaWalletLocalService } from './mha-wallet-local.service';
import { MhaWalletServerService } from './mha-wallet-server.service';
import { MhaSpentSharingService } from 'src/app/services/mha/mha-spent-sharing.service/mha-spent-sharing.service';
import { WalletSummary } from 'src/model/wallet-summary-model';
import { filter, map } from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';
import { Spent } from 'src/app/pages/entities/spent';
import { SyncStatus } from 'src/model/base-entity';
import { Observable, of } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class MhaWalletService extends MhaProxyClassService<Wallet, Workspace, Profil> {

    constructor(protected walletLocalService: MhaWalletLocalService,
        protected walletServerService: MhaWalletServerService,
        protected spentService: MhaSpentService,
        private spentSharingService: MhaSpentSharingService,
        protected spentConfigService: MhaSpentConfigService) {
        super(walletLocalService, walletServerService, [spentService, spentConfigService]);
    }

    async getWalletSummary(wallet: Wallet): Promise<WalletSummary> {
        let walletSummary = new WalletSummary(wallet);
        let spents = await this.spentService.findWhereParent([wallet]).toPromise();
        let undeletedSpents = spents.body.filter(s => s.syncStatus != SyncStatus.DELETE);
        for (let spent of undeletedSpents) {
            let spentSharings = await this.spentSharingService.findWhereParent([spent]).toPromise();
            walletSummary.addSpent(spent, spentSharings.body);
        }
        return Promise.resolve(walletSummary);
    
    }


}