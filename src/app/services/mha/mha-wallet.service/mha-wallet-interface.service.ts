import { TaskProject } from 'src/app/pages/entities/task-project';
import { MhaEntityInterfaceService } from '../mha-entity.service/mha-entity-interface.service';
import { Workspace } from 'src/app/pages/entities/workspace';
import { Profil } from 'src/app/pages/entities/profil';

export interface MhaWalletInterfaceService extends MhaEntityInterfaceService<TaskProject, Workspace, Profil> {
    
}
