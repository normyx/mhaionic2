import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { ShoppingCart, ShoppingCartService } from 'src/app/pages/entities/shopping-cart';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaEntityServerInterfaceService } from 'src/app/services/mha/mha-entity.service/mha-entity-server-interface.service';
import { MhaEntityServerRecursiveCallService } from '../mha-entity.service/mha-entity-server-recursive.service';
import { beforeCreateOrUpdate } from '../mha-entity.service/mha-entity.function';

@Injectable({ providedIn: 'root' })
export class MhaShoppingCartServerService extends ShoppingCartService implements MhaEntityServerInterfaceService<ShoppingCart, Workspace, Profil> {


    constructor(protected http: HttpClient,
        protected recursiveRetriever: MhaEntityServerRecursiveCallService<ShoppingCart, Workspace, Profil>) {
        super(http);
    }

    findWhereParent(parents: Workspace[], refreshDate: moment.Moment, profil: Profil): Observable<HttpResponse<ShoppingCart[]>> {
        return this.recursiveRetriever.findWhereParent(this, parents,'workspaceId.in', refreshDate, profil, 'ownerId.equals');
    }

    create(entity: ShoppingCart): Observable<HttpResponse<ShoppingCart>> {
        return super.create(beforeCreateOrUpdate(entity));
    }

    update(entity: ShoppingCart): Observable<HttpResponse<ShoppingCart>> {
        return super.update(beforeCreateOrUpdate(entity));
    }


}
