import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { Observable, of } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { ShoppingCart } from 'src/app/pages/entities/shopping-cart';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaEntityLocalImplService } from 'src/app/services/mha/mha-entity.service/mha-entity-local-impl.service';
import { MhaShoppingCartInterfaceService } from './mha-shopping-cart-interface.service';


@Injectable({ providedIn: 'root' })
export class MhaShoppingCartLocalService extends MhaEntityLocalImplService<ShoppingCart, Workspace, Profil> implements MhaShoppingCartInterfaceService  {
    
    

    @LocalStorage('dbShoppingCart')
    entities: ShoppingCart[];

    @LocalStorage('dbShoppingCartId')
    newId: number;

    constructor() {
        super();
    }

    findWhereParent(parents: Workspace[]): Observable<HttpResponse<ShoppingCart[]>> {
        return of(new HttpResponse(
            { body: this.entities.filter(tp => parents.some(w => {return w.id == tp.workspaceId;})) }
        ));
    }

    changeParentId(oldParentId: number, newParentId: number) {
        this.entities.forEach(t => {
            if (t.workspaceId == oldParentId) {
                t.workspaceId = newParentId;
            }
        });
        this.entities = this.entities;
    }

    sort() {
        // does nothing
    }

    
}