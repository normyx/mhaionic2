import { Injectable } from '@angular/core';
import { Profil } from 'src/app/pages/entities/profil';
import { ShoppingCart } from 'src/app/pages/entities/shopping-cart';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaProxyClassService } from '../mha-entity.service/mha-entity-proxy.service';
import { MhaShoppingItemService } from '../mha-shopping-item.service/mha-shopping-item.service';
import { MhaWorkspaceLocalService } from '../mha-workspace.service/mha-workspace-local.service';
import { MhaShoppingCartLocalService } from './mha-shopping-cart-local.service';
import { MhaShoppingCartServerService } from './mha-shopping-cart-server.service';
import { ShoppingCartSummary } from 'src/model/shopping-cart-summary-model';
import { SyncStatus } from 'src/model/base-entity';


@Injectable({ providedIn: 'root' })
export class MhaShoppingCartService extends MhaProxyClassService<ShoppingCart, Workspace, Profil> {

    constructor(protected shoppingCartLocalService: MhaShoppingCartLocalService,
        protected shoppingCartServerService: MhaShoppingCartServerService,
        protected workspaceService: MhaWorkspaceLocalService,
        protected shoppingItemService: MhaShoppingItemService) {
        super(shoppingCartLocalService, shoppingCartServerService, [shoppingItemService])
    }

    async getShoppingCartSummary(shoppingCart: ShoppingCart): Promise<ShoppingCartSummary> {
        let shoppingCartSummary = new ShoppingCartSummary(shoppingCart);
        let shoppingItems = await this.shoppingItemService.findWhereParent([shoppingCart]).toPromise();
        let activeShoppingItems = shoppingItems.body.filter(si => si.syncStatus != SyncStatus.DELETE && !si.checked);
        shoppingCartSummary.addShoppingItems(activeShoppingItems);
        
        return Promise.resolve(shoppingCartSummary);
    
    }
}