import { MhaEntityInterfaceService } from '../mha-entity.service/mha-entity-interface.service';
import { Workspace } from 'src/app/pages/entities/workspace';
import { Profil } from 'src/app/pages/entities/profil';
import { ShoppingCart } from 'src/app/pages/entities/shopping-cart';

export interface MhaShoppingCartInterfaceService extends MhaEntityInterfaceService<ShoppingCart, Workspace, Profil> {
    
}
