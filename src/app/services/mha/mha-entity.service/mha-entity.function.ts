import { BaseEntity } from "src/model/base-entity";
import * as moment from 'moment';



export function beforeCreateOrUpdate(entity: BaseEntity): BaseEntity {
    entity.lastUpdate = moment();
    const copy: BaseEntity = Object.assign({}, entity, {
        lastUpdate: entity.lastUpdate != null ? moment(entity.lastUpdate).format("YYYY-MM-DD") : null,
    });
    return copy;
}