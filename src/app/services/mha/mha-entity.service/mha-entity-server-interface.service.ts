import { BaseEntity } from 'src/model/base-entity';
import { MhaEntityInterfaceService } from './mha-entity-interface.service';

export interface MhaEntityServerInterfaceService<T extends BaseEntity, P extends BaseEntity, R extends BaseEntity> extends MhaEntityInterfaceService<T, P, R> {

}