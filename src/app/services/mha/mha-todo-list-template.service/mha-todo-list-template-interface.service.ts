import { Profil } from 'src/app/pages/entities/profil';
import { TodoListTemplate } from 'src/app/pages/entities/todo-list-template';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaEntityInterfaceService } from '../mha-entity.service/mha-entity-interface.service';

export interface MhaTodoListTemplateInterfaceService extends MhaEntityInterfaceService<TodoListTemplate, Workspace, Profil> {
    
}
