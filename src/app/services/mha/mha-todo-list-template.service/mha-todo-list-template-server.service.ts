import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { TodoListTemplate, TodoListTemplateService } from 'src/app/pages/entities/todo-list-template';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaEntityServerInterfaceService } from 'src/app/services/mha/mha-entity.service/mha-entity-server-interface.service';
import { MhaEntityServerRecursiveCallService } from '../mha-entity.service/mha-entity-server-recursive.service';
import { beforeCreateOrUpdate } from '../mha-entity.service/mha-entity.function';

@Injectable({ providedIn: 'root' })
export class MhaTodoListTemplateServerService extends TodoListTemplateService implements MhaEntityServerInterfaceService<TodoListTemplate, Workspace, Profil> {


    constructor(protected http: HttpClient,
        protected recursiveRetriever: MhaEntityServerRecursiveCallService<TodoListTemplate, Workspace, Profil>) {
        super(http);
    }

    findWhereParent(parents: Workspace[], refreshDate: moment.Moment, profil: Profil): Observable<HttpResponse<TodoListTemplate[]>> {
        return this.recursiveRetriever.findWhereParent(this, parents,'workspaceId.in', refreshDate, profil, 'ownerId.equals');
    }

    create(entity: TodoListTemplate): Observable<HttpResponse<TodoListTemplate>> {
        return super.create(beforeCreateOrUpdate(entity));
    }

    update(entity: TodoListTemplate): Observable<HttpResponse<TodoListTemplate>> {
        return super.update(beforeCreateOrUpdate(entity));
    }


}
