import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { Observable, of } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { TodoListTemplate } from 'src/app/pages/entities/todo-list-template';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaEntityLocalImplService } from 'src/app/services/mha/mha-entity.service/mha-entity-local-impl.service';
import { MhaTodoListTemplateInterfaceService } from './mha-todo-list-template-interface.service';


@Injectable({ providedIn: 'root' })
export class MhaTodoListTemplateLocalService extends MhaEntityLocalImplService<TodoListTemplate, Workspace, Profil> implements MhaTodoListTemplateInterfaceService  {
    
    

    @LocalStorage('dbTodoListTemplate')
    entities: TodoListTemplate[];

    @LocalStorage('dbTodoListTemplateId')
    newId: number;

    constructor() {
        super();
    }

    findWhereParent(parents: Workspace[]): Observable<HttpResponse<TodoListTemplate[]>> {
        return of(new HttpResponse(
            { body: this.entities.filter(tp => parents.some(w => {return w.id == tp.workspaceId;})) }
        ));
    }

    changeParentId(oldParentId: number, newParentId: number) {
        this.entities.forEach(t => {
            if (t.workspaceId == oldParentId) {
                t.workspaceId = newParentId;
            }
        });
        this.entities = this.entities;
    }

    sort() {
        // does nothing
    }

    
}