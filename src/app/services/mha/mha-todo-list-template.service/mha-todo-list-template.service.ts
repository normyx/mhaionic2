import { Injectable } from '@angular/core';
import { Profil } from 'src/app/pages/entities/profil';
import { TodoListTemplate } from 'src/app/pages/entities/todo-list-template';
import { Workspace } from 'src/app/pages/entities/workspace';
import { SyncStatus } from 'src/model/base-entity';
import { TodoListTemplateSummary } from 'src/model/todo-list-template-summary-model';
import { MhaProxyClassService } from '../mha-entity.service/mha-entity-proxy.service';
import { MhaTodoTemplateService } from '../mha-todo-template.service/mha-todo-template.service';
import { MhaWorkspaceLocalService } from '../mha-workspace.service/mha-workspace-local.service';
import { MhaTodoListTemplateLocalService } from './mha-todo-list-template-local.service';
import { MhaTodoListTemplateServerService } from './mha-todo-list-template-server.service';


@Injectable({ providedIn: 'root' })
export class MhaTodoListTemplateService extends MhaProxyClassService<TodoListTemplate, Workspace, Profil> {

    constructor(protected todoListTemplateLocalService: MhaTodoListTemplateLocalService,
        protected todoListTemplateServerService: MhaTodoListTemplateServerService,
        protected workspaceService: MhaWorkspaceLocalService,
        protected todoTemplateService: MhaTodoTemplateService) {
        super(todoListTemplateLocalService, todoListTemplateServerService, [todoTemplateService])
    }

    async getTodoListTemplateSummary(todoListTemplate: TodoListTemplate): Promise<TodoListTemplateSummary> {
        let todoListTemplateSummary = new TodoListTemplateSummary(todoListTemplate);
        let todos = await this.todoTemplateService.findWhereParent([todoListTemplate]).toPromise();
        let undeletedTodos = todos.body.filter(t => t.syncStatus != SyncStatus.DELETE);
        todoListTemplateSummary.addTodos(undeletedTodos);
        
        return Promise.resolve(todoListTemplateSummary);
    
    }

}