import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { SpentService, Spent } from 'src/app/pages/entities/spent';
import { Wallet } from 'src/app/pages/entities/wallet';
import { MhaEntityServerInterfaceService } from '../mha-entity.service/mha-entity-server-interface.service';
import { MhaEntityServerRecursiveCallService } from '../mha-entity.service/mha-entity-server-recursive.service';
import { beforeCreateOrUpdate } from '../mha-entity.service/mha-entity.function';
import * as moment from 'moment';
@Injectable({ providedIn: 'root' })
export class MhaSpentServerService extends SpentService implements MhaEntityServerInterfaceService<Spent, Wallet, Profil> {
    

    constructor(protected http: HttpClient,
        protected recursiveRetriever: MhaEntityServerRecursiveCallService<Spent, Wallet, Profil>) {
        super(http);
    }

    findWhereParent(parents: Wallet[], refreshDate: moment.Moment, profil: Profil): Observable<HttpResponse<Spent[]>> {
        return this.recursiveRetriever.findWhereParent(this, parents, 'walletId.in', refreshDate);
    }

    create(entity: Spent): Observable<HttpResponse<Spent>> {
        return super.create(beforeCreateOrUpdate(entity));
    }

    update(entity: Spent): Observable<HttpResponse<Spent>> {
        return super.update(beforeCreateOrUpdate(entity));
    }

}
