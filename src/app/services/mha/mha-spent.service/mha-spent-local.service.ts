import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { Observable, of } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { Spent } from 'src/app/pages/entities/spent';
import { Wallet } from 'src/app/pages/entities/wallet';
import { MhaEntityLocalImplService } from 'src/app/services/mha/mha-entity.service/mha-entity-local-impl.service';
import { MhaSpentInterfaceService } from './mha-spent-interface.service';
import * as moment from 'moment';


@Injectable({ providedIn: 'root' })
export class MhaSpentLocalService extends MhaEntityLocalImplService<Spent, Wallet, Profil> implements MhaSpentInterfaceService  {
    

    @LocalStorage('dbSpent')
    entities: Spent[];

    @LocalStorage('dbSpentId')
    newId: number;

    constructor() {
        super();
    }

    
    changeParentId(oldParentId: number, newParentId: number) {
        this.entities.forEach(t => {
            if (t.walletId == oldParentId) {
                t.walletId = newParentId;
            }
        });
        this.entities = this.entities;
    }
    findWhereParent(parents: Wallet[]): Observable<HttpResponse<Spent[]>> {
        return of(new HttpResponse(
            { body: this.entities.filter(e => parents.some(p => {return p.id == e.walletId} )) }
        ));
    }

    sort() {
        this.entities.sort((a: Spent, b: Spent) => {
            return moment(b.spentDate).diff(moment(a.spentDate));
        });
    }

    
}