import { MhaEntityInterfaceService } from 'src/app/services/mha/mha-entity.service/mha-entity-interface.service';
import { Task } from '../../../pages/entities/task';
import { TaskProject } from 'src/app/pages/entities/task-project';
import { Profil } from 'src/app/pages/entities/profil';

export interface MhaTaskInterfaceService extends MhaEntityInterfaceService<Task, TaskProject, Profil> {

    
}
