import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { LocalStorage } from 'ngx-webstorage';
import { Observable, of } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { TaskProject } from 'src/app/pages/entities/task-project';
import { MhaEntityLocalImplService } from 'src/app/services/mha/mha-entity.service/mha-entity-local-impl.service';
import { Task } from '../../../pages/entities/task';
import { MhaTaskInterfaceService } from './mha-task-interface.service';


@Injectable({ providedIn: 'root' })
export class MhaTaskLocalService extends MhaEntityLocalImplService<Task, TaskProject, Profil> implements MhaTaskInterfaceService {


    @LocalStorage('dbTask')
    entities: Task[];

    @LocalStorage('dbTaskId')
    newId: number;

    constructor() {
        super();
    }

    changeParentId(oldParentId: number, newParentId: number) {
        this.entities.forEach(t => {
            if (t.taskProjectId == oldParentId) {
                t.taskProjectId = newParentId;
            }
        });
        this.entities = this.entities;
    }
    findWhereParent(parents: TaskProject[]): Observable<HttpResponse<Task[]>> {
        return of(new HttpResponse(
            { body: this.entities.filter(t => parents.some(p => { return p.id == t.taskProjectId; })) }
        ));
    }
    sort() {
        this.entities.sort((a: Task, b: Task) => {
            if (a.done == b.done) {
                return moment(a.dueDate).diff(moment(b.dueDate));
            } else {
                if (a.done) {
                    return 1;
                } else {
                    return -1;
                }
            }
        });
    }
}