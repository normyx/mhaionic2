import { Injectable } from '@angular/core';
import { Task } from '../../../pages/entities/task';
import { TaskProject } from '../../../pages/entities/task-project';
import { MhaProxyClassService } from '../mha-entity.service/mha-entity-proxy.service';
import { MhaTaskLocalService } from './mha-task-local.service';
import { MhaTaskServerService } from './mha-task-server.service';
import { Profil } from 'src/app/pages/entities/profil';
import { MhaActiveProfilService } from '../mha-active-profil.service/mha-active-profil.service';
import { MhaTaskProjectService } from '../mha-task-project.service/mha-task-project.service';

@Injectable({ providedIn: 'root' })
export class MhaTaskService extends MhaProxyClassService<Task, TaskProject, Profil>  {

  constructor(protected taskLocalService: MhaTaskLocalService,
    protected taskServerService: MhaTaskServerService) {
    super(taskLocalService, taskServerService, [])
  }

  getTasksFromOwner(owner: Profil): Task[] {
    let tasks = this.taskLocalService.entities.filter(t => t.owners.some(o => o.id == owner.id && !t.done))
      .sort((t1, t2) => t1.id - t2.id);

    tasks.slice(5);
    return tasks;

  }

  canMarkDone(task: Task, taskProject: TaskProject, activeProfil: Profil): boolean {
    if (activeProfil) {
      if (!task.owners || task.owners.length == 0) {
        for (let projectOwner of taskProject.owners) {
          if (projectOwner.id == activeProfil.id) return true;
        }
        return false;
      }
      for (let owner of task.owners) {
        if (owner.id == activeProfil.id) return true;
      }
      return false;
    }
  }
}