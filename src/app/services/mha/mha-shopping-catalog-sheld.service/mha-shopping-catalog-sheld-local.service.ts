import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { Observable, of } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { MhaEntityLocalImplService } from 'src/app/services/mha/mha-entity.service/mha-entity-local-impl.service';
import { ShoppingCatalogSheld } from 'src/app/pages/entities/shopping-catalog-sheld';
import { ShoppingCatalogCart } from 'src/app/pages/entities/shopping-catalog-cart';
import { MhaShoppingCatalogSheldInterfaceService } from './mha-shopping-catalog-sheld-interface.service';


@Injectable({ providedIn: 'root' })
export class MhaShoppingCatalogSheldLocalService extends MhaEntityLocalImplService<ShoppingCatalogSheld, ShoppingCatalogCart, Profil> implements MhaShoppingCatalogSheldInterfaceService  {
    
    

    @LocalStorage('dbShoppingCatalogSheld')
    entities: ShoppingCatalogSheld[];

    @LocalStorage('dbShoppingCatalogSheldId')
    newId: number;

    constructor() {
        super();
    }

    findWhereParent(parents: ShoppingCatalogCart[]): Observable<HttpResponse<ShoppingCatalogSheld[]>> {
        return of(new HttpResponse(
            { body: this.entities.filter(t => parents.some(sc => {return sc.id == t.shoppingCatalogCartId;})) }
        ));
    }

    changeParentId(oldParentId: number, newParentId: number) {
        this.entities.forEach(t => {
            if (t.shoppingCatalogCartId == oldParentId) {
                t.shoppingCatalogCartId = newParentId;
            }
        });
        this.entities = this.entities;
    }

    sort() {
        // does nothing
    }

    
}