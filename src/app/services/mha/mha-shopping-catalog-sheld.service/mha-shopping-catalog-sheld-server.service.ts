import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { ShoppingCatalogCart } from 'src/app/pages/entities/shopping-catalog-cart';
import { ShoppingCatalogSheld, ShoppingCatalogSheldService } from 'src/app/pages/entities/shopping-catalog-sheld';
import { MhaEntityServerInterfaceService } from 'src/app/services/mha/mha-entity.service/mha-entity-server-interface.service';
import { MhaEntityServerRecursiveCallService } from '../mha-entity.service/mha-entity-server-recursive.service';
import { beforeCreateOrUpdate } from '../mha-entity.service/mha-entity.function';
@Injectable({ providedIn: 'root' })
export class MhaShoppingCatalogSheldServerService extends ShoppingCatalogSheldService implements MhaEntityServerInterfaceService<ShoppingCatalogSheld, ShoppingCatalogCart, Profil> {


    constructor(protected http: HttpClient,
        protected recursiveRetriever: MhaEntityServerRecursiveCallService<ShoppingCatalogSheld, ShoppingCatalogCart, Profil>) {
        super(http);
    }

    findWhereParent(parents: ShoppingCatalogCart[], refreshDate: moment.Moment): Observable<HttpResponse<ShoppingCatalogSheld[]>> {
        return this.recursiveRetriever.findWhereParent(this, parents,'shoppingCatalogCartId.in', refreshDate);
    }

    create(entity: ShoppingCatalogSheld): Observable<HttpResponse<ShoppingCatalogSheld>> {
        return super.create(beforeCreateOrUpdate(entity));
    }

    update(entity: ShoppingCatalogSheld): Observable<HttpResponse<ShoppingCatalogSheld>> {
        return super.update(beforeCreateOrUpdate(entity));
    }

}
