import { Injectable } from '@angular/core';
import { Profil } from 'src/app/pages/entities/profil';
import { ShoppingCatalogCart } from 'src/app/pages/entities/shopping-catalog-cart';
import { ShoppingCatalogSheld } from 'src/app/pages/entities/shopping-catalog-sheld';
import { MhaProxyClassService } from '../mha-entity.service/mha-entity-proxy.service';
import { MhaShoppingCatalogItemService } from '../mha-shopping-catalog-item.service/mha-shopping-catalog-item.service';
import { MhaShoppingCatalogSheldLocalService } from './mha-shopping-catalog-sheld-local.service';
import { MhaShoppingCatalogSheldServerService } from './mha-shopping-catalog-sheld-server.service';

@Injectable({ providedIn: 'root' })
export class MhaShoppingCatalogSheldService extends MhaProxyClassService<ShoppingCatalogSheld, ShoppingCatalogCart, Profil> {

    constructor(protected shoppingCatalogSheldLocalService: MhaShoppingCatalogSheldLocalService,
        protected shoppingCatalogSheldServerService: MhaShoppingCatalogSheldServerService,
        protected shoppingCatalogItemService: MhaShoppingCatalogItemService) {
        super(shoppingCatalogSheldLocalService, shoppingCatalogSheldServerService, [shoppingCatalogItemService])
    }

    getDefaultCatalogSheld(shoppingCatalogCartId: number): ShoppingCatalogSheld {
        let scsFromShoppingCatalogCart =  this.shoppingCatalogSheldLocalService.entities.filter(scs => scs.shoppingCatalogCartId == shoppingCatalogCartId);
        let defaultScs = scsFromShoppingCatalogCart.find(scs => scs.isDefault);
        if (!defaultScs) {
            defaultScs = scsFromShoppingCatalogCart[0];
        }
        return defaultScs;
    }
}