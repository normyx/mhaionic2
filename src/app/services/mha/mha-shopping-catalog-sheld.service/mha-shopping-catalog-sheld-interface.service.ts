import { Profil } from 'src/app/pages/entities/profil';
import { ShoppingCatalogCart } from 'src/app/pages/entities/shopping-catalog-cart';
import { ShoppingCatalogSheld } from 'src/app/pages/entities/shopping-catalog-sheld';
import { MhaEntityInterfaceService } from '../mha-entity.service/mha-entity-interface.service';

export interface MhaShoppingCatalogSheldInterfaceService extends MhaEntityInterfaceService<ShoppingCatalogSheld, ShoppingCatalogCart, Profil> {
    
}
