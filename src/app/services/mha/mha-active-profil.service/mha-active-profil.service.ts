import { Injectable } from '@angular/core';
import { Profil } from 'src/app/pages/entities/profil';
import { MhaProxyClassService } from '../mha-entity.service/mha-entity-proxy.service';
import { MhaWorkspaceService } from '../mha-workspace.service/mha-workspace.service';
import { MhaActiveProfilLocalService } from './mha-active-profil-local.service';
import { MhaActiveProfilServerService } from './mha-active-profil-server.service';
import { MhaProfilService } from '../mha-profil.service/mha-profil.service';


@Injectable({ providedIn: 'root' })
export class MhaActiveProfilService extends MhaProxyClassService<Profil, Profil, Profil> {

    constructor(protected profilLocalService: MhaActiveProfilLocalService,
        protected profilServerService: MhaActiveProfilServerService,
        protected workspaceService: MhaWorkspaceService,
        protected profilService: MhaProfilService) {
        super(profilLocalService, profilServerService, [workspaceService, profilService])
    }


    
    public getStoredProfil(): Profil {
        return this.profilLocalService.get(0);

    }

    public resetStoredProfil() {
        // this.$sessionStorage.clear(this.profilKey);
    }
}