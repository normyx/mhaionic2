import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { Observable, of } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { MhaEntityLocalImplService } from 'src/app/services/mha/mha-entity.service/mha-entity-local-impl.service';
import { TaskProject } from '../../../pages/entities/task-project';
import { MhaActiveProfilInterfaceService } from './mha-active-profil-interface.service';


@Injectable({ providedIn: 'root' })
export class MhaActiveProfilLocalService extends MhaEntityLocalImplService<Profil, Profil, Profil> implements MhaActiveProfilInterfaceService  {
    
    
    

    @LocalStorage('dbActiveProfil')
    entities: Profil[];

    @LocalStorage('dbActiveProfilId')
    newId: number;

    constructor() {
        super();
    }

    findWhereParent(): Observable<HttpResponse<Profil[]>> {
        if (this.entities && this.entities.length >1) {
            throw new Error("Size of the Profil is not good :"+ this.entities.length);
        }
        return of(new HttpResponse(
            { body: this.entities }
        ));
    }

    changeParentId(oldParentId: number, newParentId: number) {
        throw new Error("No Parent for Profil. Root of the tree !");
    }

    sort() {
        // does nothing
    }

    
}