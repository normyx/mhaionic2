import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Profil, ProfilService } from 'src/app/pages/entities/profil';
import { MhaEntityServerInterfaceService } from 'src/app/services/mha/mha-entity.service/mha-entity-server-interface.service';
import { ApiService } from '../../api/api.service';
import { MhaActiveProfilInterfaceService } from './mha-active-profil-interface.service';

@Injectable({ providedIn: 'root' })
export class MhaActiveProfilServerService extends ProfilService implements MhaEntityServerInterfaceService<Profil, Profil, Profil>, MhaActiveProfilInterfaceService {


    constructor(protected http: HttpClient) {
        super(http);
    }

    findWhereParent(): Observable<HttpResponse<Profil[]>> {
        return this.http.get(ApiService.API_URL + "/mysha-profil-where-logged-user", { observe: 'response' }).pipe(
            filter((res: HttpResponse<Profil>) => res.ok),
            map((res: HttpResponse<Profil>) => new HttpResponse({ body: [res.body] }))
        );


    }

    


}
