import { TaskProject } from '../../../pages/entities/task-project';
import { MhaEntityInterfaceService } from '../mha-entity.service/mha-entity-interface.service';
import { Workspace } from 'src/app/pages/entities/workspace';
import { Profil } from 'src/app/pages/entities/profil';

export interface MhaTaskProjectInterfaceService extends MhaEntityInterfaceService<TaskProject, Workspace, Profil> {
    
}
