import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { Observable, of } from 'rxjs';
import { MhaEntityLocalImplService } from 'src/app/services/mha/mha-entity.service/mha-entity-local-impl.service';
import { TaskProject } from '../../../pages/entities/task-project';
import { MhaTaskProjectInterfaceService } from './mha-task-project-interface.service';
import { Workspace } from 'src/app/pages/entities/workspace';
import { Profil } from 'src/app/pages/entities/profil';


@Injectable({ providedIn: 'root' })
export class MhaTaskProjectLocalService extends MhaEntityLocalImplService<TaskProject, Workspace, Profil> implements MhaTaskProjectInterfaceService  {
    
    

    @LocalStorage('dbTaskProject')
    entities: TaskProject[];

    @LocalStorage('dbTaskProjectId')
    newId: number;

    constructor() {
        super();
    }

    findWhereParent(parents: Workspace[]): Observable<HttpResponse<TaskProject[]>> {
        return of(new HttpResponse(
            { body: this.entities.filter(tp => parents.some(w => {return w.id == tp.workspaceId;})) }
        ));
    }

    changeParentId(oldParentId: number, newParentId: number) {
        this.entities.forEach(t => {
            if (t.workspaceId == oldParentId) {
                t.workspaceId = newParentId;
            }
        });
        this.entities = this.entities;
    }

    sort() {
        // does nothing
    }

    
}