import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaEntityServerInterfaceService } from 'src/app/services/mha/mha-entity.service/mha-entity-server-interface.service';
import { TaskProject, TaskProjectService } from '../../../pages/entities/task-project';
import { MhaEntityServerRecursiveCallService } from '../mha-entity.service/mha-entity-server-recursive.service';
import { beforeCreateOrUpdate } from '../mha-entity.service/mha-entity.function';
import * as moment from 'moment';
@Injectable({ providedIn: 'root' })
export class MhaTaskProjectServerService extends TaskProjectService implements MhaEntityServerInterfaceService<TaskProject, Workspace, Profil> {


    constructor(protected http: HttpClient,
        protected recursiveRetriever: MhaEntityServerRecursiveCallService<TaskProject, Workspace, Profil>) {
        super(http);
    }

    findWhereParent(parents: Workspace[], refreshDate: moment.Moment, profil: Profil): Observable<HttpResponse<TaskProject[]>> {
        return this.recursiveRetriever.findWhereParent(this, parents,'workspaceId.in', refreshDate, profil, 'ownerId.equals');
    }

    create(entity: TaskProject): Observable<HttpResponse<TaskProject>> {
        return super.create(beforeCreateOrUpdate(entity));
    }

    update(entity: TaskProject): Observable<HttpResponse<TaskProject>> {
        return super.update(beforeCreateOrUpdate(entity));
    }

}
