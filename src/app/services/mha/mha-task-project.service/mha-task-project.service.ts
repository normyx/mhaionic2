import { Injectable } from '@angular/core';
import { Workspace } from 'src/app/pages/entities/workspace';
import { TaskProject } from '../../../pages/entities/task-project';
import { MhaProxyClassService } from '../mha-entity.service/mha-entity-proxy.service';
import { MhaTaskService } from '../mha-task.service/mha-task.service';
import { MhaWorkspaceLocalService } from '../mha-workspace.service/mha-workspace-local.service';
import { MhaTaskProjectLocalService } from './mha-task-project-local.service';
import { MhaTaskProjectServerService } from './mha-task-project-server.service';
import { Profil } from 'src/app/pages/entities/profil';
import { TaskProjectSummary } from 'src/model/task-project-summary-model';
import { SyncStatus } from 'src/model/base-entity';

@Injectable({ providedIn: 'root' })
export class MhaTaskProjectService extends MhaProxyClassService<TaskProject, Workspace, Profil> {

    constructor(protected taskProjectLocalService: MhaTaskProjectLocalService,
        protected taskProjectServerService: MhaTaskProjectServerService,
        protected workspaceService: MhaWorkspaceLocalService,
        protected taskService: MhaTaskService) {
        super(taskProjectLocalService, taskProjectServerService, [taskService])
    }

    async getTaskProjectSummary(taskProject: TaskProject): Promise<TaskProjectSummary> {
        let taskProjectSummary = new TaskProjectSummary(taskProject);
        let tasks = await this.taskService.findWhereParent([taskProject]).toPromise();
        let undeletedTasks = tasks.body.filter(t => t.syncStatus != SyncStatus.DELETE);
        taskProjectSummary.addTasks(undeletedTasks);
        
        return Promise.resolve(taskProjectSummary);
    
    }

}