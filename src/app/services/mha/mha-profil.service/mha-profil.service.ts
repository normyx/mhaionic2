import { Injectable } from '@angular/core';
import { Profil } from 'src/app/pages/entities/profil';
import { MhaProxyClassService } from '../mha-entity.service/mha-entity-proxy.service';
import { MhaProfilLocalService } from './mha-profil-local.service';
import { MhaProfilServerService } from './mha-profil-server.service';


@Injectable({ providedIn: 'root' })
export class MhaProfilService extends MhaProxyClassService<Profil, Profil, Profil> {

    constructor(protected profilLocalService: MhaProfilLocalService,
        protected profilServerService: MhaProfilServerService) {
        super(profilLocalService, profilServerService, [])
    }


}