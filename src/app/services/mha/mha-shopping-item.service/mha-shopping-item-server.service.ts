import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { ShoppingCart } from 'src/app/pages/entities/shopping-cart';
import { ShoppingItem, ShoppingItemService } from 'src/app/pages/entities/shopping-item';
import { MhaEntityServerInterfaceService } from 'src/app/services/mha/mha-entity.service/mha-entity-server-interface.service';
import { MhaEntityServerRecursiveCallService } from '../mha-entity.service/mha-entity-server-recursive.service';
import { beforeCreateOrUpdate } from '../mha-entity.service/mha-entity.function';
@Injectable({ providedIn: 'root' })
export class MhaShoppingItemServerService extends ShoppingItemService implements MhaEntityServerInterfaceService<ShoppingItem, ShoppingCart, Profil> {


    constructor(protected http: HttpClient,
        protected recursiveRetriever: MhaEntityServerRecursiveCallService<ShoppingItem, ShoppingCart, Profil>) {
        super(http);
    }

    findWhereParent(parents: ShoppingCart[], refreshDate: moment.Moment): Observable<HttpResponse<ShoppingItem[]>> {
        return this.recursiveRetriever.findWhereParent(this, parents,'shoppingCartId.in', refreshDate);
    }

    create(entity: ShoppingItem): Observable<HttpResponse<ShoppingItem>> {
        return super.create(beforeCreateOrUpdate(entity));
    }

    update(entity: ShoppingItem): Observable<HttpResponse<ShoppingItem>> {
        return super.update(beforeCreateOrUpdate(entity));
    }

}
