import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { Observable, of } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { Todo } from 'src/app/pages/entities/todo';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaEntityLocalImplService } from 'src/app/services/mha/mha-entity.service/mha-entity-local-impl.service';
import { MhaShoppingItemInterfaceService } from './mha-shopping-item-interface.service';
import { ShoppingItem } from 'src/app/pages/entities/shopping-item';
import { ShoppingCart } from 'src/app/pages/entities/shopping-cart';


@Injectable({ providedIn: 'root' })
export class MhaShoppingItemLocalService extends MhaEntityLocalImplService<ShoppingItem, ShoppingCart, Profil> implements MhaShoppingItemInterfaceService  {
    
    

    @LocalStorage('dbShoppingItem')
    entities: ShoppingItem[];

    @LocalStorage('dbShoppingItemId')
    newId: number;

    constructor() {
        super();
    }

    findWhereParent(parents: ShoppingCart[]): Observable<HttpResponse<ShoppingItem[]>> {
        return of(new HttpResponse(
            { body: this.entities.filter(t => parents.some(sc => {return sc.id == t.shoppingCartId;})) }
        ));
    }

    changeParentId(oldParentId: number, newParentId: number) {
        this.entities.forEach(t => {
            if (t.shoppingCartId == oldParentId) {
                t.shoppingCartId = newParentId;
            }
        });
        this.entities = this.entities;
    }

    sort() {
        // does nothing
    }

    
}