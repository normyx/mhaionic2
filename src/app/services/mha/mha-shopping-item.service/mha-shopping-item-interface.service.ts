import { Profil } from 'src/app/pages/entities/profil';
import { ShoppingCart } from 'src/app/pages/entities/shopping-cart';
import { ShoppingItem } from 'src/app/pages/entities/shopping-item';
import { MhaEntityInterfaceService } from '../mha-entity.service/mha-entity-interface.service';

export interface MhaShoppingItemInterfaceService extends MhaEntityInterfaceService<ShoppingItem, ShoppingCart, Profil> {
    
}
