import { Injectable } from '@angular/core';
import { Profil } from 'src/app/pages/entities/profil';
import { ShoppingCart } from 'src/app/pages/entities/shopping-cart';
import { ShoppingItem } from 'src/app/pages/entities/shopping-item';
import { MhaProxyClassService } from '../mha-entity.service/mha-entity-proxy.service';
import { MhaWorkspaceLocalService } from '../mha-workspace.service/mha-workspace-local.service';
import { MhaShoppingItemLocalService } from './mha-shopping-item-local.service';
import { MhaShoppingItemServerService } from './mha-shopping-item-server.service';

@Injectable({ providedIn: 'root' })
export class MhaShoppingItemService extends MhaProxyClassService<ShoppingItem, ShoppingCart, Profil> {

    constructor(protected shoppingItemLocalService: MhaShoppingItemLocalService,
        protected shoppingItemServerService: MhaShoppingItemServerService,
        protected workspaceService: MhaWorkspaceLocalService) {
        super(shoppingItemLocalService, shoppingItemServerService, [])
    }

 

}