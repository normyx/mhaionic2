import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { SpentConfig } from 'src/app/pages/entities/spent-config';
import { SpentSharingConfig, SpentSharingConfigService } from 'src/app/pages/entities/spent-sharing-config';
import { MhaEntityServerInterfaceService } from '../mha-entity.service/mha-entity-server-interface.service';
import { MhaEntityServerRecursiveCallService } from '../mha-entity.service/mha-entity-server-recursive.service';
import { beforeCreateOrUpdate } from '../mha-entity.service/mha-entity.function';
import * as moment from 'moment';
@Injectable({ providedIn: 'root' })
export class MhaSpentSharingConfigServerService extends SpentSharingConfigService implements MhaEntityServerInterfaceService<SpentSharingConfig, SpentConfig, Profil> {

    

    constructor(protected http: HttpClient,
        protected recursiveRetriever: MhaEntityServerRecursiveCallService<SpentSharingConfig, SpentConfig, Profil>) {
        super(http);
    }

    findWhereParent(parents: SpentConfig[], refreshDate: moment.Moment, profil: Profil): Observable<HttpResponse<SpentSharingConfig[]>> {
        return this.recursiveRetriever.findWhereParent(this, parents,'spentConfigId.in', refreshDate);
    }

    create(entity: SpentSharingConfig): Observable<HttpResponse<SpentSharingConfig>> {
        return super.create(beforeCreateOrUpdate(entity));
    }

    update(entity: SpentSharingConfig): Observable<HttpResponse<SpentSharingConfig>> {
        return super.update(beforeCreateOrUpdate(entity));
    }


}
