import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { Observable, of } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { SpentConfig } from 'src/app/pages/entities/spent-config';
import { SpentSharingConfig } from 'src/app/pages/entities/spent-sharing-config';
import { MhaEntityLocalImplService } from 'src/app/services/mha/mha-entity.service/mha-entity-local-impl.service';
import { MhaSpentSharingConfigInterfaceService } from './mha-spent-sharing-config-interface.service';


@Injectable({ providedIn: 'root' })
export class MhaSpentSharingConfigLocalService extends MhaEntityLocalImplService<SpentSharingConfig, SpentConfig, Profil> implements MhaSpentSharingConfigInterfaceService  {
    

    @LocalStorage('dbSpentSharingConfig')
    entities: SpentSharingConfig[];

    @LocalStorage('dbSpentSharingConfigId')
    newId: number;

    constructor() {
        super();
    }

    
    changeParentId(oldParentId: number, newParentId: number) {
        this.entities.forEach(t => {
            if (t.spentConfigId == oldParentId) {
                t.spentConfigId = newParentId;
            }
        });
        this.entities = this.entities;
    }
    findWhereParent(parents: SpentConfig[]): Observable<HttpResponse<SpentSharingConfig[]>> {
        return of(new HttpResponse(
            { body: this.entities.filter(e => parents.some(p => {return p.id == e.spentConfigId} )) }
        ));
    }

    sort() {
        // does nothing
    }

    
}