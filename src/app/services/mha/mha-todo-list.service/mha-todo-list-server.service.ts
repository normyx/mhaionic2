import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaEntityServerInterfaceService } from 'src/app/services/mha/mha-entity.service/mha-entity-server-interface.service';
import { MhaEntityServerRecursiveCallService } from '../mha-entity.service/mha-entity-server-recursive.service';
import { TodoListService, TodoList } from 'src/app/pages/entities/todo-list';
import { beforeCreateOrUpdate } from '../mha-entity.service/mha-entity.function';
import * as moment from 'moment';

@Injectable({ providedIn: 'root' })
export class MhaTodoListServerService extends TodoListService implements MhaEntityServerInterfaceService<TodoList, Workspace, Profil> {


    constructor(protected http: HttpClient,
        protected recursiveRetriever: MhaEntityServerRecursiveCallService<TodoList, Workspace, Profil>) {
        super(http);
    }

    findWhereParent(parents: Workspace[], refreshDate: moment.Moment, profil: Profil): Observable<HttpResponse<TodoList[]>> {
        return this.recursiveRetriever.findWhereParent(this, parents,'workspaceId.in', refreshDate, profil, 'ownerId.equals');
    }

    create(entity: TodoList): Observable<HttpResponse<TodoList>> {
        return super.create(beforeCreateOrUpdate(entity));
    }

    update(entity: TodoList): Observable<HttpResponse<TodoList>> {
        return super.update(beforeCreateOrUpdate(entity));
    }


}
