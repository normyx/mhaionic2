import { Profil } from 'src/app/pages/entities/profil';
import { TodoList } from 'src/app/pages/entities/todo-list';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaEntityInterfaceService } from '../mha-entity.service/mha-entity-interface.service';

export interface MhaTodoListInterfaceService extends MhaEntityInterfaceService<TodoList, Workspace, Profil> {
    
}
