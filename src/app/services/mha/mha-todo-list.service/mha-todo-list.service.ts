import { Injectable } from '@angular/core';
import { Profil } from 'src/app/pages/entities/profil';
import { TodoList } from 'src/app/pages/entities/todo-list';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaProxyClassService } from '../mha-entity.service/mha-entity-proxy.service';
import { MhaTodoService } from '../mha-todo.service/mha-todo.service';
import { MhaWorkspaceLocalService } from '../mha-workspace.service/mha-workspace-local.service';
import { MhaTodoListLocalService } from './mha-todo-list-local.service';
import { MhaTodoListServerService } from './mha-todo-list-server.service';
import { TodoListSummary } from 'src/model/todo-list-summary-model';
import { SyncStatus } from 'src/model/base-entity';


@Injectable({ providedIn: 'root' })
export class MhaTodoListService extends MhaProxyClassService<TodoList, Workspace, Profil> {

    constructor(protected taskProjectLocalService: MhaTodoListLocalService,
        protected todoListServerService: MhaTodoListServerService,
        protected workspaceService: MhaWorkspaceLocalService,
        protected todoService: MhaTodoService) {
        super(taskProjectLocalService, todoListServerService, [todoService])
    }

    async getTodoListSummary(todoList: TodoList): Promise<TodoListSummary> {
        let todoListSummary = new TodoListSummary(todoList);
        let todos = await this.todoService.findWhereParent([todoList]).toPromise();
        let undeletedTodos = todos.body.filter(t => t.syncStatus != SyncStatus.DELETE);
        todoListSummary.addTodos(undeletedTodos);
        
        return Promise.resolve(todoListSummary);
    
    }

}