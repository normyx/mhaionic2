import { Injectable } from '@angular/core';
import { Profil } from 'src/app/pages/entities/profil';
import { Spent } from 'src/app/pages/entities/spent';
import { SpentSharing } from 'src/app/pages/entities/spent-sharing';
import { MhaProxyClassService } from '../mha-entity.service/mha-entity-proxy.service';
import { MhaSpentSharingLocalService } from './mha-spent-sharing-local.service';
import { MhaSpentSharingServerService } from './mha-spent-sharing-server.service';

@Injectable({ providedIn: 'root' })
export class MhaSpentSharingService extends MhaProxyClassService<SpentSharing, Spent, Profil> {

    constructor(protected spentSharingLocalService: MhaSpentSharingLocalService,
        protected spentSharingServerService: MhaSpentSharingServerService) {
        super(spentSharingLocalService, spentSharingServerService, []);
    }

}