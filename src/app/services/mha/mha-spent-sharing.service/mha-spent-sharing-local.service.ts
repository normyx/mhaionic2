import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { Observable, of } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { Spent } from 'src/app/pages/entities/spent';
import { SpentSharing } from 'src/app/pages/entities/spent-sharing';
import { MhaEntityLocalImplService } from 'src/app/services/mha/mha-entity.service/mha-entity-local-impl.service';
import { MhaSpentSharingInterfaceService } from './mha-spent-sharing-interface.service';
import { conditionalExpression } from '@babel/types';


@Injectable({ providedIn: 'root' })
export class MhaSpentSharingLocalService extends MhaEntityLocalImplService<SpentSharing, Spent, Profil> implements MhaSpentSharingInterfaceService  {
    

    @LocalStorage('dbSpentSharing')
    entities: SpentSharing[];

    @LocalStorage('dbSpentSharingId')
    newId: number;

    constructor() {
        super();
    }

    
    changeParentId(oldParentId: number, newParentId: number) {
        this.entities.forEach(t => {
            if (t.spentId == oldParentId) {
                t.spentId = newParentId;
            }
        });
        this.entities = this.entities;
    }
    findWhereParent(parents: Spent[]): Observable<HttpResponse<SpentSharing[]>> {
        return of(new HttpResponse(
            { body: this.entities.filter(e => parents.some(p => {return p.id == e.spentId} )) }
        ));
    }

    sort() {
        // does nothing
    }

    
}