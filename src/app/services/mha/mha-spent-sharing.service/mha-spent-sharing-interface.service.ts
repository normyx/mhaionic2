import { Profil } from 'src/app/pages/entities/profil';
import { Spent } from 'src/app/pages/entities/spent';
import { SpentSharing } from 'src/app/pages/entities/spent-sharing';
import { MhaEntityInterfaceService } from '../mha-entity.service/mha-entity-interface.service';

export interface MhaSpentSharingInterfaceService extends MhaEntityInterfaceService<SpentSharing, Spent, Profil> {
    
}
