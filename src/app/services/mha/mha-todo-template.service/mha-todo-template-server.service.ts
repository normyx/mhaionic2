import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { TodoListTemplate } from 'src/app/pages/entities/todo-list-template';
import { TodoTemplate, TodoTemplateService } from 'src/app/pages/entities/todo-template';
import { MhaEntityServerInterfaceService } from 'src/app/services/mha/mha-entity.service/mha-entity-server-interface.service';
import { MhaEntityServerRecursiveCallService } from '../mha-entity.service/mha-entity-server-recursive.service';
import { beforeCreateOrUpdate } from '../mha-entity.service/mha-entity.function';
@Injectable({ providedIn: 'root' })
export class MhaTodoTemplateServerService extends TodoTemplateService implements MhaEntityServerInterfaceService<TodoTemplate, TodoListTemplate, Profil> {


    constructor(protected http: HttpClient,
        protected recursiveRetriever: MhaEntityServerRecursiveCallService<TodoTemplate, TodoListTemplate, Profil>) {
        super(http);
    }

    findWhereParent(parents: TodoListTemplate[], refreshDate: moment.Moment): Observable<HttpResponse<TodoListTemplate[]>> {
        return this.recursiveRetriever.findWhereParent(this, parents,'todoListTemplateId.in', refreshDate);
    }

    create(entity: TodoTemplate): Observable<HttpResponse<TodoTemplate>> {
        return super.create(beforeCreateOrUpdate(entity));
    }

    update(entity: TodoTemplate): Observable<HttpResponse<TodoTemplate>> {
        return super.update(beforeCreateOrUpdate(entity));
    }

}
