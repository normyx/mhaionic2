import { Profil } from 'src/app/pages/entities/profil';
import { TodoListTemplate } from 'src/app/pages/entities/todo-list-template';
import { TodoTemplate } from 'src/app/pages/entities/todo-template';
import { MhaEntityInterfaceService } from '../mha-entity.service/mha-entity-interface.service';

export interface MhaTodoTemplateInterfaceService extends MhaEntityInterfaceService<TodoTemplate, TodoListTemplate, Profil> {
    
}
