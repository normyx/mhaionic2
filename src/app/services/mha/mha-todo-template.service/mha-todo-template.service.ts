import { Injectable } from '@angular/core';
import { Profil } from 'src/app/pages/entities/profil';
import { TodoTemplate } from 'src/app/pages/entities/todo-template';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaProxyClassService } from '../mha-entity.service/mha-entity-proxy.service';
import { MhaWorkspaceLocalService } from '../mha-workspace.service/mha-workspace-local.service';
import { MhaTodoTemplateLocalService } from './mha-todo-template-local.service';
import { MhaTodoTemplateServerService } from './mha-todo-template-server.service';

@Injectable({ providedIn: 'root' })
export class MhaTodoTemplateService extends MhaProxyClassService<TodoTemplate, Workspace, Profil> {

    constructor(protected todoTemplateLocalService: MhaTodoTemplateLocalService,
        protected todoTemplateServerService: MhaTodoTemplateServerService,
        protected workspaceService: MhaWorkspaceLocalService) {
        super(todoTemplateLocalService, todoTemplateServerService, [])
    }

 

}