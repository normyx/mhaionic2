import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalStorage } from 'ngx-webstorage';
import { Observable, of } from 'rxjs';
import { Profil } from 'src/app/pages/entities/profil';
import { TodoTemplate } from 'src/app/pages/entities/todo-template';
import { Workspace } from 'src/app/pages/entities/workspace';
import { MhaEntityLocalImplService } from 'src/app/services/mha/mha-entity.service/mha-entity-local-impl.service';
import { MhaTodoTemplateInterfaceService } from './mha-todo-template-interface.service';


@Injectable({ providedIn: 'root' })
export class MhaTodoTemplateLocalService extends MhaEntityLocalImplService<TodoTemplate, Workspace, Profil> implements MhaTodoTemplateInterfaceService  {
    
    

    @LocalStorage('dbTodoTemplate')
    entities: TodoTemplate[];

    @LocalStorage('dbTodoTemplateId')
    newId: number;

    constructor() {
        super();
    }

    findWhereParent(parents: Workspace[]): Observable<HttpResponse<TodoTemplate[]>> {
        return of(new HttpResponse(
            { body: this.entities.filter(t => parents.some(w => {return w.id == t.todoListTemplateId;})) }
        ));
    }

    changeParentId(oldParentId: number, newParentId: number) {
        this.entities.forEach(t => {
            if (t.todoListTemplateId == oldParentId) {
                t.todoListTemplateId = newParentId;
            }
        });
        this.entities = this.entities;
    }

    sort() {
        // does nothing
    }

    
}