@echo off
set "BASE_VERSION=1.1"
for /f "tokens=2 delims==" %%a in ('wmic OS Get localdatetime /value') do set "dt=%%a"
set "YY=%dt:~2,2%" & set "YYYY=%dt:~0,4%" & set "MM=%dt:~4,2%" & set "DD=%dt:~6,2%"
set "HH=%dt:~8,2%" & set "Min=%dt:~10,2%" & set "Sec=%dt:~12,2%"
set "FULL_STAMP=%YYYY%%MM%%DD%%HH%%Min%%Sec%"
set "FULL_VERSION=%BASE_VERSION%-%FULL_STAMP%"
set "APK_FILENAME=mha-%FULL_VERSION%.apk"
set "WWW_ZIP_FILENAME=mhaionic-www-%FULL_VERSION%.zip"
echo APK_FILENAME: "%APK_FILENAME%"
echo WWW_ZIP_FILENAME: "%WWW_ZIP_FILENAME%"

@setlocal

set ERROR_CODE=0


:buildIonic
@echo Start
@echo --Build
call ionic build --prod
if ERRORLEVEL 1 goto error
@echo --Copy
call npx cap copy
cd android
if ERRORLEVEL 1 goto error
@echo --Assemble
call gradlew assembleRelease
if ERRORLEVEL 1 goto error
@echo --Sign
call jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ../msha.keystore -storepass cartel2000 app/build/outputs/apk/release/app-release-unsigned.apk msha
if ERRORLEVEL   EVEL 1 goto error
@echo --Zipalign
del app\build\outputs\apk\release\msha.apk
C:\Users\mathieu.goulene\AppData\Local\Android\Sdk\build-tools\30.0.2\zipalign 4 app/build/outputs/apk/release/app-release-unsigned.apk app/build/outputs/apk/release/msha.apk
if ERRORLEVEL 1 goto error
@echo --Move
call cd ..
if ERRORLEVEL 1 goto error
move /Y android\app\build\outputs\apk\release\msha.apk %APK_FILENAME%
if ERRORLEVEL 1 goto error
@echo --Zip WWW
call 7z a -tzip -r %WWW_ZIP_FILENAME% www\*.*
if ERRORLEVEL 1 goto error
@echo --Copying APK to NAS
call pscp -pw cartel2000 -P 22 %APK_FILENAME% Mathieu@192.168.0.21:/repo/mha
if ERRORLEVEL 1 goto error
call pscp -pw cartel2000 -P 22 %APK_FILENAME% Mathieu@192.168.0.21:/repo/mha/mha.apk
if ERRORLEVEL 1 goto error
@echo --Copying WWW to NAS
call pscp -pw cartel2000 -P 22 %WWW_ZIP_FILENAME% Mathieu@192.168.0.21:/repo/mha
if ERRORLEVEL 1 goto error
@echo --Copying WWW to RPI
call pscp -pw cartel2000 -P 22 %WWW_ZIP_FILENAME% pi@192.168.0.72:/home/pi/mhapackages/
if ERRORLEVEL 1 goto error
call pscp -pw cartel2000 -P 22 %WWW_ZIP_FILENAME% pi@192.168.0.72:/home/pi/mhapackages/mhaionic-www-current.zip
if ERRORLEVEL 1 goto error
@echo --Deleting packages
del %WWW_ZIP_FILENAME%
if ERRORLEVEL 1 goto error
del %APK_FILENAME%
if ERRORLEVEL 1 goto error

:error
set ERROR_CODE=1

:end
@endlocal & set ERROR_CODE=%ERROR_CODE%

exit /B %ERROR_CODE%