import { browser, by, element, ExpectedConditions as ec } from 'protractor';
import { LoginPage } from '../../pages/login.po';
import { TodoListComponentsPage, TodoListDetailPage, TodoListUpdatePage } from './todo-list.po';

describe('TodoList e2e test', () => {
  let loginPage: LoginPage;
  let todoListComponentsPage: TodoListComponentsPage;
  let todoListUpdatePage: TodoListUpdatePage;
  let todoListDetailPage: TodoListDetailPage;
  let initNumberOfEntities: number;
  const COMPONENT_TITLE = 'Todo Lists';
  const SUBCOMPONENT_TITLE = 'Todo List';
  let lastElement: any;
  let isVisible = false;

  const label = 'label';

  beforeAll(async () => {
    loginPage = new LoginPage();
    await loginPage.navigateTo('/');
    await loginPage.signInButton.click();
    const username = process.env.E2E_USERNAME || 'admin';
    const password = process.env.E2E_PASSWORD || 'admin';
    await browser.wait(ec.elementToBeClickable(loginPage.loginButton), 3000);
    await loginPage.login(username, password);
    await browser.wait(ec.visibilityOf(loginPage.logoutButton), 1000);
  });

  it('should load TodoLists', async () => {
    // go to entity component page
    const tabEntities = element(by.css('ion-tab-button[tab="entities"]'));
    await browser.wait(ec.elementToBeClickable(tabEntities), 3000);
    await tabEntities.click();
    await element
      .all(by.css('ion-item'))
      .filter(async (el) => (await el.element(by.css('h2')).getText()) === 'TodoList')
      .first()
      .click();

    todoListComponentsPage = new TodoListComponentsPage();
    await browser.wait(ec.visibilityOf(todoListComponentsPage.title), 5000);
    expect(await todoListComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    await browser.wait(
      ec.or(ec.visibilityOf(todoListComponentsPage.entities.get(0)), ec.visibilityOf(todoListComponentsPage.noResult)),
      5000
    );
  });

  it('should create TodoList', async () => {
    initNumberOfEntities = await todoListComponentsPage.getEntitiesNumber();
    await browser.wait(ec.elementToBeClickable(todoListComponentsPage.createButton), 5000);
    await todoListComponentsPage.clickOnCreateButton();
    todoListUpdatePage = new TodoListUpdatePage();
    await browser.wait(ec.visibilityOf(todoListUpdatePage.pageTitle), 1000);
    expect(await todoListUpdatePage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

    await todoListUpdatePage.setLabelInput(label);

    await todoListUpdatePage.save();
    await browser.wait(ec.visibilityOf(todoListComponentsPage.title), 1000);
    expect(await todoListComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    expect(await todoListComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities + 1);
  });

  it('should get the last TodoList', async () => {
    todoListComponentsPage = new TodoListComponentsPage();
    await browser.wait(ec.visibilityOf(todoListComponentsPage.title), 5000);
    lastElement = await todoListComponentsPage.viewButtons.last().getWebElement();
  });

  it('should scroll the last TodoList', async () => {
    browser
      .executeScript('arguments[0].scrollIntoView()', lastElement)
      .then(async () => {
        if ((await lastElement.isEnabled()) && (await lastElement.isDisplayed())) {
          browser
            .executeScript('arguments[0].click()', lastElement)
            .then(async () => {
              isVisible = true;
            })
            .catch();
        }
      })
      .catch();
  });

  it('should view the last TodoList', async () => {
    todoListDetailPage = new TodoListDetailPage();
    if (isVisible && (await todoListDetailPage.pageTitle.isDisplayed())) {
      expect(await todoListDetailPage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

      expect(await todoListDetailPage.getLabelInput()).toEqual(label);
    }
  });

  it('should delete last TodoList', async () => {
    todoListDetailPage = new TodoListDetailPage();
    if (isVisible && (await todoListDetailPage.deleteButton.isDisplayed())) {
      await browser.executeScript('arguments[0].click()', await todoListDetailPage.deleteButton.getWebElement());

      const alertConfirmButton = element.all(by.className('alert-button')).last();

      await browser.wait(ec.elementToBeClickable(alertConfirmButton), 3000);
      alertConfirmButton.click();
      await browser.wait(ec.visibilityOf(todoListComponentsPage.title), 3000);
      expect(await todoListComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
      expect(await todoListComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities);
    }
  });

  it('finish TodoLists tests performing logout', async () => {
    // go to home page
    const tabHome = element(by.css('ion-tab-button[tab="home"]'));
    await browser.wait(ec.elementToBeClickable(tabHome), 3000);
    await tabHome.click();
    await browser.wait(ec.elementToBeClickable(loginPage.logoutButton), 3000);
    await loginPage.logout();
  });
});
