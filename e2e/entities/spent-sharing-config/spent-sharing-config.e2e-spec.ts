import { browser, by, element, ExpectedConditions as ec } from 'protractor';
import { LoginPage } from '../../pages/login.po';
import { SpentSharingConfigComponentsPage, SpentSharingConfigDetailPage, SpentSharingConfigUpdatePage } from './spent-sharing-config.po';

describe('SpentSharingConfig e2e test', () => {
  let loginPage: LoginPage;
  let spentSharingConfigComponentsPage: SpentSharingConfigComponentsPage;
  let spentSharingConfigUpdatePage: SpentSharingConfigUpdatePage;
  let spentSharingConfigDetailPage: SpentSharingConfigDetailPage;
  let initNumberOfEntities: number;
  const COMPONENT_TITLE = 'Spent Sharing Configs';
  const SUBCOMPONENT_TITLE = 'Spent Sharing Config';
  let lastElement: any;
  let isVisible = false;

  const share = '10';

  beforeAll(async () => {
    loginPage = new LoginPage();
    await loginPage.navigateTo('/');
    await loginPage.signInButton.click();
    const username = process.env.E2E_USERNAME || 'admin';
    const password = process.env.E2E_PASSWORD || 'admin';
    await browser.wait(ec.elementToBeClickable(loginPage.loginButton), 3000);
    await loginPage.login(username, password);
    await browser.wait(ec.visibilityOf(loginPage.logoutButton), 1000);
  });

  it('should load SpentSharingConfigs', async () => {
    // go to entity component page
    const tabEntities = element(by.css('ion-tab-button[tab="entities"]'));
    await browser.wait(ec.elementToBeClickable(tabEntities), 3000);
    await tabEntities.click();
    await element
      .all(by.css('ion-item'))
      .filter(async (el) => (await el.element(by.css('h2')).getText()) === 'SpentSharingConfig')
      .first()
      .click();

    spentSharingConfigComponentsPage = new SpentSharingConfigComponentsPage();
    await browser.wait(ec.visibilityOf(spentSharingConfigComponentsPage.title), 5000);
    expect(await spentSharingConfigComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    await browser.wait(
      ec.or(ec.visibilityOf(spentSharingConfigComponentsPage.entities.get(0)), ec.visibilityOf(spentSharingConfigComponentsPage.noResult)),
      5000
    );
  });

  it('should create SpentSharingConfig', async () => {
    initNumberOfEntities = await spentSharingConfigComponentsPage.getEntitiesNumber();
    await browser.wait(ec.elementToBeClickable(spentSharingConfigComponentsPage.createButton), 5000);
    await spentSharingConfigComponentsPage.clickOnCreateButton();
    spentSharingConfigUpdatePage = new SpentSharingConfigUpdatePage();
    await browser.wait(ec.visibilityOf(spentSharingConfigUpdatePage.pageTitle), 1000);
    expect(await spentSharingConfigUpdatePage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

    await spentSharingConfigUpdatePage.setShareInput(share);

    await spentSharingConfigUpdatePage.save();
    await browser.wait(ec.visibilityOf(spentSharingConfigComponentsPage.title), 1000);
    expect(await spentSharingConfigComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    expect(await spentSharingConfigComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities + 1);
  });

  it('should get the last SpentSharingConfig', async () => {
    spentSharingConfigComponentsPage = new SpentSharingConfigComponentsPage();
    await browser.wait(ec.visibilityOf(spentSharingConfigComponentsPage.title), 5000);
    lastElement = await spentSharingConfigComponentsPage.viewButtons.last().getWebElement();
  });

  it('should scroll the last SpentSharingConfig', async () => {
    browser
      .executeScript('arguments[0].scrollIntoView()', lastElement)
      .then(async () => {
        if ((await lastElement.isEnabled()) && (await lastElement.isDisplayed())) {
          browser
            .executeScript('arguments[0].click()', lastElement)
            .then(async () => {
              isVisible = true;
            })
            .catch();
        }
      })
      .catch();
  });

  it('should view the last SpentSharingConfig', async () => {
    spentSharingConfigDetailPage = new SpentSharingConfigDetailPage();
    if (isVisible && (await spentSharingConfigDetailPage.pageTitle.isDisplayed())) {
      expect(await spentSharingConfigDetailPage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

      expect(await spentSharingConfigDetailPage.getShareInput()).toEqual(share);
    }
  });

  it('should delete last SpentSharingConfig', async () => {
    spentSharingConfigDetailPage = new SpentSharingConfigDetailPage();
    if (isVisible && (await spentSharingConfigDetailPage.deleteButton.isDisplayed())) {
      await browser.executeScript('arguments[0].click()', await spentSharingConfigDetailPage.deleteButton.getWebElement());

      const alertConfirmButton = element.all(by.className('alert-button')).last();

      await browser.wait(ec.elementToBeClickable(alertConfirmButton), 3000);
      alertConfirmButton.click();
      await browser.wait(ec.visibilityOf(spentSharingConfigComponentsPage.title), 3000);
      expect(await spentSharingConfigComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
      expect(await spentSharingConfigComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities);
    }
  });

  it('finish SpentSharingConfigs tests performing logout', async () => {
    // go to home page
    const tabHome = element(by.css('ion-tab-button[tab="home"]'));
    await browser.wait(ec.elementToBeClickable(tabHome), 3000);
    await tabHome.click();
    await browser.wait(ec.elementToBeClickable(loginPage.logoutButton), 3000);
    await loginPage.logout();
  });
});
