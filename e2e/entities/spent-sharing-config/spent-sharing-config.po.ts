import { element, by, browser, ElementFinder } from 'protractor';

export class SpentSharingConfigComponentsPage {
  createButton = element(by.css('ion-fab-button'));
  viewButtons = element.all(by.css('ion-item'));
  title = element.all(by.css('ion-title')).get(2);
  noResult = element(by.cssContainingText('ion-label', 'No Spent Sharing Configs found.'));
  entities = element.all(by.css('page-spent-sharing-config ion-item'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastViewButton(): Promise<void> {
    await this.viewButtons.last().click();
  }

  async getTitle(): Promise<string> {
    return this.title.getText();
  }

  async getEntitiesNumber(): Promise<number> {
    return await this.entities.count();
  }
}

export class SpentSharingConfigUpdatePage {
  pageTitle = element.all(by.css('ion-title')).get(3);
  saveButton = element.all(by.css('ion-button')).get(1);

  shareInput = element(by.css('ion-input[formControlName="share"] input'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getText();
  }

  async setShareInput(share: string): Promise<void> {
    await this.shareInput.sendKeys(share);
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }
}

export class SpentSharingConfigDetailPage {
  pageTitle = element.all(by.css('ion-title')).get(3);
  deleteButton = element(by.css('ion-button[color="danger"]'));
  shareInput = element.all(by.css('span')).get(1);

  async getShareInput(): Promise<string> {
    return await this.shareInput.getText();
  }

  async clickOnDeleteButton(): Promise<void> {
    await this.deleteButton.click();
  }

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getText();
  }
}
