import { browser, by, element, ExpectedConditions as ec } from 'protractor';
import { LoginPage } from '../../pages/login.po';
import { ShoppingItemComponentsPage, ShoppingItemDetailPage, ShoppingItemUpdatePage } from './shopping-item.po';

describe('ShoppingItem e2e test', () => {
  let loginPage: LoginPage;
  let shoppingItemComponentsPage: ShoppingItemComponentsPage;
  let shoppingItemUpdatePage: ShoppingItemUpdatePage;
  let shoppingItemDetailPage: ShoppingItemDetailPage;
  let initNumberOfEntities: number;
  const COMPONENT_TITLE = 'Shopping Items';
  const SUBCOMPONENT_TITLE = 'Shopping Item';
  let lastElement: any;
  let isVisible = false;

  const label = 'label';
  const quantity = '10';

  beforeAll(async () => {
    loginPage = new LoginPage();
    await loginPage.navigateTo('/');
    await loginPage.signInButton.click();
    const username = process.env.E2E_USERNAME || 'admin';
    const password = process.env.E2E_PASSWORD || 'admin';
    await browser.wait(ec.elementToBeClickable(loginPage.loginButton), 3000);
    await loginPage.login(username, password);
    await browser.wait(ec.visibilityOf(loginPage.logoutButton), 1000);
  });

  it('should load ShoppingItems', async () => {
    // go to entity component page
    const tabEntities = element(by.css('ion-tab-button[tab="entities"]'));
    await browser.wait(ec.elementToBeClickable(tabEntities), 3000);
    await tabEntities.click();
    await element
      .all(by.css('ion-item'))
      .filter(async (el) => (await el.element(by.css('h2')).getText()) === 'ShoppingItem')
      .first()
      .click();

    shoppingItemComponentsPage = new ShoppingItemComponentsPage();
    await browser.wait(ec.visibilityOf(shoppingItemComponentsPage.title), 5000);
    expect(await shoppingItemComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    await browser.wait(
      ec.or(ec.visibilityOf(shoppingItemComponentsPage.entities.get(0)), ec.visibilityOf(shoppingItemComponentsPage.noResult)),
      5000
    );
  });

  it('should create ShoppingItem', async () => {
    initNumberOfEntities = await shoppingItemComponentsPage.getEntitiesNumber();
    await browser.wait(ec.elementToBeClickable(shoppingItemComponentsPage.createButton), 5000);
    await shoppingItemComponentsPage.clickOnCreateButton();
    shoppingItemUpdatePage = new ShoppingItemUpdatePage();
    await browser.wait(ec.visibilityOf(shoppingItemUpdatePage.pageTitle), 1000);
    expect(await shoppingItemUpdatePage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

    await shoppingItemUpdatePage.setLabelInput(label);
    await shoppingItemUpdatePage.setQuantityInput(quantity);
    await shoppingItemUpdatePage.unitSelectLastOption();

    await shoppingItemUpdatePage.save();
    await browser.wait(ec.visibilityOf(shoppingItemComponentsPage.title), 1000);
    expect(await shoppingItemComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    expect(await shoppingItemComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities + 1);
  });

  it('should get the last ShoppingItem', async () => {
    shoppingItemComponentsPage = new ShoppingItemComponentsPage();
    await browser.wait(ec.visibilityOf(shoppingItemComponentsPage.title), 5000);
    lastElement = await shoppingItemComponentsPage.viewButtons.last().getWebElement();
  });

  it('should scroll the last ShoppingItem', async () => {
    browser
      .executeScript('arguments[0].scrollIntoView()', lastElement)
      .then(async () => {
        if ((await lastElement.isEnabled()) && (await lastElement.isDisplayed())) {
          browser
            .executeScript('arguments[0].click()', lastElement)
            .then(async () => {
              isVisible = true;
            })
            .catch();
        }
      })
      .catch();
  });

  it('should view the last ShoppingItem', async () => {
    shoppingItemDetailPage = new ShoppingItemDetailPage();
    if (isVisible && (await shoppingItemDetailPage.pageTitle.isDisplayed())) {
      expect(await shoppingItemDetailPage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

      expect(await shoppingItemDetailPage.getLabelInput()).toEqual(label);

      expect(await shoppingItemDetailPage.getQuantityInput()).toEqual(quantity);
    }
  });

  it('should delete last ShoppingItem', async () => {
    shoppingItemDetailPage = new ShoppingItemDetailPage();
    if (isVisible && (await shoppingItemDetailPage.deleteButton.isDisplayed())) {
      await browser.executeScript('arguments[0].click()', await shoppingItemDetailPage.deleteButton.getWebElement());

      const alertConfirmButton = element.all(by.className('alert-button')).last();

      await browser.wait(ec.elementToBeClickable(alertConfirmButton), 3000);
      alertConfirmButton.click();
      await browser.wait(ec.visibilityOf(shoppingItemComponentsPage.title), 3000);
      expect(await shoppingItemComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
      expect(await shoppingItemComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities);
    }
  });

  it('finish ShoppingItems tests performing logout', async () => {
    // go to home page
    const tabHome = element(by.css('ion-tab-button[tab="home"]'));
    await browser.wait(ec.elementToBeClickable(tabHome), 3000);
    await tabHome.click();
    await browser.wait(ec.elementToBeClickable(loginPage.logoutButton), 3000);
    await loginPage.logout();
  });
});
