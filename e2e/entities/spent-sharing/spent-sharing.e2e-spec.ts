import { browser, by, element, ExpectedConditions as ec } from 'protractor';
import { LoginPage } from '../../pages/login.po';
import { SpentSharingComponentsPage, SpentSharingDetailPage, SpentSharingUpdatePage } from './spent-sharing.po';

describe('SpentSharing e2e test', () => {
  let loginPage: LoginPage;
  let spentSharingComponentsPage: SpentSharingComponentsPage;
  let spentSharingUpdatePage: SpentSharingUpdatePage;
  let spentSharingDetailPage: SpentSharingDetailPage;
  let initNumberOfEntities: number;
  const COMPONENT_TITLE = 'Spent Sharings';
  const SUBCOMPONENT_TITLE = 'Spent Sharing';
  let lastElement: any;
  let isVisible = false;

  const amountShare = '10';
  const share = '10';

  beforeAll(async () => {
    loginPage = new LoginPage();
    await loginPage.navigateTo('/');
    await loginPage.signInButton.click();
    const username = process.env.E2E_USERNAME || 'admin';
    const password = process.env.E2E_PASSWORD || 'admin';
    await browser.wait(ec.elementToBeClickable(loginPage.loginButton), 3000);
    await loginPage.login(username, password);
    await browser.wait(ec.visibilityOf(loginPage.logoutButton), 1000);
  });

  it('should load SpentSharings', async () => {
    // go to entity component page
    const tabEntities = element(by.css('ion-tab-button[tab="entities"]'));
    await browser.wait(ec.elementToBeClickable(tabEntities), 3000);
    await tabEntities.click();
    await element
      .all(by.css('ion-item'))
      .filter(async (el) => (await el.element(by.css('h2')).getText()) === 'SpentSharing')
      .first()
      .click();

    spentSharingComponentsPage = new SpentSharingComponentsPage();
    await browser.wait(ec.visibilityOf(spentSharingComponentsPage.title), 5000);
    expect(await spentSharingComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    await browser.wait(
      ec.or(ec.visibilityOf(spentSharingComponentsPage.entities.get(0)), ec.visibilityOf(spentSharingComponentsPage.noResult)),
      5000
    );
  });

  it('should create SpentSharing', async () => {
    initNumberOfEntities = await spentSharingComponentsPage.getEntitiesNumber();
    await browser.wait(ec.elementToBeClickable(spentSharingComponentsPage.createButton), 5000);
    await spentSharingComponentsPage.clickOnCreateButton();
    spentSharingUpdatePage = new SpentSharingUpdatePage();
    await browser.wait(ec.visibilityOf(spentSharingUpdatePage.pageTitle), 1000);
    expect(await spentSharingUpdatePage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

    await spentSharingUpdatePage.setAmountShareInput(amountShare);
    await spentSharingUpdatePage.setShareInput(share);

    await spentSharingUpdatePage.save();
    await browser.wait(ec.visibilityOf(spentSharingComponentsPage.title), 1000);
    expect(await spentSharingComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    expect(await spentSharingComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities + 1);
  });

  it('should get the last SpentSharing', async () => {
    spentSharingComponentsPage = new SpentSharingComponentsPage();
    await browser.wait(ec.visibilityOf(spentSharingComponentsPage.title), 5000);
    lastElement = await spentSharingComponentsPage.viewButtons.last().getWebElement();
  });

  it('should scroll the last SpentSharing', async () => {
    browser
      .executeScript('arguments[0].scrollIntoView()', lastElement)
      .then(async () => {
        if ((await lastElement.isEnabled()) && (await lastElement.isDisplayed())) {
          browser
            .executeScript('arguments[0].click()', lastElement)
            .then(async () => {
              isVisible = true;
            })
            .catch();
        }
      })
      .catch();
  });

  it('should view the last SpentSharing', async () => {
    spentSharingDetailPage = new SpentSharingDetailPage();
    if (isVisible && (await spentSharingDetailPage.pageTitle.isDisplayed())) {
      expect(await spentSharingDetailPage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

      expect(await spentSharingDetailPage.getAmountShareInput()).toEqual(amountShare);

      expect(await spentSharingDetailPage.getShareInput()).toEqual(share);
    }
  });

  it('should delete last SpentSharing', async () => {
    spentSharingDetailPage = new SpentSharingDetailPage();
    if (isVisible && (await spentSharingDetailPage.deleteButton.isDisplayed())) {
      await browser.executeScript('arguments[0].click()', await spentSharingDetailPage.deleteButton.getWebElement());

      const alertConfirmButton = element.all(by.className('alert-button')).last();

      await browser.wait(ec.elementToBeClickable(alertConfirmButton), 3000);
      alertConfirmButton.click();
      await browser.wait(ec.visibilityOf(spentSharingComponentsPage.title), 3000);
      expect(await spentSharingComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
      expect(await spentSharingComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities);
    }
  });

  it('finish SpentSharings tests performing logout', async () => {
    // go to home page
    const tabHome = element(by.css('ion-tab-button[tab="home"]'));
    await browser.wait(ec.elementToBeClickable(tabHome), 3000);
    await tabHome.click();
    await browser.wait(ec.elementToBeClickable(loginPage.logoutButton), 3000);
    await loginPage.logout();
  });
});
