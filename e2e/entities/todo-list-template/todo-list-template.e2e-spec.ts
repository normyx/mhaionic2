import { browser, by, element, ExpectedConditions as ec } from 'protractor';
import { LoginPage } from '../../pages/login.po';
import { TodoListTemplateComponentsPage, TodoListTemplateDetailPage, TodoListTemplateUpdatePage } from './todo-list-template.po';

describe('TodoListTemplate e2e test', () => {
  let loginPage: LoginPage;
  let todoListTemplateComponentsPage: TodoListTemplateComponentsPage;
  let todoListTemplateUpdatePage: TodoListTemplateUpdatePage;
  let todoListTemplateDetailPage: TodoListTemplateDetailPage;
  let initNumberOfEntities: number;
  const COMPONENT_TITLE = 'Todo List Templates';
  const SUBCOMPONENT_TITLE = 'Todo List Template';
  let lastElement: any;
  let isVisible = false;

  const label = 'label';

  beforeAll(async () => {
    loginPage = new LoginPage();
    await loginPage.navigateTo('/');
    await loginPage.signInButton.click();
    const username = process.env.E2E_USERNAME || 'admin';
    const password = process.env.E2E_PASSWORD || 'admin';
    await browser.wait(ec.elementToBeClickable(loginPage.loginButton), 3000);
    await loginPage.login(username, password);
    await browser.wait(ec.visibilityOf(loginPage.logoutButton), 1000);
  });

  it('should load TodoListTemplates', async () => {
    // go to entity component page
    const tabEntities = element(by.css('ion-tab-button[tab="entities"]'));
    await browser.wait(ec.elementToBeClickable(tabEntities), 3000);
    await tabEntities.click();
    await element
      .all(by.css('ion-item'))
      .filter(async (el) => (await el.element(by.css('h2')).getText()) === 'TodoListTemplate')
      .first()
      .click();

    todoListTemplateComponentsPage = new TodoListTemplateComponentsPage();
    await browser.wait(ec.visibilityOf(todoListTemplateComponentsPage.title), 5000);
    expect(await todoListTemplateComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    await browser.wait(
      ec.or(ec.visibilityOf(todoListTemplateComponentsPage.entities.get(0)), ec.visibilityOf(todoListTemplateComponentsPage.noResult)),
      5000
    );
  });

  it('should create TodoListTemplate', async () => {
    initNumberOfEntities = await todoListTemplateComponentsPage.getEntitiesNumber();
    await browser.wait(ec.elementToBeClickable(todoListTemplateComponentsPage.createButton), 5000);
    await todoListTemplateComponentsPage.clickOnCreateButton();
    todoListTemplateUpdatePage = new TodoListTemplateUpdatePage();
    await browser.wait(ec.visibilityOf(todoListTemplateUpdatePage.pageTitle), 1000);
    expect(await todoListTemplateUpdatePage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

    await todoListTemplateUpdatePage.setLabelInput(label);

    await todoListTemplateUpdatePage.save();
    await browser.wait(ec.visibilityOf(todoListTemplateComponentsPage.title), 1000);
    expect(await todoListTemplateComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    expect(await todoListTemplateComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities + 1);
  });

  it('should get the last TodoListTemplate', async () => {
    todoListTemplateComponentsPage = new TodoListTemplateComponentsPage();
    await browser.wait(ec.visibilityOf(todoListTemplateComponentsPage.title), 5000);
    lastElement = await todoListTemplateComponentsPage.viewButtons.last().getWebElement();
  });

  it('should scroll the last TodoListTemplate', async () => {
    browser
      .executeScript('arguments[0].scrollIntoView()', lastElement)
      .then(async () => {
        if ((await lastElement.isEnabled()) && (await lastElement.isDisplayed())) {
          browser
            .executeScript('arguments[0].click()', lastElement)
            .then(async () => {
              isVisible = true;
            })
            .catch();
        }
      })
      .catch();
  });

  it('should view the last TodoListTemplate', async () => {
    todoListTemplateDetailPage = new TodoListTemplateDetailPage();
    if (isVisible && (await todoListTemplateDetailPage.pageTitle.isDisplayed())) {
      expect(await todoListTemplateDetailPage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

      expect(await todoListTemplateDetailPage.getLabelInput()).toEqual(label);
    }
  });

  it('should delete last TodoListTemplate', async () => {
    todoListTemplateDetailPage = new TodoListTemplateDetailPage();
    if (isVisible && (await todoListTemplateDetailPage.deleteButton.isDisplayed())) {
      await browser.executeScript('arguments[0].click()', await todoListTemplateDetailPage.deleteButton.getWebElement());

      const alertConfirmButton = element.all(by.className('alert-button')).last();

      await browser.wait(ec.elementToBeClickable(alertConfirmButton), 3000);
      alertConfirmButton.click();
      await browser.wait(ec.visibilityOf(todoListTemplateComponentsPage.title), 3000);
      expect(await todoListTemplateComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
      expect(await todoListTemplateComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities);
    }
  });

  it('finish TodoListTemplates tests performing logout', async () => {
    // go to home page
    const tabHome = element(by.css('ion-tab-button[tab="home"]'));
    await browser.wait(ec.elementToBeClickable(tabHome), 3000);
    await tabHome.click();
    await browser.wait(ec.elementToBeClickable(loginPage.logoutButton), 3000);
    await loginPage.logout();
  });
});
