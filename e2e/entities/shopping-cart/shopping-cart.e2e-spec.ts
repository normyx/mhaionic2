import { browser, by, element, ExpectedConditions as ec } from 'protractor';
import { LoginPage } from '../../pages/login.po';
import { ShoppingCartComponentsPage, ShoppingCartDetailPage, ShoppingCartUpdatePage } from './shopping-cart.po';

describe('ShoppingCart e2e test', () => {
  let loginPage: LoginPage;
  let shoppingCartComponentsPage: ShoppingCartComponentsPage;
  let shoppingCartUpdatePage: ShoppingCartUpdatePage;
  let shoppingCartDetailPage: ShoppingCartDetailPage;
  let initNumberOfEntities: number;
  const COMPONENT_TITLE = 'Shopping Carts';
  const SUBCOMPONENT_TITLE = 'Shopping Cart';
  let lastElement: any;
  let isVisible = false;

  const label = 'label';

  beforeAll(async () => {
    loginPage = new LoginPage();
    await loginPage.navigateTo('/');
    await loginPage.signInButton.click();
    const username = process.env.E2E_USERNAME || 'admin';
    const password = process.env.E2E_PASSWORD || 'admin';
    await browser.wait(ec.elementToBeClickable(loginPage.loginButton), 3000);
    await loginPage.login(username, password);
    await browser.wait(ec.visibilityOf(loginPage.logoutButton), 1000);
  });

  it('should load ShoppingCarts', async () => {
    // go to entity component page
    const tabEntities = element(by.css('ion-tab-button[tab="entities"]'));
    await browser.wait(ec.elementToBeClickable(tabEntities), 3000);
    await tabEntities.click();
    await element
      .all(by.css('ion-item'))
      .filter(async (el) => (await el.element(by.css('h2')).getText()) === 'ShoppingCart')
      .first()
      .click();

    shoppingCartComponentsPage = new ShoppingCartComponentsPage();
    await browser.wait(ec.visibilityOf(shoppingCartComponentsPage.title), 5000);
    expect(await shoppingCartComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    await browser.wait(
      ec.or(ec.visibilityOf(shoppingCartComponentsPage.entities.get(0)), ec.visibilityOf(shoppingCartComponentsPage.noResult)),
      5000
    );
  });

  it('should create ShoppingCart', async () => {
    initNumberOfEntities = await shoppingCartComponentsPage.getEntitiesNumber();
    await browser.wait(ec.elementToBeClickable(shoppingCartComponentsPage.createButton), 5000);
    await shoppingCartComponentsPage.clickOnCreateButton();
    shoppingCartUpdatePage = new ShoppingCartUpdatePage();
    await browser.wait(ec.visibilityOf(shoppingCartUpdatePage.pageTitle), 1000);
    expect(await shoppingCartUpdatePage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

    await shoppingCartUpdatePage.setLabelInput(label);

    await shoppingCartUpdatePage.save();
    await browser.wait(ec.visibilityOf(shoppingCartComponentsPage.title), 1000);
    expect(await shoppingCartComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    expect(await shoppingCartComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities + 1);
  });

  it('should get the last ShoppingCart', async () => {
    shoppingCartComponentsPage = new ShoppingCartComponentsPage();
    await browser.wait(ec.visibilityOf(shoppingCartComponentsPage.title), 5000);
    lastElement = await shoppingCartComponentsPage.viewButtons.last().getWebElement();
  });

  it('should scroll the last ShoppingCart', async () => {
    browser
      .executeScript('arguments[0].scrollIntoView()', lastElement)
      .then(async () => {
        if ((await lastElement.isEnabled()) && (await lastElement.isDisplayed())) {
          browser
            .executeScript('arguments[0].click()', lastElement)
            .then(async () => {
              isVisible = true;
            })
            .catch();
        }
      })
      .catch();
  });

  it('should view the last ShoppingCart', async () => {
    shoppingCartDetailPage = new ShoppingCartDetailPage();
    if (isVisible && (await shoppingCartDetailPage.pageTitle.isDisplayed())) {
      expect(await shoppingCartDetailPage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

      expect(await shoppingCartDetailPage.getLabelInput()).toEqual(label);
    }
  });

  it('should delete last ShoppingCart', async () => {
    shoppingCartDetailPage = new ShoppingCartDetailPage();
    if (isVisible && (await shoppingCartDetailPage.deleteButton.isDisplayed())) {
      await browser.executeScript('arguments[0].click()', await shoppingCartDetailPage.deleteButton.getWebElement());

      const alertConfirmButton = element.all(by.className('alert-button')).last();

      await browser.wait(ec.elementToBeClickable(alertConfirmButton), 3000);
      alertConfirmButton.click();
      await browser.wait(ec.visibilityOf(shoppingCartComponentsPage.title), 3000);
      expect(await shoppingCartComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
      expect(await shoppingCartComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities);
    }
  });

  it('finish ShoppingCarts tests performing logout', async () => {
    // go to home page
    const tabHome = element(by.css('ion-tab-button[tab="home"]'));
    await browser.wait(ec.elementToBeClickable(tabHome), 3000);
    await tabHome.click();
    await browser.wait(ec.elementToBeClickable(loginPage.logoutButton), 3000);
    await loginPage.logout();
  });
});
