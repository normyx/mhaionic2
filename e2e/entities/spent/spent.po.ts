import { element, by, browser, ElementFinder } from 'protractor';

export class SpentComponentsPage {
  createButton = element(by.css('ion-fab-button'));
  viewButtons = element.all(by.css('ion-item'));
  title = element.all(by.css('ion-title')).get(2);
  noResult = element(by.cssContainingText('ion-label', 'No Spents found.'));
  entities = element.all(by.css('page-spent ion-item'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastViewButton(): Promise<void> {
    await this.viewButtons.last().click();
  }

  async getTitle(): Promise<string> {
    return this.title.getText();
  }

  async getEntitiesNumber(): Promise<number> {
    return await this.entities.count();
  }
}

export class SpentUpdatePage {
  pageTitle = element.all(by.css('ion-title')).get(3);
  saveButton = element.all(by.css('ion-button')).get(1);

  labelInput = element(by.css('ion-input[formControlName="label"] input'));
  descriptionInput = element(by.css('ion-input[formControlName="description"] input'));
  amountInput = element(by.css('ion-input[formControlName="amount"] input'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getText();
  }

  async setLabelInput(label: string): Promise<void> {
    await this.labelInput.sendKeys(label);
  }
  async setDescriptionInput(description: string): Promise<void> {
    await this.descriptionInput.sendKeys(description);
  }
  async setAmountInput(amount: string): Promise<void> {
    await this.amountInput.sendKeys(amount);
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }
}

export class SpentDetailPage {
  pageTitle = element.all(by.css('ion-title')).get(3);
  deleteButton = element(by.css('ion-button[color="danger"]'));
  labelInput = element.all(by.css('span')).get(1);

  descriptionInput = element.all(by.css('span')).get(2);

  amountInput = element.all(by.css('span')).get(3);

  async getLabelInput(): Promise<string> {
    return await this.labelInput.getText();
  }

  async getDescriptionInput(): Promise<string> {
    return await this.descriptionInput.getText();
  }

  async getAmountInput(): Promise<string> {
    return await this.amountInput.getText();
  }

  async clickOnDeleteButton(): Promise<void> {
    await this.deleteButton.click();
  }

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getText();
  }
}
