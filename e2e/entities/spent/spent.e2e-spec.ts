import { browser, by, element, ExpectedConditions as ec } from 'protractor';
import { LoginPage } from '../../pages/login.po';
import { SpentComponentsPage, SpentDetailPage, SpentUpdatePage } from './spent.po';

describe('Spent e2e test', () => {
  let loginPage: LoginPage;
  let spentComponentsPage: SpentComponentsPage;
  let spentUpdatePage: SpentUpdatePage;
  let spentDetailPage: SpentDetailPage;
  let initNumberOfEntities: number;
  const COMPONENT_TITLE = 'Spents';
  const SUBCOMPONENT_TITLE = 'Spent';
  let lastElement: any;
  let isVisible = false;

  const label = 'label';
  const description = 'description';
  const amount = '10';

  beforeAll(async () => {
    loginPage = new LoginPage();
    await loginPage.navigateTo('/');
    await loginPage.signInButton.click();
    const username = process.env.E2E_USERNAME || 'admin';
    const password = process.env.E2E_PASSWORD || 'admin';
    await browser.wait(ec.elementToBeClickable(loginPage.loginButton), 3000);
    await loginPage.login(username, password);
    await browser.wait(ec.visibilityOf(loginPage.logoutButton), 1000);
  });

  it('should load Spents', async () => {
    // go to entity component page
    const tabEntities = element(by.css('ion-tab-button[tab="entities"]'));
    await browser.wait(ec.elementToBeClickable(tabEntities), 3000);
    await tabEntities.click();
    await element
      .all(by.css('ion-item'))
      .filter(async (el) => (await el.element(by.css('h2')).getText()) === 'Spent')
      .first()
      .click();

    spentComponentsPage = new SpentComponentsPage();
    await browser.wait(ec.visibilityOf(spentComponentsPage.title), 5000);
    expect(await spentComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    await browser.wait(ec.or(ec.visibilityOf(spentComponentsPage.entities.get(0)), ec.visibilityOf(spentComponentsPage.noResult)), 5000);
  });

  it('should create Spent', async () => {
    initNumberOfEntities = await spentComponentsPage.getEntitiesNumber();
    await browser.wait(ec.elementToBeClickable(spentComponentsPage.createButton), 5000);
    await spentComponentsPage.clickOnCreateButton();
    spentUpdatePage = new SpentUpdatePage();
    await browser.wait(ec.visibilityOf(spentUpdatePage.pageTitle), 1000);
    expect(await spentUpdatePage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

    await spentUpdatePage.setLabelInput(label);
    await spentUpdatePage.setDescriptionInput(description);
    await spentUpdatePage.setAmountInput(amount);

    await spentUpdatePage.save();
    await browser.wait(ec.visibilityOf(spentComponentsPage.title), 1000);
    expect(await spentComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    expect(await spentComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities + 1);
  });

  it('should get the last Spent', async () => {
    spentComponentsPage = new SpentComponentsPage();
    await browser.wait(ec.visibilityOf(spentComponentsPage.title), 5000);
    lastElement = await spentComponentsPage.viewButtons.last().getWebElement();
  });

  it('should scroll the last Spent', async () => {
    browser
      .executeScript('arguments[0].scrollIntoView()', lastElement)
      .then(async () => {
        if ((await lastElement.isEnabled()) && (await lastElement.isDisplayed())) {
          browser
            .executeScript('arguments[0].click()', lastElement)
            .then(async () => {
              isVisible = true;
            })
            .catch();
        }
      })
      .catch();
  });

  it('should view the last Spent', async () => {
    spentDetailPage = new SpentDetailPage();
    if (isVisible && (await spentDetailPage.pageTitle.isDisplayed())) {
      expect(await spentDetailPage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

      expect(await spentDetailPage.getLabelInput()).toEqual(label);

      expect(await spentDetailPage.getDescriptionInput()).toEqual(description);

      expect(await spentDetailPage.getAmountInput()).toEqual(amount);
    }
  });

  it('should delete last Spent', async () => {
    spentDetailPage = new SpentDetailPage();
    if (isVisible && (await spentDetailPage.deleteButton.isDisplayed())) {
      await browser.executeScript('arguments[0].click()', await spentDetailPage.deleteButton.getWebElement());

      const alertConfirmButton = element.all(by.className('alert-button')).last();

      await browser.wait(ec.elementToBeClickable(alertConfirmButton), 3000);
      alertConfirmButton.click();
      await browser.wait(ec.visibilityOf(spentComponentsPage.title), 3000);
      expect(await spentComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
      expect(await spentComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities);
    }
  });

  it('finish Spents tests performing logout', async () => {
    // go to home page
    const tabHome = element(by.css('ion-tab-button[tab="home"]'));
    await browser.wait(ec.elementToBeClickable(tabHome), 3000);
    await tabHome.click();
    await browser.wait(ec.elementToBeClickable(loginPage.logoutButton), 3000);
    await loginPage.logout();
  });
});
