import { browser, by, element, ExpectedConditions as ec } from 'protractor';
import { LoginPage } from '../../pages/login.po';
import { TodoComponentsPage, TodoDetailPage, TodoUpdatePage } from './todo.po';

describe('Todo e2e test', () => {
  let loginPage: LoginPage;
  let todoComponentsPage: TodoComponentsPage;
  let todoUpdatePage: TodoUpdatePage;
  let todoDetailPage: TodoDetailPage;
  let initNumberOfEntities: number;
  const COMPONENT_TITLE = 'Todos';
  const SUBCOMPONENT_TITLE = 'Todo';
  let lastElement: any;
  let isVisible = false;

  const label = 'label';

  beforeAll(async () => {
    loginPage = new LoginPage();
    await loginPage.navigateTo('/');
    await loginPage.signInButton.click();
    const username = process.env.E2E_USERNAME || 'admin';
    const password = process.env.E2E_PASSWORD || 'admin';
    await browser.wait(ec.elementToBeClickable(loginPage.loginButton), 3000);
    await loginPage.login(username, password);
    await browser.wait(ec.visibilityOf(loginPage.logoutButton), 1000);
  });

  it('should load Todos', async () => {
    // go to entity component page
    const tabEntities = element(by.css('ion-tab-button[tab="entities"]'));
    await browser.wait(ec.elementToBeClickable(tabEntities), 3000);
    await tabEntities.click();
    await element
      .all(by.css('ion-item'))
      .filter(async (el) => (await el.element(by.css('h2')).getText()) === 'Todo')
      .first()
      .click();

    todoComponentsPage = new TodoComponentsPage();
    await browser.wait(ec.visibilityOf(todoComponentsPage.title), 5000);
    expect(await todoComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    await browser.wait(ec.or(ec.visibilityOf(todoComponentsPage.entities.get(0)), ec.visibilityOf(todoComponentsPage.noResult)), 5000);
  });

  it('should create Todo', async () => {
    initNumberOfEntities = await todoComponentsPage.getEntitiesNumber();
    await browser.wait(ec.elementToBeClickable(todoComponentsPage.createButton), 5000);
    await todoComponentsPage.clickOnCreateButton();
    todoUpdatePage = new TodoUpdatePage();
    await browser.wait(ec.visibilityOf(todoUpdatePage.pageTitle), 1000);
    expect(await todoUpdatePage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

    await todoUpdatePage.setLabelInput(label);

    await todoUpdatePage.save();
    await browser.wait(ec.visibilityOf(todoComponentsPage.title), 1000);
    expect(await todoComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    expect(await todoComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities + 1);
  });

  it('should get the last Todo', async () => {
    todoComponentsPage = new TodoComponentsPage();
    await browser.wait(ec.visibilityOf(todoComponentsPage.title), 5000);
    lastElement = await todoComponentsPage.viewButtons.last().getWebElement();
  });

  it('should scroll the last Todo', async () => {
    browser
      .executeScript('arguments[0].scrollIntoView()', lastElement)
      .then(async () => {
        if ((await lastElement.isEnabled()) && (await lastElement.isDisplayed())) {
          browser
            .executeScript('arguments[0].click()', lastElement)
            .then(async () => {
              isVisible = true;
            })
            .catch();
        }
      })
      .catch();
  });

  it('should view the last Todo', async () => {
    todoDetailPage = new TodoDetailPage();
    if (isVisible && (await todoDetailPage.pageTitle.isDisplayed())) {
      expect(await todoDetailPage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

      expect(await todoDetailPage.getLabelInput()).toEqual(label);
    }
  });

  it('should delete last Todo', async () => {
    todoDetailPage = new TodoDetailPage();
    if (isVisible && (await todoDetailPage.deleteButton.isDisplayed())) {
      await browser.executeScript('arguments[0].click()', await todoDetailPage.deleteButton.getWebElement());

      const alertConfirmButton = element.all(by.className('alert-button')).last();

      await browser.wait(ec.elementToBeClickable(alertConfirmButton), 3000);
      alertConfirmButton.click();
      await browser.wait(ec.visibilityOf(todoComponentsPage.title), 3000);
      expect(await todoComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
      expect(await todoComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities);
    }
  });

  it('finish Todos tests performing logout', async () => {
    // go to home page
    const tabHome = element(by.css('ion-tab-button[tab="home"]'));
    await browser.wait(ec.elementToBeClickable(tabHome), 3000);
    await tabHome.click();
    await browser.wait(ec.elementToBeClickable(loginPage.logoutButton), 3000);
    await loginPage.logout();
  });
});
