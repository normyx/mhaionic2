import { browser, by, element, ExpectedConditions as ec } from 'protractor';
import { LoginPage } from '../../pages/login.po';
import { WalletComponentsPage, WalletDetailPage, WalletUpdatePage } from './wallet.po';

describe('Wallet e2e test', () => {
  let loginPage: LoginPage;
  let walletComponentsPage: WalletComponentsPage;
  let walletUpdatePage: WalletUpdatePage;
  let walletDetailPage: WalletDetailPage;
  let initNumberOfEntities: number;
  const COMPONENT_TITLE = 'Wallets';
  const SUBCOMPONENT_TITLE = 'Wallet';
  let lastElement: any;
  let isVisible = false;

  const label = 'label';

  beforeAll(async () => {
    loginPage = new LoginPage();
    await loginPage.navigateTo('/');
    await loginPage.signInButton.click();
    const username = process.env.E2E_USERNAME || 'admin';
    const password = process.env.E2E_PASSWORD || 'admin';
    await browser.wait(ec.elementToBeClickable(loginPage.loginButton), 3000);
    await loginPage.login(username, password);
    await browser.wait(ec.visibilityOf(loginPage.logoutButton), 1000);
  });

  it('should load Wallets', async () => {
    // go to entity component page
    const tabEntities = element(by.css('ion-tab-button[tab="entities"]'));
    await browser.wait(ec.elementToBeClickable(tabEntities), 3000);
    await tabEntities.click();
    await element
      .all(by.css('ion-item'))
      .filter(async (el) => (await el.element(by.css('h2')).getText()) === 'Wallet')
      .first()
      .click();

    walletComponentsPage = new WalletComponentsPage();
    await browser.wait(ec.visibilityOf(walletComponentsPage.title), 5000);
    expect(await walletComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    await browser.wait(ec.or(ec.visibilityOf(walletComponentsPage.entities.get(0)), ec.visibilityOf(walletComponentsPage.noResult)), 5000);
  });

  it('should create Wallet', async () => {
    initNumberOfEntities = await walletComponentsPage.getEntitiesNumber();
    await browser.wait(ec.elementToBeClickable(walletComponentsPage.createButton), 5000);
    await walletComponentsPage.clickOnCreateButton();
    walletUpdatePage = new WalletUpdatePage();
    await browser.wait(ec.visibilityOf(walletUpdatePage.pageTitle), 1000);
    expect(await walletUpdatePage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

    await walletUpdatePage.setLabelInput(label);

    await walletUpdatePage.save();
    await browser.wait(ec.visibilityOf(walletComponentsPage.title), 1000);
    expect(await walletComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    expect(await walletComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities + 1);
  });

  it('should get the last Wallet', async () => {
    walletComponentsPage = new WalletComponentsPage();
    await browser.wait(ec.visibilityOf(walletComponentsPage.title), 5000);
    lastElement = await walletComponentsPage.viewButtons.last().getWebElement();
  });

  it('should scroll the last Wallet', async () => {
    browser
      .executeScript('arguments[0].scrollIntoView()', lastElement)
      .then(async () => {
        if ((await lastElement.isEnabled()) && (await lastElement.isDisplayed())) {
          browser
            .executeScript('arguments[0].click()', lastElement)
            .then(async () => {
              isVisible = true;
            })
            .catch();
        }
      })
      .catch();
  });

  it('should view the last Wallet', async () => {
    walletDetailPage = new WalletDetailPage();
    if (isVisible && (await walletDetailPage.pageTitle.isDisplayed())) {
      expect(await walletDetailPage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

      expect(await walletDetailPage.getLabelInput()).toEqual(label);
    }
  });

  it('should delete last Wallet', async () => {
    walletDetailPage = new WalletDetailPage();
    if (isVisible && (await walletDetailPage.deleteButton.isDisplayed())) {
      await browser.executeScript('arguments[0].click()', await walletDetailPage.deleteButton.getWebElement());

      const alertConfirmButton = element.all(by.className('alert-button')).last();

      await browser.wait(ec.elementToBeClickable(alertConfirmButton), 3000);
      alertConfirmButton.click();
      await browser.wait(ec.visibilityOf(walletComponentsPage.title), 3000);
      expect(await walletComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
      expect(await walletComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities);
    }
  });

  it('finish Wallets tests performing logout', async () => {
    // go to home page
    const tabHome = element(by.css('ion-tab-button[tab="home"]'));
    await browser.wait(ec.elementToBeClickable(tabHome), 3000);
    await tabHome.click();
    await browser.wait(ec.elementToBeClickable(loginPage.logoutButton), 3000);
    await loginPage.logout();
  });
});
