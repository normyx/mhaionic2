import { browser, by, element, ExpectedConditions as ec } from 'protractor';
import { LoginPage } from '../../pages/login.po';
import { TaskComponentsPage, TaskDetailPage, TaskUpdatePage } from './task.po';

describe('Task e2e test', () => {
  let loginPage: LoginPage;
  let taskComponentsPage: TaskComponentsPage;
  let taskUpdatePage: TaskUpdatePage;
  let taskDetailPage: TaskDetailPage;
  let initNumberOfEntities: number;
  const COMPONENT_TITLE = 'Tasks';
  const SUBCOMPONENT_TITLE = 'Task';
  let lastElement: any;
  let isVisible = false;

  const label = 'label';
  const description = 'description';

  beforeAll(async () => {
    loginPage = new LoginPage();
    await loginPage.navigateTo('/');
    await loginPage.signInButton.click();
    const username = process.env.E2E_USERNAME || 'admin';
    const password = process.env.E2E_PASSWORD || 'admin';
    await browser.wait(ec.elementToBeClickable(loginPage.loginButton), 3000);
    await loginPage.login(username, password);
    await browser.wait(ec.visibilityOf(loginPage.logoutButton), 1000);
  });

  it('should load Tasks', async () => {
    // go to entity component page
    const tabEntities = element(by.css('ion-tab-button[tab="entities"]'));
    await browser.wait(ec.elementToBeClickable(tabEntities), 3000);
    await tabEntities.click();
    await element
      .all(by.css('ion-item'))
      .filter(async (el) => (await el.element(by.css('h2')).getText()) === 'Task')
      .first()
      .click();

    taskComponentsPage = new TaskComponentsPage();
    await browser.wait(ec.visibilityOf(taskComponentsPage.title), 5000);
    expect(await taskComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    await browser.wait(ec.or(ec.visibilityOf(taskComponentsPage.entities.get(0)), ec.visibilityOf(taskComponentsPage.noResult)), 5000);
  });

  it('should create Task', async () => {
    initNumberOfEntities = await taskComponentsPage.getEntitiesNumber();
    await browser.wait(ec.elementToBeClickable(taskComponentsPage.createButton), 5000);
    await taskComponentsPage.clickOnCreateButton();
    taskUpdatePage = new TaskUpdatePage();
    await browser.wait(ec.visibilityOf(taskUpdatePage.pageTitle), 1000);
    expect(await taskUpdatePage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

    await taskUpdatePage.setLabelInput(label);
    await taskUpdatePage.setDescriptionInput(description);

    await taskUpdatePage.save();
    await browser.wait(ec.visibilityOf(taskComponentsPage.title), 1000);
    expect(await taskComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    expect(await taskComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities + 1);
  });

  it('should get the last Task', async () => {
    taskComponentsPage = new TaskComponentsPage();
    await browser.wait(ec.visibilityOf(taskComponentsPage.title), 5000);
    lastElement = await taskComponentsPage.viewButtons.last().getWebElement();
  });

  it('should scroll the last Task', async () => {
    browser
      .executeScript('arguments[0].scrollIntoView()', lastElement)
      .then(async () => {
        if ((await lastElement.isEnabled()) && (await lastElement.isDisplayed())) {
          browser
            .executeScript('arguments[0].click()', lastElement)
            .then(async () => {
              isVisible = true;
            })
            .catch();
        }
      })
      .catch();
  });

  it('should view the last Task', async () => {
    taskDetailPage = new TaskDetailPage();
    if (isVisible && (await taskDetailPage.pageTitle.isDisplayed())) {
      expect(await taskDetailPage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

      expect(await taskDetailPage.getLabelInput()).toEqual(label);

      expect(await taskDetailPage.getDescriptionInput()).toEqual(description);
    }
  });

  it('should delete last Task', async () => {
    taskDetailPage = new TaskDetailPage();
    if (isVisible && (await taskDetailPage.deleteButton.isDisplayed())) {
      await browser.executeScript('arguments[0].click()', await taskDetailPage.deleteButton.getWebElement());

      const alertConfirmButton = element.all(by.className('alert-button')).last();

      await browser.wait(ec.elementToBeClickable(alertConfirmButton), 3000);
      alertConfirmButton.click();
      await browser.wait(ec.visibilityOf(taskComponentsPage.title), 3000);
      expect(await taskComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
      expect(await taskComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities);
    }
  });

  it('finish Tasks tests performing logout', async () => {
    // go to home page
    const tabHome = element(by.css('ion-tab-button[tab="home"]'));
    await browser.wait(ec.elementToBeClickable(tabHome), 3000);
    await tabHome.click();
    await browser.wait(ec.elementToBeClickable(loginPage.logoutButton), 3000);
    await loginPage.logout();
  });
});
