import { element, by, browser, ElementFinder } from 'protractor';

export class SpentConfigComponentsPage {
  createButton = element(by.css('ion-fab-button'));
  viewButtons = element.all(by.css('ion-item'));
  title = element.all(by.css('ion-title')).get(2);
  noResult = element(by.cssContainingText('ion-label', 'No Spent Configs found.'));
  entities = element.all(by.css('page-spent-config ion-item'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastViewButton(): Promise<void> {
    await this.viewButtons.last().click();
  }

  async getTitle(): Promise<string> {
    return this.title.getText();
  }

  async getEntitiesNumber(): Promise<number> {
    return await this.entities.count();
  }
}

export class SpentConfigUpdatePage {
  pageTitle = element.all(by.css('ion-title')).get(3);
  saveButton = element.all(by.css('ion-button')).get(1);

  labelInput = element(by.css('ion-input[formControlName="label"] input'));
  amountInput = element(by.css('ion-input[formControlName="amount"] input'));
  spentLabelInput = element(by.css('ion-input[formControlName="spentLabel"] input'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getText();
  }

  async setLabelInput(label: string): Promise<void> {
    await this.labelInput.sendKeys(label);
  }
  async setAmountInput(amount: string): Promise<void> {
    await this.amountInput.sendKeys(amount);
  }
  async setSpentLabelInput(spentLabel: string): Promise<void> {
    await this.spentLabelInput.sendKeys(spentLabel);
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }
}

export class SpentConfigDetailPage {
  pageTitle = element.all(by.css('ion-title')).get(3);
  deleteButton = element(by.css('ion-button[color="danger"]'));
  labelInput = element.all(by.css('span')).get(1);

  amountInput = element.all(by.css('span')).get(2);

  spentLabelInput = element.all(by.css('span')).get(3);

  async getLabelInput(): Promise<string> {
    return await this.labelInput.getText();
  }

  async getAmountInput(): Promise<string> {
    return await this.amountInput.getText();
  }

  async getSpentLabelInput(): Promise<string> {
    return await this.spentLabelInput.getText();
  }

  async clickOnDeleteButton(): Promise<void> {
    await this.deleteButton.click();
  }

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getText();
  }
}
