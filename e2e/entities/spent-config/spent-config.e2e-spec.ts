import { browser, by, element, ExpectedConditions as ec } from 'protractor';
import { LoginPage } from '../../pages/login.po';
import { SpentConfigComponentsPage, SpentConfigDetailPage, SpentConfigUpdatePage } from './spent-config.po';

describe('SpentConfig e2e test', () => {
  let loginPage: LoginPage;
  let spentConfigComponentsPage: SpentConfigComponentsPage;
  let spentConfigUpdatePage: SpentConfigUpdatePage;
  let spentConfigDetailPage: SpentConfigDetailPage;
  let initNumberOfEntities: number;
  const COMPONENT_TITLE = 'Spent Configs';
  const SUBCOMPONENT_TITLE = 'Spent Config';
  let lastElement: any;
  let isVisible = false;

  const label = 'label';
  const amount = '10';
  const spentLabel = 'spentLabel';

  beforeAll(async () => {
    loginPage = new LoginPage();
    await loginPage.navigateTo('/');
    await loginPage.signInButton.click();
    const username = process.env.E2E_USERNAME || 'admin';
    const password = process.env.E2E_PASSWORD || 'admin';
    await browser.wait(ec.elementToBeClickable(loginPage.loginButton), 3000);
    await loginPage.login(username, password);
    await browser.wait(ec.visibilityOf(loginPage.logoutButton), 1000);
  });

  it('should load SpentConfigs', async () => {
    // go to entity component page
    const tabEntities = element(by.css('ion-tab-button[tab="entities"]'));
    await browser.wait(ec.elementToBeClickable(tabEntities), 3000);
    await tabEntities.click();
    await element
      .all(by.css('ion-item'))
      .filter(async (el) => (await el.element(by.css('h2')).getText()) === 'SpentConfig')
      .first()
      .click();

    spentConfigComponentsPage = new SpentConfigComponentsPage();
    await browser.wait(ec.visibilityOf(spentConfigComponentsPage.title), 5000);
    expect(await spentConfigComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    await browser.wait(
      ec.or(ec.visibilityOf(spentConfigComponentsPage.entities.get(0)), ec.visibilityOf(spentConfigComponentsPage.noResult)),
      5000
    );
  });

  it('should create SpentConfig', async () => {
    initNumberOfEntities = await spentConfigComponentsPage.getEntitiesNumber();
    await browser.wait(ec.elementToBeClickable(spentConfigComponentsPage.createButton), 5000);
    await spentConfigComponentsPage.clickOnCreateButton();
    spentConfigUpdatePage = new SpentConfigUpdatePage();
    await browser.wait(ec.visibilityOf(spentConfigUpdatePage.pageTitle), 1000);
    expect(await spentConfigUpdatePage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

    await spentConfigUpdatePage.setLabelInput(label);
    await spentConfigUpdatePage.setAmountInput(amount);
    await spentConfigUpdatePage.setSpentLabelInput(spentLabel);

    await spentConfigUpdatePage.save();
    await browser.wait(ec.visibilityOf(spentConfigComponentsPage.title), 1000);
    expect(await spentConfigComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    expect(await spentConfigComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities + 1);
  });

  it('should get the last SpentConfig', async () => {
    spentConfigComponentsPage = new SpentConfigComponentsPage();
    await browser.wait(ec.visibilityOf(spentConfigComponentsPage.title), 5000);
    lastElement = await spentConfigComponentsPage.viewButtons.last().getWebElement();
  });

  it('should scroll the last SpentConfig', async () => {
    browser
      .executeScript('arguments[0].scrollIntoView()', lastElement)
      .then(async () => {
        if ((await lastElement.isEnabled()) && (await lastElement.isDisplayed())) {
          browser
            .executeScript('arguments[0].click()', lastElement)
            .then(async () => {
              isVisible = true;
            })
            .catch();
        }
      })
      .catch();
  });

  it('should view the last SpentConfig', async () => {
    spentConfigDetailPage = new SpentConfigDetailPage();
    if (isVisible && (await spentConfigDetailPage.pageTitle.isDisplayed())) {
      expect(await spentConfigDetailPage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

      expect(await spentConfigDetailPage.getLabelInput()).toEqual(label);

      expect(await spentConfigDetailPage.getAmountInput()).toEqual(amount);

      expect(await spentConfigDetailPage.getSpentLabelInput()).toEqual(spentLabel);
    }
  });

  it('should delete last SpentConfig', async () => {
    spentConfigDetailPage = new SpentConfigDetailPage();
    if (isVisible && (await spentConfigDetailPage.deleteButton.isDisplayed())) {
      await browser.executeScript('arguments[0].click()', await spentConfigDetailPage.deleteButton.getWebElement());

      const alertConfirmButton = element.all(by.className('alert-button')).last();

      await browser.wait(ec.elementToBeClickable(alertConfirmButton), 3000);
      alertConfirmButton.click();
      await browser.wait(ec.visibilityOf(spentConfigComponentsPage.title), 3000);
      expect(await spentConfigComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
      expect(await spentConfigComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities);
    }
  });

  it('finish SpentConfigs tests performing logout', async () => {
    // go to home page
    const tabHome = element(by.css('ion-tab-button[tab="home"]'));
    await browser.wait(ec.elementToBeClickable(tabHome), 3000);
    await tabHome.click();
    await browser.wait(ec.elementToBeClickable(loginPage.logoutButton), 3000);
    await loginPage.logout();
  });
});
