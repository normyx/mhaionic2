import { browser, by, element, ExpectedConditions as ec } from 'protractor';
import { LoginPage } from '../../pages/login.po';
import {
  ShoppingCatalogSheldComponentsPage,
  ShoppingCatalogSheldDetailPage,
  ShoppingCatalogSheldUpdatePage,
} from './shopping-catalog-sheld.po';

describe('ShoppingCatalogSheld e2e test', () => {
  let loginPage: LoginPage;
  let shoppingCatalogSheldComponentsPage: ShoppingCatalogSheldComponentsPage;
  let shoppingCatalogSheldUpdatePage: ShoppingCatalogSheldUpdatePage;
  let shoppingCatalogSheldDetailPage: ShoppingCatalogSheldDetailPage;
  let initNumberOfEntities: number;
  const COMPONENT_TITLE = 'Shopping Catalog Shelds';
  const SUBCOMPONENT_TITLE = 'Shopping Catalog Sheld';
  let lastElement: any;
  let isVisible = false;

  const label = 'label';

  beforeAll(async () => {
    loginPage = new LoginPage();
    await loginPage.navigateTo('/');
    await loginPage.signInButton.click();
    const username = process.env.E2E_USERNAME || 'admin';
    const password = process.env.E2E_PASSWORD || 'admin';
    await browser.wait(ec.elementToBeClickable(loginPage.loginButton), 3000);
    await loginPage.login(username, password);
    await browser.wait(ec.visibilityOf(loginPage.logoutButton), 1000);
  });

  it('should load ShoppingCatalogShelds', async () => {
    // go to entity component page
    const tabEntities = element(by.css('ion-tab-button[tab="entities"]'));
    await browser.wait(ec.elementToBeClickable(tabEntities), 3000);
    await tabEntities.click();
    await element
      .all(by.css('ion-item'))
      .filter(async (el) => (await el.element(by.css('h2')).getText()) === 'ShoppingCatalogSheld')
      .first()
      .click();

    shoppingCatalogSheldComponentsPage = new ShoppingCatalogSheldComponentsPage();
    await browser.wait(ec.visibilityOf(shoppingCatalogSheldComponentsPage.title), 5000);
    expect(await shoppingCatalogSheldComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    await browser.wait(
      ec.or(
        ec.visibilityOf(shoppingCatalogSheldComponentsPage.entities.get(0)),
        ec.visibilityOf(shoppingCatalogSheldComponentsPage.noResult)
      ),
      5000
    );
  });

  it('should create ShoppingCatalogSheld', async () => {
    initNumberOfEntities = await shoppingCatalogSheldComponentsPage.getEntitiesNumber();
    await browser.wait(ec.elementToBeClickable(shoppingCatalogSheldComponentsPage.createButton), 5000);
    await shoppingCatalogSheldComponentsPage.clickOnCreateButton();
    shoppingCatalogSheldUpdatePage = new ShoppingCatalogSheldUpdatePage();
    await browser.wait(ec.visibilityOf(shoppingCatalogSheldUpdatePage.pageTitle), 1000);
    expect(await shoppingCatalogSheldUpdatePage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

    await shoppingCatalogSheldUpdatePage.setLabelInput(label);

    await shoppingCatalogSheldUpdatePage.save();
    await browser.wait(ec.visibilityOf(shoppingCatalogSheldComponentsPage.title), 1000);
    expect(await shoppingCatalogSheldComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    expect(await shoppingCatalogSheldComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities + 1);
  });

  it('should get the last ShoppingCatalogSheld', async () => {
    shoppingCatalogSheldComponentsPage = new ShoppingCatalogSheldComponentsPage();
    await browser.wait(ec.visibilityOf(shoppingCatalogSheldComponentsPage.title), 5000);
    lastElement = await shoppingCatalogSheldComponentsPage.viewButtons.last().getWebElement();
  });

  it('should scroll the last ShoppingCatalogSheld', async () => {
    browser
      .executeScript('arguments[0].scrollIntoView()', lastElement)
      .then(async () => {
        if ((await lastElement.isEnabled()) && (await lastElement.isDisplayed())) {
          browser
            .executeScript('arguments[0].click()', lastElement)
            .then(async () => {
              isVisible = true;
            })
            .catch();
        }
      })
      .catch();
  });

  it('should view the last ShoppingCatalogSheld', async () => {
    shoppingCatalogSheldDetailPage = new ShoppingCatalogSheldDetailPage();
    if (isVisible && (await shoppingCatalogSheldDetailPage.pageTitle.isDisplayed())) {
      expect(await shoppingCatalogSheldDetailPage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

      expect(await shoppingCatalogSheldDetailPage.getLabelInput()).toEqual(label);
    }
  });

  it('should delete last ShoppingCatalogSheld', async () => {
    shoppingCatalogSheldDetailPage = new ShoppingCatalogSheldDetailPage();
    if (isVisible && (await shoppingCatalogSheldDetailPage.deleteButton.isDisplayed())) {
      await browser.executeScript('arguments[0].click()', await shoppingCatalogSheldDetailPage.deleteButton.getWebElement());

      const alertConfirmButton = element.all(by.className('alert-button')).last();

      await browser.wait(ec.elementToBeClickable(alertConfirmButton), 3000);
      alertConfirmButton.click();
      await browser.wait(ec.visibilityOf(shoppingCatalogSheldComponentsPage.title), 3000);
      expect(await shoppingCatalogSheldComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
      expect(await shoppingCatalogSheldComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities);
    }
  });

  it('finish ShoppingCatalogShelds tests performing logout', async () => {
    // go to home page
    const tabHome = element(by.css('ion-tab-button[tab="home"]'));
    await browser.wait(ec.elementToBeClickable(tabHome), 3000);
    await tabHome.click();
    await browser.wait(ec.elementToBeClickable(loginPage.logoutButton), 3000);
    await loginPage.logout();
  });
});
