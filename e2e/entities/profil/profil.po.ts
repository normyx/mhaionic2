import { element, by, browser, ElementFinder } from 'protractor';

export class ProfilComponentsPage {
  createButton = element(by.css('ion-fab-button'));
  viewButtons = element.all(by.css('ion-item'));
  title = element.all(by.css('ion-title')).get(2);
  noResult = element(by.cssContainingText('ion-label', 'No Profils found.'));
  entities = element.all(by.css('page-profil ion-item'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastViewButton(): Promise<void> {
    await this.viewButtons.last().click();
  }

  async getTitle(): Promise<string> {
    return this.title.getText();
  }

  async getEntitiesNumber(): Promise<number> {
    return await this.entities.count();
  }
}

export class ProfilUpdatePage {
  pageTitle = element.all(by.css('ion-title')).get(3);
  saveButton = element.all(by.css('ion-button')).get(1);

  displayNameInput = element(by.css('ion-input[formControlName="displayName"] input'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getText();
  }

  async setDisplayNameInput(displayName: string): Promise<void> {
    await this.displayNameInput.sendKeys(displayName);
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }
}

export class ProfilDetailPage {
  pageTitle = element.all(by.css('ion-title')).get(3);
  deleteButton = element(by.css('ion-button[color="danger"]'));
  displayNameInput = element.all(by.css('span')).get(1);

  async getDisplayNameInput(): Promise<string> {
    return await this.displayNameInput.getText();
  }

  async clickOnDeleteButton(): Promise<void> {
    await this.deleteButton.click();
  }

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getText();
  }
}
