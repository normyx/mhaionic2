import { browser, by, element, ExpectedConditions as ec } from 'protractor';
import { LoginPage } from '../../pages/login.po';
import { ProfilComponentsPage, ProfilDetailPage, ProfilUpdatePage } from './profil.po';

describe('Profil e2e test', () => {
  let loginPage: LoginPage;
  let profilComponentsPage: ProfilComponentsPage;
  let profilUpdatePage: ProfilUpdatePage;
  let profilDetailPage: ProfilDetailPage;
  let initNumberOfEntities: number;
  const COMPONENT_TITLE = 'Profils';
  const SUBCOMPONENT_TITLE = 'Profil';
  let lastElement: any;
  let isVisible = false;

  const displayName = 'displayName';

  beforeAll(async () => {
    loginPage = new LoginPage();
    await loginPage.navigateTo('/');
    await loginPage.signInButton.click();
    const username = process.env.E2E_USERNAME || 'admin';
    const password = process.env.E2E_PASSWORD || 'admin';
    await browser.wait(ec.elementToBeClickable(loginPage.loginButton), 3000);
    await loginPage.login(username, password);
    await browser.wait(ec.visibilityOf(loginPage.logoutButton), 1000);
  });

  it('should load Profils', async () => {
    // go to entity component page
    const tabEntities = element(by.css('ion-tab-button[tab="entities"]'));
    await browser.wait(ec.elementToBeClickable(tabEntities), 3000);
    await tabEntities.click();
    await element
      .all(by.css('ion-item'))
      .filter(async (el) => (await el.element(by.css('h2')).getText()) === 'Profil')
      .first()
      .click();

    profilComponentsPage = new ProfilComponentsPage();
    await browser.wait(ec.visibilityOf(profilComponentsPage.title), 5000);
    expect(await profilComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    await browser.wait(ec.or(ec.visibilityOf(profilComponentsPage.entities.get(0)), ec.visibilityOf(profilComponentsPage.noResult)), 5000);
  });

  it('should create Profil', async () => {
    initNumberOfEntities = await profilComponentsPage.getEntitiesNumber();
    await browser.wait(ec.elementToBeClickable(profilComponentsPage.createButton), 5000);
    await profilComponentsPage.clickOnCreateButton();
    profilUpdatePage = new ProfilUpdatePage();
    await browser.wait(ec.visibilityOf(profilUpdatePage.pageTitle), 1000);
    expect(await profilUpdatePage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

    await profilUpdatePage.setDisplayNameInput(displayName);

    await profilUpdatePage.save();
    await browser.wait(ec.visibilityOf(profilComponentsPage.title), 1000);
    expect(await profilComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    expect(await profilComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities + 1);
  });

  it('should get the last Profil', async () => {
    profilComponentsPage = new ProfilComponentsPage();
    await browser.wait(ec.visibilityOf(profilComponentsPage.title), 5000);
    lastElement = await profilComponentsPage.viewButtons.last().getWebElement();
  });

  it('should scroll the last Profil', async () => {
    browser
      .executeScript('arguments[0].scrollIntoView()', lastElement)
      .then(async () => {
        if ((await lastElement.isEnabled()) && (await lastElement.isDisplayed())) {
          browser
            .executeScript('arguments[0].click()', lastElement)
            .then(async () => {
              isVisible = true;
            })
            .catch();
        }
      })
      .catch();
  });

  it('should view the last Profil', async () => {
    profilDetailPage = new ProfilDetailPage();
    if (isVisible && (await profilDetailPage.pageTitle.isDisplayed())) {
      expect(await profilDetailPage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

      expect(await profilDetailPage.getDisplayNameInput()).toEqual(displayName);
    }
  });

  it('should delete last Profil', async () => {
    profilDetailPage = new ProfilDetailPage();
    if (isVisible && (await profilDetailPage.deleteButton.isDisplayed())) {
      await browser.executeScript('arguments[0].click()', await profilDetailPage.deleteButton.getWebElement());

      const alertConfirmButton = element.all(by.className('alert-button')).last();

      await browser.wait(ec.elementToBeClickable(alertConfirmButton), 3000);
      alertConfirmButton.click();
      await browser.wait(ec.visibilityOf(profilComponentsPage.title), 3000);
      expect(await profilComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
      expect(await profilComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities);
    }
  });

  it('finish Profils tests performing logout', async () => {
    // go to home page
    const tabHome = element(by.css('ion-tab-button[tab="home"]'));
    await browser.wait(ec.elementToBeClickable(tabHome), 3000);
    await tabHome.click();
    await browser.wait(ec.elementToBeClickable(loginPage.logoutButton), 3000);
    await loginPage.logout();
  });
});
