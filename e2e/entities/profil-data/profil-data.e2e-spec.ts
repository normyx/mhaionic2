import { browser, by, element, ExpectedConditions as ec } from 'protractor';
import { LoginPage } from '../../pages/login.po';
import { ProfilDataComponentsPage, ProfilDataDetailPage, ProfilDataUpdatePage } from './profil-data.po';

describe('ProfilData e2e test', () => {
  let loginPage: LoginPage;
  let profilDataComponentsPage: ProfilDataComponentsPage;
  let profilDataUpdatePage: ProfilDataUpdatePage;
  let profilDataDetailPage: ProfilDataDetailPage;
  let initNumberOfEntities: number;
  const COMPONENT_TITLE = 'Profil Data';
  const SUBCOMPONENT_TITLE = 'Profil Data';
  let lastElement: any;
  let isVisible = false;

  beforeAll(async () => {
    loginPage = new LoginPage();
    await loginPage.navigateTo('/');
    await loginPage.signInButton.click();
    const username = process.env.E2E_USERNAME || 'admin';
    const password = process.env.E2E_PASSWORD || 'admin';
    await browser.wait(ec.elementToBeClickable(loginPage.loginButton), 3000);
    await loginPage.login(username, password);
    await browser.wait(ec.visibilityOf(loginPage.logoutButton), 1000);
  });

  it('should load ProfilData', async () => {
    // go to entity component page
    const tabEntities = element(by.css('ion-tab-button[tab="entities"]'));
    await browser.wait(ec.elementToBeClickable(tabEntities), 3000);
    await tabEntities.click();
    await element
      .all(by.css('ion-item'))
      .filter(async (el) => (await el.element(by.css('h2')).getText()) === 'ProfilData')
      .first()
      .click();

    profilDataComponentsPage = new ProfilDataComponentsPage();
    await browser.wait(ec.visibilityOf(profilDataComponentsPage.title), 5000);
    expect(await profilDataComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    await browser.wait(
      ec.or(ec.visibilityOf(profilDataComponentsPage.entities.get(0)), ec.visibilityOf(profilDataComponentsPage.noResult)),
      5000
    );
  });

  it('should create ProfilData', async () => {
    initNumberOfEntities = await profilDataComponentsPage.getEntitiesNumber();
    await browser.wait(ec.elementToBeClickable(profilDataComponentsPage.createButton), 5000);
    await profilDataComponentsPage.clickOnCreateButton();
    profilDataUpdatePage = new ProfilDataUpdatePage();
    await browser.wait(ec.visibilityOf(profilDataUpdatePage.pageTitle), 1000);
    expect(await profilDataUpdatePage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

    await profilDataUpdatePage.setPhotoInput(photo);

    await profilDataUpdatePage.save();
    await browser.wait(ec.visibilityOf(profilDataComponentsPage.title), 1000);
    expect(await profilDataComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    expect(await profilDataComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities + 1);
  });

  it('should get the last ProfilData', async () => {
    profilDataComponentsPage = new ProfilDataComponentsPage();
    await browser.wait(ec.visibilityOf(profilDataComponentsPage.title), 5000);
    lastElement = await profilDataComponentsPage.viewButtons.last().getWebElement();
  });

  it('should scroll the last ProfilData', async () => {
    browser
      .executeScript('arguments[0].scrollIntoView()', lastElement)
      .then(async () => {
        if ((await lastElement.isEnabled()) && (await lastElement.isDisplayed())) {
          browser
            .executeScript('arguments[0].click()', lastElement)
            .then(async () => {
              isVisible = true;
            })
            .catch();
        }
      })
      .catch();
  });

  it('should view the last ProfilData', async () => {
    profilDataDetailPage = new ProfilDataDetailPage();
    if (isVisible && (await profilDataDetailPage.pageTitle.isDisplayed())) {
      expect(await profilDataDetailPage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

      expect(await profilDataDetailPage.getPhotoInput()).toEqual(photo);
    }
  });

  it('should delete last ProfilData', async () => {
    profilDataDetailPage = new ProfilDataDetailPage();
    if (isVisible && (await profilDataDetailPage.deleteButton.isDisplayed())) {
      await browser.executeScript('arguments[0].click()', await profilDataDetailPage.deleteButton.getWebElement());

      const alertConfirmButton = element.all(by.className('alert-button')).last();

      await browser.wait(ec.elementToBeClickable(alertConfirmButton), 3000);
      alertConfirmButton.click();
      await browser.wait(ec.visibilityOf(profilDataComponentsPage.title), 3000);
      expect(await profilDataComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
      expect(await profilDataComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities);
    }
  });

  it('finish ProfilData tests performing logout', async () => {
    // go to home page
    const tabHome = element(by.css('ion-tab-button[tab="home"]'));
    await browser.wait(ec.elementToBeClickable(tabHome), 3000);
    await tabHome.click();
    await browser.wait(ec.elementToBeClickable(loginPage.logoutButton), 3000);
    await loginPage.logout();
  });
});
