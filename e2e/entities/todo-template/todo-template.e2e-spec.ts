import { browser, by, element, ExpectedConditions as ec } from 'protractor';
import { LoginPage } from '../../pages/login.po';
import { TodoTemplateComponentsPage, TodoTemplateDetailPage, TodoTemplateUpdatePage } from './todo-template.po';

describe('TodoTemplate e2e test', () => {
  let loginPage: LoginPage;
  let todoTemplateComponentsPage: TodoTemplateComponentsPage;
  let todoTemplateUpdatePage: TodoTemplateUpdatePage;
  let todoTemplateDetailPage: TodoTemplateDetailPage;
  let initNumberOfEntities: number;
  const COMPONENT_TITLE = 'Todo Templates';
  const SUBCOMPONENT_TITLE = 'Todo Template';
  let lastElement: any;
  let isVisible = false;

  const label = 'label';

  beforeAll(async () => {
    loginPage = new LoginPage();
    await loginPage.navigateTo('/');
    await loginPage.signInButton.click();
    const username = process.env.E2E_USERNAME || 'admin';
    const password = process.env.E2E_PASSWORD || 'admin';
    await browser.wait(ec.elementToBeClickable(loginPage.loginButton), 3000);
    await loginPage.login(username, password);
    await browser.wait(ec.visibilityOf(loginPage.logoutButton), 1000);
  });

  it('should load TodoTemplates', async () => {
    // go to entity component page
    const tabEntities = element(by.css('ion-tab-button[tab="entities"]'));
    await browser.wait(ec.elementToBeClickable(tabEntities), 3000);
    await tabEntities.click();
    await element
      .all(by.css('ion-item'))
      .filter(async (el) => (await el.element(by.css('h2')).getText()) === 'TodoTemplate')
      .first()
      .click();

    todoTemplateComponentsPage = new TodoTemplateComponentsPage();
    await browser.wait(ec.visibilityOf(todoTemplateComponentsPage.title), 5000);
    expect(await todoTemplateComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    await browser.wait(
      ec.or(ec.visibilityOf(todoTemplateComponentsPage.entities.get(0)), ec.visibilityOf(todoTemplateComponentsPage.noResult)),
      5000
    );
  });

  it('should create TodoTemplate', async () => {
    initNumberOfEntities = await todoTemplateComponentsPage.getEntitiesNumber();
    await browser.wait(ec.elementToBeClickable(todoTemplateComponentsPage.createButton), 5000);
    await todoTemplateComponentsPage.clickOnCreateButton();
    todoTemplateUpdatePage = new TodoTemplateUpdatePage();
    await browser.wait(ec.visibilityOf(todoTemplateUpdatePage.pageTitle), 1000);
    expect(await todoTemplateUpdatePage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

    await todoTemplateUpdatePage.setLabelInput(label);

    await todoTemplateUpdatePage.save();
    await browser.wait(ec.visibilityOf(todoTemplateComponentsPage.title), 1000);
    expect(await todoTemplateComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    expect(await todoTemplateComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities + 1);
  });

  it('should get the last TodoTemplate', async () => {
    todoTemplateComponentsPage = new TodoTemplateComponentsPage();
    await browser.wait(ec.visibilityOf(todoTemplateComponentsPage.title), 5000);
    lastElement = await todoTemplateComponentsPage.viewButtons.last().getWebElement();
  });

  it('should scroll the last TodoTemplate', async () => {
    browser
      .executeScript('arguments[0].scrollIntoView()', lastElement)
      .then(async () => {
        if ((await lastElement.isEnabled()) && (await lastElement.isDisplayed())) {
          browser
            .executeScript('arguments[0].click()', lastElement)
            .then(async () => {
              isVisible = true;
            })
            .catch();
        }
      })
      .catch();
  });

  it('should view the last TodoTemplate', async () => {
    todoTemplateDetailPage = new TodoTemplateDetailPage();
    if (isVisible && (await todoTemplateDetailPage.pageTitle.isDisplayed())) {
      expect(await todoTemplateDetailPage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

      expect(await todoTemplateDetailPage.getLabelInput()).toEqual(label);
    }
  });

  it('should delete last TodoTemplate', async () => {
    todoTemplateDetailPage = new TodoTemplateDetailPage();
    if (isVisible && (await todoTemplateDetailPage.deleteButton.isDisplayed())) {
      await browser.executeScript('arguments[0].click()', await todoTemplateDetailPage.deleteButton.getWebElement());

      const alertConfirmButton = element.all(by.className('alert-button')).last();

      await browser.wait(ec.elementToBeClickable(alertConfirmButton), 3000);
      alertConfirmButton.click();
      await browser.wait(ec.visibilityOf(todoTemplateComponentsPage.title), 3000);
      expect(await todoTemplateComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
      expect(await todoTemplateComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities);
    }
  });

  it('finish TodoTemplates tests performing logout', async () => {
    // go to home page
    const tabHome = element(by.css('ion-tab-button[tab="home"]'));
    await browser.wait(ec.elementToBeClickable(tabHome), 3000);
    await tabHome.click();
    await browser.wait(ec.elementToBeClickable(loginPage.logoutButton), 3000);
    await loginPage.logout();
  });
});
