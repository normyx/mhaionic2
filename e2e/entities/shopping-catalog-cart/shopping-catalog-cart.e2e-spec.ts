import { browser, by, element, ExpectedConditions as ec } from 'protractor';
import { LoginPage } from '../../pages/login.po';
import {
  ShoppingCatalogCartComponentsPage,
  ShoppingCatalogCartDetailPage,
  ShoppingCatalogCartUpdatePage,
} from './shopping-catalog-cart.po';

describe('ShoppingCatalogCart e2e test', () => {
  let loginPage: LoginPage;
  let shoppingCatalogCartComponentsPage: ShoppingCatalogCartComponentsPage;
  let shoppingCatalogCartUpdatePage: ShoppingCatalogCartUpdatePage;
  let shoppingCatalogCartDetailPage: ShoppingCatalogCartDetailPage;
  let initNumberOfEntities: number;
  const COMPONENT_TITLE = 'Shopping Catalog Carts';
  const SUBCOMPONENT_TITLE = 'Shopping Catalog Cart';
  let lastElement: any;
  let isVisible = false;

  const label = 'label';

  beforeAll(async () => {
    loginPage = new LoginPage();
    await loginPage.navigateTo('/');
    await loginPage.signInButton.click();
    const username = process.env.E2E_USERNAME || 'admin';
    const password = process.env.E2E_PASSWORD || 'admin';
    await browser.wait(ec.elementToBeClickable(loginPage.loginButton), 3000);
    await loginPage.login(username, password);
    await browser.wait(ec.visibilityOf(loginPage.logoutButton), 1000);
  });

  it('should load ShoppingCatalogCarts', async () => {
    // go to entity component page
    const tabEntities = element(by.css('ion-tab-button[tab="entities"]'));
    await browser.wait(ec.elementToBeClickable(tabEntities), 3000);
    await tabEntities.click();
    await element
      .all(by.css('ion-item'))
      .filter(async (el) => (await el.element(by.css('h2')).getText()) === 'ShoppingCatalogCart')
      .first()
      .click();

    shoppingCatalogCartComponentsPage = new ShoppingCatalogCartComponentsPage();
    await browser.wait(ec.visibilityOf(shoppingCatalogCartComponentsPage.title), 5000);
    expect(await shoppingCatalogCartComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    await browser.wait(
      ec.or(
        ec.visibilityOf(shoppingCatalogCartComponentsPage.entities.get(0)),
        ec.visibilityOf(shoppingCatalogCartComponentsPage.noResult)
      ),
      5000
    );
  });

  it('should create ShoppingCatalogCart', async () => {
    initNumberOfEntities = await shoppingCatalogCartComponentsPage.getEntitiesNumber();
    await browser.wait(ec.elementToBeClickable(shoppingCatalogCartComponentsPage.createButton), 5000);
    await shoppingCatalogCartComponentsPage.clickOnCreateButton();
    shoppingCatalogCartUpdatePage = new ShoppingCatalogCartUpdatePage();
    await browser.wait(ec.visibilityOf(shoppingCatalogCartUpdatePage.pageTitle), 1000);
    expect(await shoppingCatalogCartUpdatePage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

    await shoppingCatalogCartUpdatePage.setLabelInput(label);

    await shoppingCatalogCartUpdatePage.save();
    await browser.wait(ec.visibilityOf(shoppingCatalogCartComponentsPage.title), 1000);
    expect(await shoppingCatalogCartComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    expect(await shoppingCatalogCartComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities + 1);
  });

  it('should get the last ShoppingCatalogCart', async () => {
    shoppingCatalogCartComponentsPage = new ShoppingCatalogCartComponentsPage();
    await browser.wait(ec.visibilityOf(shoppingCatalogCartComponentsPage.title), 5000);
    lastElement = await shoppingCatalogCartComponentsPage.viewButtons.last().getWebElement();
  });

  it('should scroll the last ShoppingCatalogCart', async () => {
    browser
      .executeScript('arguments[0].scrollIntoView()', lastElement)
      .then(async () => {
        if ((await lastElement.isEnabled()) && (await lastElement.isDisplayed())) {
          browser
            .executeScript('arguments[0].click()', lastElement)
            .then(async () => {
              isVisible = true;
            })
            .catch();
        }
      })
      .catch();
  });

  it('should view the last ShoppingCatalogCart', async () => {
    shoppingCatalogCartDetailPage = new ShoppingCatalogCartDetailPage();
    if (isVisible && (await shoppingCatalogCartDetailPage.pageTitle.isDisplayed())) {
      expect(await shoppingCatalogCartDetailPage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

      expect(await shoppingCatalogCartDetailPage.getLabelInput()).toEqual(label);
    }
  });

  it('should delete last ShoppingCatalogCart', async () => {
    shoppingCatalogCartDetailPage = new ShoppingCatalogCartDetailPage();
    if (isVisible && (await shoppingCatalogCartDetailPage.deleteButton.isDisplayed())) {
      await browser.executeScript('arguments[0].click()', await shoppingCatalogCartDetailPage.deleteButton.getWebElement());

      const alertConfirmButton = element.all(by.className('alert-button')).last();

      await browser.wait(ec.elementToBeClickable(alertConfirmButton), 3000);
      alertConfirmButton.click();
      await browser.wait(ec.visibilityOf(shoppingCatalogCartComponentsPage.title), 3000);
      expect(await shoppingCatalogCartComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
      expect(await shoppingCatalogCartComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities);
    }
  });

  it('finish ShoppingCatalogCarts tests performing logout', async () => {
    // go to home page
    const tabHome = element(by.css('ion-tab-button[tab="home"]'));
    await browser.wait(ec.elementToBeClickable(tabHome), 3000);
    await tabHome.click();
    await browser.wait(ec.elementToBeClickable(loginPage.logoutButton), 3000);
    await loginPage.logout();
  });
});
