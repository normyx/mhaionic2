import { browser, by, element, ExpectedConditions as ec } from 'protractor';
import { LoginPage } from '../../pages/login.po';
import { TaskProjectComponentsPage, TaskProjectDetailPage, TaskProjectUpdatePage } from './task-project.po';

describe('TaskProject e2e test', () => {
  let loginPage: LoginPage;
  let taskProjectComponentsPage: TaskProjectComponentsPage;
  let taskProjectUpdatePage: TaskProjectUpdatePage;
  let taskProjectDetailPage: TaskProjectDetailPage;
  let initNumberOfEntities: number;
  const COMPONENT_TITLE = 'Task Projects';
  const SUBCOMPONENT_TITLE = 'Task Project';
  let lastElement: any;
  let isVisible = false;

  const label = 'label';

  beforeAll(async () => {
    loginPage = new LoginPage();
    await loginPage.navigateTo('/');
    await loginPage.signInButton.click();
    const username = process.env.E2E_USERNAME || 'admin';
    const password = process.env.E2E_PASSWORD || 'admin';
    await browser.wait(ec.elementToBeClickable(loginPage.loginButton), 3000);
    await loginPage.login(username, password);
    await browser.wait(ec.visibilityOf(loginPage.logoutButton), 1000);
  });

  it('should load TaskProjects', async () => {
    // go to entity component page
    const tabEntities = element(by.css('ion-tab-button[tab="entities"]'));
    await browser.wait(ec.elementToBeClickable(tabEntities), 3000);
    await tabEntities.click();
    await element
      .all(by.css('ion-item'))
      .filter(async (el) => (await el.element(by.css('h2')).getText()) === 'TaskProject')
      .first()
      .click();

    taskProjectComponentsPage = new TaskProjectComponentsPage();
    await browser.wait(ec.visibilityOf(taskProjectComponentsPage.title), 5000);
    expect(await taskProjectComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    await browser.wait(
      ec.or(ec.visibilityOf(taskProjectComponentsPage.entities.get(0)), ec.visibilityOf(taskProjectComponentsPage.noResult)),
      5000
    );
  });

  it('should create TaskProject', async () => {
    initNumberOfEntities = await taskProjectComponentsPage.getEntitiesNumber();
    await browser.wait(ec.elementToBeClickable(taskProjectComponentsPage.createButton), 5000);
    await taskProjectComponentsPage.clickOnCreateButton();
    taskProjectUpdatePage = new TaskProjectUpdatePage();
    await browser.wait(ec.visibilityOf(taskProjectUpdatePage.pageTitle), 1000);
    expect(await taskProjectUpdatePage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

    await taskProjectUpdatePage.setLabelInput(label);

    await taskProjectUpdatePage.save();
    await browser.wait(ec.visibilityOf(taskProjectComponentsPage.title), 1000);
    expect(await taskProjectComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    expect(await taskProjectComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities + 1);
  });

  it('should get the last TaskProject', async () => {
    taskProjectComponentsPage = new TaskProjectComponentsPage();
    await browser.wait(ec.visibilityOf(taskProjectComponentsPage.title), 5000);
    lastElement = await taskProjectComponentsPage.viewButtons.last().getWebElement();
  });

  it('should scroll the last TaskProject', async () => {
    browser
      .executeScript('arguments[0].scrollIntoView()', lastElement)
      .then(async () => {
        if ((await lastElement.isEnabled()) && (await lastElement.isDisplayed())) {
          browser
            .executeScript('arguments[0].click()', lastElement)
            .then(async () => {
              isVisible = true;
            })
            .catch();
        }
      })
      .catch();
  });

  it('should view the last TaskProject', async () => {
    taskProjectDetailPage = new TaskProjectDetailPage();
    if (isVisible && (await taskProjectDetailPage.pageTitle.isDisplayed())) {
      expect(await taskProjectDetailPage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

      expect(await taskProjectDetailPage.getLabelInput()).toEqual(label);
    }
  });

  it('should delete last TaskProject', async () => {
    taskProjectDetailPage = new TaskProjectDetailPage();
    if (isVisible && (await taskProjectDetailPage.deleteButton.isDisplayed())) {
      await browser.executeScript('arguments[0].click()', await taskProjectDetailPage.deleteButton.getWebElement());

      const alertConfirmButton = element.all(by.className('alert-button')).last();

      await browser.wait(ec.elementToBeClickable(alertConfirmButton), 3000);
      alertConfirmButton.click();
      await browser.wait(ec.visibilityOf(taskProjectComponentsPage.title), 3000);
      expect(await taskProjectComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
      expect(await taskProjectComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities);
    }
  });

  it('finish TaskProjects tests performing logout', async () => {
    // go to home page
    const tabHome = element(by.css('ion-tab-button[tab="home"]'));
    await browser.wait(ec.elementToBeClickable(tabHome), 3000);
    await tabHome.click();
    await browser.wait(ec.elementToBeClickable(loginPage.logoutButton), 3000);
    await loginPage.logout();
  });
});
