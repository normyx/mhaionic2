import { browser, by, element, ExpectedConditions as ec } from 'protractor';
import { LoginPage } from '../../pages/login.po';
import { WorkspaceComponentsPage, WorkspaceDetailPage, WorkspaceUpdatePage } from './workspace.po';

describe('Workspace e2e test', () => {
  let loginPage: LoginPage;
  let workspaceComponentsPage: WorkspaceComponentsPage;
  let workspaceUpdatePage: WorkspaceUpdatePage;
  let workspaceDetailPage: WorkspaceDetailPage;
  let initNumberOfEntities: number;
  const COMPONENT_TITLE = 'Workspaces';
  const SUBCOMPONENT_TITLE = 'Workspace';
  let lastElement: any;
  let isVisible = false;

  const label = 'label';

  beforeAll(async () => {
    loginPage = new LoginPage();
    await loginPage.navigateTo('/');
    await loginPage.signInButton.click();
    const username = process.env.E2E_USERNAME || 'admin';
    const password = process.env.E2E_PASSWORD || 'admin';
    await browser.wait(ec.elementToBeClickable(loginPage.loginButton), 3000);
    await loginPage.login(username, password);
    await browser.wait(ec.visibilityOf(loginPage.logoutButton), 1000);
  });

  it('should load Workspaces', async () => {
    // go to entity component page
    const tabEntities = element(by.css('ion-tab-button[tab="entities"]'));
    await browser.wait(ec.elementToBeClickable(tabEntities), 3000);
    await tabEntities.click();
    await element
      .all(by.css('ion-item'))
      .filter(async (el) => (await el.element(by.css('h2')).getText()) === 'Workspace')
      .first()
      .click();

    workspaceComponentsPage = new WorkspaceComponentsPage();
    await browser.wait(ec.visibilityOf(workspaceComponentsPage.title), 5000);
    expect(await workspaceComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    await browser.wait(
      ec.or(ec.visibilityOf(workspaceComponentsPage.entities.get(0)), ec.visibilityOf(workspaceComponentsPage.noResult)),
      5000
    );
  });

  it('should create Workspace', async () => {
    initNumberOfEntities = await workspaceComponentsPage.getEntitiesNumber();
    await browser.wait(ec.elementToBeClickable(workspaceComponentsPage.createButton), 5000);
    await workspaceComponentsPage.clickOnCreateButton();
    workspaceUpdatePage = new WorkspaceUpdatePage();
    await browser.wait(ec.visibilityOf(workspaceUpdatePage.pageTitle), 1000);
    expect(await workspaceUpdatePage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

    await workspaceUpdatePage.setLabelInput(label);

    await workspaceUpdatePage.save();
    await browser.wait(ec.visibilityOf(workspaceComponentsPage.title), 1000);
    expect(await workspaceComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    expect(await workspaceComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities + 1);
  });

  it('should get the last Workspace', async () => {
    workspaceComponentsPage = new WorkspaceComponentsPage();
    await browser.wait(ec.visibilityOf(workspaceComponentsPage.title), 5000);
    lastElement = await workspaceComponentsPage.viewButtons.last().getWebElement();
  });

  it('should scroll the last Workspace', async () => {
    browser
      .executeScript('arguments[0].scrollIntoView()', lastElement)
      .then(async () => {
        if ((await lastElement.isEnabled()) && (await lastElement.isDisplayed())) {
          browser
            .executeScript('arguments[0].click()', lastElement)
            .then(async () => {
              isVisible = true;
            })
            .catch();
        }
      })
      .catch();
  });

  it('should view the last Workspace', async () => {
    workspaceDetailPage = new WorkspaceDetailPage();
    if (isVisible && (await workspaceDetailPage.pageTitle.isDisplayed())) {
      expect(await workspaceDetailPage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

      expect(await workspaceDetailPage.getLabelInput()).toEqual(label);
    }
  });

  it('should delete last Workspace', async () => {
    workspaceDetailPage = new WorkspaceDetailPage();
    if (isVisible && (await workspaceDetailPage.deleteButton.isDisplayed())) {
      await browser.executeScript('arguments[0].click()', await workspaceDetailPage.deleteButton.getWebElement());

      const alertConfirmButton = element.all(by.className('alert-button')).last();

      await browser.wait(ec.elementToBeClickable(alertConfirmButton), 3000);
      alertConfirmButton.click();
      await browser.wait(ec.visibilityOf(workspaceComponentsPage.title), 3000);
      expect(await workspaceComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
      expect(await workspaceComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities);
    }
  });

  it('finish Workspaces tests performing logout', async () => {
    // go to home page
    const tabHome = element(by.css('ion-tab-button[tab="home"]'));
    await browser.wait(ec.elementToBeClickable(tabHome), 3000);
    await tabHome.click();
    await browser.wait(ec.elementToBeClickable(loginPage.logoutButton), 3000);
    await loginPage.logout();
  });
});
