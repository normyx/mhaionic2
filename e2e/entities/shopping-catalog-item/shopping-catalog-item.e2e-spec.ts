import { browser, by, element, ExpectedConditions as ec } from 'protractor';
import { LoginPage } from '../../pages/login.po';
import {
  ShoppingCatalogItemComponentsPage,
  ShoppingCatalogItemDetailPage,
  ShoppingCatalogItemUpdatePage,
} from './shopping-catalog-item.po';

describe('ShoppingCatalogItem e2e test', () => {
  let loginPage: LoginPage;
  let shoppingCatalogItemComponentsPage: ShoppingCatalogItemComponentsPage;
  let shoppingCatalogItemUpdatePage: ShoppingCatalogItemUpdatePage;
  let shoppingCatalogItemDetailPage: ShoppingCatalogItemDetailPage;
  let initNumberOfEntities: number;
  const COMPONENT_TITLE = 'Shopping Catalog Items';
  const SUBCOMPONENT_TITLE = 'Shopping Catalog Item';
  let lastElement: any;
  let isVisible = false;

  const label = 'label';

  beforeAll(async () => {
    loginPage = new LoginPage();
    await loginPage.navigateTo('/');
    await loginPage.signInButton.click();
    const username = process.env.E2E_USERNAME || 'admin';
    const password = process.env.E2E_PASSWORD || 'admin';
    await browser.wait(ec.elementToBeClickable(loginPage.loginButton), 3000);
    await loginPage.login(username, password);
    await browser.wait(ec.visibilityOf(loginPage.logoutButton), 1000);
  });

  it('should load ShoppingCatalogItems', async () => {
    // go to entity component page
    const tabEntities = element(by.css('ion-tab-button[tab="entities"]'));
    await browser.wait(ec.elementToBeClickable(tabEntities), 3000);
    await tabEntities.click();
    await element
      .all(by.css('ion-item'))
      .filter(async (el) => (await el.element(by.css('h2')).getText()) === 'ShoppingCatalogItem')
      .first()
      .click();

    shoppingCatalogItemComponentsPage = new ShoppingCatalogItemComponentsPage();
    await browser.wait(ec.visibilityOf(shoppingCatalogItemComponentsPage.title), 5000);
    expect(await shoppingCatalogItemComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    await browser.wait(
      ec.or(
        ec.visibilityOf(shoppingCatalogItemComponentsPage.entities.get(0)),
        ec.visibilityOf(shoppingCatalogItemComponentsPage.noResult)
      ),
      5000
    );
  });

  it('should create ShoppingCatalogItem', async () => {
    initNumberOfEntities = await shoppingCatalogItemComponentsPage.getEntitiesNumber();
    await browser.wait(ec.elementToBeClickable(shoppingCatalogItemComponentsPage.createButton), 5000);
    await shoppingCatalogItemComponentsPage.clickOnCreateButton();
    shoppingCatalogItemUpdatePage = new ShoppingCatalogItemUpdatePage();
    await browser.wait(ec.visibilityOf(shoppingCatalogItemUpdatePage.pageTitle), 1000);
    expect(await shoppingCatalogItemUpdatePage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

    await shoppingCatalogItemUpdatePage.setLabelInput(label);

    await shoppingCatalogItemUpdatePage.save();
    await browser.wait(ec.visibilityOf(shoppingCatalogItemComponentsPage.title), 1000);
    expect(await shoppingCatalogItemComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
    expect(await shoppingCatalogItemComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities + 1);
  });

  it('should get the last ShoppingCatalogItem', async () => {
    shoppingCatalogItemComponentsPage = new ShoppingCatalogItemComponentsPage();
    await browser.wait(ec.visibilityOf(shoppingCatalogItemComponentsPage.title), 5000);
    lastElement = await shoppingCatalogItemComponentsPage.viewButtons.last().getWebElement();
  });

  it('should scroll the last ShoppingCatalogItem', async () => {
    browser
      .executeScript('arguments[0].scrollIntoView()', lastElement)
      .then(async () => {
        if ((await lastElement.isEnabled()) && (await lastElement.isDisplayed())) {
          browser
            .executeScript('arguments[0].click()', lastElement)
            .then(async () => {
              isVisible = true;
            })
            .catch();
        }
      })
      .catch();
  });

  it('should view the last ShoppingCatalogItem', async () => {
    shoppingCatalogItemDetailPage = new ShoppingCatalogItemDetailPage();
    if (isVisible && (await shoppingCatalogItemDetailPage.pageTitle.isDisplayed())) {
      expect(await shoppingCatalogItemDetailPage.getPageTitle()).toEqual(SUBCOMPONENT_TITLE);

      expect(await shoppingCatalogItemDetailPage.getLabelInput()).toEqual(label);
    }
  });

  it('should delete last ShoppingCatalogItem', async () => {
    shoppingCatalogItemDetailPage = new ShoppingCatalogItemDetailPage();
    if (isVisible && (await shoppingCatalogItemDetailPage.deleteButton.isDisplayed())) {
      await browser.executeScript('arguments[0].click()', await shoppingCatalogItemDetailPage.deleteButton.getWebElement());

      const alertConfirmButton = element.all(by.className('alert-button')).last();

      await browser.wait(ec.elementToBeClickable(alertConfirmButton), 3000);
      alertConfirmButton.click();
      await browser.wait(ec.visibilityOf(shoppingCatalogItemComponentsPage.title), 3000);
      expect(await shoppingCatalogItemComponentsPage.getTitle()).toEqual(COMPONENT_TITLE);
      expect(await shoppingCatalogItemComponentsPage.getEntitiesNumber()).toEqual(initNumberOfEntities);
    }
  });

  it('finish ShoppingCatalogItems tests performing logout', async () => {
    // go to home page
    const tabHome = element(by.css('ion-tab-button[tab="home"]'));
    await browser.wait(ec.elementToBeClickable(tabHome), 3000);
    await tabHome.click();
    await browser.wait(ec.elementToBeClickable(loginPage.logoutButton), 3000);
    await loginPage.logout();
  });
});
